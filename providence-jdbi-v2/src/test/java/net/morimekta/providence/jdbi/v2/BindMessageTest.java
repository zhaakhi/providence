package net.morimekta.providence.jdbi.v2;

import net.morimekta.providence.jdbi.v2.annotations.BindEnumName;
import net.morimekta.providence.jdbi.v2.annotations.BindEnumValue;
import net.morimekta.providence.jdbi.v2.annotations.BindField;
import net.morimekta.providence.jdbi.v2.annotations.BindMessage;
import net.morimekta.providence.jdbi.v2.annotations.BindType;
import net.morimekta.providence.jdbi.v2.annotations.RegisterEnumValueMapper;
import net.morimekta.providence.jdbi.v2.annotations.RegisterMessageMapper;
import net.morimekta.test.providence.storage.jdbc.CompactFields;
import net.morimekta.test.providence.storage.jdbc.Fibonacci;
import net.morimekta.test.providence.storage.jdbc.OptionalFields;
import net.morimekta.test.providence.storage.jdbc.Other;
import org.junit.Rule;
import org.junit.Test;
import org.skife.jdbi.v2.Handle;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterArgumentFactory;

import java.sql.Types;
import java.util.List;

import static net.morimekta.util.collect.UnmodifiableList.listOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class BindMessageTest {
    @Rule
    public TestDatabase db = new TestDatabase("/mappings.sql");

    @RegisterMessageMapper(value = OptionalFields.class, fields = {
            @BindField(field = "message", column = "compact")
    })
    @RegisterMessageMapper(CompactFields.class)
    @RegisterArgumentFactory(EnumValueArgumentFactory.class)
    @RegisterEnumValueMapper(Fibonacci.class)
    @RegisterEnumValueMapper(value = Other.class, acceptUnknown = true)
    public interface TestDAO {
        @SqlUpdate("INSERT INTO mappings.default_mappings\n" +
                   "(id, timestamp_s, timestamp_ms, name)\n" +
                   "VALUES (:id, :timestamp_s, :timestamp_ms, :binary_message.name)")
        void insert(
                @BindMessage(types = {
                        @BindType(name = "timestamp_s", type = Types.TIMESTAMP),
                        @BindType(name = "timestamp_ms", type = Types.TIMESTAMP),
                }) OptionalFields fields);

        @SqlQuery("SELECT * FROM mappings.default_mappings\n" +
                  "WHERE id = :id")
        OptionalFields get(@Bind("id") int id);

        @SqlUpdate("INSERT INTO mappings.default_mappings\n" +
                   "(id, fib, compact, name)\n" +
                   "VALUES (:id, :fib, :message, :name)")
        void insert(@Bind("id") Fibonacci id,
                    @Bind("fib") Fibonacci fib,
                    @BindEnumName("message") Fibonacci message,
                    @BindEnumName("name") Fibonacci name);
    }

    public interface AltTestDAO {
        @SqlUpdate("INSERT INTO mappings.default_mappings\n" +
                   "(id, timestamp_s, timestamp_ms, name)\n" +
                   "VALUES (:id, :timestamp_s, :timestamp_ms, :binary_message.name)")
        void insert(
                @BindMessage(types = {
                        @BindType(name = "timestamp_s", type = Types.TIMESTAMP),
                        @BindType(name = "timestamp_ms", type = Types.TIMESTAMP),
                }) OptionalFields fields);

        @SqlUpdate("INSERT INTO mappings.default_mappings\n" +
                   "(id, fib, compact, name)\n" +
                   "VALUES (:id, :fib, :message, :name)")
        void insert(@BindEnumValue("id") Fibonacci id,
                    @BindEnumValue("fib") Fibonacci fib,
                    @BindEnumName("message") Fibonacci message,
                    @BindEnumName("name") Fibonacci name);

        @SqlQuery("SELECT * FROM mappings.default_mappings\n" +
                  "WHERE id = :id")
        @RegisterMessageMapper(OptionalFields.class)
        OptionalFields get(@Bind("id") int id);

        @SqlQuery("SELECT id FROM mappings.default_mappings\n")
        @RegisterEnumValueMapper(value = Fibonacci.class, acceptUnknown = true)
        List<Fibonacci> allFibs();
    }

    @Test
    public void testDAO() {
        try (Handle handle = db.getDBI().open()) {
            TestDAO dao = handle.attach(TestDAO.class);

            OptionalFields source = OptionalFields
                    .builder()
                    .setId(42)
                    .setTimestampS(1234565)
                    .setTimestampMs(123456789012345L)
                    .build();

            dao.insert(source);

            OptionalFields ret = dao.get(42);
            assertThat(ret, is(notNullValue()));
            assertThat(ret, is(source));

            dao.insert(Fibonacci.FIFTEENTH, null, null, Fibonacci.EIGHTEENTH);
        }
    }

    @Test
    public void testAltDAO() {
        try (Handle handle = db.getDBI().open()) {
            AltTestDAO dao = handle.attach(AltTestDAO.class);

            OptionalFields source = OptionalFields
                    .builder()
                    .setId(42)
                    .setTimestampS(1234565)
                    .setTimestampMs(1234)
                    .build();

            dao.insert(source);

            OptionalFields ret = dao.get(42);
            assertThat(ret, is(notNullValue()));
            assertThat(ret, is(source));

            dao.insert(Fibonacci.FIFTEENTH, null, null, Fibonacci.EIGHTEENTH);
            assertThat(dao.allFibs(), is(listOf(null, Fibonacci.FIFTEENTH)));
        }
    }

    @Test
    public void testDummy() {
        assertThat(
                new RegisterMessageMapper.Factory().createForParameter(
                        null, null, null, null),
                is(notNullValue()));
    }
}
