/*
 * Copyright 2018-2019 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.jdbi.v2;

import net.morimekta.providence.PMessage;
import net.morimekta.providence.PMessageOrBuilder;
import net.morimekta.providence.descriptor.PField;
import net.morimekta.providence.descriptor.PMessageDescriptor;
import net.morimekta.providence.jdbi.v2.util.FieldType;
import net.morimekta.util.collect.UnmodifiableMap;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static net.morimekta.providence.jdbi.v2.MessageRowMapper.ALL_FIELDS;

/**
 * Utility class and helper to make mappers and argument helpers for
 * JDBI queries and updates.
 */
public class ProvidenceJdbi {
    /**
     * Bind to the given field for the message.
     *
     * @param message The message tp bind value from.
     * @param field The field to bind to.
     * @param <M> The message type.
     * @return The message field argument.
     */
    public static <M extends PMessage<M>>
    MessageFieldArgument<M> toField(PMessageOrBuilder<M> message, PField<M> field) {
        return new MessageFieldArgument<>(message, field);
    }

    /**
     * Bind to the given field for the message.
     *
     * @param message The message tp bind value from.
     * @param field The field to bind to.
     * @param type The SQL type.
     * @param <M> The message type.
     * @return The message field argument.
     */
    public static <M extends PMessage<M>>
    MessageFieldArgument<M> toField(PMessageOrBuilder<M> message, PField<M> field, int type) {
        return new MessageFieldArgument<>(message, field, type);
    }

    /**
     * With all column with default types.
     *
     * @param <M> The message type.
     * @return The mapped field.
     */
    public static <M extends PMessage<M>>
    MappedField<M> columnsFromAllFields() {
        return new MappedField<>(ALL_FIELDS, null);
    }

    /**
     * With column mapped to field using the field name.
     *
     * @param field Field it is mapped to.
     * @param <M> The message type.
     * @return The mapped field.
     */
    public static <M extends PMessage<M>>
    MappedField<M> withColumn(PField<M> field) {
        return new MappedField<>(field.getName(), field);
    }

    /**
     * With column mapped to field.
     *
     * @param name Name of column.
     * @param field Field it is mapped to.
     * @param <M> The message type.
     * @return The mapped field.
     */
    public static <M extends PMessage<M>>
    MappedField<M> withColumn(String name, PField<M> field) {
        return new MappedField<>(name, field);
    }

    /**
     * Bind to message using row mapper.
     *
     * @param descriptor The message descriptor.
     * @param fieldMapping Extra field mapping.
     * @param <M> The message type.
     * @return The row mapper.
     */
    @SafeVarargs
    public static <M extends PMessage<M>>
    MessageRowMapper<M> toMessage(@Nonnull PMessageDescriptor<M> descriptor,
                                  @Nonnull MappedField<M>... fieldMapping) {
        return new MessageRowMapper<>(descriptor, makeMapping(fieldMapping));
    }

    /**
     * Bind to message using row mapper.
     *
     * @param tableName Table name to restrict field lookup to.
     * @param descriptor The message descriptor.
     * @param fieldMapping Extra field mapping.
     * @param <M> The message type.
     * @return The row mapper.
     */
    @SafeVarargs
    public static <M extends PMessage<M>>
    MessageRowMapper<M> toMessage(@Nonnull String tableName,
                                  @Nonnull PMessageDescriptor<M> descriptor,
                                  @Nonnull MappedField<M>... fieldMapping) {
        return new MessageRowMapper<>(tableName, descriptor, makeMapping(fieldMapping));
    }

    /**
     * With field mapped to SQL type.
     *
     * @param field The field to be mapped.
     * @param type The SQL type. See {@link java.sql.Types}.
     * @param <M> The message type.
     * @return The field type mapping.
     */
    public static <M extends PMessage<M>>
    FieldType<M> withType(PField<M> field, int type) {
        return new FieldType<>(field, type);
    }

    /**
     * Get named argument finder for message.
     *
     * @param message The message to map fields from.
     * @param fieldTypes Field type mappings.
     * @param <M> The message type.
     * @return The named argument finder.
     */
    @SafeVarargs
    public static <M extends PMessage<M>>
    MessageNamedArgumentFinder<M> forMessage(@Nonnull M message,
                                             @Nonnull FieldType<M>... fieldTypes) {
        return new MessageNamedArgumentFinder<>(null, message, makeFieldTypes(fieldTypes));
    }

    /**
     * Get named argument finder for message.
     *
     * @param prefix Name prefix for naming distinction.
     * @param message The message to map fields from.
     * @param fieldTypes Field type mappings.
     * @param <M> The message type.
     * @return The named argument finder.
     */
    @SafeVarargs
    public static <M extends PMessage<M>>
    MessageNamedArgumentFinder<M> forMessage(@Nonnull String prefix,
                                             @Nonnull PMessageOrBuilder<M> message,
                                             @Nonnull FieldType<M>... fieldTypes) {
        return new MessageNamedArgumentFinder<>(prefix, message, makeFieldTypes(fieldTypes));
    }

    public static class MappedField<M extends PMessage<M>> {
        private final String name;
        private final PField<M> field;

        MappedField(String name, PField<M> field) {
            this.name = name;
            this.field = field;
        }
    }

    @SafeVarargs
    private static <M extends PMessage<M>> Map<PField<M>, Integer> makeFieldTypes(FieldType<M>... mappings) {
        UnmodifiableMap.Builder<PField<M>, Integer> builder = UnmodifiableMap.builder();
        for (FieldType<M> mapping : mappings) {
            builder.put(mapping.field, mapping.type);
        }
        return builder.build();
    }

    @SafeVarargs
    private static <M extends PMessage<M>> Map<String,PField<M>> makeMapping(@Nonnull MappedField<M>... mappedFields) {
        HashMap<String, PField<M>> out = new HashMap<>();
        for (MappedField<M> mappedField : mappedFields) {
            out.put(mappedField.name.toUpperCase(Locale.US), mappedField.field);
        }
        return out;
    }
}
