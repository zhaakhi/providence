package net.morimekta.providence.jdbi.v2;

import net.morimekta.providence.PEnumValue;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.Argument;
import org.skife.jdbi.v2.tweak.ArgumentFactory;

public class EnumValueArgumentFactory implements ArgumentFactory<PEnumValue> {
    private final Class<?> klass;

    @SuppressWarnings("unused")
    public EnumValueArgumentFactory() {
        this(PEnumValue.class);
    }

    public EnumValueArgumentFactory(Class<?> klass) {
        this.klass = klass;
    }

    @Override
    public boolean accepts(Class<?> expectedType, Object value, StatementContext ctx) {
        return klass.isAssignableFrom(expectedType);
    }

    @Override
    public Argument build(Class<?> expectedType, PEnumValue value, StatementContext ctx) {
        return new EnumValueArgument(value);
    }
}
