<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <name>Providence CLI : Schema Tool</name>
    <description>
        Schema Generator. Generates schema matching
        the definition created by providence-graphql utils and
        similar. Note: does not generate thrift or providence
        IDL.
    </description>

    <parent>
        <groupId>net.morimekta.providence</groupId>
        <artifactId>providence</artifactId>
        <version>2.7.0-SNAPSHOT</version>
    </parent>
    <artifactId>providence-tools-schema</artifactId>

    <dependencies>
        <!-- INTERNAL -->
        <dependency>
            <groupId>net.morimekta.providence</groupId>
            <artifactId>providence-core</artifactId>
        </dependency>
        <dependency>
            <groupId>net.morimekta.providence</groupId>
            <artifactId>providence-reflect</artifactId>
        </dependency>
        <dependency>
            <groupId>net.morimekta.providence</groupId>
            <artifactId>providence-graphql</artifactId>
        </dependency>
        <dependency>
            <groupId>net.morimekta.providence</groupId>
            <artifactId>providence-jax-rs</artifactId>
        </dependency>
        <dependency>
            <groupId>net.morimekta.providence</groupId>
            <artifactId>providence-tools-common</artifactId>
        </dependency>
        <dependency>
            <groupId>net.morimekta.providence</groupId>
            <artifactId>providence-tools-common-formats</artifactId>
        </dependency>

        <!-- EXTERNAL -->
        <dependency>
            <groupId>net.morimekta.utils</groupId>
            <artifactId>io-util</artifactId>
        </dependency>
        <dependency>
            <groupId>net.morimekta.utils</groupId>
            <artifactId>console-util</artifactId>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-databind</artifactId>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-annotations</artifactId>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.dataformat</groupId>
            <artifactId>jackson-dataformat-yaml</artifactId>
        </dependency>
        <dependency>
            <groupId>io.swagger.core.v3</groupId>
            <artifactId>swagger-core</artifactId>
        </dependency>

        <!-- TESTING -->
        <dependency>
            <groupId>net.morimekta.providence</groupId>
            <artifactId>providence-testing-junit4</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>net.morimekta.utils</groupId>
            <artifactId>testing-util</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-core</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-install-plugin</artifactId>
                <configuration>
                    <skip>true</skip>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-deploy-plugin</artifactId>
                <configuration>
                    <skip>true</skip>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-site-plugin</artifactId>
                <configuration>
                    <skip>true</skip>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-shade-plugin</artifactId>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>shade</goal>
                        </goals>
                        <configuration>
                            <finalName>providence-tools-schema</finalName>
                            <minimizeJar>true</minimizeJar>
                            <filters>
                                <filter>
                                    <artifact>*:*</artifact>
                                    <excludes>
                                        <!-- The accompanied license and notice is not from this project. -->
                                        <exclude>META-INF/LICENSE</exclude>
                                        <exclude>META-INF/NOTICE</exclude>
                                        <exclude>META-INF/LICENSE.txt</exclude>
                                        <exclude>META-INF/NOTICE.txt</exclude>
                                        <exclude>META-INF/DEPENDENCIES</exclude>
                                        <exclude>META-INF/maven/</exclude>
                                        <exclude>META-INF/*.SF</exclude>
                                        <exclude>META-INF/*.DSA</exclude>
                                        <exclude>META-INF/*.RSA</exclude>
                                        <exclude>**/package.html</exclude>
                                        <exclude>**/overview.html</exclude>
                                        <!-- special for pure annotation packages -->
                                        <exclude>com/google/j2objc/annotations/</exclude>
                                        <exclude>com/google/errorprone/annotations/</exclude>
                                        <exclude>org/codehaus/mojo/animal_sniffer/</exclude>
                                        <!-- Lots of files in commons/codec -->
                                        <exclude>org/apache/commons/codec/language/bm/*</exclude>
                                    </excludes>
                                </filter>
                            </filters>
                            <transformers>
                                <transformer implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
                                    <mainClass>net.morimekta.providence.tools.schema.Schema</mainClass>
                                </transformer>
                                <transformer implementation="org.apache.maven.plugins.shade.resource.ServicesResourceTransformer" />
                            </transformers>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <configuration>
                    <skip>true</skip>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.sonatype.plugins</groupId>
                <artifactId>nexus-staging-maven-plugin</artifactId>
                <configuration>
                    <skipStaging>true</skipStaging>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-gpg-plugin</artifactId>
                <configuration>
                    <skip>true</skip>
                </configuration>
            </plugin>
        </plugins>
    </build>
</project>
