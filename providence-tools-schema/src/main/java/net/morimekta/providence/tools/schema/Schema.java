package net.morimekta.providence.tools.schema;

import net.morimekta.console.args.ArgumentException;
import net.morimekta.console.args.ArgumentParser;
import net.morimekta.console.util.STTY;
import net.morimekta.providence.serializer.SerializerException;
import net.morimekta.providence.tools.schema.cmd.Command;
import net.morimekta.providence.tools.schema.cmd.Help;
import net.morimekta.util.lexer.LexerException;

import java.io.IOException;
import java.io.UncheckedIOException;

import static net.morimekta.providence.tools.common.Utils.getVersionString;

public class Schema {
    private final SchemaOptions options;

    protected Schema() {
        this(new STTY());
    }

    protected Schema(STTY tty) {
        options = new SchemaOptions(tty);
    }

    void run(String... args) {
        try {
            ArgumentParser cli = options.getArgumentParser("pvd-schema", "Providence Schema Tool");

            try {
                cli.parse(args);
                if (options.showHelp()) {
                    new Help(options.getSubCommandSet(), cli).execute(options);
                    return;
                } else if (options.showVersion()) {
                    System.out.println("Providence RPC Tool - " + getVersionString());
                    return;
                }

                cli.validate();
                Command command = options.getCommand();
                if (command == null) {
                    throw new ArgumentException("Foo");
                }

                command.execute(options);
                return;
            } catch (LexerException e) {
                System.out.flush();
                System.err.println(e.displayString());
                if (options.verbose()) {
                    System.err.println();
                    e.printStackTrace();
                }
            } catch (ArgumentException e) {
                System.out.flush();
                System.err.println(e.getMessage());
                System.err.println("Usage: " + cli.getSingleLineUsage());
                System.err.println();
                System.err.println("Run $ pvd-schema --help # for available options.");
            } catch (SerializerException e) {
                System.out.flush();
                System.err.println("Serializer error: " + e.displayString());
                if (options.verbose()) {
                    System.err.println();
                    e.printStackTrace();
                }
            } catch (UncheckedIOException | IOException e) {
                System.out.flush();
                System.err.println("I/O error: " + e.getMessage());
                if (options.verbose()) {
                    System.out.flush();
                    e.printStackTrace();
                }
            } catch (IllegalArgumentException e) {
                System.out.flush();
                System.err.println("Internal Error: " + e.getMessage());
                if (options.verbose()) {
                    System.err.println();
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            System.out.flush();
            System.err.println("Unchecked exception: " + e.getMessage());
            if (options.verbose()) {
                System.out.flush();
                e.printStackTrace();
            }
        }
        System.err.flush();
        exit(1);
    }

    protected void exit(int i) {
        System.exit(i);
    }

    public static void main(String[] args) {
        new Schema().run(args);
    }

}
