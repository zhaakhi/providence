package net.morimekta.providence.tools.schema.cmd;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import io.swagger.v3.core.converter.AnnotatedType;
import io.swagger.v3.core.converter.ModelConverter;
import io.swagger.v3.core.converter.ModelConverterContext;
import io.swagger.v3.core.converter.ModelConverterContextImpl;
import io.swagger.v3.oas.models.media.Schema;
import net.morimekta.console.args.Argument;
import net.morimekta.console.args.ArgumentException;
import net.morimekta.console.args.ArgumentParser;
import net.morimekta.console.args.Flag;
import net.morimekta.console.args.Option;
import net.morimekta.providence.descriptor.PDeclaredDescriptor;
import net.morimekta.providence.jax.rs.ProvidenceModelConverter;
import net.morimekta.providence.jax.rs.schema.JsonSchemaModel;
import net.morimekta.providence.reflect.ProgramLoader;
import net.morimekta.providence.reflect.util.ReflectionUtils;
import net.morimekta.providence.tools.schema.SchemaOptions;
import net.morimekta.providence.types.SimpleTypeRegistry;
import net.morimekta.providence.types.TypeReference;
import net.morimekta.providence.types.TypeRegistry;
import net.morimekta.util.Strings;
import net.morimekta.util.collect.UnmodifiableList;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static com.fasterxml.jackson.databind.SerializationFeature.WRITE_NULL_MAP_VALUES;
import static java.nio.file.Files.createDirectories;
import static java.nio.file.Files.newOutputStream;
import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING;
import static net.morimekta.providence.jax.rs.OpenAPIUtils.setIncludeNonNullOnSchema;
import static net.morimekta.util.collect.UnmodifiableList.listOf;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class JsonSchema implements Command {
    private List<String>            includeTypes  = new ArrayList<>();
    private AtomicReference<String> programName   = new AtomicReference<>();
    private AtomicReference<String> urlBase       = new AtomicReference<>();
    private AtomicBoolean           allowCompact  = new AtomicBoolean();
    private AtomicBoolean           allowEnumId   = new AtomicBoolean();
    private AtomicBoolean           complexUnions = new AtomicBoolean();

    @Override
    public ArgumentParser parser(ArgumentParser parent) {
        ArgumentParser parser = new ArgumentParser(parent, "json-schema", "Generate json schema");
        parser.add(new Flag("--allow-compact", "c", "Allow compact JSON definitions in output", allowCompact::set));
        parser.add(new Flag("--allow-enum-id", "e", "Allow enum ID for value definition in output.", allowEnumId::set));
        parser.add(new Flag("--complex-unions", "x", "Use complex union definitions.", complexUnions::set));
        parser.add(new Option("--program", "p", "PROG", "Add all types from this program", programName::set));
        parser.add(new Argument("type", "Message or enum types to be included in the output.", includeTypes::add, null, null, true, false, false));
        return parser;
    }

    @Override
    @SuppressWarnings("deprecation")
    public void execute(SchemaOptions options) throws IOException {
        Map<String, Path> includes = options.getIncludes();

        ProgramLoader loader = new ProgramLoader();

        List<String> programs = new ArrayList<>();
        if (programName.get() != null) {
            programs.add(programName.get());
        }
        if (includeTypes.size() > 0) {
            for (String type : includeTypes) {
                programs.add(type.replaceAll("\\..*", ""));
            }
        }

        if (programs.isEmpty()) {
            throw new ArgumentException("program or root type must be specified");
        }

        for (String program : programs) {
            if (!includes.containsKey(program)) {
                throw new ArgumentException("No program " + program + " found in include path for query.\n" +
                                            "Found: " + Strings.join(", ", new TreeSet<Object>(includes.keySet())));
            }

            loader.load(includes.get(program));
        }

        List<PDeclaredDescriptor<?>> descriptors = new ArrayList<>();
        if (includeTypes.size() > 0) {
            SimpleTypeRegistry simple = new SimpleTypeRegistry();
            for (String type : includeTypes) {
                String programName = type.replaceAll("\\..*", "");
                TypeRegistry registry = loader.getGlobalRegistry()
                                              .registryForPath(includes.get(programName).toString());
                simple.registerType(registry.getDeclaredType(TypeReference.parseType(type))
                                            .orElseThrow(() -> new ArgumentException("No type for name " + type)));
            }
            descriptors.addAll(simple.getDeclaredTypes()
                                     .stream()
                                     .filter(ReflectionUtils::isDeclaredType)
                                     .collect(Collectors.toList()));
        } else {
            descriptors.addAll(loader.getGlobalRegistry()
                                     .getDeclaredTypes()
                                     .stream()
                                     .filter(ReflectionUtils::isDeclaredType)
                                     .collect(Collectors.toList()));
        }

        if (descriptors.isEmpty()) {
            throw new ArgumentException("No types to include in schema");
        }

        JsonSchemaModel model = new JsonSchemaModel();
        if (Strings.isNotEmpty(urlBase.get())) {
            model.set$id(urlBase.get() + "/" + programName + ".json");
        }

        ProvidenceModelConverter converter = new ProvidenceModelConverter(
                JsonSchemaModel.REF_PREFIX,
                complexUnions.get(),
                allowCompact.get(),
                allowEnumId.get());
        ModelConverterContext    context   = new ModelConverterContextImpl(listOf(converter));
        for (PDeclaredDescriptor<?> desc : descriptors) {
            AnnotatedType type = new AnnotatedType();
            type.setResolveAsRef(false);
            type.setType(desc);
            Schema<?> schema = converter.resolve(type, context, UnmodifiableList.<ModelConverter>listOf().iterator());
            if (schema != null) {
                model.getDefinitions().put(desc.getQualifiedName(), schema);
            }
        }

        model.getDefinitions().putAll(context.getDefinedModels());

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(WRITE_NULL_MAP_VALUES, false);
        setIncludeNonNullOnSchema(objectMapper);
        ObjectWriter objectWriter = objectMapper
                .writerWithDefaultPrettyPrinter();
        if (options.getOutput() != null) {
            createDirectories(options.getOutput().toAbsolutePath().getParent());
            try (OutputStream fos = new BufferedOutputStream(newOutputStream(options.getOutput(), CREATE, TRUNCATE_EXISTING))) {
                objectWriter.writeValue(fos, model);
            }
        } else {
            objectWriter.writeValue(System.out, model);
        }
    }
}
