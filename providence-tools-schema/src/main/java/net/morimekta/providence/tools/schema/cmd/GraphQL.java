package net.morimekta.providence.tools.schema.cmd;

import net.morimekta.console.args.ArgumentException;
import net.morimekta.console.args.ArgumentParser;
import net.morimekta.console.args.Option;
import net.morimekta.providence.descriptor.PMessageDescriptor;
import net.morimekta.providence.graphql.GQLDefinition;
import net.morimekta.providence.reflect.ProgramLoader;
import net.morimekta.providence.tools.schema.SchemaOptions;
import net.morimekta.util.Strings;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import static net.morimekta.providence.types.TypeReference.parseType;

public class GraphQL implements Command {
    private List<String> idFields = new ArrayList<>();
    private String       query;
    private String       mutation;

    @Override
    public ArgumentParser parser(ArgumentParser parent) {
        ArgumentParser parser = new ArgumentParser(parent, "graphql", "Generate graphql schema");
        parser.add(new Option("--query", "q", "srv", "Qualified identifier name from definitions to use for query.",
                              this::setQuery, null, false, true, false));
        parser.add(new Option("--mutation",
                              "m",
                              "srv",
                              "Qualified identifier name from definitions to use for mutation.",
                              this::setMutation));
        parser.add(new Option("--id-field",
                              "i",
                              "field",
                              "Field qualifier for specified 'id' field. Using the format 'program.Type:field'",
                              this::addIdField,
                              null,
                              true,
                              false,
                              false));
        return parser;
    }

    @Override
    public void execute(SchemaOptions options) throws IOException {
        GQLDefinition def = getDefinition(options.getIncludes());
        Path out = options.getOutput();
        if (out != null) {
            out = out.normalize().toAbsolutePath();
            Files.createDirectories(out.getParent());
            try (FileOutputStream fos = new FileOutputStream(out.toFile());
                 BufferedOutputStream bos = new BufferedOutputStream(fos);
                 OutputStreamWriter osw = new OutputStreamWriter(bos);
                 PrintWriter writer = new PrintWriter(osw)) {
                writer.print(def.getSchema());
            }
        } else {
            System.out.print(def.getSchema());
        }
    }

    private void setQuery(String service) {
        this.query = service;
    }

    private void setMutation(String service) {
        this.mutation = service;
    }

    private void addIdField(String field) {
        if (field.contains(",")) {
            for (String f : field.split(",")) {
                addIdField(f);
            }
            return;
        }
        if (!field.contains(":")) {
            throw new ArgumentException("Field must be like 'program.Type:field'");
        }
        this.idFields.add(field);
    }

    public GQLDefinition getDefinition(Map<String, Path> includeMap) throws IOException {
        ProgramLoader loader = new ProgramLoader();
        GQLDefinition.Builder builder = GQLDefinition.builder();

        {
            String queryProgram = query.replaceAll("\\..*", "");
            if (!includeMap.containsKey(queryProgram)) {
                throw new ArgumentException("No program " + queryProgram + " found in include path for query.\n" +
                                            "Found: " + Strings.join(", ", new TreeSet<Object>(includeMap.keySet())));
            }

            loader.load(includeMap.get(queryProgram));
            builder.query(loader.getGlobalRegistry().requireService(parseType(this.query)));
        }

        if (mutation != null) {
            String queryProgram = mutation.replaceAll("\\..*", "");

            if (!includeMap.containsKey(queryProgram)) {
                throw new ArgumentException("No program " + queryProgram + " found in include path for mutation.\n" +
                                            "Found: " + Strings.join(", ", new TreeSet<Object>(includeMap.keySet())));
            }

            loader.load(includeMap.get(queryProgram));
            builder.mutation(loader.getGlobalRegistry().requireService(parseType(this.mutation)));
        }

        for (String field : idFields) {
            // Each of the form "program.Type:field"
            String typeName = field.replaceAll(":.*", "");
            String fieldName = field.replaceAll(".*:", "");
            try {
                PMessageDescriptor<?> type = loader.getGlobalRegistry().requireMessageType(parseType(typeName));
                builder.idField(type.fieldForName(fieldName));
            } catch (IllegalArgumentException e) {
                throw new ArgumentException("Bad field '" + field + "': " + e.getMessage(), e);
            }
        }

        return builder.build();
    }

}
