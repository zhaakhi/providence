package net.morimekta.providence.tools.schema.cmd;

import net.morimekta.console.args.ArgumentParser;
import net.morimekta.providence.tools.schema.SchemaOptions;

import java.io.IOException;

public interface Command {
    void execute(SchemaOptions options) throws IOException;

    ArgumentParser parser(ArgumentParser parent);
}
