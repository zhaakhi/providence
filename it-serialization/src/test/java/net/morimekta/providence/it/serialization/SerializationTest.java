package net.morimekta.providence.it.serialization;

import net.morimekta.testing.rules.ConsoleWatcher;
import org.apache.thrift.TException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.IOException;

/**
 * Speed test running through:
 * - Read a file for each factory / serialization format.
 * - Write the same file back to a temp file.
 */
public class SerializationTest {
    @Rule
    public ConsoleWatcher console = new ConsoleWatcher().dumpOnFailure();

    @Rule
    public TemporaryFolder temp = new TemporaryFolder();

    @Test
    public void testSerializationSpeed_consistentData() throws IOException, TException, InterruptedException {
        TestOptions options = new TestOptions();
        options.no_progress.set(true);
        options.runs.set(2);
        options.generate.set(10);


        new TestRunner<>(net.morimekta.test.providence.serialization.containers.ManyContainers.kDescriptor,
                         net.morimekta.test.thrift.serialization.containers.ManyContainers::new,
                         options).run();
        new TestRunner<>(net.morimekta.test.providence.serialization.messages.ManyFields.kDescriptor,
                         net.morimekta.test.thrift.serialization.messages.ManyFields::new,
                         options).run();
        new TestRunner<>(net.morimekta.test.providence.serialization.messages.ManyRequiredFields.kDescriptor,
                         net.morimekta.test.thrift.serialization.messages.ManyRequiredFields::new,
                         options).run();
        new TestRunner<>(net.morimekta.test.providence.serialization.deep.DeepStructure.kDescriptor,
                         net.morimekta.test.thrift.serialization.deep.DeepStructure::new,
                         options).run();
    }
}
