[[1;34mINFO[m] Scanning for projects...
[[1;34mINFO[m] [1m------------------------------------------------------------------------[m
[[1;34mINFO[m] [1mReactor Build Order:[m
[[1;34mINFO[m] 
[[1;34mINFO[m] Providence Utils : JavaX WS RESTful compatibility                  [jar]
[[1;34mINFO[m] Providence Utils : GraphQL                                         [jar]
[[1;34mINFO[m] Providence CLI : Common Utils                                      [jar]
[[1;34mINFO[m] Providence CLI : Common Formats Utils                              [jar]
[[1;34mINFO[m] Providence CLI : Code Generator                                    [jar]
[[1;34mINFO[m] Providence CLI : Schema Tool                                       [jar]
[[1;34mINFO[m] Providence CLI : Config Helper                                     [jar]
[[1;34mINFO[m] Providence CLI : Converter                                         [jar]
[[1;34mINFO[m] Providence CLI : Remote Procedure Call                             [jar]
[[1;34mINFO[m] Providence CLI : Packaging                                         [jar]
[[1;34mINFO[m] 
[[1;34mINFO[m] [1m-------------< [0;36mnet.morimekta.providence:providence-jax-rs[0;1m >-------------[m
[[1;34mINFO[m] [1mBuilding Providence Utils : JavaX WS RESTful compatibility 2.3.1-SNAPSHOT [1/10][m
[[1;34mINFO[m] [1m--------------------------------[ jar ]---------------------------------[m
