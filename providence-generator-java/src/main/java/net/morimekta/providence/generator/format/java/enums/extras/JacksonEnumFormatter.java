/*
 * Copyright 2016 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.generator.format.java.enums.extras;

import net.morimekta.providence.generator.GeneratorException;
import net.morimekta.providence.generator.format.java.shared.EnumMemberFormatter;
import net.morimekta.providence.generator.format.java.utils.JUtils;
import net.morimekta.providence.reflect.contained.CEnumDescriptor;
import net.morimekta.util.Strings;
import net.morimekta.util.io.IndentedPrintWriter;

import java.io.IOException;

import static net.morimekta.providence.generator.format.java.utils.Jackson.DESERIALIZATION_CONTEXT;
import static net.morimekta.providence.generator.format.java.utils.Jackson.JSON_DESERIALIZE;
import static net.morimekta.providence.generator.format.java.utils.Jackson.JSON_DESERIALIZER;
import static net.morimekta.providence.generator.format.java.utils.Jackson.JSON_PARSER;
import static net.morimekta.providence.generator.format.java.utils.Jackson.JSON_PARSE_EXCEPTION;
import static net.morimekta.providence.generator.format.java.utils.Jackson.JSON_PROCESSING_EXCEPTION;
import static net.morimekta.providence.generator.format.java.utils.Jackson.JSON_TOKEN;
import static net.morimekta.providence.generator.format.java.utils.Jackson.JSON_VALUE;

/**
 * Formatter for extending for jackson annotated serialization.
 */
public class JacksonEnumFormatter implements EnumMemberFormatter {
    private final IndentedPrintWriter writer;

    public JacksonEnumFormatter(IndentedPrintWriter writer) {
        this.writer = writer;
    }

    @Override
    public void appendClassAnnotations(CEnumDescriptor type) throws GeneratorException {
        String simpleClass = JUtils.getClassName(type);

        writer.formatln("@%s(", JSON_DESERIALIZE)
              .formatln("        using = %s._Deserializer.class)", simpleClass);
    }

    @Override
    public void appendMethods(CEnumDescriptor type) throws GeneratorException {
        writer.formatln("@%s", JSON_VALUE)
              .appendln("public int jsonValue() {")
              .appendln("    return mId;")
              .appendln('}')
              .newline();
    }

    @Override
    public void appendExtraProperties(CEnumDescriptor type) throws GeneratorException {
        String simpleClass = JUtils.getClassName(type);
        String spacesClass = Strings.times(" ", simpleClass.length());

        writer.formatln("public static final class _Deserializer extends %s<%s> {",
                        JSON_DESERIALIZER, simpleClass)
              .appendln("    @Override")
              .formatln("    public %s deserialize(%s jp,",
                        simpleClass, JSON_PARSER)
              .formatln("           %s             %s ctxt)",
                        spacesClass, DESERIALIZATION_CONTEXT)
              .formatln("            throws %s,", IOException.class.getName())
              .formatln("                   %s {", JSON_PROCESSING_EXCEPTION)
              .formatln("        if (jp.getCurrentToken() == %s.VALUE_NUMBER_INT) {", JSON_TOKEN)
              .formatln("            return %s.findById(jp.getIntValue());", simpleClass)
              .formatln("        } else if (jp.getCurrentToken() == %s.VALUE_STRING) {", JSON_TOKEN)
              .formatln("            if (%s.isInteger(jp.getText())) {", Strings.class.getName())
              .formatln("                return %s.findById(Integer.parseInt(jp.getText()));", simpleClass)
              .appendln("            }")
              .formatln("            return %s.findByName(jp.getText());", simpleClass)
              .appendln("        } else {")
              .formatln("            throw new %s(jp, \"Invalid token for enum %s deserialization \" + jp.getText());",
                        JSON_PARSE_EXCEPTION,
                        type.getQualifiedName())
              .appendln("        }")
              .appendln("    }")
              .appendln('}')
              .newline();
    }
}
