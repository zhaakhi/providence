package net.morimekta.providence.generator.format.java.utils;

public class Jackson {
    public static final String JSON_VALUE = "com.fasterxml.jackson.annotation.JsonValue";
    public static final String JSON_PARSE_EXCEPTION = "com.fasterxml.jackson.core.JsonParseException";
    public static final String JSON_PARSER = "com.fasterxml.jackson.core.JsonParser";
    public static final String JSON_PROCESSING_EXCEPTION = "com.fasterxml.jackson.core.JsonProcessingException";
    public static final String JSON_TOKEN = "com.fasterxml.jackson.core.JsonToken";
    public static final String DESERIALIZATION_CONTEXT = "com.fasterxml.jackson.databind.DeserializationContext";
    public static final String JSON_DESERIALIZER = "com.fasterxml.jackson.databind.JsonDeserializer";
    public static final String JSON_DESERIALIZE = "com.fasterxml.jackson.databind.annotation.JsonDeserialize";

    public static final String JSON_GENERATOR = "com.fasterxml.jackson.core.JsonGenerator";
    public static final String JAVA_TYPE = "com.fasterxml.jackson.databind.JavaType";
    public static final String JSON_SERIALIZER = "com.fasterxml.jackson.databind.JsonSerializer";
    public static final String SERIALIZER_PROVIDER = "com.fasterxml.jackson.databind.SerializerProvider";
    public static final String JSON_SERIALIZE = "com.fasterxml.jackson.databind.annotation.JsonSerialize";
    public static final String COLLECTION_TYPE = "com.fasterxml.jackson.databind.type.CollectionType";
    public static final String MAP_TYPE = "com.fasterxml.jackson.databind.type.MapType";

    private Jackson() {}
}
