/*
 * Copyright 2016 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.generator.format.java.messages;

import net.morimekta.providence.PException;
import net.morimekta.providence.PMessage;
import net.morimekta.providence.PMessageVariant;
import net.morimekta.providence.PUnion;
import net.morimekta.providence.descriptor.PAnnotation;
import net.morimekta.providence.descriptor.PDefaultValueProvider;
import net.morimekta.providence.descriptor.PDescriptor;
import net.morimekta.providence.descriptor.PDescriptorProvider;
import net.morimekta.providence.descriptor.PField;
import net.morimekta.providence.descriptor.PInterfaceDescriptor;
import net.morimekta.providence.descriptor.PMessageDescriptor;
import net.morimekta.providence.descriptor.PRequirement;
import net.morimekta.providence.descriptor.PStructDescriptor;
import net.morimekta.providence.descriptor.PStructDescriptorProvider;
import net.morimekta.providence.descriptor.PValueProvider;
import net.morimekta.providence.generator.GeneratorException;
import net.morimekta.providence.generator.format.java.shared.MessageMemberFormatter;
import net.morimekta.providence.generator.format.java.utils.BlockCommentBuilder;
import net.morimekta.providence.generator.format.java.utils.JAnnotation;
import net.morimekta.providence.generator.format.java.utils.JField;
import net.morimekta.providence.generator.format.java.utils.JHelper;
import net.morimekta.providence.generator.format.java.utils.JMessage;
import net.morimekta.providence.serializer.json.JsonCompactible;
import net.morimekta.providence.serializer.json.JsonCompactibleDescriptor;
import net.morimekta.util.collect.UnmodifiableList;
import net.morimekta.util.io.IndentedPrintWriter;

import java.util.Collection;
import java.util.Locale;

import static net.morimekta.providence.types.TypeReference.parseType;
import static net.morimekta.util.Strings.isNotEmpty;

/**
 * @author Stein Eldar Johnsen
 * @since 08.01.16.
 */
public class CoreOverridesFormatter implements MessageMemberFormatter {
    public static final String UNION_FIELD = "tUnionField";

    protected final IndentedPrintWriter writer;
    private final JHelper helper;

    public CoreOverridesFormatter(IndentedPrintWriter writer, JHelper helper) {
        this.helper = helper;
        this.writer = writer;
    }

    @Override
    public Collection<String> getExtraImplements(JMessage<?> message) throws GeneratorException {
        UnmodifiableList.Builder<String> builder = UnmodifiableList.builder();

        builder.add(String.format(Locale.US, "%s<%s>",
                                  message.isUnion() ? PUnion.class.getName() : PMessage.class.getName(),
                                  message.instanceType()));

        if (message.isException()) {
            builder.add(PException.class.getName());
        }
        if (message.jsonCompactible()) {
            builder.add(JsonCompactible.class.getName());
        }

        return builder.build();
    }

    @Override
    public void appendFields(JMessage<?> message) throws GeneratorException {
        if (message.isUnion()) {
            writer.formatln("private transient final _Field %s;", UNION_FIELD)
                  .newline();
        }
    }

    @Override
    public void appendMethods(JMessage<?> message) {
        appendPresence(message);
        appendGetter(message);
        appendJsonCompact(message);

        // Exception
        if (message.isException()) {
            appendPExceptionOverrides();
            appendExceptionOverrides(message);
        }

        if (message.isUnion()) {
            writer.appendln(JAnnotation.OVERRIDE)
                  .appendln("public boolean unionFieldIsSet() {")
                  .formatln("    return %s != null;", UNION_FIELD)
                  .appendln('}')
                  .newline();

            writer.appendln(JAnnotation.OVERRIDE)
                  .appendln(JAnnotation.NON_NULL)
                  .appendln("public _Field unionField() {")
                  .formatln("    if (%s == null) throw new IllegalStateException(\"No union field set in %s\");",
                            UNION_FIELD, message.descriptor().getQualifiedName())
                  .formatln("    return %s;", UNION_FIELD)
                  .appendln('}')
                  .newline();
        }
    }

    @Override
    public void appendExtraProperties(JMessage<?> message) throws GeneratorException {
        appendFieldEnum(message);
        appendDescriptor(message);
    }

    private void appendPExceptionOverrides() {
        writer.appendln(JAnnotation.OVERRIDE)
              .appendln("public String origGetMessage() {")
              .appendln("    return super.getMessage();")
              .appendln('}')
              .newline();

        writer.appendln(JAnnotation.OVERRIDE)
              .appendln("public String origGetLocalizedMessage() {")
              .appendln("    return super.getLocalizedMessage();")
              .appendln('}')
              .newline();
    }

    private void appendExceptionOverrides(JMessage<?> message) {
        writer.appendln(JAnnotation.OVERRIDE)
              .formatln("public %s initCause(Throwable cause) {", message.instanceType())
              .formatln("    return (%s) super.initCause(cause);", message.instanceType())
              .appendln('}')
              .newline();

        writer.appendln(JAnnotation.OVERRIDE)
              .formatln("public %s fillInStackTrace() {", message.instanceType())
              .formatln("    return (%s) super.fillInStackTrace();", message.instanceType())
              .appendln('}')
              .newline();
    }

    private void appendDescriptor(JMessage<?> message) throws GeneratorException {
        String typeClass = message.getDescriptorClass();
        String providerClass = message.getProviderClass();

        writer.appendln(JAnnotation.NON_NULL)
              .formatln("public static %s<%s> provider() {", providerClass, message.instanceType())
              .begin()
              .formatln("return new _Provider();")
              .end()
              .appendln('}')
              .newline();

        writer.appendln(JAnnotation.OVERRIDE)
              .appendln(JAnnotation.NON_NULL)
              .formatln("public %s<%s> descriptor() {", typeClass, message.instanceType())
              .begin()
              .appendln("return kDescriptor;")
              .end()
              .appendln('}')
              .newline();

        writer.formatln("public static final %s<%s> kDescriptor;", typeClass, message.instanceType())
              .newline();

        String jsonCompactibleDescriptor = "";
        if (message.jsonCompactible()) {
            jsonCompactibleDescriptor = " implements " + JsonCompactibleDescriptor.class.getName();
        }

        writer.formatln("private static final class _Descriptor")
              .formatln("        extends %s<%s>%s {", typeClass, message.instanceType(), jsonCompactibleDescriptor)
              .begin()
              .appendln("public _Descriptor() {")
              .begin();
        writer.formatln("super(\"%s\", \"%s\", _Builder::new, %b);",
                        message.descriptor().getProgramName(),
                        message.descriptor().getName(),
                        message.descriptor().isSimple());

        writer.end()
              .appendln('}')
              .newline()
              .appendln(JAnnotation.OVERRIDE)
              .appendln(JAnnotation.NON_NULL)
              .appendln("public boolean isInnerType() {")
              .formatln("    return %b;", message.descriptor().isInnerType())
              .appendln('}')
              .newline()
              .appendln(JAnnotation.OVERRIDE)
              .appendln(JAnnotation.NON_NULL)
              .appendln("public boolean isAutoType() {")
              .formatln("    return %b;", message.descriptor().isAutoType())
              .appendln('}')
              .newline()
              .appendln(JAnnotation.OVERRIDE)
              .appendln(JAnnotation.NON_NULL)
              .appendln("public _Field[] getFields() {")
              .appendln("    return _Field.values();")
              .appendln('}')
              .newline()
              .appendln(JAnnotation.OVERRIDE)
              .appendln(JAnnotation.NULLABLE)
              .appendln("public _Field findFieldByName(String name) {")
              .appendln("    return _Field.findByName(name);")
              .appendln('}')
              .newline()
              .appendln(JAnnotation.OVERRIDE)
              .appendln(JAnnotation.NULLABLE)
              .appendln("public _Field findFieldByPojoName(String name) {")
              .appendln("    return _Field.findByPojoName(name);")
              .appendln('}')
              .newline()
              .appendln(JAnnotation.OVERRIDE)
              .appendln(JAnnotation.NULLABLE)
              .appendln("public _Field findFieldById(int id) {")
              .appendln("    return _Field.findById(id);")
              .appendln('}');

        if (message.descriptor().getImplementing() != null) {
            String ifName = helper.getValueType(message.descriptor().getImplementing());

            writer.newline()
                  .appendln(JAnnotation.OVERRIDE)
                  .formatln("public %s getImplementing() {", PInterfaceDescriptor.class.getName())
                  .formatln("    return %s.kDescriptor;", ifName)
                  .appendln('}');
        }

        writer.end()
              .appendln('}')
              .newline();

        writer.formatln("static {", typeClass, message.instanceType())
              .begin()
              .appendln("kDescriptor = new _Descriptor();")
              .end()
              .appendln('}')
              .newline();

        writer.formatln("private static final class _Provider extends %s<%s> {",
                        providerClass,
                        message.instanceType())
              .begin()
              .appendln(JAnnotation.OVERRIDE)
              .appendln(JAnnotation.NON_NULL)
              .formatln("public %s<%s> descriptor() {", typeClass, message.instanceType())
              .begin()
              .appendln("return kDescriptor;")
              .end()
              .appendln('}')
              .end()
              .appendln("}")
              .newline();
    }

    private void appendFieldEnum( JMessage<?> message) throws GeneratorException {
        writer.formatln("public enum _Field implements %s<%s> {", PField.class.getName(), message.instanceType())
              .begin();

        for (JField field : message.numericalOrderFields()) {
            String defValue = "null";
            if (field.field()
                     .hasDefaultValue()) {
                defValue = String.format(Locale.US, "new %s<>(%s)",
                                         PDefaultValueProvider.class.getName(),
                                         field.kDefault());
            }
            String arguments = "null";
            String argsType = field.field().getAnnotationValue(PAnnotation.ARGUMENTS_TYPE);
            if (isNotEmpty(argsType)) {
                try {
                    PMessageDescriptor<?> descriptor = helper.getRegistry().requireMessageType(parseType(
                            message.descriptor().getProgramName(), argsType));
                    if (descriptor.getVariant() != PMessageVariant.STRUCT) {
                        throw new GeneratorException("Bad arguments type " + descriptor.getQualifiedName() +
                                                     " for field " + field.name() + " in " + message.descriptor().getQualifiedName() +
                                                     ", not a struct.");
                    }
                    arguments = helper.getProviderName(descriptor);
                } catch (IllegalArgumentException e) {
                    throw new GeneratorException("No such arguments type available " +
                                                 field.field().getAnnotationValue(PAnnotation.ARGUMENTS_TYPE) +
                                                 " for field " + field.name() + " in " + message.descriptor().getQualifiedName());
                }
            }

            writer.formatln("%s(%d, %s.%s, \"%s\", \"%s\", %s, %s, %s),",
                            field.fieldEnum(),
                            field.id(),
                            PRequirement.class.getName(),
                            field.field()
                                 .getRequirement()
                                 .name(),
                            field.name(),
                            field.field().getPojoName(),
                            field.getProvider(),
                            arguments,
                            defValue);
        }
        writer.appendln(';')
              .newline();

        writer.appendln("private final int mId;")
              .formatln("private final %s mRequired;", PRequirement.class.getName())
              .appendln("private final String mName;")
              .appendln("private final String mPojoName;")
              .formatln("private final %s mTypeProvider;", PDescriptorProvider.class.getName())
              .formatln("private final %s mArgumentsProvider;", PStructDescriptorProvider.class.getName())
              .formatln("private final %s<?> mDefaultValue;", PValueProvider.class.getName())
              .newline();

        writer.formatln("_Field(int id, %s required, String name, String pojoName, %s typeProvider, %s argumentsProvider, %s<?> defaultValue) {",
                        PRequirement.class.getName(),
                        PDescriptorProvider.class.getName(),
                        PStructDescriptorProvider.class.getName(),
                        PValueProvider.class.getName())
              .begin()
              .appendln("mId = id;")
              .appendln("mRequired = required;")
              .appendln("mName = name;")
              .appendln("mPojoName = pojoName;")
              .appendln("mTypeProvider = typeProvider;")
              .appendln("mArgumentsProvider = argumentsProvider;")
              .appendln("mDefaultValue = defaultValue;")
              .end()
              .appendln('}')
              .newline();
        writer.appendln(JAnnotation.OVERRIDE)
              .appendln("public int getId() { return mId; }")
              .newline();
        writer.appendln(JAnnotation.NON_NULL)
              .appendln(JAnnotation.OVERRIDE)
              .formatln("public %s getRequirement() { return mRequired; }",
                        PRequirement.class.getName())
              .newline();
        writer.appendln(JAnnotation.NON_NULL)
              .appendln(JAnnotation.OVERRIDE)
              .formatln("public %s getDescriptor() { return mTypeProvider.descriptor(); }",
                        PDescriptor.class.getName())
              .newline();
        writer.appendln(JAnnotation.OVERRIDE)
              .appendln(JAnnotation.NULLABLE)
              .formatln("public %s getArgumentsType() { return mArgumentsProvider == null ? null : mArgumentsProvider.descriptor(); }",
                        PStructDescriptor.class.getName())
              .newline();
        writer.appendln(JAnnotation.NON_NULL)
              .appendln(JAnnotation.OVERRIDE)
              .appendln("public String getName() { return mName; }")
              .newline();
        writer.appendln(JAnnotation.NON_NULL)
              .appendln(JAnnotation.OVERRIDE)
              .appendln("public String getPojoName() { return mPojoName; }")
              .newline();
        writer.appendln(JAnnotation.OVERRIDE)
              .appendln("public boolean hasDefaultValue() { return mDefaultValue != null; }")
              .newline();
        writer.appendln(JAnnotation.OVERRIDE)
              .appendln(JAnnotation.NULLABLE)
              .appendln("public Object getDefaultValue() {")
              .appendln("    return hasDefaultValue() ? mDefaultValue.get() : null;")
              .appendln('}')
              .newline();

        writer.appendln(JAnnotation.OVERRIDE)
              .appendln(JAnnotation.NON_NULL)
              .formatln("public %s<%s> onMessageType() {",
                        PMessageDescriptor.class.getName(),
                        message.instanceType())
              .appendln("    return kDescriptor;")
              .appendln('}')
              .newline();

        writer.appendln(JAnnotation.OVERRIDE)
              .appendln("public String toString() {")
              .formatln("    return %s.asString(this);", PField.class.getName())
              .appendln('}')
              .newline();

        new BlockCommentBuilder(writer)
                .param_("id", "Field ID")
                .return_("The identified field or null")
                .finish();
        writer.appendln("public static _Field findById(int id) {")
              .begin()
              .appendln("switch (id) {")
              .begin();
        for (JField field : message.declaredOrderFields()) {
            writer.formatln("case %d: return _Field.%s;", field.id(), field.fieldEnum());
        }
        writer.end()
              .appendln('}')
              .appendln("return null;")
              .end()
              .appendln('}')
              .newline();

        new BlockCommentBuilder(writer)
                .param_("name", "Field name")
                .return_("The named field or null")
                .finish();
        writer.appendln("public static _Field findByName(String name) {")
              .begin()
              .appendln("if (name == null) return null;")
              .appendln("switch (name) {")
              .begin();
        for (JField field : message.declaredOrderFields()) {
            writer.formatln("case \"%s\": return _Field.%s;", field.name(), field.fieldEnum());
        }
        writer.end()
              .appendln('}')
              .appendln("return null;")
              .end()
              .appendln('}')
              .newline();

        new BlockCommentBuilder(writer)
                .param_("name", "Field POJO name")
                .return_("The named field or null")
                .finish();
        writer.appendln("public static _Field findByPojoName(String name) {")
              .begin()
              .appendln("if (name == null) return null;")
              .appendln("switch (name) {")
              .begin();
        for (JField field : message.declaredOrderFields()) {
            if (!field.field().getName().equals(field.field().getPojoName())) {
                writer.formatln("case \"%s\": return _Field.%s;",
                                field.field().getPojoName(),
                                field.fieldEnum());
            }
            writer.formatln("case \"%s\": return _Field.%s;",
                            field.field().getName(),
                            field.fieldEnum());
        }
        writer.end()
              .appendln('}')
              .appendln("return null;")
              .end()
              .appendln('}')
              .newline();

        new BlockCommentBuilder(writer)
                .param_("id", "Field ID")
                .return_("The identified field")
                .throws_("IllegalArgumentException", "If no such field")
                .finish();
        writer.appendln("public static _Field fieldForId(int id) {")
              .begin()
              .appendln("_Field field = findById(id);")
              .appendln("if (field == null) {")
              .formatln("    throw new IllegalArgumentException(\"No such field id \" + id + \" in %s\");",
                        message.descriptor().getQualifiedName())
              .appendln("}")
              .appendln("return field;")
              .end()
              .appendln('}')
              .newline();

        new BlockCommentBuilder(writer)
                .param_("name", "Field name")
                .return_("The named field")
                .throws_("IllegalArgumentException", "If no such field")
                .finish();
        writer.appendln("public static _Field fieldForName(String name) {")
              .begin()
              .appendln("if (name == null) {")
              .appendln("    throw new IllegalArgumentException(\"Null name argument\");")
              .appendln("}")
              .appendln("_Field field = findByName(name);")
              .appendln("if (field == null) {")
              .formatln("    throw new IllegalArgumentException(\"No such field \\\"\" + name + \"\\\" in %s\");",
                        message.descriptor().getQualifiedName())
              .appendln("}")
              .appendln("return field;")
              .end()
              .appendln('}')
              .newline();

        new BlockCommentBuilder(writer)
                .param_("name", "Field POJO name")
                .return_("The named field")
                .throws_("IllegalArgumentException", "If no such field")
                .finish();
        writer.appendln("public static _Field fieldForPojoName(String name) {")
              .begin()
              .appendln("if (name == null) {")
              .appendln("    throw new IllegalArgumentException(\"Null name argument\");")
              .appendln("}")
              .appendln("_Field field = findByPojoName(name);")
              .appendln("if (field == null) {")
              .formatln("    throw new IllegalArgumentException(\"No such field \\\"\" + name + \"\\\" in %s\");",
                        message.descriptor().getQualifiedName())
              .appendln("}")
              .appendln("return field;")
              .end()
              .appendln('}');

        writer.end()
              .appendln('}')
              .newline();
    }

    private void appendJsonCompact(JMessage<?> message) {
        if (!message.jsonCompactible()) {
            return;
        }

        writer.appendln(JAnnotation.OVERRIDE)
              .appendln("public boolean jsonCompact() {")
              .begin();

        boolean hasCheck = false;
        boolean hasMissingVar = false;
        for (JField field : message.numericalOrderFields()) {
            if (!field.alwaysPresent()) {
                if (!hasMissingVar) {
                    writer.appendln("boolean missing = false;");
                }
                hasMissingVar = true;
                hasCheck = true;
                writer.formatln("if (%s()) {", field.presence())
                      .appendln("    if (missing) return false;")
                      .appendln("} else {")
                      .appendln("    missing = true;")
                      .appendln('}');
            } else if (hasCheck) {
                writer.appendln("if (missing) return false;");
                hasCheck = false;
            }
        }

        writer.appendln("return true;")
              .end()
              .appendln('}')
              .newline();
    }

    private void appendGetter(JMessage<?> message) {
        writer.appendln(JAnnotation.OVERRIDE)
              .appendln("@SuppressWarnings(\"unchecked\")")
              .appendln("public <T> T get(int key) {")
              .begin()
              .appendln("switch(key) {")
              .begin();

        for (JField field : message.numericalOrderFields()) {
            if (field.isVoid()) {
                // Void fields have no value.
                writer.formatln("case %d: return %s() ? (T) Boolean.TRUE : null;",
                                field.id(), field.presence());
            } else if (field.isPrimitiveJavaValue()) {
                if (field.alwaysPresent()) {
                    writer.formatln("case %d: return (T) (%s) %s;",
                                    field.id(), field.instanceType(), field.member());
                } else {
                    writer.formatln("case %d: return (T) %s;",
                                    field.id(), field.member());
                }
            } else {
                writer.formatln("case %d: return (T) %s;",
                                field.id(), field.member());
            }
        }

        writer.appendln("default: return null;")
              .end()
              .appendln('}')
              .end()
              .appendln('}')
              .newline();
    }

    private void appendPresence(JMessage<?> message) {
        writer.appendln(JAnnotation.OVERRIDE)
              .appendln("public boolean has(int key) {")
              .begin()
              .appendln("switch(key) {")
              .begin();

        if (message.isUnion()) {
            for (JField field : message.numericalOrderFields()) {
                writer.formatln("case %s: return %s == _Field.%s;",
                                field.id(), UNION_FIELD, field.fieldEnum());
            }
        } else {
            for (JField field : message.numericalOrderFields()) {
                if (field.alwaysPresent()) {
                    writer.formatln("case %d: return true;", field.id());
                } else {
                    writer.formatln("case %d: return %s != null;", field.id(), field.member());
                }
            }
        }

        writer.appendln("default: return false;")
              .end()
              .appendln('}')
              .end()
              .appendln('}')
              .newline();
    }
}
