/*
 * Copyright 2016 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.generator.format.java;

import net.morimekta.providence.PApplicationException;
import net.morimekta.providence.PApplicationExceptionType;
import net.morimekta.providence.PClient;
import net.morimekta.providence.PMessage;
import net.morimekta.providence.PProcessor;
import net.morimekta.providence.PServiceCall;
import net.morimekta.providence.PServiceCallHandler;
import net.morimekta.providence.PServiceCallType;
import net.morimekta.providence.descriptor.PRequirement;
import net.morimekta.providence.descriptor.PService;
import net.morimekta.providence.descriptor.PServiceMethod;
import net.morimekta.providence.descriptor.PServiceProvider;
import net.morimekta.providence.descriptor.PStructDescriptor;
import net.morimekta.providence.descriptor.PUnionDescriptor;
import net.morimekta.providence.generator.GeneratorException;
import net.morimekta.providence.generator.GeneratorOptions;
import net.morimekta.providence.generator.format.java.messages.BaseInterfaceFormatter;
import net.morimekta.providence.generator.format.java.shared.BaseServiceFormatter;
import net.morimekta.providence.generator.format.java.utils.BlockCommentBuilder;
import net.morimekta.providence.generator.format.java.utils.JAnnotation;
import net.morimekta.providence.generator.format.java.utils.JField;
import net.morimekta.providence.generator.format.java.utils.JHelper;
import net.morimekta.providence.generator.format.java.utils.JMessage;
import net.morimekta.providence.generator.format.java.utils.JService;
import net.morimekta.providence.generator.format.java.utils.JServiceMethod;
import net.morimekta.providence.reflect.contained.CService;
import net.morimekta.util.io.IndentedPrintWriter;

import javax.annotation.Generated;
import java.io.IOException;
import java.time.Clock;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.function.BiFunction;

import static net.morimekta.util.Strings.times;

public class JavaServiceFormatter implements BaseServiceFormatter {
    private final JHelper                helper;
    private final BaseInterfaceFormatter interfaceFormat;
    private final JavaMessageFormatter   messageFormat;
    private final IndentedPrintWriter    writer;
    private final JavaOptions            javaOptions;
    private final GeneratorOptions       generatorOptions;

    JavaServiceFormatter(IndentedPrintWriter writer,
                         JHelper helper,
                         BaseInterfaceFormatter interfaceFormat,
                         JavaMessageFormatter messageFormat,
                         GeneratorOptions generatorOptions,
                         JavaOptions javaOptions) {
        this.writer = writer;
        this.helper = helper;
        this.interfaceFormat = interfaceFormat;
        this.messageFormat = messageFormat;
        this.generatorOptions = generatorOptions;
        this.javaOptions = javaOptions;
    }

    @Override
    public void appendServiceClass(CService cs) throws GeneratorException {
        JService service = new JService(cs, helper);

        if (cs.getDocumentation() != null) {
            new BlockCommentBuilder(writer)
                    .comment(cs.getDocumentation())
                    .finish();
        }

        String inherits = "";
        if (service.getService().getExtendsService() != null) {
            CService other = service.getService().getExtendsService();
            inherits = "extends " + helper.getJavaPackage(other) + "." +
                       new JService(other, helper).className() + " ";
        }

        writer.appendln("@SuppressWarnings(\"unused\")");
        writer.formatln("@%s(", Generated.class.getName())
              .begin("        ")
              .formatln("value = \"net.morimekta.providence:providence-generator-java%s\",",
                        javaOptions.generated_annotation_version ? ":" + generatorOptions.program_version : "");
        if (javaOptions.generated_annotation_date) {
            writer.formatln("date = \"%s\",",
                            DateTimeFormatter.ISO_DATE_TIME.format(ZonedDateTime.now(Clock.systemUTC())
                                                                                .withNano(0)));
        }
        writer.formatln("comments = \"%s\")",
                        javaOptions.toString())
              .end();

        writer.formatln("public class %s %s{", service.className(), inherits)
              .begin();

        appendIface(writer, service);

        appendClient(writer, service);

        appendProcessor(writer, service);

        appendDescriptor(writer, service);

        appendStructs(writer, service);

        // protected constructor should defeat instantiation, but can inherit
        // from parent to be able to get access to inner protected classes.
        writer.formatln("protected %s() {}", service.className());

        writer.end()
              .appendln('}')
              .newline();
    }

    private void appendClient(IndentedPrintWriter writer, JService service) throws GeneratorException {
        BlockCommentBuilder comment = new BlockCommentBuilder(writer);
        if (service.getService()
                   .getDocumentation() != null) {
            comment.comment(service.getService().getDocumentation())
                   .newline();
        }
        comment.comment("Client implementation for " + service.getService().getQualifiedName())
               .finish();

        writer.appendln("public static final class Client")
              .formatln("        extends %s", PClient.class.getName())
              .formatln("        implements Iface {")
              .begin();

        writer.formatln("private final %s handler;", PServiceCallHandler.class.getName())
              .newline();

        new BlockCommentBuilder(writer)
                .comment("Create " + service.getService().getQualifiedName() + " service client.")
                .newline()
                .param_("handler", "The client handler.")
                .finish();

        writer.formatln("public Client(%s handler) {", PServiceCallHandler.class.getName())
              .appendln("    this.handler = handler;")
              .appendln('}')
              .newline();

        boolean firstMethod = true;
        for (JServiceMethod method : service.methods()) {
            if (firstMethod) {
                firstMethod = false;
            } else {
                writer.newline();
            }

            writer.appendln("@Override");

            // Field ID 0 is the return type.
            JField ret = method.getResponse();
            if (ret != null && !ret.isPrimitiveJavaValue()) {
                writer.appendln(JAnnotation.NON_NULL);
            }

            writer.appendln("public ");
            if (ret != null) {
                writer.append(ret.valueType());
            } else {
                writer.append("void");
            }

            writer.format(" %s(", method.methodName())
                  .begin("        ");

            if (method.getMethod().isProtoStub()) {
                writer.formatln("%s %s", method.getRequestClass(), "request");
            } else {
                boolean first = true;
                for (JField param : method.params()) {
                    if (first) {
                        first = false;
                    } else {
                        writer.append(",");
                    }
                    writer.formatln("%s %s", param.fieldType(), param.param());
                }
            }

            writer.end()
                  .format(")")
                  .formatln("        throws %s", IOException.class.getName())
                  .begin(   "               ");

            for (JField ex : method.exceptions()) {
                writer.append(",");
                writer.appendln(ex.instanceType());
            }

            writer.format(" {")
                  .end()
                  .begin();

            String request = "rq.build()";
            if (method.getMethod().isProtoStub()) {
                request = "request";
            } else {
                writer.formatln("%s._Builder rq = %s.builder();",
                                method.getRequestClass(),
                                method.getRequestClass());

                for (JField param : method.params()) {
                    if (!param.alwaysPresent()) {
                        writer.formatln("if (%s != null) {", param.param())
                              .begin();
                    }
                    writer.formatln("rq.%s(%s);", param.setter(), param.param());
                    if (!param.alwaysPresent()) {
                        writer.end()
                              .appendln("}");
                    }
                }
            }

            String type = method.getMethod().isOneway()
                          ? PServiceCallType.ONEWAY.name()
                          : PServiceCallType.CALL.name();
            writer.newline()
                  .formatln("%s<%s> call = new %s<>(\"%s\", %s.%s, getNextSequenceId(), %s);",
                            PServiceCall.class.getName(),
                            method.getRequestClass(),
                            PServiceCall.class.getName(),
                            method.name(),
                            PServiceCallType.class.getName(),
                            type,
                            request)
                  .appendln();

            if (method.getResponseClass() != null) {
                writer.format("%s resp = ",
                              PServiceCall.class.getName());
            }

            writer.format("handler.handleCall(call, %s.kDescriptor);", service.className());

            if (method.getResponseClass() != null) {
                writer.newline()
                      .formatln("if (resp.getType() == %s.%s) {", PServiceCallType.class.getName(), PServiceCallType.EXCEPTION.name())
                      .formatln("    throw (%s) resp.getMessage();",
                                PApplicationException.class.getName())
                      .appendln('}')
                      .newline()
                      .formatln("%s msg = (%s) resp.getMessage();",
                                method.getResponseClass(),
                                method.getResponseClass());

                writer.appendln("if (msg.unionFieldIsSet()) {")
                      .begin()
                      .appendln("switch (msg.unionField()) {")
                      .begin();
                if (method.exceptions().length > 0) {
                    for (JField ex : method.exceptions()) {
                        writer.formatln("case %s:", ex.fieldEnum())
                              .formatln("    throw msg.%s();", ex.getter());
                    }
                }
                if (method.getResponse() != null) {
                    writer.formatln("case %s:", method.getResponse().fieldEnum());
                    if (method.getResponse().isVoid()) {
                        writer.formatln("    return;");
                    } else {
                        writer.formatln("    return msg.%s();", method.getResponse().getter());
                    }
                }

                writer.end()
                      .appendln("}")  // switch
                      .end()
                      .appendln("}")  // if unionFieldIsSet
                      .newline();

                // In case there is no return value, and no exception,
                // the union field is not set. This *should* cause an error.
                writer.formatln("throw new %s(\"Result field for %s.%s() not set\",",
                                PApplicationException.class.getName(),
                                service.getService().getQualifiedName(),
                                method.name())
                      .formatln("          %s %s.%s);",
                                times(" ", PApplicationException.class.getName().length()),
                                PApplicationExceptionType.class.getName(),
                                PApplicationExceptionType.MISSING_RESULT.name());
            }

            writer.end()
                  .appendln('}');  // method wrapper
        }

        writer.end()
              .appendln('}')  // Client
              .newline();
    }

    private void appendProcessor(IndentedPrintWriter writer, JService service) throws GeneratorException {
        writer.formatln("public static final class Processor implements %s {", PProcessor.class.getName())
              .begin()
              .appendln("private final Iface impl;")
              .formatln("private final %s<%s, Exception, Exception> exceptionTransformer;",
                        BiFunction.class.getName(),
                        PServiceMethod.class.getName());

        writer.formatln("public Processor(%s Iface impl) {", JAnnotation.NON_NULL)
              .appendln("    this(impl, (m, e) -> e);")
              .appendln('}')
              .newline();

        writer.formatln("public Processor(%s Iface impl,",
                        JAnnotation.NON_NULL)
              .formatln("                 %s %s<%s, Exception, Exception> exceptionTransformer) {",
                        JAnnotation.NON_NULL, BiFunction.class.getName(), PServiceMethod.class.getName())
              .appendln("    this.impl = impl;")
              .appendln("    this.exceptionTransformer = exceptionTransformer;")
              .appendln('}')
              .newline();

        writer.appendln("@Override")
              .appendln(JAnnotation.NON_NULL)
              .formatln("public %s getDescriptor() {", PService.class.getName())
              .appendln("    return kDescriptor;")
              .appendln('}')
              .newline();

        writer.appendln("@Override")
              .appendln("@SuppressWarnings(\"unchecked\")")
              .formatln("public <Request extends %s<Request>,", PMessage.class.getName())
              .formatln("        Response extends %s<Response>>", PMessage.class.getName())
              .formatln("%s<Response> handleCall(", PServiceCall.class.getName())
              .formatln("        %s<Request> call,", PServiceCall.class.getName())
              .formatln("        %s service)", PService.class.getName())
              .formatln("        throws %s {", PApplicationException.class.getName())
              .begin();

        writer.formatln("%s ex = null;", PApplicationException.class.getName());
        writer.appendln("switch(call.getMethod()) {")
              .begin();

        for (JServiceMethod method : service.methods()) {
            writer.formatln("case \"%s\": {", method.name())
                  .begin();
            if (method.getResponseClass() != null) {
                writer.formatln("%s._Builder rsp = %s.builder();",
                                method.getResponseClass(),
                                method.getResponseClass());
            }

            writer.appendln("try {")
                  .begin();

            writer.formatln("%s req = (%s) call.getMessage();",
                            method.getRequestClass(),
                            method.getRequestClass());

            writer.appendln("try {")
                  .begin();

            String indent = "      " + times(" ", method.methodName().length());
            if (method.getResponse() != null && !method.getResponse().isVoid()) {
                writer.formatln("%s result =", method.getResponse().valueType());
                writer.appendln("        ");
                indent += "        ";
            } else {
                writer.appendln();
            }

            writer.format("impl.%s(", method.methodName())
                  .begin(indent);

            boolean first = true;
            if (method.getMethod().isProtoStub()) {
                writer.print("req");
            } else {
                for (JField param : method.params()) {
                    if (first) {
                        first = false;
                    } else {
                        writer.append(',')
                              .appendln();
                    }
                    // An optional primitive value without an explicit default
                    // should be called with null value if not present in the
                    // request.
                    boolean optionalPrimitiveNoDefault =
                            param.isPrimitiveJavaValue() &&
                            param.field().getRequirement() == PRequirement.OPTIONAL &&
                            param.field().getDefaultValue() == null;
                    if (optionalPrimitiveNoDefault) {
                        writer.format("req.%s() ? ", param.presence());
                    }
                    writer.format("req.%s()", param.getter());
                    if (optionalPrimitiveNoDefault) {
                        writer.append(" : null");
                    }
                }
            }
            writer.end()
                  .append(");");

            if (method.getResponse() != null &&
                !method.getResponse().isVoid() &&
                !method.getResponse().isPrimitiveJavaValue()) {
                writer.appendln("if (result == null) {")
                      .begin()
                      .formatln("ex = new %s(", PApplicationException.class.getName())
                      .formatln("        \"Returning null value from %s in %s\",",
                                method.name(),
                                service.getService().getQualifiedName())
                      .formatln("        %s.%s);",
                                PApplicationExceptionType.class.getName(),
                                PApplicationExceptionType.MISSING_RESULT.toString())
                      .appendln("break;")
                      .end()
                      .appendln("}");
            }

            if (method.getResponse() != null) {
                if (method.getResponse().isVoid()) {
                    writer.formatln("rsp.%s();", method.getResponse().setter());
                } else {
                    writer.formatln("rsp.%s(result);", method.getResponse().setter());
                }
            }

            writer.end();
            if (method.exceptions().length > 0) {
                writer.appendln("} catch (")
                      .begin(   "         ");
                boolean firstEx = true;
                for (JField ex : method.exceptions()) {
                    if (firstEx) {
                        firstEx = false;
                    } else {
                        writer.append(" |").appendln();
                    }
                    writer.append(ex.instanceType());
                }
                writer.end()
                      .append(" e) {")
                      .appendln("    throw e;");
            }

            writer.appendln("} catch (Exception e) {")
                  .formatln("    throw exceptionTransformer.apply(Method.%s, e);",
                            method.constant())
                  .appendln("}");

            writer.end()
                  .formatln("} catch (%s e) {", PApplicationException.class.getName())
                  .begin();
            if (method.getResponse() != null) {
                writer.formatln("ex = new %s(", PApplicationException.class.getName())
                      .formatln(
                              "        \"Wrapped application error \" + e.getType() + \" in method %s in %s: \" + e.getMessage(),",
                              method.name(),
                              service.getService().getQualifiedName())
                      .formatln("        %s.%s).initCause(e);",
                                PApplicationExceptionType.class.getName(),
                                PApplicationExceptionType.INTERNAL_ERROR.toString())
                      .appendln("break;");
            } else {
                writer.formatln("throw new %s(", PApplicationException.class.getName())
                      .formatln(
                              "        \"Wrapped application error \" + e.getType() + \" in method %s in %s: \" + e.getMessage(),",
                              method.name(),
                              service.getService().getQualifiedName())
                      .formatln("        %s.%s).initCause(e);",
                                PApplicationExceptionType.class.getName(),
                                PApplicationExceptionType.INTERNAL_ERROR.toString());
            }
            writer.end();

            for (JField ex : method.exceptions()) {
                writer.formatln("} catch (%s e) {", ex.instanceType())
                      .formatln("    rsp.%s(e);", ex.setter());
            }

            writer.formatln("} catch (Exception e) {")
                  .begin();
            if (method.getResponse() != null) {
                writer.formatln("ex = new %s(", PApplicationException.class.getName())
                      .formatln("        \"Unhandled \" + e.getClass().getSimpleName() + " +
                                "\" in method %s in %s: \" + e.getMessage(),",
                                method.name(),
                                service.getService().getQualifiedName())
                      .formatln("        %s.%s).initCause(e);",
                                PApplicationExceptionType.class.getName(),
                                PApplicationExceptionType.INTERNAL_ERROR.toString())
                      .appendln("break;");
            } else {
                writer.formatln("throw new %s(", PApplicationException.class.getName())
                      .formatln("        \"Unhandled \" + e.getClass().getSimpleName() + " +
                                "\" in method %s in %s: \" + e.getMessage(),",
                                method.name(),
                                service.getService().getQualifiedName())
                      .formatln("        %s.%s).initCause(e);",
                                PApplicationExceptionType.class.getName(),
                                PApplicationExceptionType.INTERNAL_ERROR.toString());
            }
            writer.end();

            writer.appendln('}');

            if (method.getResponseClass() != null) {
                String spaces = PServiceCall.class.getName().replaceAll("[\\S]", " ");
                writer.formatln("%s reply =", PServiceCall.class.getName())
                      .formatln("        new %s<>(call.getMethod(),",
                                PServiceCall.class.getName())
                      .formatln("            %s   %s.%s,",
                                spaces,
                                PServiceCallType.class.getName(),
                                PServiceCallType.REPLY.name())
                      .formatln("            %s   call.getSequence(),",
                                spaces)
                      .formatln("            %s   rsp.build());",
                                spaces)
                      .appendln("return reply;");
            } else {
                // No reply, but it's fine.
                writer.appendln("return null;");
            }

            writer.end()
                  .appendln('}');
        }

        writer.appendln("default: {")
              .begin()
              .formatln("ex = new %s(", PApplicationException.class.getName())
              .formatln("        \"Unknown method \\\"\" + call.getMethod() + \"\\\" on %s.\",",
                        service.getService().getQualifiedName())
              .formatln("        %s.%s);",
                        PApplicationExceptionType.class.getName(),
                        PApplicationExceptionType.UNKNOWN_METHOD.asString());

        writer.appendln("break;")
              .end()
              .appendln('}') // default
              .end()
              .appendln('}');  // switch

        String spaces = PServiceCall.class.getName().replaceAll("[\\S]", " ");
        writer.formatln("%s reply =", PServiceCall.class.getName())
              .formatln("        new %s<>(call.getMethod(),",
                        PServiceCall.class.getName())
              .formatln("            %s %s.%s,",
                        spaces,
                        PServiceCallType.class.getName(),
                        PServiceCallType.EXCEPTION.name())
              .formatln("            %s call.getSequence(),",
                        spaces)
              .formatln("            %s ex);",
                        spaces)
              .appendln("return reply;");
        writer.end()
              .appendln('}');  // handleCall

        writer.end()
              .appendln('}')  // processor
              .newline();
    }

    private void appendDescriptor(IndentedPrintWriter writer, JService service) throws GeneratorException {
        writer.formatln("public enum Method implements %s {", PServiceMethod.class.getName())
              .begin();

        for (JServiceMethod method : service.methods()) {
            String responseDesc = method.getResponseClass() == null
                                  ? "null"
                                  : method.getResponseClass() + ".kDescriptor";

            writer.formatln("%s(\"%s\", %b, %b, %s.kDescriptor, %s),",
                            method.constant(),
                            method.name(),
                            method.getMethod().isOneway(),
                            method.getMethod().isProtoStub(),
                            method.getRequestClass(),
                            responseDesc);
        }

        writer.appendln(';')
              .newline();

        writer.appendln("private final String name;")
              .appendln("private final boolean oneway;")
              .appendln("private final boolean protoStub;")
              .formatln("private final %s request;", PStructDescriptor.class.getName())
              .formatln("private final %s response;", PUnionDescriptor.class.getName())
              .newline();

        writer.formatln("private Method(String name, boolean oneway, boolean protoStub, %s request, %s response) {",
                        PStructDescriptor.class.getName(), PUnionDescriptor.class.getName())
              .appendln("    this.name = name;")
              .appendln("    this.oneway = oneway;")
              .appendln("    this.protoStub = protoStub;")
              .appendln("    this.request = request;")
              .appendln("    this.response = response;")
              .appendln('}')
              .newline();

        writer.appendln(JAnnotation.NON_NULL)
              .appendln(JAnnotation.OVERRIDE)
              .appendln("public String getName() {")
              .appendln("    return name;")
              .appendln('}')
              .newline()
              .appendln("public boolean isOneway() {")
              .appendln("    return oneway;")
              .appendln('}')
              .newline()
              .appendln("public boolean isProtoStub() {")
              .appendln("    return protoStub;")
              .appendln('}')
              .newline()
              .appendln(JAnnotation.NON_NULL)
              .appendln(JAnnotation.OVERRIDE)
              .formatln("public %s getRequestType() {",
                        PStructDescriptor.class.getName())
              .formatln("    return request;")
              .appendln('}')
              .newline()
              .appendln(JAnnotation.NULLABLE)
              .appendln(JAnnotation.OVERRIDE)
              .formatln("public %s getResponseType() {",
                        PUnionDescriptor.class.getName())
              .formatln("    return response;")
              .appendln('}')
              .newline()
              .appendln(JAnnotation.NON_NULL)
              .appendln(JAnnotation.OVERRIDE)
              .formatln("public %s getService() {", PService.class.getName())
              .formatln("    return kDescriptor;")
              .appendln('}')
              .newline();

        writer.appendln(JAnnotation.NULLABLE)
              .appendln("public static Method findByName(String name) {")
              .begin()
              .appendln("if (name == null) return null;")
              .appendln("switch (name) {")
              .begin();

        for (JServiceMethod method : service.methods()) {
            writer.formatln("case \"%s\": return %s;",
                            method.name(), method.constant());
        }

        writer.end()
              .appendln('}')
              .appendln("return null;")
              .end()
              .appendln('}');

        writer.appendln(JAnnotation.NON_NULL)
              .formatln("public static Method methodForName(%s String name) {", JAnnotation.NON_NULL)
              .begin()
              .appendln("Method method = findByName(name);")
              .appendln("if (method == null) {")
              .formatln("    throw new IllegalArgumentException(\"No such method \\\"\" + name + \"\\\" in service %s\");",
                        service.getService().getQualifiedName())
              .appendln("}")
              .appendln("return method;")
              .end()
              .appendln('}');

        writer.end()
              .appendln('}')
              .newline();
        // Ended methods enum.

        String inherits = "null";
        if (service.getService().getExtendsService() != null) {
            CService other = service.getService().getExtendsService();
            inherits = helper.getJavaPackage(other) + "." +
                       new JService(other, helper).className() + ".provider()";
        }

        writer.formatln("private static final class _Descriptor extends %s {",
                        PService.class.getName())
              .begin()
              .appendln("private _Descriptor() {")
              .formatln("    super(\"%s\", \"%s\", %s, Method.values());",
                        service.getService().getProgramName(),
                        service.getService().getName(),
                        inherits)
              .appendln('}')
              .newline()
              .appendln("@Override")
              .appendln("public Method getMethod(String name) {")
              .appendln("    return Method.findByName(name);")
              .appendln("}")
              .end()
              .appendln('}')
              .newline();

        writer.formatln("private static final class _Provider implements %s {",
                        PServiceProvider.class.getName())
              .begin()
              .appendln("@Override")
              .appendln(JAnnotation.NON_NULL)
              .formatln("public %s getService() {", PService.class.getName())
              .appendln("    return kDescriptor;")
              .appendln("}")
              .end()
              .appendln('}')
              .newline();

        writer.formatln("public static final %s kDescriptor = new _Descriptor();",
                        PService.class.getName())
              .newline();

        writer.formatln("public static %s provider() {",
                        PServiceProvider.class.getName())
              .appendln("    return new _Provider();")
              .appendln('}')
              .newline();
    }

    @SuppressWarnings("unchecked")
    private void appendStructs(IndentedPrintWriter writer, JService service) throws GeneratorException {
        for (JServiceMethod method : service.declaredMethods()) {
            if (!method.getMethod().isProtoStub() &&
                method.getMethod().getRequestType().isInnerType() &&
                method.getMethod().getRequestType().isAutoType()) {
                JMessage request = new JMessage<>(method.getMethod().getRequestType(), helper);
                writer.formatln("// type --> %s", request.descriptor().getName());

                interfaceFormat.appendInterface(method.getMethod().getRequestType());
                messageFormat.appendMessageClass(method.getMethod().getRequestType());
            }

            if (method.getMethod().getResponseType() != null &&
                method.getMethod().getResponseType().isInnerType() &&
                method.getMethod().getResponseType().isAutoType()) {
                JMessage response = new JMessage(method.getMethod().getResponseType(), helper);
                writer.formatln("// type <-- %s", response.descriptor().getName());

                interfaceFormat.appendInterface(method.getMethod().getResponseType());
                messageFormat.appendMessageClass(method.getMethod().getResponseType());
            }
        }
    }

    private void appendIface(IndentedPrintWriter writer, JService service) throws GeneratorException {
        String inherits = "";
        if (service.getService().getExtendsService() != null) {
            CService other = service.getService().getExtendsService();
            inherits = "extends " + helper.getJavaPackage(other) + "." +
                       new JService(other, helper).className() + ".Iface ";
        }

        if (service.getService().getDocumentation() != null) {
            new BlockCommentBuilder(writer)
                    .comment(service.getService().getDocumentation())
                    .finish();
        }

        writer.formatln("public interface Iface %s{", inherits)
              .begin();

        boolean firstMethod = true;
        for (JServiceMethod method : service.declaredMethods()) {
            if (firstMethod) {
                firstMethod = false;
            } else {
                writer.newline();
            }
            String methodThrows = service.methodsThrows(method);

            BlockCommentBuilder comment = new BlockCommentBuilder(writer);

            if (method.getMethod().getDocumentation() != null) {
                comment.comment(method.getMethod().getDocumentation())
                       .newline();
            }

            if (method.getMethod().isProtoStub()) {
                comment.param_("request", "The request message");
            } else {
                for (JField param : method.params()) {
                    if (param.comment() != null) {
                        comment.param_(param.param(), param.comment());
                    } else {
                        comment.param_(param.param(), "The " + param.name() + " value.");
                    }
                }
            }

            if (method.getResponse() != null && !method.getResponse().isVoid()) {
                comment.return_("The " + method.name() + " result.");
            }

            if (methodThrows != null) {
                comment.throws_(methodThrows, "On any declared exception.");
            } else {
                for (JField param : method.exceptions()) {
                    if (param.comment() != null) {
                        comment.throws_(param.fieldType(), param.comment());
                    } else {
                        comment.throws_(param.fieldType(), "The " + param.name() + " exception.");
                    }
                }
            }
            comment.throws_(IOException.class, "On providence or non-declared exceptions.")
                   .finish();

            JField ret = method.getResponse();
            if (ret != null) {
                if (!ret.isPrimitiveJavaValue()) {
                    writer.appendln(JAnnotation.NON_NULL);
                }
                writer.appendln(ret.valueType());
            } else {
                writer.appendln("void");
            }

            writer.format(" %s(", method.methodName())
                  .begin("        ");

            if (method.getMethod().isProtoStub()) {
                writer.formatln("%s %s %s", JAnnotation.NON_NULL, method.getRequestClass(), "request");
            } else {
                boolean first = true;
                for (JField param : method.params()) {
                    if (first) {
                        first = false;
                    } else {
                        writer.append(",");
                    }
                    writer.formatln("%s %s", param.fieldType(), param.param());
                }
            }

            writer.end()
                  .format(")");
            writer.formatln("        throws %s", IOException.class.getName())
                  .begin("               ");
            if (methodThrows != null) {
                writer.append(",");
                writer.formatln("%s", methodThrows);
            } else {
                for (JField ex : method.exceptions()) {
                    writer.append(",");
                    writer.appendln(ex.instanceType());
                }
            }
            writer.format(";")
                  .end();
        }

        writer.end()
              .appendln('}')
              .newline();
    }
}
