/*
 * Copyright 2016 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.generator.format.java.messages.extras;

import net.morimekta.providence.descriptor.PAnnotation;
import net.morimekta.providence.descriptor.PEnumDescriptor;
import net.morimekta.providence.generator.GeneratorException;
import net.morimekta.providence.generator.format.java.shared.MessageMemberFormatter;
import net.morimekta.providence.generator.format.java.utils.BlockCommentBuilder;
import net.morimekta.providence.generator.format.java.utils.JAnnotation;
import net.morimekta.providence.generator.format.java.utils.JField;
import net.morimekta.providence.generator.format.java.utils.JHelper;
import net.morimekta.providence.generator.format.java.utils.JMessage;
import net.morimekta.providence.generator.format.java.utils.JUtils;
import net.morimekta.providence.reflect.contained.CInterface;
import net.morimekta.providence.reflect.contained.CInterfaceDescriptor;
import net.morimekta.providence.reflect.contained.CMessageDescriptor;
import net.morimekta.util.io.IndentedPrintWriter;

import java.util.Optional;

/**
 * This class annotates specifically the providence.Any type with two utility
 * methods:
 *
 * - Message unwrapMessage(descriptor);
 * - static Any wrapMessage(message);
 */
public class UnionOfFormatter implements MessageMemberFormatter {
    private final IndentedPrintWriter writer;
    private final JHelper             helper;

    public UnionOfFormatter(IndentedPrintWriter writer, JHelper helper) {
        this.writer = writer;
        this.helper = helper;
    }

    @Override
    public void appendMethods(JMessage<?> message) throws GeneratorException {
        if (!message.isUnion()) return;

        CMessageDescriptor   cmd          = (CMessageDescriptor) message.descriptor();
        CInterfaceDescriptor implementing;
        try {
            implementing = cmd.getImplementing();
        } catch (Exception e) {
            throw new GeneratorException("Implementing class for " + message.descriptor().getQualifiedName() +
                                         " does not exist: " + e.getMessage(), e);
        }

        if (implementing != null) {
            JMessage<CInterface> impl = new JMessage<>(implementing, helper);

            // asImplName
            String implName = JUtils.getClassName(implementing);
            String implType = helper.getFieldType(implementing);

            BlockCommentBuilder comment = new BlockCommentBuilder(writer);
            comment.return_("The union field as implemented type.");
            comment.finish();
            writer.formatln("public %s as%s() {", implType, implName)
                  .begin()
                  .appendln("switch (unionField()) {")
                  .begin();

            for (JField field : message.numericalOrderFields()) {
                writer.formatln("case %s: return %s();",
                                field.fieldEnum(),
                                field.getter());
            }

            writer.appendln("default: throw new IllegalStateException(\"Impossible\");")
                  .end()
                  .appendln("}")
                  .end()
                  .appendln("}");
            for (JField field : impl.declaredOrderFields()) {
                writer.newline();

                boolean isDeprecated     = false;
                String  deprecatedReason;
                {
                    BlockCommentBuilder getComment = new BlockCommentBuilder(writer);
                    if (field.hasComment()) {
                        getComment.comment(field.comment());
                        getComment.newline();
                    }

                    getComment.return_("The " + field.name() + " value.");
                    deprecatedReason = field.field().getAnnotationValue(PAnnotation.DEPRECATED);
                    if (deprecatedReason != null) {
                        if (deprecatedReason.trim().length() > 0) {
                            getComment.deprecated_(deprecatedReason);
                        } else {
                            deprecatedReason = null;
                        }
                        isDeprecated = true;
                    }
                    getComment.finish();
                }

                if (isDeprecated) {
                    writer.appendln(JAnnotation.DEPRECATED);
                }
                if (field.alwaysPresent() && !field.isPrimitiveJavaValue()) {
                    writer.appendln(JAnnotation.NON_NULL);
                }
                writer.formatln("public %s %s() {",
                                field.valueType(),
                                field.getter())
                      .formatln("    return as%s().%s();",
                                implName, field.getter())
                      .appendln("}");
                writer.newline();

                PEnumDescriptor enumType = field.refEnum(message.descriptor(), helper);
                if (enumType != null) {
                    comment = new BlockCommentBuilder(writer);
                    if (field.hasComment()) {
                        comment.comment(field.comment())
                               .newline();
                    }
                    comment.return_("The <code>" + enumType.getName() + "</code> ref for the " +
                                    "<code>" + field.name() + "</code> field, if it has a known value.");
                    if (JAnnotation.isDeprecated(field)) {
                        String reason = field.field().getAnnotationValue(PAnnotation.DEPRECATED);
                        if (reason != null && reason.trim().length() > 0) {
                            comment.deprecated_(reason);
                        }
                    }
                    comment.finish();

                    if (JAnnotation.isDeprecated(message.descriptor())) {
                        writer.appendln(JAnnotation.DEPRECATED);
                    }
                    writer.appendln(JAnnotation.NULLABLE);
                    writer.formatln("public %s %s() {", helper.getValueType(enumType), field.ref())
                          .formatln("    return as%s().%s();", implName, field.ref())
                          .appendln("}")
                          .newline();
                }

                if (!field.alwaysPresent()) {
                    BlockCommentBuilder getComment = new BlockCommentBuilder(writer);
                    if (field.hasComment()) {
                        getComment.comment(field.comment());
                        getComment.newline();
                    }

                    getComment.return_("Optional " + field.name() + " value.");
                    deprecatedReason = field.field().getAnnotationValue(PAnnotation.DEPRECATED);
                    if (deprecatedReason != null) {
                        if (deprecatedReason.trim().length() > 0) {
                            getComment.deprecated_(deprecatedReason);
                        } else {
                            deprecatedReason = null;
                        }
                        isDeprecated = true;
                    }
                    getComment.finish();
                    writer.appendln(JAnnotation.NON_NULL);
                    writer.formatln("public %s<%s> %s() {",
                                    Optional.class.getName(),
                                    field.fieldType(),
                                    field.optional())
                          .formatln("    return as%s().%s();",
                                    implName, field.optional())
                          .appendln("}");
                    writer.newline();
                }

                {
                    BlockCommentBuilder hasComment = new BlockCommentBuilder(writer);
                    hasComment.return_("If " + field.name() + " is present.");
                    if (deprecatedReason != null) {
                        hasComment.deprecated_(deprecatedReason);
                    }
                    hasComment.finish();
                }

                if (isDeprecated) {
                    writer.appendln(JAnnotation.DEPRECATED);
                }
                writer.formatln("public boolean %s() {", field.presence())
                      .formatln("    return as%s().%s();", implName, field.presence())
                      .appendln("}");

                if (field.container()) {
                    writer.newline();

                    {
                        BlockCommentBuilder numComment = new BlockCommentBuilder(writer);
                        numComment.return_("Number of entries in " + field.name() + ".");
                        if (deprecatedReason != null) {
                            numComment.deprecated_(deprecatedReason);
                        }
                        numComment.finish();
                    }
                    if (isDeprecated) {
                        writer.appendln(JAnnotation.DEPRECATED);
                    }
                    writer.formatln("public int %s() {", field.counter())
                          .formatln("    return as%s().%s();", implName, field.counter())
                          .appendln("}");
                }
            }
            writer.newline();
        }
    }
}
