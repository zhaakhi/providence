package net.morimekta.providence.generator.format.java;

import net.morimekta.providence.generator.Generator;
import net.morimekta.providence.generator.GeneratorException;
import net.morimekta.providence.generator.GeneratorOptions;
import net.morimekta.providence.generator.util.FileManager;
import net.morimekta.providence.reflect.ProgramLoader;
import net.morimekta.providence.reflect.contained.CProgram;
import net.morimekta.providence.reflect.GlobalRegistry;
import net.morimekta.providence.reflect.ProgramRegistry;
import net.morimekta.util.collect.UnmodifiableList;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static net.morimekta.testing.ResourceUtils.copyResourceTo;

/**
 * Note: This test mainly just runs the same test generation that is also
 * done in /providence-testing. Testing of the output is done there, this
 * just assures no exception is thrown, and generates correct coverage.
 */
public class JavaGeneratorTest {
    @Rule
    public  TemporaryFolder tmp = new TemporaryFolder();

    private GeneratorOptions    generatorOptions;
    private FileManager         fileManager;
    private GlobalRegistry      globalRegistry;
    private ArrayList<CProgram> programs;
    private ProgramLoader       programLoader;

    @Before
    public void setUp() throws IOException {
        File out = tmp.newFolder("out");

        generatorOptions = new GeneratorOptions();
        generatorOptions.program_version = "0.1-SNAPSHOT";
        generatorOptions.generator_program_name = "providence-generator-java/test";

        fileManager = new FileManager(out.toPath());
        programLoader = new ProgramLoader();
        globalRegistry = programLoader.getGlobalRegistry();
        programs = new ArrayList<>();
    }

    private void defaultSources() throws IOException {
        File src = tmp.newFolder("src");
        for (String res : UnmodifiableList.listOf(
                "/providence/number.thrift",
                "/providence/calculator.thrift",
                "/providence/providence.thrift",
                "/providence/service.thrift")) {
            File            f   = copyResourceTo(res, src).getAbsoluteFile().getCanonicalFile();
            ProgramRegistry tmp = programLoader.load(f);
            programs.add(tmp.getProgram());
        }
    }

    @Test
    public void testGenerate_defaults() throws GeneratorException, IOException {
        defaultSources();

        JavaOptions options = new JavaOptions();
        for (CProgram program : programs) {
            Generator generator = new JavaGenerator(fileManager,
                                                    generatorOptions,
                                                    options);
            generator.generate(globalRegistry.registryForPath(program.getProgramFilePath()));
        }
    }

    @Test
    public void testGenerate_jackson() throws GeneratorException, IOException {
        defaultSources();

        JavaOptions options = new JavaOptions();
        options.jackson = true;
        for (CProgram program : programs) {
            Generator generator = new JavaGenerator(fileManager,
                                                    generatorOptions,
                                                    options);
            generator.generate(globalRegistry.registryForPath(program.getProgramFilePath()));
        }
    }

    @Test
    public void testHazelcast() throws IOException {
        File src = tmp.newFolder("hz");
        for (String res : UnmodifiableList.listOf(
                "/hazelcast/hazelcast.thrift")) {
            CProgram program = programLoader.load(copyResourceTo(res, src)).getProgram();
            programs.add(program);
        }

        JavaOptions options = new JavaOptions();
        options.hazelcast_portable = true;
        for (CProgram program : programs) {
            Generator generator = new JavaGenerator(fileManager,
                                                    generatorOptions,
                                                    options);
            generator.generate(globalRegistry.registryForPath(program.getProgramFilePath()));
        }
    }
}
