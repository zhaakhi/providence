/*
 * Copyright 2016 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.reflect.contained;

import net.morimekta.providence.PMessageBuilder;

import javax.annotation.Nonnull;
import java.util.Map;

/**
 * A contained message of variant interface. Note that this is a dummy class, only
 * used for type-setting the interface descriptor.
 */
public class CInterface implements CMessage<CInterface> {
    CInterface() {
    }

    @Override
    public Map<Integer,Object> values() {
        throw new UnsupportedOperationException("Not allowed to be instantiated");
    }

    @Nonnull
    @Override
    public PMessageBuilder<CInterface> mutate() {
        throw new UnsupportedOperationException("Not allowed to be instantiated");
    }

    @Nonnull
    @Override
    public CInterfaceDescriptor descriptor() {
        throw new UnsupportedOperationException("Not allowed to be instantiated");
    }
}
