package net.morimekta.providence.reflect.model;

import net.morimekta.providence.reflect.parser.ThriftToken;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

/**
 * Declaration of a const value.
 *
 * <pre>{@code
 * const ::= 'const' {type} {name} '=' {value} {annotations}?;
 * }</pre>
 */
public class ConstDeclaration extends Declaration {
    private final List<ThriftToken> typeTokens;
    private final List<ThriftToken> valueTokens;
    private final ThriftToken       constToken;

    public ConstDeclaration(@Nullable String documentation,
                            @Nonnull ThriftToken constToken,
                            @Nonnull ThriftToken name,
                            @Nonnull List<ThriftToken> typeTokens,
                            List<ThriftToken> valueTokens,
                            List<AnnotationDeclaration> annotations) {
        super(documentation, name, annotations);
        this.constToken = constToken;
        this.typeTokens = typeTokens;
        this.valueTokens = valueTokens;
    }

    public ThriftToken getConstToken() {
        return constToken;
    }

    public String getType() {
        return DeclarationUtil.toTypeString(typeTokens);
    }

    public List<ThriftToken> getTypeTokens() {
        return typeTokens;
    }

    public String getValue() {
        return DeclarationUtil.toValueString(valueTokens);
    }

    public List<ThriftToken> getValueTokens() {
        return valueTokens;
    }
}
