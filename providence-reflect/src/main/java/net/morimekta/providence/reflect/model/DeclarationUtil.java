package net.morimekta.providence.reflect.model;

import net.morimekta.providence.reflect.parser.ThriftToken;
import net.morimekta.providence.reflect.parser.ThriftTokenType;

import javax.annotation.Nonnull;
import java.util.List;

class DeclarationUtil {
    @Nonnull
    static String toTypeString(@Nonnull List<ThriftToken> typeTokens) {
        StringBuilder builder = new StringBuilder();
        for (ThriftToken token : typeTokens) {
            builder.append(token);
        }
        return builder.toString();
    }

    @Nonnull
    static ThriftToken toTypeToken(@Nonnull List<ThriftToken> typeTokens) {
        ThriftToken type1 = typeTokens.get(0);
        if (typeTokens.size() == 1) {
            return type1;
        }
        CharSequence line = type1.line();
        ThriftToken last = typeTokens.get(typeTokens.size() - 1);
        return new ThriftToken(line.toString().toCharArray(),
                               type1.linePos() - 1,
                               last.lineNo() == type1.lineNo() ?
                               last.linePos() + last.length() - type1.linePos() :
                               line.length() - type1.linePos(),
                               ThriftTokenType.IDENTIFIER,
                               type1.lineNo(),
                               type1.linePos());
    }

    @Nonnull
    static String toValueString(@Nonnull List<ThriftToken> typeTokens) {
        StringBuilder builder = new StringBuilder();
        for (ThriftToken token : typeTokens) {
            builder.append(token);
        }
        return builder.toString();
    }
}
