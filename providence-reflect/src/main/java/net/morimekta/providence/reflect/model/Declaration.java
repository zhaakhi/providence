package net.morimekta.providence.reflect.model;

import net.morimekta.providence.reflect.parser.ThriftToken;
import net.morimekta.util.collect.UnmodifiableList;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

public abstract class Declaration {
    private final String                      documentation;
    private final ThriftToken                 nameToken;
    private final List<AnnotationDeclaration> annotations;

    protected Declaration(@Nullable String documentation,
                          @Nonnull ThriftToken nameToken,
                          @Nullable List<AnnotationDeclaration> annotations) {
        this.documentation = documentation;
        this.nameToken = nameToken;
        this.annotations = annotations == null ?
                           UnmodifiableList.listOf() :
                           UnmodifiableList.copyOf(annotations);
    }

    /**
     * @return Documentation for the specific declaration.
     */
    @Nullable
    public String getDocumentation() {
        return documentation;
    }

    /**
     * @return Declared name.
     */
    @Nonnull
    public String getName() {
        return getNameToken().toString();
    }

    /**
     * @return The token where the name is declared.
     */
    @Nonnull
    public ThriftToken getNameToken() {
        return nameToken;
    }

    /**
     * @return Annotations related to the declaration.
     */
    @Nonnull
    public List<AnnotationDeclaration> getAnnotations() {
        return annotations;
    }
}
