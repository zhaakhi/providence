package net.morimekta.providence.reflect.model;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * <pre>{@code
 * declaration ::= {documentation} ({typedef} | {enum} | {message} | {service} | {const})
 * program     ::= {namespace|include}* {declaration}*
 * }</pre>
 */
public class ProgramDeclaration {
    private final String documentation;
    private final String programName;
    private final List<IncludeDeclaration> includes;
    private final List<NamespaceDeclaration> namespaces;
    private final List<Declaration> declarationList;

    public ProgramDeclaration(@Nullable String documentation,
                              @Nonnull String programName,
                              @Nonnull List<IncludeDeclaration> includes,
                              @Nonnull List<NamespaceDeclaration> namespaces,
                              @Nonnull List<Declaration> declarationList) {
        this.documentation = documentation;
        this.programName = programName;
        this.includes = includes;
        this.namespaces = namespaces;
        this.declarationList = declarationList;
    }

    public String getDocumentation() {
        return documentation;
    }

    public String getProgramName() {
        return programName;
    }

    public List<IncludeDeclaration> getIncludes() {
        return includes;
    }

    public List<NamespaceDeclaration> getNamespaces() {
        return namespaces;
    }

    public List<Declaration> getDeclarationList() {
        return declarationList;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof ProgramDeclaration)) return false;
        ProgramDeclaration other = (ProgramDeclaration) obj;
        return Objects.equals(documentation, other.documentation) &&
               Objects.equals(programName, other.programName) &&
               Objects.equals(includes, other.includes) &&
               Objects.equals(namespaces, other.namespaces) &&
               Objects.equals(declarationList, other.declarationList);
    }

    @Override
    public String toString() {
        return "ThriftProgram{" + programName + ".{" +
               declarationList.stream().map(Declaration::getName).collect(Collectors.joining(",")) + "}}";
    }
}
