/*
 * Copyright 2016 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.reflect.contained;

import net.morimekta.providence.PType;
import net.morimekta.providence.descriptor.PDeclaredDescriptor;
import net.morimekta.providence.descriptor.PDescriptor;
import net.morimekta.providence.descriptor.PDescriptorProvider;
import net.morimekta.providence.descriptor.PValueProvider;
import net.morimekta.util.collect.UnmodifiableMap;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * First stage before we have a totally separate CConst from the CField contained type class.
 */
public class CConst extends PDeclaredDescriptor<Object> implements CAnnotatedDescriptor {
    private final String              comment;
    private final PDescriptorProvider typeProvider;
    private final PValueProvider<?>   valueProvider;
    private final Map<String, String> annotations;

    public CConst(@Nullable String docs,
                  @Nonnull String programName,
                  @Nonnull String name,
                  @Nonnull PDescriptorProvider typeProvider,
                  @Nonnull PValueProvider<?> valueProvider,
                  @Nullable Map<String, String> annotations) {
        super(programName, name);
        this.comment = docs;
        this.typeProvider = typeProvider;
        this.valueProvider = valueProvider;
        this.annotations = annotations == null ? UnmodifiableMap.mapOf() : UnmodifiableMap.copyOf(annotations);
    }

    @Override
    public boolean isInnerType() {
        return false;
    }

    @Override
    public boolean isAutoType() {
        return false;
    }

    @Override
    public String getDocumentation() {
        return comment;
    }

    @Nonnull
    public PDescriptor getDescriptor() {
        return typeProvider.descriptor();
    }

    public boolean hasDefaultValue() {
        return true;
    }

    @Override
    public Object getDefaultValue() {
        try {
            return valueProvider.get();
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new IllegalStateException("Unable to parse default value " + getName(), e);
        }
    }

    @Nonnull
    @Override
    public PType getType() {
        return typeProvider.descriptor().getType();
    }

    @Nonnull
    @Override
    public Set<String> getAnnotations() {
        return annotations.keySet();
    }

    @Override
    public boolean hasAnnotation(@Nonnull String name) {
        return annotations.containsKey(name);
    }

    @Override
    public String getAnnotationValue(@Nonnull String name) {
        if (annotations.containsKey(name)) {
            return annotations.get(name);
        }
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof CField)) {
            return false;
        }
        CField<?> other = (CField<?>) o;
        return // We cannot test that the types are deep-equals as it may have circular
               // containment.
               equalsQualifiedName(getDescriptor(), other.getDescriptor()) &&
               getName().equals(other.getName()) &&
               Objects.equals(valueProvider.get(), other.getDefaultValue());
    }

    @Override
    public int hashCode() {
        return Objects.hash(CField.class, getName(), getDescriptor(), getDefaultValue());
    }

    /**
     * Check if the two descriptors has the same qualified name, i..e
     * symbolically represent the same type.
     *
     * @param a The first type.
     * @param b The second type.
     * @return If the two types are the same.
     */
    private static boolean equalsQualifiedName(PDescriptor a, PDescriptor b) {
        return (a != null) && (b != null) && (a.getQualifiedName()
                                               .equals(b.getQualifiedName()));
    }
}
