package net.morimekta.providence.reflect.model;

import net.morimekta.providence.PMessageVariant;
import net.morimekta.providence.reflect.parser.ThriftToken;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

/**
 * <pre>{@code
 * variant ::= 'struct' | 'union' | 'exception' | 'interface'
 * message ::= {variant} {name} (('implements' | 'of') {implementing})? '{' {field}* '}' {annotations}?
 * }</pre>
 */
public class MessageDeclaration extends Declaration {
    private final ThriftToken            variant;
    private final ThriftToken            implementing;
    private final List<FieldDeclaration> fields;

    public MessageDeclaration(@Nullable String documentation,
                              @Nonnull ThriftToken variant,
                              @Nonnull ThriftToken name,
                              @Nullable ThriftToken implementing,
                              @Nonnull List<FieldDeclaration> fields,
                              @Nullable List<AnnotationDeclaration> annotations) {
        super(documentation, name, annotations);
        this.variant = variant;
        this.implementing = implementing;
        this.fields = fields;
    }

    public ThriftToken getImplementing() {
        return implementing;
    }

    @Nonnull
    public List<FieldDeclaration> getFields() {
        return fields;
    }

    public PMessageVariant getVariant() {
        switch (variant.toString()) {
            case "union": return PMessageVariant.UNION;
            case "exception": return PMessageVariant.EXCEPTION;
            case "interface": return PMessageVariant.INTERFACE;
        }
        return PMessageVariant.STRUCT;
    }

    @Nonnull
    public ThriftToken getVariantToken() {
        return variant;
    }
}
