package net.morimekta.providence.reflect.model;

import net.morimekta.providence.reflect.parser.ThriftToken;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.nio.file.Paths;

import static net.morimekta.providence.reflect.util.ReflectionUtils.programNameFromPath;

/**
 * <pre>{@code
 * include ::= 'include' {file_path} ('as' {programName})?
 * }</pre>
 */
public class IncludeDeclaration {
    private final ThriftToken includeToken;
    private final ThriftToken filePath;
    private final ThriftToken programNameAlias;
    private final String programName;

    public IncludeDeclaration(@Nonnull ThriftToken includeToken,
                              @Nonnull ThriftToken filePath,
                              @Nullable ThriftToken programNameAlias) {
        this.includeToken = includeToken;
        this.filePath = filePath;
        this.programNameAlias = programNameAlias;
        this.programName = programNameAlias == null
                ? programNameFromPath(Paths.get(getFilePath()))
                : programNameAlias.toString();
    }

    @Nonnull
    public ThriftToken getIncludeToken() {
        return includeToken;
    }

    @Nonnull
    public String getFilePath() {
        return filePath.decodeString(false);
    }

    @Nonnull
    public ThriftToken getFilePathToken() {
        return filePath;
    }

    @Nonnull
    public String getProgramName() {
        return programName;
    }

    @Nullable
    public ThriftToken getProgramNameToken() {
        return programNameAlias;
    }

    @Override
    public String toString() {
        return "ThriftInclude{" + programNameAlias + "=\"" + getFilePath() + "\"}";
    }
}
