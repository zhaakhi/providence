package net.morimekta.providence.reflect.parser;

import net.morimekta.util.lexer.Lexer;
import net.morimekta.util.lexer.Tokenizer;

import java.io.InputStream;

public class ThriftLexer extends Lexer<ThriftTokenType, ThriftToken> {
    public ThriftLexer(InputStream in) {
        this(new ThriftTokenizer(in));
    }

    public ThriftLexer(Tokenizer<ThriftTokenType, ThriftToken> tokenizer) {
        super(tokenizer);
    }
}
