/*
 * Copyright 2016 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.reflect.contained;

import net.morimekta.providence.PMessageBuilder;
import net.morimekta.providence.descriptor.PExceptionDescriptor;
import net.morimekta.providence.descriptor.PField;
import net.morimekta.util.collect.UnmodifiableMap;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;

/**
 * @author Stein Eldar Johnsen
 * @since 07.09.15
 */
public class CExceptionDescriptor extends PExceptionDescriptor<CException> implements CMessageDescriptor {
    private final CField<CException>[]             fields;
    private final Map<Integer, CField<CException>> fieldIdMap;
    private final Map<String, CField<CException>>  fieldNameMap;
    private final Map<String, CField<CException>>  fieldPojoNameMap;
    private final Map<String, String>  annotations;
    private final String               comment;

    @SuppressWarnings("unchecked")
    public CExceptionDescriptor(String comment,
                                String packageName,
                                String name,
                                List<CField<CException>> fields,
                                Map<String, String> annotations) {
        super(packageName, name, new _BuilderSupplier(),
              // overrides isSimple instead to avoid having to check fields
              // types before it's converted.
              false);
        ((_BuilderSupplier) getBuilderSupplier()).setType(this);

        this.comment = comment;
        this.fields = fields.toArray((CField<CException>[]) CField.EMPTY_ARRAY);
        this.annotations = annotations;

        UnmodifiableMap.Builder<Integer, CField<CException>> fieldIdMap   = UnmodifiableMap.builder(this.fields.length);
        UnmodifiableMap.Builder<String, CField<CException>>  fieldNameMap = UnmodifiableMap.builder(this.fields.length);
        UnmodifiableMap.Builder<String, CField<CException>>  fieldPojoNameMap = UnmodifiableMap.builder(this.fields.length);
        for (CField<CException> field : fields) {
            field.setMessageType(this);
            fieldIdMap.put(field.getId(), field);
            fieldNameMap.put(field.getName(), field);
            fieldPojoNameMap.put(field.getPojoName(), field);
        }
        this.fieldIdMap = fieldIdMap.build();
        this.fieldNameMap = fieldNameMap.build();
        this.fieldPojoNameMap = fieldPojoNameMap.build();
    }

    @Override
    public final String getDocumentation() {
        return comment;
    }

    @Nonnull
    @Override
    public CField<CException>[] getFields() {
        return Arrays.copyOf(fields, fields.length);
    }

    @Override
    public CField<CException> findFieldByName(String name) {
        return fieldNameMap.get(name);
    }

    @Override
    public CField<CException> findFieldByPojoName(String pojoName) {
        return fieldPojoNameMap.get(pojoName);
    }

    @Override
    public CField<CException> findFieldById(int id) {
        return fieldIdMap.get(id);
    }

    @Nullable
    @Override
    public CInterfaceDescriptor getImplementing() {
        return null;
    }

    @Nonnull
    @Override
    @SuppressWarnings("unchecked")
    public Set<String> getAnnotations() {
        if (annotations != null) {
            return annotations.keySet();
        }
        return Collections.EMPTY_SET;
    }

    @Override
    public boolean hasAnnotation(@Nonnull String name) {
        if (annotations != null) {
            return annotations.containsKey(name);
        }
        return false;
    }

    @Override
    public String getAnnotationValue(@Nonnull String name) {
        if (annotations != null) {
            return annotations.get(name);
        }
        return null;
    }

    @Override
    public boolean isSimple() {
        for (PField<CException> field : getFields()) {
            switch (field.getType()) {
                case MAP:
                case SET:
                case LIST:
                case MESSAGE:
                    return false;
                default:
                    break;
            }
        }
        return true;
    }

    private static class _BuilderSupplier implements Supplier<PMessageBuilder<CException>> {
        private CExceptionDescriptor mType;

        public void setType(CExceptionDescriptor type) {
            mType = type;
        }

        @Nonnull
        @Override
        public PMessageBuilder<CException> get() {
            return new CException.Builder(mType);
        }
    }
}
