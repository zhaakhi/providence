/*
 * Copyright 2016 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.reflect.contained;

import net.morimekta.providence.descriptor.PInterfaceDescriptor;
import net.morimekta.providence.descriptor.PMessageDescriptor;
import net.morimekta.util.collect.UnmodifiableList;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Descriptor for an interface. Note that this does not have an equivalent
 * in providence-core, as these interfaces are there for the code generation.
 * They do not alter serialization.
 */
public class CInterfaceDescriptor extends PInterfaceDescriptor<CInterface> implements CMessageDescriptor {
    private final Map<String, String>              annotations;
    private final String                           comment;
    private final ArrayList<PMessageDescriptor<?>> possibleTypes;

    @SuppressWarnings("unchecked")
    public CInterfaceDescriptor(String comment,
                                String packageName,
                                String name,
                                List<CField<CInterface>> fields,
                                Map<String, String> annotations) {
        super(packageName, name, fields.toArray((CField<CInterface>[]) CField.EMPTY_ARRAY));
        for (CField<CInterface> field : fields) {
            field.setMessageType(this);
        }
        this.comment = comment;
        this.annotations = annotations;
        this.possibleTypes = new ArrayList<>();
    }

    public void addPossibleType(PMessageDescriptor<?> descriptor) {
        possibleTypes.add(descriptor);
    }

    @Override
    public List<PMessageDescriptor<?>> getPossibleTypes() {
        return UnmodifiableList.copyOf(possibleTypes);
    }

    @Override
    public final String getDocumentation() {
        return comment;
    }

    @Nullable
    @Override
    public CInterfaceDescriptor getImplementing() {
        return null;
    }

    @Nonnull
    @Override
    @SuppressWarnings("unchecked")
    public Set<String> getAnnotations() {
        if (annotations != null) {
            return annotations.keySet();
        }
        return Collections.EMPTY_SET;
    }

    @Override
    public boolean hasAnnotation(@Nonnull String name) {
        if (annotations != null) {
            return annotations.containsKey(name);
        }
        return false;
    }

    @Override
    public String getAnnotationValue(@Nonnull String name) {
        if (annotations != null) {
            return annotations.get(name);
        }
        return null;
    }
}
