/*
 * Copyright 2016 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.reflect.parser;

import net.morimekta.util.lexer.LexerException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Locale;

/**
 * Token specialization for the thrift parser and tokenizer.
 */
public class ThriftException extends LexerException {
    private String file;

    public ThriftException(String message, Object... params) {
        super(params.length == 0 ? message : String.format(Locale.US, message, params));
    }

    public ThriftException(Throwable cause, String message, Object... params) {
        super(cause, params.length == 0 ? message : String.format(Locale.US, message, params));
    }

    public ThriftException(ThriftToken token, String message, Object... params) {
        super(token, params.length == 0 ? message : String.format(Locale.US, message, params));
    }

    @Nullable
    public String getFile() {
        return file;
    }

    @Nonnull
    public ThriftException setFile(String file) {
        this.file = file;
        return this;
    }

    @Nonnull
    @Override
    public ThriftException initCause(Throwable cause) {
        return (ThriftException) super.initCause(cause);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "\n" + displayString();
    }

    @Override
    protected String getError() {
        if (file != null) {
            return "Error in " + file;
        }
        return "Error";
    }
}
