package net.morimekta.providence.reflect.model;

import net.morimekta.providence.reflect.parser.ThriftToken;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

/**
 * <pre>{@code
 * service ::= 'service' {name} ('extends' {extending})? '{' {function}* '}' {annotations}?
 * }</pre>
 */
public class ServiceDeclaration extends Declaration {
    private final ThriftToken             serviceToken;
    private final ThriftToken             extending;
    private final List<MethodDeclaration> methods;

    public ServiceDeclaration(@Nullable String documentation,
                              @Nonnull ThriftToken serviceToken,
                              @Nonnull ThriftToken name,
                              @Nullable ThriftToken extending,
                              @Nonnull List<MethodDeclaration> methods,
                              @Nullable List<AnnotationDeclaration> annotations) {
        super(documentation, name, annotations);
        this.serviceToken = serviceToken;
        this.extending = extending;
        this.methods = methods;
    }

    @Nonnull
    public ThriftToken getServiceToken() {
        return serviceToken;
    }

    @Nullable
    public String getExtending() {
        if (extending != null) {
            return extending.toString();
        }
        return null;
    }

    @Nullable
    public ThriftToken getExtendingToken() {
        return extending;
    }

    @Nonnull
    public List<MethodDeclaration> getMethods() {
        return methods;
    }
}
