// This is used as a valid reference in failure tests.
namespace java org.apache.test.failure

union MyEnum {
    1: optional string name;
}

struct MyStruct {
    1: optional i32 name (ref.enum = "MyEnum");
}
