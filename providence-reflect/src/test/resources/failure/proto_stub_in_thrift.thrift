namespace java net.morimekta.test.service2

struct TextArgs {
    1: optional string language;
}

struct Request {
    1: string text (arguments.type = "TextArgs");
    2: bool valid;
}

service Base2Service {
    i64 inBase2Service()

    TextArgs protoMethod(Request);
}
