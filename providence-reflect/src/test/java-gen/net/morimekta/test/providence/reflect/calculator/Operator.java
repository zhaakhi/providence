package net.morimekta.test.providence.reflect.calculator;

@javax.annotation.Generated(
        value = "net.morimekta.providence:providence-generator-java",
        comments = "java:serializable")
public enum Operator
        implements net.morimekta.providence.PEnumValue<Operator> {
    IDENTITY(1, "IDENTITY", "IDENTITY"),
    ADD(2, "ADD", "ADD"),
    SUBTRACT(3, "SUBTRACT", "SUBTRACT"),
    MULTIPLY(4, "MULTIPLY", "MULTIPLY"),
    DIVIDE(5, "DIVIDE", "DIVIDE"),
    ;

    private final int    mId;
    private final String mName;
    private final String mPojoName;

    Operator(int id, String name, String pojoName) {
        mId = id;
        mName = name;
        mPojoName = pojoName;
    }

    @Override
    public int asInteger() {
        return mId;
    }

    @Override
    @javax.annotation.Nonnull
    public String asString() {
        return mName;
    }

    @Override
    @javax.annotation.Nonnull
    public String getPojoName() {
        return mPojoName;
    }

    /**
     * Find a value based in its ID
     *
     * @param id Id of value
     * @return Value found or null
     */
    @javax.annotation.Nullable
    public static Operator findById(Integer id) {
        if (id == null) {
            return null;
        }
        switch (id) {
            case 1: return Operator.IDENTITY;
            case 2: return Operator.ADD;
            case 3: return Operator.SUBTRACT;
            case 4: return Operator.MULTIPLY;
            case 5: return Operator.DIVIDE;
            default: return null;
        }
    }

    /**
     * Find a value based in its name
     *
     * @param name Name of value
     * @return Value found or null
     */
    @javax.annotation.Nullable
    public static Operator findByName(String name) {
        if (name == null) {
            return null;
        }
        switch (name) {
            case "IDENTITY": return Operator.IDENTITY;
            case "ADD": return Operator.ADD;
            case "SUBTRACT": return Operator.SUBTRACT;
            case "MULTIPLY": return Operator.MULTIPLY;
            case "DIVIDE": return Operator.DIVIDE;
            default: return null;
        }
    }

    /**
     * Find a value based in its POJO name
     *
     * @param name Name of value
     * @return Value found or null
     */
    @javax.annotation.Nullable
    public static Operator findByPojoName(String name) {
        if (name == null) {
            return null;
        }
        switch (name) {
            case "IDENTITY": return Operator.IDENTITY;
            case "ADD": return Operator.ADD;
            case "SUBTRACT": return Operator.SUBTRACT;
            case "MULTIPLY": return Operator.MULTIPLY;
            case "DIVIDE": return Operator.DIVIDE;
            default: return null;
        }
    }

    /**
     * Get a value based in its ID
     *
     * @param id Id of value
     * @return Value found
     * @throws IllegalArgumentException If no value for id is found
     */
    @javax.annotation.Nonnull
    public static Operator valueForId(int id) {
        Operator value = findById(id);
        if (value == null) {
            throw new IllegalArgumentException("No calculator.Operator for id " + id);
        }
        return value;
    }

    /**
     * Get a value based in its name
     *
     * @param name Name of value
     * @return Value found
     * @throws IllegalArgumentException If no value for name is found, or null name
     */
    @javax.annotation.Nonnull
    public static Operator valueForName(@javax.annotation.Nonnull String name) {
        Operator value = findByName(name);
        if (value == null) {
            throw new IllegalArgumentException("No calculator.Operator for name \"" + name + "\"");
        }
        return value;
    }

    /**
     * Get a value based in its name
     *
     * @param name Name of value
     * @return Value found
     * @throws IllegalArgumentException If no value for name is found, or null name
     */
    @javax.annotation.Nonnull
    public static Operator valueForPojoName(@javax.annotation.Nonnull String name) {
        Operator value = findByPojoName(name);
        if (value == null) {
            throw new IllegalArgumentException("No calculator.Operator for name \"" + name + "\"");
        }
        return value;
    }

    public static final class _Builder extends net.morimekta.providence.PEnumBuilder<Operator> {
        private Operator mValue;

        @Override
        @javax.annotation.Nonnull
        public _Builder setById(int value) {
            mValue = Operator.findById(value);
            return this;
        }

        @Override
        @javax.annotation.Nonnull
        public _Builder setByName(String name) {
            mValue = Operator.findByName(name);
            return this;
        }

        @Override
        public boolean valid() {
            return mValue != null;
        }

        @Override
        public Operator build() {
            return mValue;
        }
    }

    public static final net.morimekta.providence.descriptor.PEnumDescriptor<Operator> kDescriptor;

    @Override
    @javax.annotation.Nonnull
    public net.morimekta.providence.descriptor.PEnumDescriptor<Operator> descriptor() {
        return kDescriptor;
    }

    public static net.morimekta.providence.descriptor.PEnumDescriptorProvider<Operator> provider() {
        return new net.morimekta.providence.descriptor.PEnumDescriptorProvider<Operator>(kDescriptor);
    }

    private static final class _Descriptor
            extends net.morimekta.providence.descriptor.PEnumDescriptor<Operator> {
        public _Descriptor() {
            super("calculator", "Operator", _Builder::new);
        }

        @Override
        @javax.annotation.Nonnull
        public boolean isInnerType() {
            return false;
        }

        @Override
        @javax.annotation.Nonnull
        public boolean isAutoType() {
            return false;
        }

        @Override
        @javax.annotation.Nonnull
        public Operator[] getValues() {
            return Operator.values();
        }

        @Override
        @javax.annotation.Nullable
        public Operator findById(int id) {
            return Operator.findById(id);
        }

        @Override
        @javax.annotation.Nullable
        public Operator findByName(String name) {
            return Operator.findByName(name);
        }

        @Override
        @javax.annotation.Nullable
        public Operator findByPojoName(String name) {
            return Operator.findByPojoName(name);
        }
    }

    static {
        kDescriptor = new _Descriptor();
    }
}
