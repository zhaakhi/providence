package net.morimekta.test.providence.reflect;

@SuppressWarnings("unused")
@javax.annotation.Generated(
        value = "net.morimekta.providence:providence-generator-java",
        comments = "java:serializable")
@javax.annotation.concurrent.Immutable
public class ConsumeAll
        implements ConsumeAll_OrBuilder,
                   net.morimekta.providence.PMessage<ConsumeAll>,
                   Comparable<ConsumeAll>,
                   java.io.Serializable,
                   net.morimekta.providence.serializer.binary.BinaryWriter {
    private final static long serialVersionUID = -9118592554502001384L;


    private volatile transient int tHashCode;

    // Transient object used during java deserialization.
    private transient ConsumeAll tSerializeInstance;

    private ConsumeAll(_Builder builder) {
    }

    @Override
    public boolean has(int key) {
        switch(key) {
            default: return false;
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T get(int key) {
        switch(key) {
            default: return null;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) return true;
        if (o == null || !o.getClass().equals(getClass())) return false;
        return true;
    }

    @Override
    public int hashCode() {
        if (tHashCode == 0) {
            tHashCode = java.util.Objects.hash(
                    ConsumeAll.class);
        }
        return tHashCode;
    }

    @Override
    public String toString() {
        return "providence.ConsumeAll" + asString();
    }

    @Override
    @javax.annotation.Nonnull
    public String asString() {
        StringBuilder out = new StringBuilder();
        out.append("{");

        out.append('}');
        return out.toString();
    }

    @Override
    public int compareTo(ConsumeAll other) {
        int c;

        return 0;
    }

    private void writeObject(java.io.ObjectOutputStream oos) throws java.io.IOException {
        oos.defaultWriteObject();
        net.morimekta.providence.serializer.BinarySerializer.INSTANCE.serialize(oos, this);
    }

    private void readObject(java.io.ObjectInputStream ois)
            throws java.io.IOException, ClassNotFoundException {
        ois.defaultReadObject();
        tSerializeInstance = net.morimekta.providence.serializer.BinarySerializer.INSTANCE.deserialize(ois, kDescriptor);
    }

    private Object readResolve() throws java.io.ObjectStreamException {
        return tSerializeInstance;
    }

    @Override
    public int writeBinary(net.morimekta.util.io.BigEndianBinaryWriter writer) throws java.io.IOException {
        int length = 0;

        length += writer.writeByte((byte) 0);
        return length;
    }

    @javax.annotation.Nonnull
    @Override
    public _Builder mutate() {
        return new _Builder(this);
    }

    public enum _Field implements net.morimekta.providence.descriptor.PField<ConsumeAll> {
        ;

        private final int mId;
        private final net.morimekta.providence.descriptor.PRequirement mRequired;
        private final String mName;
        private final String mPojoName;
        private final net.morimekta.providence.descriptor.PDescriptorProvider mTypeProvider;
        private final net.morimekta.providence.descriptor.PStructDescriptorProvider mArgumentsProvider;
        private final net.morimekta.providence.descriptor.PValueProvider<?> mDefaultValue;

        _Field(int id, net.morimekta.providence.descriptor.PRequirement required, String name, String pojoName, net.morimekta.providence.descriptor.PDescriptorProvider typeProvider, net.morimekta.providence.descriptor.PStructDescriptorProvider argumentsProvider, net.morimekta.providence.descriptor.PValueProvider<?> defaultValue) {
            mId = id;
            mRequired = required;
            mName = name;
            mPojoName = pojoName;
            mTypeProvider = typeProvider;
            mArgumentsProvider = argumentsProvider;
            mDefaultValue = defaultValue;
        }

        @Override
        public int getId() { return mId; }

        @javax.annotation.Nonnull
        @Override
        public net.morimekta.providence.descriptor.PRequirement getRequirement() { return mRequired; }

        @javax.annotation.Nonnull
        @Override
        public net.morimekta.providence.descriptor.PDescriptor getDescriptor() { return mTypeProvider.descriptor(); }

        @Override
        @javax.annotation.Nullable
        public net.morimekta.providence.descriptor.PStructDescriptor getArgumentsType() { return mArgumentsProvider == null ? null : mArgumentsProvider.descriptor(); }

        @javax.annotation.Nonnull
        @Override
        public String getName() { return mName; }

        @javax.annotation.Nonnull
        @Override
        public String getPojoName() { return mPojoName; }

        @Override
        public boolean hasDefaultValue() { return mDefaultValue != null; }

        @Override
        @javax.annotation.Nullable
        public Object getDefaultValue() {
            return hasDefaultValue() ? mDefaultValue.get() : null;
        }

        @Override
        @javax.annotation.Nonnull
        public net.morimekta.providence.descriptor.PMessageDescriptor<ConsumeAll> onMessageType() {
            return kDescriptor;
        }

        @Override
        public String toString() {
            return net.morimekta.providence.descriptor.PField.asString(this);
        }

        /**
         * @param id Field ID
         * @return The identified field or null
         */
        public static _Field findById(int id) {
            switch (id) {
            }
            return null;
        }

        /**
         * @param name Field name
         * @return The named field or null
         */
        public static _Field findByName(String name) {
            if (name == null) return null;
            switch (name) {
            }
            return null;
        }

        /**
         * @param name Field POJO name
         * @return The named field or null
         */
        public static _Field findByPojoName(String name) {
            if (name == null) return null;
            switch (name) {
            }
            return null;
        }

        /**
         * @param id Field ID
         * @return The identified field
         * @throws IllegalArgumentException If no such field
         */
        public static _Field fieldForId(int id) {
            _Field field = findById(id);
            if (field == null) {
                throw new IllegalArgumentException("No such field id " + id + " in providence.ConsumeAll");
            }
            return field;
        }

        /**
         * @param name Field name
         * @return The named field
         * @throws IllegalArgumentException If no such field
         */
        public static _Field fieldForName(String name) {
            if (name == null) {
                throw new IllegalArgumentException("Null name argument");
            }
            _Field field = findByName(name);
            if (field == null) {
                throw new IllegalArgumentException("No such field \"" + name + "\" in providence.ConsumeAll");
            }
            return field;
        }

        /**
         * @param name Field POJO name
         * @return The named field
         * @throws IllegalArgumentException If no such field
         */
        public static _Field fieldForPojoName(String name) {
            if (name == null) {
                throw new IllegalArgumentException("Null name argument");
            }
            _Field field = findByPojoName(name);
            if (field == null) {
                throw new IllegalArgumentException("No such field \"" + name + "\" in providence.ConsumeAll");
            }
            return field;
        }
    }

    @javax.annotation.Nonnull
    public static net.morimekta.providence.descriptor.PStructDescriptorProvider<ConsumeAll> provider() {
        return new _Provider();
    }

    @Override
    @javax.annotation.Nonnull
    public net.morimekta.providence.descriptor.PStructDescriptor<ConsumeAll> descriptor() {
        return kDescriptor;
    }

    public static final net.morimekta.providence.descriptor.PStructDescriptor<ConsumeAll> kDescriptor;

    private static final class _Descriptor
            extends net.morimekta.providence.descriptor.PStructDescriptor<ConsumeAll> {
        public _Descriptor() {
            super("providence", "ConsumeAll", _Builder::new, true);
        }

        @Override
        @javax.annotation.Nonnull
        public boolean isInnerType() {
            return false;
        }

        @Override
        @javax.annotation.Nonnull
        public boolean isAutoType() {
            return false;
        }

        @Override
        @javax.annotation.Nonnull
        public _Field[] getFields() {
            return _Field.values();
        }

        @Override
        @javax.annotation.Nullable
        public _Field findFieldByName(String name) {
            return _Field.findByName(name);
        }

        @Override
        @javax.annotation.Nullable
        public _Field findFieldByPojoName(String name) {
            return _Field.findByPojoName(name);
        }

        @Override
        @javax.annotation.Nullable
        public _Field findFieldById(int id) {
            return _Field.findById(id);
        }
    }

    static {
        kDescriptor = new _Descriptor();
    }

    private static final class _Provider extends net.morimekta.providence.descriptor.PStructDescriptorProvider<ConsumeAll> {
        @Override
        @javax.annotation.Nonnull
        public net.morimekta.providence.descriptor.PStructDescriptor<ConsumeAll> descriptor() {
            return kDescriptor;
        }
    }

    /**
     * Make a <code>providence.ConsumeAll</code> builder.
     * @return The builder instance.
     */
    public static _Builder builder() {
        return new _Builder();
    }

    public static class _Builder
            extends net.morimekta.providence.PMessageBuilder<ConsumeAll>
            implements ConsumeAll_OrBuilder,
                       net.morimekta.providence.serializer.binary.BinaryReader {
        private java.util.BitSet optionals;
        private java.util.BitSet modified;

        /**
         * Make a providence.ConsumeAll builder instance.
         */
        public _Builder() {
            optionals = new java.util.BitSet(0);
            modified = new java.util.BitSet(0);
        }

        /**
         * Make a mutating builder off a base providence.ConsumeAll.
         *
         * @param base The base ConsumeAll
         */
        public _Builder(ConsumeAll base) {
            this();

        }

        @javax.annotation.Nonnull
        @Override
        public ConsumeAll._Builder merge(ConsumeAll from) {
            return this;
        }

        @Override
        public boolean equals(Object o) {
            if (o == this) return true;
            if (o == null || !o.getClass().equals(getClass())) return false;
            return true;
        }

        @Override
        public int hashCode() {
            return java.util.Objects.hash(
                    ConsumeAll.class, optionals);
        }

        @Override
        @SuppressWarnings("unchecked")
        public net.morimekta.providence.PMessageBuilder mutator(int key) {
            switch (key) {
                default: throw new IllegalArgumentException("Not a message field ID: " + key);
            }
        }

        @javax.annotation.Nonnull
        @Override
        @SuppressWarnings("unchecked")
        public ConsumeAll._Builder set(int key, Object value) {
            if (value == null) return clear(key);
            switch (key) {
                default: break;
            }
            return this;
        }

        @Override
        public boolean isSet(int key) {
            switch (key) {
                default: break;
            }
            return false;
        }

        @Override
        public boolean isModified(int key) {
            switch (key) {
                default: break;
            }
            return false;
        }

        @Override
        @SuppressWarnings("unchecked")
        public <T> T get(int key) {
            switch(key) {
                default: return null;
            }
        }

        @Override
        public boolean has(int key) {
            switch(key) {
                default: return false;
            }
        }

        @javax.annotation.Nonnull
        @Override
        @SuppressWarnings("unchecked")
        public ConsumeAll._Builder addTo(int key, Object value) {
            switch (key) {
                default: break;
            }
            return this;
        }

        @javax.annotation.Nonnull
        @Override
        public ConsumeAll._Builder clear(int key) {
            switch (key) {
                default: break;
            }
            return this;
        }

        @Override
        public boolean valid() {
            return true;
        }

        @Override
        public ConsumeAll._Builder validate() {
            return this;
        }

        @javax.annotation.Nonnull
        @Override
        public net.morimekta.providence.descriptor.PStructDescriptor<ConsumeAll> descriptor() {
            return ConsumeAll.kDescriptor;
        }

        @Override
        public void readBinary(net.morimekta.util.io.BigEndianBinaryReader reader, boolean strict) throws java.io.IOException {
            byte type = reader.expectByte();
            while (type != 0) {
                int field = reader.expectShort();
                switch (field) {
                    default: {
                        net.morimekta.providence.serializer.binary.BinaryFormatUtils.readFieldValue(reader, new net.morimekta.providence.serializer.binary.BinaryFormatUtils.FieldInfo(field, type), null, false);
                        break;
                    }
                }
                type = reader.expectByte();
            }
        }

        @Override
        @javax.annotation.Nonnull
        public ConsumeAll build() {
            return new ConsumeAll(this);
        }
    }
}
