/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.morimekta.providence.reflect;

import net.morimekta.providence.reflect.contained.CMessage;
import net.morimekta.test.providence.reflect.ContainerService;
import net.morimekta.test.providence.reflect.calculator.Calculator;
import net.morimekta.test.providence.reflect.calculator.Operation;
import net.morimekta.test.providence.reflect.calculator.Operator;
import net.morimekta.test.providence.reflect.number.Imaginary;
import net.morimekta.testing.ResourceUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;

import static net.morimekta.providence.types.TypeReference.parseType;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.fail;

/**
 * Tests for the document registry.
 */
public class GlobalRegistryTest {
    @Rule
    public TemporaryFolder tmp = new TemporaryFolder();

    private GlobalRegistry registry;
    private File calc;
    private File num;

    @Before
    public void setUp() throws IOException {
        num = ResourceUtils.copyResourceTo("/parser/calculator/number.thrift", tmp.getRoot());
        calc = ResourceUtils.copyResourceTo("/parser/calculator/calculator.thrift", tmp.getRoot());

        ProgramLoader loader = new ProgramLoader();
        registry = loader.getGlobalRegistry();
        loader.load(num.toPath());
        loader.load(calc.toPath());
    }

    @Test
    public void testInternal() {
        assertThat(registry.getRegistries(), hasSize(2));
        assertThat(registry.isKnownProgram("calculator"), is(true));
        assertThat(registry.isKnownProgram("unknown"), is(false));
        try {
            registry.registryForPath("/../foo");
            fail("No exception");
        } catch (UncheckedIOException e) {
            assertThat(e.getMessage(), is("Parent of root does not exist!"));
        }
    }

    @Test
    public void testProgramRegistry() {
        ProgramRegistry pr = registry.registryForPath(calc.getAbsolutePath());
        assertThat(pr, is(notNullValue()));
        assertThat(pr.getRegistry("calculator").orElse(null), is(sameInstance(pr)));
        assertThat(pr.getRegistry("number").orElse(null), is(notNullValue()));
        assertThat(pr.getRegistry("unknown").orElse(null), is(nullValue()));

        assertThat(pr.getDeclaredType(parseType("calculator.Bar")).orElse(null), is(nullValue()));
        assertThat(pr.getDeclaredType(parseType("number.Bar")).orElse(null), is(nullValue()));
        assertThat(pr.getDeclaredType(parseType("foo.Bar")).orElse(null), is(nullValue()));

        assertThat(pr.getConstantValue(parseType("calculator.Bar")).orElse(null), is(nullValue()));
        assertThat(pr.getConstantValue(parseType("number.Bar")).orElse(null), is(nullValue()));
        assertThat(pr.getConstantValue(parseType("foo.Bar")).orElse(null), is(nullValue()));

        assertThat(pr.getService(parseType("calculator.Bar")).orElse(null), is(nullValue()));
        assertThat(pr.getService(parseType("number.Bar")).orElse(null), is(nullValue()));
        assertThat(pr.getService(parseType("foo.Bar")).orElse(null), is(nullValue()));

        try {
            pr.registerTypedef(parseType("foo.Bar"), parseType("bar.foo"));
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Unable to register typedef foo.Bar = bar.foo"));
        }
        try {
            pr.registerType(Imaginary.kDescriptor);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Unable to register message number.Imaginary"));
        }
        try {
            pr.registerService(ContainerService.kDescriptor);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Unable to register service providence.ContainerService"));
        }
        try {
            pr.registerConstant(parseType("foo.kBar"), () -> 42);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Unable to register constant foo.kBar"));
        }
    }

    @Test
    public void testGetDeclared() {
        assertThat(registry.requireDeclaredType(parseType("number.I")).getQualifiedName(),
                   is(Imaginary.kDescriptor.getQualifiedName()));
        assertThat(registry.requireMessageType(parseType("calculator.Operation")).getQualifiedName(),
                   is(Operation.kDescriptor.getQualifiedName()));
        assertThat(registry.requireService(parseType("calculator.Calculator")).getQualifiedName(),
                   is(Calculator.kDescriptor.getQualifiedName()));
        assertThat(registry.requireEnumType(parseType("calculator.Operator")).getQualifiedName(),
                   is(Operator.kDescriptor.getQualifiedName()));
        assertThat(registry.getConstantValue(parseType("number.kSqrtMinusOne")).orElse(null),
                   is(instanceOf(CMessage.class)));

        assertThat(registry.getDeclaredType(parseType("calculator.Bar")).orElse(null), is(nullValue()));
        assertThat(registry.getDeclaredType(parseType("number.Bar")).orElse(null), is(nullValue()));
        assertThat(registry.getDeclaredType(parseType("foo.Bar")).orElse(null), is(nullValue()));

        assertThat(registry.getConstantValue(parseType("calculator.Bar")).orElse(null), is(nullValue()));
        assertThat(registry.getConstantValue(parseType("number.Bar")).orElse(null), is(nullValue()));
        assertThat(registry.getConstantValue(parseType("foo.Bar")).orElse(null), is(nullValue()));

        assertThat(registry.getService(parseType("calculator.Bar")).orElse(null), is(nullValue()));
        assertThat(registry.getService(parseType("number.Bar")).orElse(null), is(nullValue()));
        assertThat(registry.getService(parseType("foo.Bar")).orElse(null), is(nullValue()));
    }
}
