package net.morimekta.providence.reflect.contained;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.fail;

public class CInterfaceTest {
    @Test
    public void testInterface() {
        CInterface ci = new CInterface();
        try {
            ci.descriptor();
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Not allowed to be instantiated"));
        }
        try {
            ci.mutate();
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Not allowed to be instantiated"));
        }
        try {
            ci.values();
            fail("no exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("Not allowed to be instantiated"));
        }
    }
}
