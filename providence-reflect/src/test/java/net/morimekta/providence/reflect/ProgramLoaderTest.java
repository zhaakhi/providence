/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.morimekta.providence.reflect;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import net.morimekta.providence.reflect.model.Declaration;
import net.morimekta.providence.reflect.model.ProgramDeclaration;
import net.morimekta.providence.reflect.parser.ThriftException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static net.morimekta.testing.ResourceUtils.copyResourceTo;
import static net.morimekta.testing.ResourceUtils.writeContentTo;
import static net.morimekta.util.collect.UnmodifiableList.listOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.fail;

@RunWith(DataProviderRunner.class)
public class ProgramLoaderTest {
    @Rule
    public TemporaryFolder tmp = new TemporaryFolder();

    @DataProvider
    public static Object[][] thriftOK() {
        return new Object[][]{
                {"struct MyStruct {\n" +
                 "  1: optional string value;\n" +
                 "}\n", listOf("MyStruct")},
                };
    }

    @Test
    @UseDataProvider("thriftOK")
    public void testThriftOK(String content, List<String> loadedTypes) throws IOException {
        File               test        = writeContentTo(content, new File(tmp.getRoot(), "test.thrift"));
        ProgramLoader      loader      = new ProgramLoader();
        ProgramDeclaration programType = loader.load(test).getProgramType();
        assertThat(programType.getDeclarationList().stream().map(Declaration::getName).collect(Collectors.toList()),
                   is(loadedTypes));
    }

    @DataProvider
    public static Object[][] providenceOK() {
        return new Object[][]{
                {"interface MyIF {\n" +
                 "  i32 id;\n" +
                 "}\n" +
                 "\n" +
                 "struct MyStruct implements MyIF {\n" +
                 "  1: i32 id;\n" +
                 "}\n" +
                 "\n" +
                 "union MyUnion of MyIF {\n" +
                 "  1: optional MyStruct value;\n" +
                 "}\n", listOf("MyIF", "MyStruct", "MyUnion")},
                };
    }

    @Test
    @UseDataProvider("providenceOK")
    public void testProvidenceOK(String content, List<String> loadedTypes) throws IOException {
        File          test        = writeContentTo(content, new File(tmp.getRoot(), "test.providence"));
        ProgramLoader loader      = new ProgramLoader();
        ProgramDeclaration   programType = loader.load(test).getProgramType();
        assertThat(programType.getDeclarationList().stream().map(Declaration::getName).collect(Collectors.toList()),
                   is(loadedTypes));
    }

    @DataProvider
    public static Object[][] providenceBad() {
        return new Object[][]{
                {"struct MyIF {\n" +
                 "  i32 id;\n" +
                 "}\n" +
                 "\n" +
                 "struct MyStruct implements MyIF {\n" +
                 "  1: i32 id;\n" +
                 "}\n",
                 "Error in test.providence on line 5 row 28-31: Bad implements type: MyIF is not an interface.\n" +
                 "struct MyStruct implements MyIF {\n" +
                 "---------------------------^^^^"},
                {"interface MyIF {\n" +
                 "  i32 id;\n" +
                 "}\n" +
                 "\n" +
                 "struct MyStruct {\n" +
                 "  1: i32 id;\n" +
                 "}\n" +
                 "\n" +
                 "union MyUnion of MyIF {\n" +
                 "  1: optional MyStruct value;\n" +
                 "}\n",
                 "Error in test.providence on line 10 row 15-22: Field 'value' in union test.MyUnion of MyIF does not implement required interface.\n" +
                 "  1: optional MyStruct value;\n" +
                 "--------------^^^^^^^^"},
                {"interface MyIF {\n" +
                 "  i32 id;\n" +
                 "}\n" +
                 "\n" +
                 "struct MyStruct {\n" +
                 "  1: i32 id;\n" +
                 "}\n" +
                 "\n" +
                 "union MyUnion of MyIF {\n" +
                 "  1: optional i32 value;\n" +
                 "}\n",
                 "Error in test.providence on line 10 row 15-17: Field value in union test.MyUnion of MyIF is not a message.\n" +
                 "  1: optional i32 value;\n" +
                 "--------------^^^"},
                };
    }

    @Test
    @UseDataProvider("providenceBad")
    public void testProvidenceBad(String content, String message) throws IOException {
        try {
            File          test   = writeContentTo(content, new File(tmp.getRoot(), "test.providence"));
            new ProgramLoader().load(test);
            fail("No exception");
        } catch (ThriftException e) {
            assertThat(e.displayString(), is(message));
        }
    }

    @Test
    public void testLoadServices() throws IOException {
        copyResourceTo("/parser/tests/service2.providence", tmp.getRoot());
        File service = copyResourceTo("/parser/tests/service.providence", tmp.getRoot());

        ProgramLoader loader = new ProgramLoader();

        ProgramRegistry reg = loader.load(service);
        assertThat(reg.getProgramContext(), is("service"));

        ProgramRegistry rep = loader.load(service);
        assertThat(rep, sameInstance(reg));

        File calc = tmp.newFolder("calculator");
        File math = tmp.newFolder("math");

        copyResourceTo("/parser/calculator/number.thrift", calc);
        File calculator = copyResourceTo("/parser/math/calculator.thrift", math);

        loader.load(calculator);
    }

    @Test
    public void testFailures() throws IOException {
        ProgramLoader loader = new ProgramLoader();

        File folder = tmp.newFolder("boo");
        try {
            loader.load(folder);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(),
                       is("Unable to load thrift program: " + folder.getCanonicalFile() + " is not a file."));
        }

        File noFile = new File(tmp.getRoot(), "boo");
        try {
            loader.load(noFile);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(),
                       is("Unable to load thrift program: " + noFile.getCanonicalFile() + " is not a file."));
        }

        File fail = copyResourceTo("/failure/duplicate_field_id.thrift", tmp.getRoot());
        try {
            loader.load(fail);
            fail("no exception");
        } catch (ThriftException e) {
            assertThat(e.getMessage(), is("Field with id 1 already exists on line 5"));
            assertThat(e.getFile(), is(fail.getName()));
        }
    }
}
