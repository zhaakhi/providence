package net.morimekta.providence.reflect.parser;

import net.morimekta.util.lexer.LexerException;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.fail;

public class ThriftTokenizerTest {
    @Test
    public void testJavaComments() throws IOException {
        try {
            tokenizer("/").parseNextToken();
            fail("no exception");
        } catch (LexerException e) {
            try {
                assertThat(e.displayString(), is(
                        "Error on line 1 row 2: Expected java-style comment, got end of file\n" +
                        "/\n" +
                        "-^"));
            } catch (AssertionError a) {
                a.initCause(e);
                throw a;
            }
        }

        try {
            tokenizer("/b").parseNextToken();
            fail("no exception");
        } catch (LexerException e) {
            assertThat(e.displayString(), is(
                    "Error on line 1 row 1-2: Expected java-style comment, got 'b' after '/'\n" +
                    "/b\n" +
                    "^^"));
        }

        ThriftToken token = tokenizer("\n\n// b\n" +
                                      "foo").parseNextToken();
        assertThat(token, is(notNullValue()));
        assertThat(token.toString(), is("foo"));
    }

    private ThriftTokenizer tokenizer(String data) {
        ByteArrayInputStream in = new ByteArrayInputStream(data.getBytes(StandardCharsets.UTF_8));
        return new ThriftTokenizer(in);
    }
}
