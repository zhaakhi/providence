package net.morimekta.providence.reflect.util;

import net.morimekta.providence.reflect.GlobalRegistry;
import net.morimekta.providence.reflect.parser.ThriftTokenizer;
import net.morimekta.providence.types.TypeReference;
import org.junit.Test;

import java.io.IOException;
import java.io.StringReader;

import static net.morimekta.util.collect.UnmodifiableList.listOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class ConstValueProviderTest {
    @Test
    public void testProvider() throws IOException {
        GlobalRegistry registry = new GlobalRegistry();
        ThriftTokenizer tokenizer = new ThriftTokenizer(new StringReader("\"value\""));
        ConstValueProvider provider = new ConstValueProvider(registry.registryForPath("prog.thrift"),
                                                             "prog",
                                                             TypeReference.parseType("string"),
                                                             listOf(tokenizer.parseNextToken()));

        assertThat(provider.get(), is("value"));
    }
}
