/*
 * Copyright (c) 2016, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.reflect.util;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import net.morimekta.util.collect.UnmodifiableList;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static net.morimekta.providence.reflect.util.ReflectionUtils.getFileName;
import static net.morimekta.providence.reflect.util.ReflectionUtils.getFileSuffix;
import static net.morimekta.providence.reflect.util.ReflectionUtils.isApacheThriftFile;
import static net.morimekta.providence.reflect.util.ReflectionUtils.isProvidenceFile;
import static net.morimekta.providence.reflect.util.ReflectionUtils.isThriftBasedFileSyntax;
import static net.morimekta.providence.reflect.util.ReflectionUtils.longestCommonPrefixPath;
import static net.morimekta.providence.reflect.util.ReflectionUtils.programNameFromPath;
import static net.morimekta.providence.reflect.util.ReflectionUtils.stripCommonPrefix;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Tests for the ReflectionUtil utiltiy class.
 */
@RunWith(DataProviderRunner.class)
public class ReflectionUtilTest {
    @DataProvider
    public static Object[][] metaFromPath() {
        return new Object[][]{
                {".foo", ".foo", "", false, false, ""},
                {".foo.thrift", ".foo", ".thrift", true, false, "_foo"},
                {"..foo.thrift", "..foo", ".thrift", true, false, "__foo"},
                {"foo.providence", "foo", ".providence", false, true, "foo"},
                {"/bar/baz/foo", "foo", "", false, false, ""},
                {"/bar/baz/foo.pvd", "foo", ".pvd", false, true, "foo"},
                {"/bar/baz/.foo.pvd", ".foo", ".pvd", false, true, "_foo"},
                {"/bar/baz/..foo.pvd", "..foo", ".pvd", false, true, "__foo"},

                {"/my/package/test.thrift", "test", ".thrift", true, false, "test"},
                {"my/package/test.thr", "test", ".thr", true, false, "test"},
                {"/my/package/test.providence", "test", ".providence", false, true, "test"},
                {"my/package/test.pvd", "test", ".pvd", false, true, "test"},
                {"../other/test.pvd", "test", ".pvd", false, true, "test"},
                {"test.thrift", "test", ".thrift", true, false, "test"},
                {"TEST.THRIFT", "TEST", ".THRIFT", true, false, "TEST"},

                {"../other/.pvd", ".pvd", "", false, false, ""},
                {".thrift", ".thrift", "", false, false, ""},
                {"/my/package/test.txt", "test", ".txt", false, false, ""},

        };
    }

    @Test
    @UseDataProvider("metaFromPath")
    public void testMetaFromPath(String path,
                                 String name,
                                 String suffix,
                                 boolean isApacheThrift,
                                 boolean isProvidence,
                                 String programName) {
        Path filePath = Paths.get(path);
        assertThat(getFileName(filePath), is(name));
        assertThat(getFileSuffix(filePath), is(suffix));

        assertThat(isApacheThriftFile(filePath), is(isApacheThrift));
        assertThat(isProvidenceFile(filePath), is(isProvidence));
        assertThat(isThriftBasedFileSyntax(filePath), is(isProvidence || isApacheThrift));
        assertThat(programNameFromPath(filePath), is(programName));
    }

    @Test
    public void testLongestCommonPrefix() {
        assertThat(longestCommonPrefixPath(UnmodifiableList.listOf("/abba/anne", "/abba/annika")),
                   is("/abba/"));
        assertThat(longestCommonPrefixPath(UnmodifiableList.listOf("/u2/bono", "/abba/annika")),
                   is("/"));
        assertThat(longestCommonPrefixPath(UnmodifiableList.listOf("../u2/bono", "../abba/annika")),
                   is("../"));
        assertThat(longestCommonPrefixPath(UnmodifiableList.listOf("foo/bar", "bar/foo")),
                   is(""));
    }

    @Test
    public void testStripCommonPrefix() {
        assertThat(stripCommonPrefix(UnmodifiableList.listOf("/abba/anne", "/abba/annika")),
                   is(UnmodifiableList.listOf("anne", "annika")));
        assertThat(stripCommonPrefix(UnmodifiableList.listOf("/u2/bono", "/abba/annika")),
                   is(UnmodifiableList.listOf("u2/bono", "abba/annika")));
        assertThat(stripCommonPrefix(UnmodifiableList.listOf("../u2/bono", "../abba/annika")),
                   is(UnmodifiableList.listOf("u2/bono", "abba/annika")));
        assertThat(stripCommonPrefix(UnmodifiableList.listOf("foo/bar", "bar/foo")),
                   is(UnmodifiableList.listOf("foo/bar", "bar/foo")));
    }

    @Test
    @SuppressWarnings("deprecation")
    public void testConstructor()
            throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Constructor<ReflectionUtils> constructor = ReflectionUtils.class.getDeclaredConstructor();
        assertThat(constructor.isAccessible(), is(false));
        try {
            constructor.setAccessible(true);
            ReflectionUtils instance = constructor.newInstance();
            assertThat(instance, is(notNullValue()));
        } finally {
            constructor.setAccessible(false);
        }
    }
}
