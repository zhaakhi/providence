#!/bin/sh

cd $(dirname $0)

mkdir -p target/minimal-jar/tmp
cd target/minimal-jar

cp ../../../providence-tools-config/target/providence-tools-config.jar .
cp ../../../providence-tools-converter/target/providence-tools-converter.jar .
cp ../../../providence-tools-generator/target/providence-tools-generator.jar .
cp ../../../providence-tools-schema/target/providence-tools-schema.jar .
cp ../../../providence-tools-rpc/target/providence-tools-rpc.jar .

for i in providence-tools-*.jar
do
  jar -tf ${i} || exit 1
done | grep '[.]class$' | sort | uniq -c | grep -e '5 ' | sed 's/.*[5] //' > common.txt

cd tmp

# use providence-tools-generator as source, as it it not minimized.
# This eliminates risk of stripped methods from classes.
echo "jar -xf ../providence-tools-generator.jar \$(cat ../common.txt)"
jar -xf ../providence-tools-generator.jar $(cat ../common.txt) || exit 1

echo "jar -cf ../providence-common.jar ."
jar -cf ../providence-common.jar . || exit 1

cd ..

for i in providence-tools-*.jar
do
  echo "zip -d ${i} \$(cat common.txt)"
  zip -d ${i} $(cat common.txt) > /dev/null || exit 1
done

rm common.txt
rm -rf tmp
