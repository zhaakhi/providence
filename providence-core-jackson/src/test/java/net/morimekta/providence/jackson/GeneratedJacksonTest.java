package net.morimekta.providence.jackson;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import net.morimekta.providence.PMessage;
import net.morimekta.providence.serializer.JsonSerializer;
import net.morimekta.providence.testing.junit4.GeneratorWatcher;
import net.morimekta.providence.testing.junit4.SimpleGeneratorWatcher;
import net.morimekta.test.providence.jackson.CompactFields;
import net.morimekta.test.providence.jackson.Containers;
import net.morimekta.test.providence.jackson.OptionalFields;
import net.morimekta.test.providence.jackson.UnionFields;
import net.morimekta.test.providence.jackson.Value;
import net.morimekta.util.Binary;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import static net.morimekta.providence.testing.EqualToMessage.equalToMessage;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Jackson serialization and seserialization testing.
 */
@RunWith(DataProviderRunner.class)
public class GeneratedJacksonTest {
    private static final ObjectMapper MAPPER = new ObjectMapper();
    static {
        // required to skip unknown fields.
        MAPPER.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        MAPPER.findAndRegisterModules();
    }

    @Rule
    public SimpleGeneratorWatcher generator = GeneratorWatcher.create();

    @DataProvider
    public static Object[][] dataDeserialize() {
        return new Object[][]{
                // Unknown fields are skipped, if allowed to.
                {"{\"foo\":{\"bar\":44},\"byteValue\":44}",
                 OptionalFields.builder().setByteValue((byte) 44).build()},
                };
    }

    @Test
    @UseDataProvider
    @SuppressWarnings("unchecked")
    public void testDeserialize(String content, PMessage expected) throws IOException {
        PMessage out = MAPPER.readValue(content, expected.getClass());
        assertThat(out, is(equalToMessage(expected)));
    }

    @DataProvider
    public static Object[][] dataDeserializationFail() {
        return new Object[][]{
                // Unknown fields are skipped, if allowed to.
                {"{\"foo\":{\"bar\":44},\"byteValue\":44}",
                 OptionalFields.class,
                 "Unrecognized field \"foo\" (class net.morimekta.test.providence.jackson.OptionalFields), not marked as ignorable\n" +
                 " at [Source: (String)\"{\"foo\":{\"bar\":44},\"byteValue\":44}\"; line: 1, column: 9] (through reference chain: net.morimekta.test.providence.jackson.OptionalFields[\"foo\"])"},
        };
    }

    @Test
    @UseDataProvider
    public void testDeserializationFail(String content, Class<PMessage> type, String message) throws IOException {
        ObjectMapper MAPPER = new ObjectMapper();
        // required to skip unknown fields.
        // And not applied: MAPPER.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        MAPPER.findAndRegisterModules();

        try {
            MAPPER.readValue(content, type);
        } catch (JsonProcessingException e) {
            assertThat(e.getMessage(), is(message));
        }
    }

    @DataProvider
    public static Object[][] dataSerialization() {
        return new Object[][]{
                {"[\"foo\",3]", new CompactFields("foo", 3, null)},
                {"{}", OptionalFields.builder().build()},
                {"{" +
                 "\"booleanValue\":true," +
                 "\"byteValue\":64," +
                 "\"shortValue\":12345," +
                 "\"integerValue\":1234567890," +
                 "\"longValue\":1234567890123456789," +
                 "\"doubleValue\":1.23456789012345E9," +
                 "\"stringValue\":\"Ûñı©óð€\"," +
                 "\"binaryValue\":\"AAECAwQFBgcICQA\"," +
                 "\"enumValue\":1," +
                 "\"compactValue\":[\"Test\",4]" +
                 "}",
                 new OptionalFields(true,
                                    (byte) 64,
                                    (short) 12345,
                                    1234567890,
                                    1234567890123456789L,
                                    1234567890.12345,
                                    "Ûñı©óð€",
                                    Binary.wrap(new byte[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0}),
                                    Value.FIRST,
                                    new CompactFields("Test", 4, null))},
                {"{\"booleanValue\":true}",
                 UnionFields.withBooleanValue(true)},
                {"{\"compactValue\":[\"test\",4]}",
                 UnionFields.withCompactValue(new CompactFields("test", 4, null))},
                {"{\"binaryValue\":\"AAECAwQFBgcICQA\"}",
                 UnionFields.withBinaryValue(Binary.fromBase64("AAECAwQFBgcICQA"))},
        };
    }

    @Test
    @UseDataProvider
    @SuppressWarnings("unchecked")
    public void testSerialization(String expectedContent, PMessage message) throws IOException {
        String content = MAPPER.writeValueAsString(message);
        assertThat(content, is(expectedContent));
        PMessage out = MAPPER.readValue(content, message.getClass());
        assertThat(out, is(equalToMessage(message)));

        List<PMessage> collection = new ArrayList<>();
        collection.add(message);
        collection.add(message);

        String listContent = MAPPER.writeValueAsString(collection);
        assertThat(listContent, is("[" + content + "," + content + "]"));
        JavaType listType = MAPPER.getTypeFactory().constructCollectionType(ArrayList.class, message.getClass());
        List<PMessage> back = MAPPER.readValue(listContent, listType);
        assertThat(back, is(collection));
    }

    @Test
    public void testJsonSerializerCompat() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        ProvidenceModule.register(mapper);

        JsonSerializer compact = new JsonSerializer();
        JsonSerializer pretty = new JsonSerializer().pretty();

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        for (int i = 0; i < 100; ++i) {
            Containers containers = generator.generate(Containers.kDescriptor);

            out.reset();
            compact.serialize(out, containers);
            Containers res = mapper.readValue(out.toByteArray(), Containers.class);
            assertThat(res, is(equalToMessage(containers)));

            out.reset();
            pretty.serialize(out, containers);
            res = mapper.readValue(out.toByteArray(), Containers.class);
            assertThat(res, is(equalToMessage(containers)));
        }
    }

    @Test
    public void testSerializable() throws IOException, ClassNotFoundException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);

        OptionalFields original = generator.generate(OptionalFields.kDescriptor);

        oos.writeObject(original);
        oos.close();

        ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
        ObjectInputStream in = new ObjectInputStream(bais);

        OptionalFields actual = (OptionalFields) in.readObject();

        assertThat(actual, is(equalToMessage(original)));
    }
}
