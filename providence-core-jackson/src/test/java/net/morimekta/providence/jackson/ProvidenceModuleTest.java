package net.morimekta.providence.jackson;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.morimekta.test.providence.jackson.client.Request;
import net.morimekta.util.Binary;
import org.junit.Test;

import java.io.IOException;

import static net.morimekta.util.collect.UnmodifiableList.listOf;
import static net.morimekta.util.collect.UnmodifiableMap.mapOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class ProvidenceModuleTest {
    @Test
    public void testProvidenceModule_ServiceProvider() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.findAndRegisterModules();

        Request src = new Request(Binary.fromHexString("ABCDEF"),
                                  "ABCDEF",
                                  listOf(Binary.fromBase64("2VjwBLYvTR6HkyjQY5rvZw")),
                                  mapOf(Binary.fromBase64("5S+zeblRQF6kV5ScoTtSxA"),
                                                  Binary.fromBase64("LlWY54WoTpOonW9sWnM7Kw")));

        String tmp = mapper.writerFor(Request.class).writeValueAsString(src);

        assertThat(tmp, is(
                "{\"the_binary\":\"q83v\"," +
                "\"not_binary\":\"ABCDEF\"," +
                "\"list_of\":[\"2VjwBLYvTR6HkyjQY5rvZw\"]," +
                "\"map_of\":{\"5S+zeblRQF6kV5ScoTtSxA\":\"LlWY54WoTpOonW9sWnM7Kw\"}}"));

        Request req = mapper.readerFor(Request.class).readValue(tmp);
        assertThat(req, is(src));
    }

    @Test
    public void testProvidenceModule_ManualRegister() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        ProvidenceModule.register(mapper);

        Request src = new Request(Binary.fromHexString("ABCDEF"),
                                  "ABCDEF",
                                  listOf(Binary.fromBase64("2VjwBLYvTR6HkyjQY5rvZw")),
                                  mapOf(Binary.fromBase64("5S+zeblRQF6kV5ScoTtSxA"),
                                                  Binary.fromBase64("LlWY54WoTpOonW9sWnM7Kw")));

        String tmp = mapper.writerFor(Request.class).writeValueAsString(src);

        assertThat(tmp, is(
                "{\"the_binary\":\"q83v\"," +
                "\"not_binary\":\"ABCDEF\"," +
                "\"list_of\":[\"2VjwBLYvTR6HkyjQY5rvZw\"]," +
                "\"map_of\":{\"5S+zeblRQF6kV5ScoTtSxA\":\"LlWY54WoTpOonW9sWnM7Kw\"}}"));

        Request req = mapper.readerFor(Request.class).readValue(tmp);
        assertThat(req, is(src));
    }
}
