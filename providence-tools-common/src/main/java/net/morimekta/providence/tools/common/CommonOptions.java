/*
 * Copyright (c) 2016, Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.morimekta.providence.tools.common;

import net.morimekta.console.args.ArgumentOptions;
import net.morimekta.console.args.ArgumentParser;
import net.morimekta.console.args.Flag;
import net.morimekta.console.args.Option;
import net.morimekta.console.util.Parser;
import net.morimekta.console.util.STTY;
import net.morimekta.providence.config.ConfigLoader;
import net.morimekta.providence.config.parser.ConfigException;
import net.morimekta.providence.types.SimpleTypeRegistry;
import net.morimekta.util.collect.UnmodifiableList;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Options used by the providence converter.
 */
public class CommonOptions {
    private   boolean help;
    private   boolean verbose;
    private   boolean version;
    protected STTY    tty;
    private   Path    rc = Paths.get(System.getenv("HOME"), ".pvdrc");

    public CommonOptions(STTY tty) {
        this.tty = tty;
    }

    protected ArgumentOptions getArgumentOptions() {
        return ArgumentOptions.defaults(tty).withMaxUsageWidth(120);
    }

    public ArgumentParser getArgumentParser(String prog, String description) throws IOException {
        ArgumentParser parser = new ArgumentParser(prog, Utils.getVersionString(), description, getArgumentOptions());

        parser.add(new Flag("--help", "h?", "This help listing.", this::setHelp));
        parser.add(new Flag("--verbose", "V", "Show verbose output and error messages.", this::setVerbose));
        parser.add(new Flag("--version", "v", "Show program version.", this::setVersion));
        parser.add(new Option("--rc", null, "FILE", "Providence RC to use",
                              Parser.filePath(this::setRc), "~" + File.separator + ".pvdrc"));

        return parser;
    }

    public boolean showHelp() {
        return help;
    }
    public boolean showVersion() {
        return version;
    }
    public boolean verbose() {
        return verbose;
    }
    public Path getRc() {
        return rc;
    }
    public ProvidenceTools getConfig() throws ConfigException {
        SimpleTypeRegistry registry = new SimpleTypeRegistry();
        registry.registerType(ProvidenceTools.kDescriptor);
        ConfigLoader loader = new ConfigLoader(registry, warning -> {
            if (verbose) {
                System.err.println(warning.displayString());
            }
        });

        ProvidenceTools config = ProvidenceTools.builder()
                                                .setGeneratorPaths(UnmodifiableList.listOf())
                                                .build();
        if (Files.exists(rc)) {
            config = loader.getConfig(rc, config);
        }
        return config;
    }
    private void setHelp(boolean help) {
        this.help = help;
    }
    private void setVersion(boolean version) {
        this.version = version;
    }
    private void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }
    protected void setRc(Path file) {
        this.rc = file;
    }
}
