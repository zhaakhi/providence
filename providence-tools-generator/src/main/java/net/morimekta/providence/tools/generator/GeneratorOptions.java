/*
 * Copyright (c) 2017, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.tools.generator;

import net.morimekta.console.args.Argument;
import net.morimekta.console.args.ArgumentException;
import net.morimekta.console.args.ArgumentOptions;
import net.morimekta.console.args.ArgumentParser;
import net.morimekta.console.args.Flag;
import net.morimekta.console.args.Option;
import net.morimekta.console.util.STTY;
import net.morimekta.providence.config.parser.ConfigException;
import net.morimekta.providence.config.util.UncheckedConfigException;
import net.morimekta.providence.generator.Generator;
import net.morimekta.providence.generator.GeneratorException;
import net.morimekta.providence.generator.GeneratorFactory;
import net.morimekta.providence.generator.format.json.JsonGeneratorFactory;
import net.morimekta.providence.generator.util.FactoryLoader;
import net.morimekta.providence.generator.util.FileManager;
import net.morimekta.providence.reflect.ProgramLoader;
import net.morimekta.providence.serializer.JsonSerializer;
import net.morimekta.providence.serializer.PrettySerializer;
import net.morimekta.providence.tools.common.CommonOptions;
import net.morimekta.providence.tools.common.ProvidenceTools;
import net.morimekta.providence.tools.common.Utils;
import net.morimekta.providence.tools.generator.options.GeneratorSpec;
import net.morimekta.providence.tools.generator.options.GeneratorSpecParser;
import net.morimekta.providence.tools.generator.options.HelpOption;
import net.morimekta.providence.tools.generator.options.HelpSpec;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import static net.morimekta.console.util.Parser.filePath;
import static net.morimekta.console.util.Parser.outputDir;

/**
 * Options helper for the generator binary.
 */
@SuppressWarnings("all")
public class GeneratorOptions extends CommonOptions {
    protected File          out                        = new File(".");
    protected HelpSpec      help                       = null;
    protected GeneratorSpec gen                        = null;
    protected List<Path>    files                      = new ArrayList<>();
    protected List<Path>    extraGenerators            = new ArrayList<>();
    protected boolean       version                    = false;
    protected boolean       verbose                    = false;
    protected boolean       requireEnumValue           = false;
    protected boolean       requireFieldId             = false;
    protected boolean       allowLanguageReservedNames = true;
    protected boolean       skipIfMissingNamespace     = false;

    public GeneratorOptions(STTY tty) {
        super(tty);
    }

    public ArgumentParser getArgumentParser(String prog, String description) throws IOException {
        ArgumentOptions opts   = ArgumentOptions.defaults(tty).withMaxUsageWidth(120);
        ArgumentParser  parser = new ArgumentParser(prog, Utils.getVersionString(), description, opts);

        parser.add(new Option("--gen", "g", "generator", "Generate files for this language spec.",
                              new GeneratorSpecParser(this::getFactories).andApply(this::setGenerator)));
        parser.add(new HelpOption("--help",
                                  "h?",
                                  "Show this help or about language.",
                                  this::getFactories,
                                  this::setHelp));
        parser.add(new Flag("--verbose", "V", "Show verbose output and error messages.", this::setVerbose));
        parser.add(new Flag("--version", "v", "Show program version.", this::setVersion));
        parser.add(new Option("--rc",
                              null,
                              "FILE",
                              "Providence RC to use",
                              filePath(this::setRc),
                              "~" + File.separator + ".pvdrc"));
        parser.add(new Option("--out", "o", "dir", "Output directory", outputDir(this::setOut), "${PWD}"));
        parser.add(new Flag("--require-field-id",
                            null,
                            "Require all fields to have a defined ID",
                            this::setRequireFieldId));
        parser.add(new Flag("--require-enum-value",
                            null,
                            "Require all enum values to have a defined ID",
                            this::setRequireEnumValue));
        parser.add(new Flag("--allow-language-reserved-names",
                            null,
                            "Allow language-reserved words in type names",
                            this::setAllowLanguageReservedNames,
                            true));
        parser.add(new Flag("--no-language-reserved-names",
                            null,
                            "Do not allow language-reserved words in type names",
                            b -> this.setAllowLanguageReservedNames(!b)));
        parser.add(new Flag("--skip-if-missing-namespace",
                            "N",
                            "Skip generation for files without requested namespace",
                            this::setSkipIfMissingNamespace));
        parser.add(new Option("--add-generator",
                              null,
                              "FILE",
                              "Add extra generator .jar file",
                              filePath(extraGenerators::add)));
        parser.add(new Argument("file", "Files to compile.", filePath(this::addFile), null, null, true, true, false));

        return parser;
    }

    private void setSkipIfMissingNamespace(boolean skip) {
        this.skipIfMissingNamespace = skip;
    }

    public File currentJarDirectory() {
        try {
            ProvidenceTools config = getConfig();
            URL             url    = getClass().getProtectionDomain().getCodeSource().getLocation();

            if ("file".equals(url.getProtocol())) {
                String path = url.getPath();
                if (path.endsWith(".jar")) {
                    return new File(path).getParentFile();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private void addToMap(Map<String, GeneratorFactory> factoryMap, List<GeneratorFactory> factories) {
        for (GeneratorFactory factory : factories) {
            factoryMap.put(factory.generatorName(), factory);
        }
    }

    public Map<String, GeneratorFactory> getFactories() {
        try {
            FactoryLoader<GeneratorFactory> loader    = new FactoryLoader<>(GeneratorFactory.MANIFEST_PROPERTY);
            Map<String, GeneratorFactory>   factories = new TreeMap<>();
            factories.put("json", new JsonGeneratorFactory());

            File currentJarDir = currentJarDirectory();
            if (currentJarDir != null) {
                File generators = new File(currentJarDir, "generator");
                if (generators.isDirectory()) {
                    addToMap(factories, loader.getFactories(generators));
                }
            }

            getLocalGenerators(currentJarDir, factories, loader);
            getEtcGenerators(currentJarDir, factories, loader);
            getConfigGenerators(factories, loader);

            for (Path extra : extraGenerators) {
                GeneratorFactory factory = loader.getFactory(extra.toFile());
                factories.put(factory.generatorName(), factory);
            }

            return factories;
        } catch (ConfigException e) {
            throw new UncheckedConfigException(e);
        } catch (IOException e) {
            throw new UncheckedIOException(e.getMessage(), e);
        }
    }

    protected void getLocalGenerators(File currentJarDir,
                                      Map<String, GeneratorFactory> factories,
                                      FactoryLoader<GeneratorFactory> loader) {
        if (currentJarDir != null) {
            File generators = new File(currentJarDir, "generator");
            if (generators.isDirectory()) {
                addToMap(factories, loader.getFactories(generators));
            }
        }
    }

    protected void getEtcGenerators(File currentJarDir,
                                    Map<String, GeneratorFactory> factories,
                                    FactoryLoader<GeneratorFactory> loader) throws IOException {
        ArrayList<Path> paths = new ArrayList<>();
        paths.add(Paths.get("/etc/providence"));
        if (currentJarDir != null) {
            String name = currentJarDir.getAbsoluteFile().getCanonicalPath();
            if (name.contains("/Cellar/providence/")) {
                // we are in brew land, and need to check the special
                // ${BREW_HOME}/etc/providence
                name = name.substring(0, name.indexOf("/Cellar/providence/"));
                paths.add(Paths.get(name, "etc", "providence"));
            }
        }

        for (Path etc : paths) {
            if (Files.isDirectory(etc)) {
                Files.list(etc).forEach(file -> {
                    String name = file.getFileName().toString().toLowerCase();
                    ProvidenceTools cfg = null;
                    if (name.endsWith(".json")) {
                        try (InputStream in = new FileInputStream(file.toFile())) {
                            cfg = JSON.deserialize(in, ProvidenceTools.kDescriptor);
                        } catch (IOException e) {
                            throw new UncheckedIOException(e.getMessage(), e);
                        }
                    } else if (name.endsWith(".cfg") || name.endsWith(".config")) {
                        try (InputStream in = new FileInputStream(file.toFile())) {
                            cfg = CFG.deserialize(in, ProvidenceTools.kDescriptor);
                        } catch (IOException e) {
                            throw new UncheckedIOException(e.getMessage(), e);
                        }
                    } else {
                        return;
                    }

                    for (String path : cfg.getGeneratorPaths()) {
                        Path tmp = Paths.get(path);
                        if (Files.isRegularFile(tmp)) {
                            GeneratorFactory factory = loader.getFactory(tmp.toFile());
                            factories.put(factory.generatorName(), factory);
                        } else if (Files.isDirectory(tmp)) {
                            addToMap(factories, loader.getFactories(tmp.toFile()));
                        }
                    }
                });
            }
        }
    }

    protected void getConfigGenerators(Map<String, GeneratorFactory> factories,
                                       FactoryLoader<GeneratorFactory> loader) throws IOException {
        ProvidenceTools config = getConfig();
        for (String path : config.getGeneratorPaths()) {
            Path tmp = Paths.get(path);
            if (Files.isRegularFile(tmp)) {
                GeneratorFactory factory = loader.getFactory(tmp.toFile());
                factories.put(factory.generatorName(), factory);
            } else if (Files.isDirectory(tmp)) {
                addToMap(factories, loader.getFactories(tmp.toFile()));
            }
        }
    }

    private void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }
    private void setVersion(boolean version) {
        this.version = version;
    }
    private void setRequireEnumValue(boolean requireEnumValue) {
        this.requireEnumValue = requireEnumValue;
    }
    private void setRequireFieldId(boolean requireFieldId) {
        this.requireFieldId = requireFieldId;
    }
    private void setAllowLanguageReservedNames(boolean allowLanguageReservedNames) {
        this.allowLanguageReservedNames = allowLanguageReservedNames;
    }

    public void setOut(File out) {
        this.out = out;
    }

    public void setHelp(HelpSpec help) {
        this.help = help;
    }

    public void setGenerator(GeneratorSpec gen) {
        this.gen = gen;
    }

    public void addFile(Path files) {
        this.files.add(files);
    }

    public boolean isHelp() {
        return help != null;
    }

    public List<Path> getInputFiles() throws ArgumentException {
        return files;
    }

    public FileManager getFileManager() throws ArgumentException {
        if (!out.exists()) {
            if (!out.mkdirs()) {
                throw new ArgumentException("Unable to create directory %s", out.toString());
            }
        }
        return new FileManager(out.toPath());
    }

    public Generator getGenerator(ProgramLoader loader) throws ArgumentException, GeneratorException, IOException {
        net.morimekta.providence.generator.GeneratorOptions generatorOptions = new net.morimekta.providence.generator.GeneratorOptions();
        generatorOptions.generator_program_name = "pvdgen";
        generatorOptions.program_version = Utils.getVersionString();

        try {
            return gen.factory.createGenerator(getFileManager(), generatorOptions, gen.options);
        } catch (GeneratorException e) {
            throw new ArgumentException(e, e.getMessage());
        }
    }

    private static final JsonSerializer JSON = new JsonSerializer().pretty();
    private static final PrettySerializer CFG = new PrettySerializer();
}
