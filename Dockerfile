FROM java:openjdk-8-jre-alpine

RUN apk add --no-cache curl bash

COPY providence-tools/src/deb/bin/*             /usr/bin/
COPY providence-tools/target/minimal-jar/*.jar  /usr/share/providence/
COPY providence-generator-java/target/java.jar  /usr/share/providence/generator/

ENTRYPOINT ["/bin/bash"]
CMD []
