package net.morimekta.hazelcast.it;

import net.morimekta.test.hazelcast.v2.PortableFields;
import net.morimekta.test.hazelcast.v2.PortableListFields;
import net.morimekta.test.hazelcast.v2.PortableMapFields;
import net.morimekta.test.hazelcast.v2.PortableMapListFields;
import net.morimekta.test.hazelcast.v2.PortableMapSetFields;
import net.morimekta.test.hazelcast.v2.PortableSetFields;

import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * TBD
 */
public class HazelcastVersion2Test extends GenericMethods {
    private static HazelcastInstance instance1;
    private static HazelcastInstance instance2;

    @BeforeClass
    public static void setUpHazelcast() {
        instance1 = Hazelcast.newHazelcastInstance(getV2Config());
        instance2 = Hazelcast.newHazelcastInstance(getV2Config());
    }

    @AfterClass
    public static void tearDownHazelcast() {
        instance1.shutdown();
        instance2.shutdown();
    }

    @Test
    public void testMapIntegrityAll() {
        generator.context().setFillRate(1.0);
        assertMapIntegrity(instance1, instance2, PortableFields.kDescriptor);
    }

    @Test
    public void testMapIntegrityAll_list() {
        assertMapIntegrity(instance1, instance2, PortableListFields.kDescriptor);
    }

    @Test
    public void testMapIntegrityAll_set() {
        assertMapIntegrity(instance1, instance2, PortableSetFields.kDescriptor);
    }

    @Test
    public void testMapIntegrityAll_map() {
        assertMapIntegrity(instance1, instance2, PortableMapFields.kDescriptor);
    }

    @Test
    public void testMapIntegrityAll_maplist() {
        assertMapIntegrity(instance1, instance2, PortableMapListFields.kDescriptor);
    }

    @Test
    public void testMapIntegrityAll_mapset() {
        assertMapIntegrity(instance1, instance2, PortableMapSetFields.kDescriptor);
    }

    @Test
    public void testMapIntegrityRand() {
        generator.context().setFillRate(0.5);
        assertMapIntegrity(instance1, instance2, PortableFields.kDescriptor);
    }

    @Test
    public void testMapIntegrityRand_list() {
        generator.context().setFillRate(0.5);
        assertMapIntegrity(instance1, instance2, PortableListFields.kDescriptor);
    }

    @Test
    public void testMapIntegrityRand_set() {
        generator.context().setFillRate(0.5);
        assertMapIntegrity(instance1, instance2, PortableSetFields.kDescriptor);
    }

    @Test
    public void testMapIntegrityRand_map() {
        generator.context().setFillRate(0.5);
        assertMapIntegrity(instance1, instance2, PortableMapFields.kDescriptor);
    }

    @Test
    public void testMapIntegrityRand_maplist() {
        generator.context().setFillRate(0.5);
        assertMapIntegrity(instance1, instance2, PortableMapListFields.kDescriptor);
    }

    @Test
    public void testMapIntegrityRand_mapset() {
        generator.context().setFillRate(0.5);
        assertMapIntegrity(instance1, instance2, PortableMapSetFields.kDescriptor);
    }
}
