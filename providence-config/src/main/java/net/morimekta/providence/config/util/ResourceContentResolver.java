package net.morimekta.providence.config.util;

import javax.annotation.Nonnull;
import javax.annotation.WillNotClose;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ResourceContentResolver implements ContentResolver {
    @Nonnull
    @Override
    public Path canonical(Path file) {
        if (!file.isAbsolute()) {
            file = Paths.get("/" + file.toString());
        }
        file = file.normalize();
        try (InputStream tmp = ResourceContentResolver.class.getResourceAsStream(file.toString())) {
            if (tmp == null) {
                throw new FileNotFoundException(file.toString());
            }
        } catch (IOException ignore) {}
        return file;
    }

    @Nonnull
    @Override
    public Path reference(Path referenceLocation, Path from, String reference) {
        return from.getParent().resolve(reference).normalize();
    }

    @Nonnull
    @Override
    public Path referenceLocationPath(Path referenceLocation, Path from, String reference) {
        return reference(referenceLocation, from, reference).getParent();
    }

    @Nonnull
    @Override
    @WillNotClose
    public InputStream open(Path file) {
        return ResourceContentResolver.class.getResourceAsStream(canonical(file).toString());
    }
}
