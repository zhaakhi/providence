/*
 * Copyright 2017 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.config.parser;

import net.morimekta.providence.serializer.JsonSerializerException;
import net.morimekta.providence.serializer.pretty.PrettyException;
import net.morimekta.util.lexer.LexerException;

import javax.annotation.Nonnull;
import java.util.Objects;

/**
 * Providence config exceptions are extensions of the serializer exception (as
 * parsing config can be seen as parsing or de-serializing any serialized
 * message).
 */
public class ConfigException extends LexerException {
    protected String file;

    public ConfigException(String format, Object... args) {
        super(args.length == 0 ? format : String.format(format, args));
    }

    public ConfigException(@Nonnull PrettyException cause) {
        super(Objects.requireNonNull(cause.getLine()),
              cause.getLineNo(),
              cause.getLinePos(),
              cause.getLength(),
              cause.getMessage());
        initCause(cause);
    }

    public ConfigException(@Nonnull JsonSerializerException cause) {
        super(Objects.requireNonNull(cause.getLine()),
              cause.getLineNo(),
              cause.getLinePos(),
              cause.getLen(),
              cause.getMessage());
        initCause(cause);
    }

    public ConfigException(Throwable cause, String format, Object... args) {
        super(cause, args.length == 0 ? format : String.format(format, args));
    }

    public ConfigException(ConfigToken token, String format, Object... args) {
        super(token, args.length == 0 ? format : String.format(format, args));
    }

    public ConfigException setFile(String file) {
        this.file = file;
        return this;
    }

    public String getFile() {
        return file;
    }

    @Override
    protected String getError() {
        if (file != null) {
            return "Error in " + file;
        }
        return "Error";
    }

    @Nonnull
    @Override
    public ConfigException initCause(Throwable cause) {
        return (ConfigException) super.initCause(cause);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "\n" + displayString();
    }
}
