package net.morimekta.providence.config.parser;

import net.morimekta.util.lexer.Lexer;

import javax.annotation.Nullable;
import javax.annotation.WillNotClose;
import java.io.IOException;
import java.io.Reader;

public class ConfigLexer extends Lexer<ConfigTokenType, ConfigToken> {
    public ConfigLexer(@WillNotClose Reader in) {
        super(new ConfigTokenizer(in));
    }

    @Nullable
    public ConfigToken readBinary(char term) throws IOException {
        String end = new String(new char[]{term});
        return readUntil(end, ConfigTokenType.BINARY, false);
    }
}
