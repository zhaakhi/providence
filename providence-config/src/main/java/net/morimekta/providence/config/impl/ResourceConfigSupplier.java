/*
 * Copyright 2016,2017 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.config.impl;

import net.morimekta.providence.PMessage;
import net.morimekta.providence.config.parser.ConfigException;
import net.morimekta.providence.config.parser.ConfigParser;

import javax.annotation.Nonnull;
import java.nio.file.Path;
import java.time.Clock;

/**
 * A supplier to get a config (aka message) from a providence config. This is
 * essentially the initiator for the config. It will always have a config
 * message instance, and will log (error) if it later fails to load an updated
 * config.
 */
public class ResourceConfigSupplier<Message extends PMessage<Message>>
        extends FixedConfigSupplier<Message> {
    private final Path resourcePath;

    public ResourceConfigSupplier(@Nonnull Path resourcePath,
                                  @Nonnull ConfigParser configParser,
                                  @Nonnull Clock clock)
            throws ConfigException {
        super(configParser.<Message>parseConfig(resourcePath, null).first,
              clock.millis(), clock);
        this.resourcePath = resourcePath;
    }

    @Override
    public String toString() {
        return "ResourceConfig{" + resourcePath + "}";
    }

    @Override
    public String getName() {
        return "ResourceConfig{" + resourcePath.getFileName() + "}";
    }
}
