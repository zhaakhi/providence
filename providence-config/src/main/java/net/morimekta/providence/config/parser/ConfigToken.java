package net.morimekta.providence.config.parser;

import net.morimekta.util.lexer.Token;

import javax.annotation.Nonnull;
import java.util.regex.Pattern;

public class ConfigToken extends Token<ConfigTokenType> {
    // Various symbols.
    public static final char kMessageStart  = '{';
    public static final char kMessageEnd    = '}';
    public static final char kKeyValueSep   = ':';
    public static final char kFieldValueSep = '=';
    public static final char kParamsStart   = '(';
    public static final char kParamsEnd     = ')';
    public static final char kListStart     = '[';
    public static final char kListEnd       = ']';
    public static final char kEntrySep      = ',';
    public static final char kLineSep       = ';';

    // Not really 'symbols'.
    public static final char kIdentifierSep      = '.';

    public static final String B64 = "b64";
    public static final String HEX = "hex";

    private static final Pattern RE_IDENTIFIER           = Pattern.compile(
            "[_a-zA-Z][_a-zA-Z0-9]*");
    private static final Pattern RE_QUALIFIED_IDENTIFIER = Pattern.compile(
            "[_a-zA-Z][_a-zA-Z0-9]*[.][_a-zA-Z][_a-zA-Z0-9]*");
    private static final Pattern RE_DOUBLE_QUALIFIED_IDENTIFIER = Pattern.compile(
            "[_a-zA-Z][_a-zA-Z0-9]*[.][_a-zA-Z][_a-zA-Z0-9]*[.][_a-zA-Z][_a-zA-Z0-9]*");
    private static final Pattern RE_REFERENCE_IDENTIFIER = Pattern.compile(
            "[_a-zA-Z][_a-zA-Z0-9]*([.][_a-zA-Z][_a-zA-Z0-9]*)*");
    private static final Pattern RE_INTEGER              = Pattern.compile(
            "-?(0|[1-9][0-9]*|0[0-7]+|0x[0-9a-fA-F]+)");
    private static final Pattern RE_REAL                 = Pattern.compile(
            "-?(0?\\.[0-9]+|[1-9][0-9]*\\.[0-9]*)([eE][+-]?[0-9][0-9]*)?");

    /**
     * Create a slice instance. The slice is only meant to be internal state
     * immutable, and not representing an immutable byte content.
     *
     * @param fb      The buffer to wrap.
     * @param off     The start offset to wrap.
     * @param len     The length to represent.
     * @param type    The token type represented.
     * @param lineNo  The current line number.
     * @param linePos The current line position.
     */
    public ConfigToken(char[] fb,
                       int off,
                       int len,
                       @Nonnull ConfigTokenType type, int lineNo, int linePos) {
        super(fb, off, len, type, lineNo, linePos);
    }

    public boolean isString() {
        return type == ConfigTokenType.STRING;
    }

    public boolean isIdentifier() {
        return RE_IDENTIFIER.matcher(this).matches();
    }

    public boolean isDoubleQualifiedIdentifier() {
        return RE_DOUBLE_QUALIFIED_IDENTIFIER.matcher(this).matches();
    }

    public boolean isQualifiedIdentifier() {
        return RE_QUALIFIED_IDENTIFIER.matcher(this).matches();
    }

    public boolean isReferenceIdentifier() {
        return RE_REFERENCE_IDENTIFIER.matcher(this).matches();
    }

    public boolean isInteger() {
        return RE_INTEGER.matcher(this)
                         .matches();
    }

    public boolean isReal() {
        return RE_REAL.matcher(this)
                      .matches();
    }
}
