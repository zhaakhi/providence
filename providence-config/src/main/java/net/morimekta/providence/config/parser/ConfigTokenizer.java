package net.morimekta.providence.config.parser;

import net.morimekta.util.lexer.TokenizerBase;

import javax.annotation.Nonnull;
import java.io.Reader;

public class ConfigTokenizer extends TokenizerBase<ConfigTokenType, ConfigToken> {
    ConfigTokenizer(@Nonnull Reader in) {
        super(in, TokenizerBase.DEFAULT_BUFFER_SIZE, true);
    }

    @Override
    protected ConfigToken genericToken(char[] buffer,
                                       int offset,
                                       int len,
                                       @Nonnull ConfigTokenType type,
                                       int lineNo,
                                       int linePos) {
        return new ConfigToken(buffer, offset, len, type, lineNo, linePos);
    }

    @Override
    protected ConfigToken identifierToken(char[] buffer, int offset, int len, int lineNo, int linePos) {
        return new ConfigToken(buffer, offset, len, ConfigTokenType.IDENTIFIER, lineNo, linePos);
    }

    @Override
    protected ConfigToken stringToken(char[] buffer, int offset, int len, int lineNo, int linePos) {
        return new ConfigToken(buffer, offset, len, ConfigTokenType.STRING, lineNo, linePos);
    }

    @Override
    protected ConfigToken numberToken(char[] buffer, int offset, int len, int lineNo, int linePos) {
        return new ConfigToken(buffer, offset, len, ConfigTokenType.NUMBER, lineNo, linePos);
    }

    @Override
    protected ConfigToken symbolToken(char[] buffer, int offset, int len, int lineNo, int linePos) {
        return new ConfigToken(buffer, offset, len, ConfigTokenType.SYMBOL, lineNo, linePos);
    }

    @Override
    protected boolean startString() {
        return lastChar == '\"' || lastChar == '\'';
    }
}
