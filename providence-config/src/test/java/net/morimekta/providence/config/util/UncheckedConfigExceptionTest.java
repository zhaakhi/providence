package net.morimekta.providence.config.util;

import net.morimekta.providence.config.parser.ConfigException;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class UncheckedConfigExceptionTest {
    @Test
    public void testException() {
        ConfigException          e  = new ConfigException("Test");
        UncheckedConfigException ue = new UncheckedConfigException(e);

        assertThat(ue.getMessage(), is(e.getMessage()));
        assertThat(ue.displayString(), is(e.displayString()));
        assertThat(ue.toString(), is(e.toString()));
    }
}
