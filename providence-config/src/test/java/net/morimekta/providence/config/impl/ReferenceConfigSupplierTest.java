package net.morimekta.providence.config.impl;

import net.morimekta.providence.config.ConfigSupplier;
import net.morimekta.providence.config.parser.ConfigException;
import net.morimekta.providence.config.testing.TestConfigSupplier;
import net.morimekta.test.providence.config.Service;
import net.morimekta.test.providence.config.ServicePort;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class ReferenceConfigSupplierTest {
    @Test
    public void testReferenceConfig() throws ConfigException {
        TestConfigSupplier<Service> parent = new TestConfigSupplier<>(
                Service.builder()
                       .setAdmin(ServicePort.builder()
                                            .setPort((short) 1234)
                                            .build())
                       .build());

        ConfigSupplier<ServicePort> ref = parent.reference(Service._Field.ADMIN);

        assertThat(ref.get(), is(notNullValue()));
        assertThat(ref.get().getPort(), is((short) 1234));

        parent.testUpdate(Service.builder()
                                 .setAdmin(ServicePort.builder()
                                                      .setPort((short) 4321)
                                                      .build())
                                 .build());

        assertThat(ref.get(), is(notNullValue()));
        assertThat(ref.get().getPort(), is((short) 4321));

        parent.testUpdate(Service.builder()
                                 .setHttp(ServicePort.builder()
                                                     .setPort((short) 8080)
                                                     .build())
                                 .build());

        assertThat(ref.get(), is(notNullValue()));
        assertThat(ref.get().getPort(), is((short) 4321));  // not updated as that failed.

        assertThat(ref.getName(), is("ReferenceConfig{admin}"));
        assertThat(ref.toString(), is("ReferenceConfig{admin, parent=TestConfig}"));
    }

    @Test
    public void testName() throws ConfigException {
        FixedConfigSupplier<Service> parent = new FixedConfigSupplier<>(
                Service.builder()
                       .setAdmin(ServicePort.builder()
                                            .setPort((short) 1234)
                                            .build())
                       .build()
        );

        ConfigSupplier<ServicePort> ref = parent.reference(Service._Field.ADMIN);

        assertThat(ref.getName(), is("InMemoryConfig"));
        assertThat(ref.toString(), is("InMemoryConfig"));
    }

    @Test
    public void testDefaultValue() throws ConfigException {
        TestConfigSupplier<Service> parent = new TestConfigSupplier<>(
                Service.builder()
                       .setAdmin(ServicePort.builder()
                                           .setPort((short) 1234)
                                           .build())
                       .build());

        ConfigSupplier<ServicePort> ref = parent.reference(Service._Field.HTTP);

        assertThat(ref.get(), is(notNullValue()));
        assertThat(ref.get().getPort(), is((short) 80));

        parent.testUpdate(
                Service.builder()
                       .setHttp(ServicePort.builder()
                                           .setPort((short) 2345)
                                           .build())
                       .build());

        assertThat(ref.get().getPort(), is((short) 2345));
    }
}
