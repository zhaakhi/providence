package net.morimekta.providence.config.parser;

import net.morimekta.providence.PApplicationExceptionType;
import net.morimekta.providence.config.ConfigLoader;
import net.morimekta.providence.descriptor.PList;
import net.morimekta.providence.descriptor.PMap;
import net.morimekta.providence.descriptor.PPrimitive;
import net.morimekta.providence.descriptor.PSet;
import net.morimekta.providence.types.SimpleTypeRegistry;
import net.morimekta.test.providence.config.Database;
import net.morimekta.test.providence.config.Service;
import net.morimekta.test.providence.config.ServicePort;
import net.morimekta.test.providence.config.Value;
import net.morimekta.util.Binary;
import net.morimekta.util.collect.UnmodifiableSortedMap;
import net.morimekta.util.lexer.LexerException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import static net.morimekta.providence.config.parser.ConfigUtil.asType;
import static net.morimekta.providence.config.parser.ConfigUtil.canonicalFileLocation;
import static net.morimekta.providence.config.parser.ConfigUtil.consumeValue;
import static net.morimekta.providence.config.parser.ConfigUtil.resolveFile;
import static net.morimekta.testing.ResourceUtils.copyResourceTo;
import static net.morimekta.testing.ResourceUtils.writeContentTo;
import static net.morimekta.util.FileUtil.readCanonicalPath;
import static net.morimekta.util.FileUtil.replaceSymbolicLink;
import static net.morimekta.util.collect.UnmodifiableList.listOf;
import static net.morimekta.util.collect.UnmodifiableMap.mapOf;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

/**
 * Testing for the providence config utils.
 */
public class ConfigUtilTest {
    @Rule
    public TemporaryFolder tmp = new TemporaryFolder();

    private Service declaration;
    private Service service;

    @Before
    public void setUp() throws IOException {
        declaration = Service.builder()
                             .setAdmin(ServicePort.builder()
                                                  .setPort((short) 1234)
                                                  .setContext("/foo")
                                                  .build())
                             .build();
        SimpleTypeRegistry registry = new SimpleTypeRegistry();
        registry.registerType(Service.kDescriptor);

        copyResourceTo("/net/morimekta/providence/config/files/base_service.cfg", tmp.getRoot());
        copyResourceTo("/net/morimekta/providence/config/files/stage_db.cfg", tmp.getRoot());

        File cfg = copyResourceTo("/net/morimekta/providence/config/files/stage.cfg", tmp.getRoot());

        service = new ConfigLoader(registry).getConfig(cfg.toPath());
    }

    @Test
    public void testGetInMessage() throws ConfigException {
        // Return the field value.
        assertEquals((short) 1234, ConfigUtil.getInMessage(declaration, "admin.port"));
        assertEquals("/foo", ConfigUtil.getInMessage(declaration, "admin.context"));
        // Return the field value even when default is set.
        assertEquals((short) 1234, ConfigUtil.getInMessage(declaration, "admin.port", (short) 66));
        // Return null when there are no default in thrift, and none specified.
        assertNull(ConfigUtil.getInMessage(declaration, "http.context"));

        assertThat(ConfigUtil.getInMessage(service, "name"), is("stage"));
        assertThat(ConfigUtil.getInMessage(service, "admin"), is(nullValue()));
        ServicePort def = ServicePort.builder().build();
        assertThat(ConfigUtil.getInMessage(service, "admin", def), is(sameInstance(def)));
    }

    @Test
    public void testGetInMessage_fail() {
        try {
            ConfigUtil.getInMessage(service, "does_not_exist");
            fail("No exception");
        } catch (ConfigException e) {
            assertThat(e.getMessage(), is("Message config.Service has no field named does_not_exist"));
        }

        try {
            ConfigUtil.getInMessage(service, "does_not_exist.name");
            fail("No exception");
        } catch (ConfigException e) {
            assertThat(e.getMessage(), is("Message config.Service has no field named does_not_exist"));
        }

        try {
            ConfigUtil.getInMessage(service, "db.does_not_exist");
            fail("No exception");
        } catch (ConfigException e) {
            assertThat(e.getMessage(), is("Message config.Database has no field named does_not_exist"));
        }

        try {
            ConfigUtil.getInMessage(service, "name.db");
            fail("No exception");
        } catch (ConfigException e) {
            assertThat(e.getMessage(), is("Field 'name' is not of message type in config.Service"));
        }
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testAsType() throws ConfigException {
        // null value.
        assertThat(asType(PPrimitive.STRING, null), is(nullValue()));

        // ENUM
        assertThat(asType(Value.kDescriptor, Value.SECOND), is(Value.SECOND));
        assertThat(asType(Value.kDescriptor, 2), is(Value.SECOND));
        assertThat(asType(Value.kDescriptor, "SECOND"), is(Value.SECOND));

        try {
            asType(Value.kDescriptor, Value.kDescriptor);
            fail("no exception");
        } catch (ConfigException e) {
            assertThat(e.getMessage(),
                       is("Invalid value type class net.morimekta.test.providence.config.Value$_Descriptor for enum config.Value"));
        }
        try {
            asType(Value.kDescriptor, PApplicationExceptionType.INVALID_MESSAGE_TYPE);
            fail("no exception");
        } catch (ConfigException e) {
            assertThat(e.getMessage(), is("Invalid value type class net.morimekta.providence.PApplicationExceptionType for enum config.Value"));
        }

        // MESSAGE
        Service service = Service.builder().build();
        Database db = Database.builder().build();
        assertThat(asType(Service.kDescriptor, service), is(service));

        try {
            asType(Service.kDescriptor, db);
            fail("no exception");
        } catch (ConfigException e) {
            assertThat(e.getMessage(),
                       is("Unable to cast message type config.Database to config.Service"));
        }

        try {
            asType(Service.kDescriptor, "foo");
            fail("no exception");
        } catch (ConfigException e) {
            assertThat(e.getMessage(),
                       is("Invalid value type class java.lang.String for message config.Service"));
        }

        // BINARY
        assertThat(asType(PPrimitive.BINARY, Binary.fromHexString("abcd")),
                   is(Binary.fromHexString("abcd")));
        try {
            asType(PPrimitive.BINARY, 123);
            fail("no exception");
        } catch (ConfigException e) {
            assertThat(e.getMessage(),
                       is("Invalid value type class java.lang.Integer for type binary"));
        }

        // LIST
        assertThat(asType(PList.provider(PPrimitive.STRING.provider()).descriptor(),
                          listOf(1, 2)),
                   is(listOf("1", "2")));

        // SET
        assertThat(new ArrayList((Collection) asType(PSet.sortedProvider(PPrimitive.STRING.provider()).descriptor(),
                                                     listOf(3, 4, 2, 1))),
                   is(listOf("1", "2", "3", "4")));
        // MAP
        Map<String,String> map = (Map) asType(PMap.sortedProvider(PPrimitive.STRING.provider(),
                                                                  PPrimitive.STRING.provider()).descriptor(),
                                              mapOf(1, 2, 3, 4));
        assertThat(map, is(instanceOf(UnmodifiableSortedMap.class)));
        assertThat(map, is(UnmodifiableSortedMap.sortedMapOf("1", "2", "3", "4")));

        // General Failure
        try {
            asType(PPrimitive.VOID, "true");
            fail("no exception");
        } catch (ConfigException e) {
            assertThat(e.getMessage(),
                       is("Invalid value type class java.lang.String for type void"));
        }
    }

    @Test
    public void testConsumeValue() throws IOException {
        validateConsume("undefined __after__");
        validateConsume("not.a.ref\n__after__");

        validateConsume("ref __after__");

        validateConsume("{} __after__");
        validateConsume("unknown {} __after__");

        validateConsume("{ FIRST: SECOND } __after__");
        validateConsume("{ foo = \"bar\" } __after__");
        validateConsume("{ foo { bar = 12 } } __after__");
        validateConsume("{ foo = 12 bar = 13 } __after__");

        validateConsume("[] __after__");
        validateConsume("[ \"foo\", ] __after__");
        validateConsume("[ \"foo\" ] __after__");

        validateConsume("hex(abcdef) __after__");
        validateConsume("b64(AbDfm_) __after__");
    }

    @Test
    public void testConsumeValue_fails() throws IOException {
        try {
            validateConsume("{ first.Value: OOPS }");
            fail("no exception");
        } catch (ConfigException e) {
            assertThat(e.getMessage(), is("Invalid map key: first.Value"));
        }

        try {
            validateConsume("{ 12 = OOPS }");
            fail("no exception");
        } catch (ConfigException e) {
            assertThat(e.getMessage(), is("Invalid field name: 12"));
        }

        try {
            validateConsume("{ foo = bar ; other : 12 }");
            fail("no exception");
        } catch (ConfigException e) {
            assertThat(e.getMessage(), is("Unknown field value sep: :"));
        }

        try {
            validateConsume("[ 12 13 ]");
            fail("no exception");
        } catch (LexerException e) {
            assertThat(e.getMessage(),
                       is("Expected list separator or end, but got '13'"));
        }

        try {
            validateConsume("[ 12; 13 ]");
            fail("no exception");
        } catch (LexerException e) {
            assertThat(e.getMessage(),
                       is("Expected list separator or end, but got ';'"));
        }
   }

    private void validateConsume(String content) throws IOException {
        ConfigLexer tokenizer = tokenizer(content);
        consumeValue(tokenizer, tokenizer.expect("consumed value"));
        ConfigToken next = tokenizer.expect("after consuming");
        assertThat(next, is(notNullValue()));
        assertThat(next.toString(), is("__after__"));
    }

    private ConfigLexer tokenizer(String content) {
        StringReader in = new StringReader(content);
        return new ConfigLexer(in);
    }

    @Test
    public void testResolveFile() throws IOException {
        File test = tmp.newFolder("test");
        File other = tmp.newFolder("other");

        File f1_1 = new File(test, "test.cfg");
        File f1_2 = new File(test, "same.cfg");
        File f2_1 = new File(other, "other.cfg");
        File f2_2 = tmp.newFile("third.cfg");

        writeContentTo("a", f1_1);
        writeContentTo("a", f1_2);
        writeContentTo("a", f2_1);

        assertEquals(f1_1.getCanonicalFile().toPath(), resolveFile(null, tmp.getRoot() + "/test/test.cfg").toAbsolutePath());
        assertEquals(f1_2.getCanonicalFile().toPath(), resolveFile(f1_1.toPath(), "same.cfg").toAbsolutePath());
        assertEquals(f2_1.getCanonicalFile().toPath(), resolveFile(f1_1.toPath(), "../other/other.cfg").toAbsolutePath());
        assertEquals(f2_2.getCanonicalFile().toPath(), resolveFile(f1_1.toPath(), "../third.cfg").toAbsolutePath());

        assertFileNotResolved(f1_1, "../", "../ is a directory, expected file");
        assertFileNotResolved(f1_1, "../fourth.cfg", "Included file ../fourth.cfg not found");
        assertFileNotResolved(f1_1, "fourth.cfg", "Included file fourth.cfg not found");
        assertFileNotResolved(f1_1, "/fourth.cfg", "Absolute path includes not allowed: /fourth.cfg");
        assertFileNotResolved(f1_1, "other/fourth.cfg", "Included file other/fourth.cfg not found");
        assertFileNotResolved(f1_1, "../other", "../other is a directory, expected file");
        assertFileNotResolved(f1_1, "other", "Included file other not found");
        assertFileNotResolved(f1_1, "../../../../../../../../other", "Parent of root does not exist!");

        assertFileNotResolved(null, "../", "../ is a directory, expected file");
        assertFileNotResolved(null, "../fourth.cfg", "File ../fourth.cfg not found");
        assertFileNotResolved(null, "fourth.cfg", "File fourth.cfg not found");
        assertFileNotResolved(null, "/fourth.cfg", "File /fourth.cfg not found");
        assertFileNotResolved(null, "other/fourth.cfg", "File other/fourth.cfg not found");
        assertFileNotResolved(null, "../other", "File ../other not found");
        assertFileNotResolved(null, "other", "File other not found");
    }

    private void assertFileNotResolved(File ref, String file, String message) {
        try {
            resolveFile(ref == null ? null : ref.toPath(), file);
            fail("no exception on unresolved file");
        } catch (IOException e) {
            assertEquals(message, e.getMessage());
        }
    }

    @Test
    public void testCanonicalFileLocation() {
        try {
            canonicalFileLocation(Paths.get("/"));
        } catch (ConfigException e) {
            assertThat(e.getMessage(), is("Trying to read root directory"));
        }
    }

    @Test
    public void testReadCanonicalPath() throws IOException {
        Path dot = new File(".").getAbsoluteFile()
                                .getCanonicalFile()
                                .toPath();
        Path root = tmp.getRoot()
                       .getCanonicalFile()
                       .getAbsoluteFile()
                       .toPath();

        assertThat(readCanonicalPath(Paths.get(".")), is(dot));

        Path target1 = Files.createDirectory(root.resolve("target1"));
        Path target2 = Files.createDirectory(root.resolve("target2"));

        Path link = root.resolve("link");
        replaceSymbolicLink(link, target1);
        assertThat(readCanonicalPath(link), is(target1));
        replaceSymbolicLink(link, target2);

        assertThat(readCanonicalPath(link), is(target2));

        Path link2 = root.resolve("link2");
        replaceSymbolicLink(link2, Paths.get("target1"));
        assertThat(readCanonicalPath(link2), is(target1));

        Path link3 = target1.resolve("link3");
        Path target3 = Paths.get("../target2");
        replaceSymbolicLink(link3, target3);
        assertThat(readCanonicalPath(link3), is(target2));
    }

    @Test
    public void testReadCanonicalPath_fail() throws IOException {
        Path root = tmp.getRoot()
                       .getCanonicalFile()
                       .getAbsoluteFile()
                       .toPath();

        try {
            readCanonicalPath(root.resolve("../../../../../../../.."));
            fail("no exception");
        } catch (IOException e) {
            assertThat(e.getMessage(), is("Parent of root does not exist!"));
        }

        try {
            readCanonicalPath(Paths.get("/.."));
            fail("no exception");
        } catch (IOException e) {
            assertThat(e.getMessage(), is("Parent of root does not exist!"));
        }
    }

    @Test
    @SuppressWarnings("deprecation")
    public void testConstructor() throws
                                  NoSuchMethodException,
                                  IllegalAccessException,
                                  InvocationTargetException,
                                  InstantiationException {
        Constructor<ConfigUtil> constructor = ConfigUtil.class.getDeclaredConstructor();
        assertThat(constructor.isAccessible(), is(false));
        try {
            constructor.setAccessible(true);
            assertThat(constructor.newInstance(), is(instanceOf(ConfigUtil.class)));
        } finally {
            constructor.setAccessible(false);
        }
    }
}
