package net.morimekta.providence.config.testing;

import net.morimekta.providence.config.ConfigSupplier;
import net.morimekta.providence.config.parser.ConfigException;
import net.morimekta.test.providence.config.Service;
import net.morimekta.test.providence.config.ServicePort;
import net.morimekta.util.Binary;
import org.junit.Before;
import org.junit.Test;

import static net.morimekta.providence.testing.EqualToMessage.equalToMessage;
import static net.morimekta.util.collect.UnmodifiableMap.mapOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.fail;


public class SerializedConfigSupplierTest {
    private Service expected;

    @Before
    public void setUp() {
        expected = Service.builder()
                          .setAdmin(ServicePort.builder()
                                               .setPort((short) 8088)
                                               .setOauthTokenKey(Binary.fromBase64("VGVzdCBPYXV0aCBLZXkK"))
                                               .build())
                          .setHttp(ServicePort.builder()
                                              .setPort((short) 8080)
                                              .setContext("/app")
                                              .setSignatureKeys(mapOf(
                                                      "app1", Binary.fromBase64("VGVzdCBPYXV0aCBLZXkK")
                                              ))
                                              .addToSignatureOverrideKeys("not_really_app_1")
                                              .build())
                          .build();
    }

    @Test
    public void testWithJson() throws ConfigException {
        ConfigSupplier<Service> supplier = new TestConfigSupplier<>(
                "/net/morimekta/providence/config/service.json", Service.kDescriptor);

        assertThat(supplier.get(), is(equalToMessage(expected)));
    }

    @Test
    public void testWithPretty() throws ConfigException {
        ConfigSupplier<Service> supplier = new TestConfigSupplier<>(
                "/net/morimekta/providence/config/service.cfg", Service.kDescriptor);

        assertThat(supplier.get(), is(equalToMessage(expected)));
    }

    @Test
    public void testWithBadConfig() {
        try {
            new TestConfigSupplier<>("/net/morimekta/providence/config/bad.config", Service.kDescriptor);
            fail("no exception");
        } catch (ConfigException e) {
            try {
                assertThat(e.displayString(),
                           is("Error in bad.config on line 1 row 1-19: Expected qualifier config.Service or message start, but got 'config.DoesNotExist'\n" +
                              "config.DoesNotExist {\n" +
                              "^^^^^^^^^^^^^^^^^^^"));
            } catch (AssertionError a) {
                a.initCause(e);
                throw a;
            }
        }
    }

    @Test
    public void testWithBadJson() {
        try {
            new TestConfigSupplier<>("/net/morimekta/providence/config/bad.json", Service.kDescriptor);
            fail("no exception");
        } catch (ConfigException e) {
            assertThat(e.displayString(),
                       is("Error in bad.json on line 2 row 5: Wrongly terminated JSON number: '12:'\n" +
                          "  12: \"bar\"\n" +
                          "----^"));
        }
    }

    @Test
    public void testWithBadFormat() {
        try {
            new TestConfigSupplier<>("/net/morimekta/providence/config/bad.yaml", Service.kDescriptor);
            fail("no exception");
        } catch (ConfigException e) {
            assertThat(e.displayString(),
                       is("Error: Unrecognized resource config type: .yaml (/net/morimekta/providence/config/bad.yaml)"));
        }
    }

    @Test
    public void testWithBadName() {
        try {
            new TestConfigSupplier<>("/net/morimekta/providence/config/bad", Service.kDescriptor);
            fail("no exception");
        } catch (ConfigException e) {
            assertThat(e.displayString(),
                       is("Error: No file ending, or no resource file name: /net/morimekta/providence/config/bad"));
        }
    }

    @Test
    public void testNoSuchResource() {
        try {
            new TestConfigSupplier<>("/net/morimekta/providence/config/does.not.config", Service.kDescriptor);
            fail("no exception");
        } catch (ConfigException e) {
            assertThat(e.displayString(),
                       is("Error: No such config resource: /net/morimekta/providence/config/does.not.config"));
        }

    }

    @Test
    public void testName() throws ConfigException {
        TestConfigSupplier<Service> supplier = new TestConfigSupplier<>(
                "/net/morimekta/providence/config/service.cfg", Service.kDescriptor);

        assertThat(supplier.getName(), is("TestConfig"));
        assertThat(supplier.toString(), is("TestConfig{config.Service}"));

        supplier.testUpdate("/net/morimekta/providence/config/service.json");

        assertThat(supplier.getName(), is("TestConfig"));
        assertThat(supplier.toString(), is("TestConfig{config.Service}"));
    }
}
