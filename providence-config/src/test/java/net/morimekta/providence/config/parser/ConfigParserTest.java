package net.morimekta.providence.config.parser;

import net.morimekta.providence.PMessage;
import net.morimekta.providence.config.util.FileContentResolver;
import net.morimekta.providence.types.SimpleTypeRegistry;
import net.morimekta.test.providence.config.Database;
import net.morimekta.test.providence.config.RefConfig1;
import net.morimekta.test.providence.config.RefConfig2;
import net.morimekta.test.providence.config.RefMerge;
import net.morimekta.test.providence.config.Service;
import net.morimekta.test.providence.config.ServicePort;
import net.morimekta.test.providence.config.Value;
import net.morimekta.util.Binary;
import net.morimekta.util.Pair;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;

import static net.morimekta.providence.serializer.PrettySerializer.toDebugString;
import static net.morimekta.providence.testing.EqualToMessage.equalToMessage;
import static net.morimekta.testing.ResourceUtils.copyResourceTo;
import static net.morimekta.testing.ResourceUtils.writeContentTo;
import static net.morimekta.util.collect.UnmodifiableMap.mapOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class ConfigParserTest {
    @Rule
    public TemporaryFolder temp = new TemporaryFolder();

    private SimpleTypeRegistry registry;
    private ConfigParser       parser;

    @Before
    public void setUp() {
        registry = new SimpleTypeRegistry();
        registry.registerType(Service.kDescriptor);
        registry.registerType(Value.kDescriptor);
        registry.registerType(RefMerge.kDescriptor);

        parser = new ConfigParser(registry, new FileContentResolver(), this::warning, false);
    }

    private void warning(ConfigWarning warning) {
        System.err.println(warning.displayString());
    }

    @Test
    public void testParseWithUnknownInclude() throws IOException {
        copyResourceTo("/net/morimekta/providence/config/files/unknown.cfg", temp.getRoot());
        File file = copyResourceTo("/net/morimekta/providence/config/files/unknown_include.cfg", temp.getRoot());

        Pair<Database, Set<String>> cfg = parser.parseConfig(file.toPath(), null);

        // all the unknowns are skipped.
        assertEquals("{\n" +
                     "  uri = \"jdbc:h2:localhost:mem\"\n" +
                     "  driver = \"org.h2.Driver\"\n" +
                     "}",
                     toDebugString(cfg.first));

        file = copyResourceTo("/net/morimekta/providence/config/files/unknown_field.cfg", temp.getRoot());
        cfg = parser.parseConfig(file.toPath(), null);
        assertEquals("{\n" +
                     "  uri = \"jdbc:h2:localhost:mem\"\n" +
                     "  driver = \"org.h2.Driver\"\n" +
                     "}",
                     toDebugString(cfg.first));

        file = copyResourceTo("/net/morimekta/providence/config/files/unknown_enum_value.cfg", temp.getRoot());
        cfg = parser.parseConfig(file.toPath(), null);
        assertEquals("{\n" +
                     "  uri = \"jdbc:h2:localhost:mem\"\n" +
                     "  driver = \"org.h2.Driver\"\n" +
                     "}",
                     toDebugString(cfg.first));
    }

    @Test
    public void testParseWithUnknown_strict() {
        copyResourceTo("/net/morimekta/providence/config/files/unknown.cfg", temp.getRoot());
        File file = copyResourceTo("/net/morimekta/providence/config/files/unknown_include.cfg", temp.getRoot());

        ConfigParser config = new ConfigParser(registry, new FileContentResolver(), this::warning, true);
        try {
            config.parseConfig(file.toPath(), null);
            fail("no exception");
        } catch (ConfigException e) {
            assertEquals("Unknown declared type: unknown.OtherConfig", e.getMessage());
        }

        file = copyResourceTo("/net/morimekta/providence/config/files/unknown_field.cfg", temp.getRoot());
        try {
            config.parseConfig(file.toPath(), null);
            fail("no exception");
        } catch (ConfigException e) {
            assertEquals("No such field unknown_field in config.Database", e.getMessage());
        }

        file = copyResourceTo("/net/morimekta/providence/config/files/unknown_enum_value.cfg", temp.getRoot());
        try {
            config.parseConfig(file.toPath(), null);
            fail("no exception");
        } catch (ConfigException e) {
            assertEquals("No such enum value 'LAST' for config.Value.", e.getMessage());
        }

        file = copyResourceTo("/net/morimekta/providence/config/files/unknown_enum_value2.cfg", temp.getRoot());
        try {
            config.parseConfig(file.toPath(), null);
            fail("no exception");
        } catch (ConfigException e) {
            assertEquals("No such enum value 'second' for config.Value, did you mean 'SECOND'?", e.getMessage());
        }
    }

    @Test
    public void testCircularIncludes() throws IOException {
        File a = temp.newFile("a.cfg");
        File b = temp.newFile("b.cfg");
        File c = temp.newFile("c.cfg");

        writeContentTo("include \"b.cfg\" as a\n" +
                       "config.Database {}\n", a);
        writeContentTo("include \"c.cfg\" as a\n" +
                       "config.Database {}\n", b);
        writeContentTo("include \"a.cfg\" as a\n" +
                       "config.Database {}\n", c);

        try {
            parser.parseConfig(a.toPath(), null);
            fail("no exception on circular deps");
        } catch (ConfigException e) {
            assertEquals("Circular includes detected: a.cfg -> b.cfg -> c.cfg -> a.cfg", e.getMessage());
        }
    }

    @Test
    public void testIncludeNoSuchFile() throws IOException {
        File a = temp.newFile("a.cfg");
        writeContentTo("include \"b.cfg\" as a\n" +
                       "config.Database {}\n", a);

        try {
            parser.parseConfig(a.toPath(), null);
            fail("no exception on circular deps");
        } catch (ConfigException e) {
            assertEquals("Included file \"b.cfg\" not found.", e.getMessage());
        }
    }

    @Test
    public void testReference() throws IOException {
        assertGoodConfig(
                "def boo = false\n" +
                "def db = config.Database {\n" +
                "  driver = \"Driver\"\n" +
                "}\n" +
                "def db2 = config.Database : db {\n" +
                "  uri = \"someuri\"\n" +
                "}\n" +
                "def first = config.RefConfig1 {\n" +
                "  bool_value = boo\n" +
                "  msg_value = db\n" +
                "}\n" +
                "\n" +
                "config.RefMerge {\n" +
                "  ref1 = first\n" +
                "  ref1_1 = first {\n" +
                "    i16_value = 12345\n" +
                "    msg_value = db2\n" +
                "  }\n" +
                "  ref2 {\n" +
                "    bool_value = boo" +
                "    msg_value = db2\n" +
                "  }\n" +
                "}\n",
                RefMerge.builder()
                        .setRef1(RefConfig1.builder()
                                           .setBoolValue(false)
                                           .setMsgValue(Database.builder().setDriver("Driver").build())
                                           .build())
                        .setRef11(RefConfig1.builder()
                                            .setBoolValue(false)
                                            .setI16Value((short) 12345)
                                            .setMsgValue(Database.builder()
                                                                 .setUri("someuri")
                                                                 .setDriver("Driver")
                                                                 .build())
                                            .build())
                        .setRef2(RefConfig2.builder()
                                           .setBoolValue(false)
                                           .setMsgValue(Database.builder()
                                                                .setUri("someuri")
                                                                .setDriver("Driver")
                                                                .build())
                                           .build())
                        .build(),
                null);
    }

    @Test
    public void testReferenceFails() throws IOException {
        writeContentTo("config.Database {}\n", temp.newFile("db.cfg"));
        writeContentTo("config.Database {}\n", temp.newFile("db2.cfg"));

        assertConfigFails("include \"db.cfg\" as db\n" +
                          "include \"db2.cfg\" as db\n" +
                          "config.RefMerge {\n" +
                          "  ref1 {\n" +
                          "  }\n" +
                          "}\n",
                          "Alias \"db\" is already used.");
        assertConfigFails("include \"db.cfg\" as db\n" +
                          "def db = config.RefMerge {\n" +
                          "  ref1 {\n" +
                          "  }\n" +
                          "}\n" +
                          "config.RefMerge {\n" +
                          "  ref1 {\n" +
                          "  }\n" +
                          "}\n",
                          "Trying to reassign include alias 'db' to reference.");
        assertConfigFails("def db = config.RefMerge : db {\n" +
                          "  ref1 = db2{\n" +
                          "  }\n" +
                          "}\n" +
                          "config.RefMerge {\n" +
                          "  ref1 {\n" +
                          "  }\n" +
                          "}\n",
                          "Trying to reference 'db' while it's being defined, original at line 1");
        assertConfigFails("def db = \"database\"\n" +
                          "def db = config.RefMerge {\n" +
                          "  ref1 {\n" +
                          "  }\n" +
                          "}\n" +
                          "config.RefMerge {\n" +
                          "}\n",
                          "Trying to reassign reference 'db', original at line 1");
        assertConfigFails("\n" +
                          "def def = 12\n" +
                          "config.RefMerge {\n" +
                          "  ref1 {\n" +
                          "  }\n" +
                          "}\n",
                          "Trying to assign reference id 'def', which is reserved.");
        assertConfigFails("\n" +
                          "def true = 1\n" +
                          "config.RefMerge {\n" +
                          "  ref1 {\n" +
                          "  }\n" +
                          "}\n",
                          "Trying to assign reference id 'true', which is reserved.");
        assertConfigFails("\n" +
                          "def false = 0\n" +
                          "config.RefMerge {\n" +
                          "  ref1 {\n" +
                          "  }\n" +
                          "}\n",
                          "Trying to assign reference id 'false', which is reserved.");
        assertConfigFails("\n" +
                          "def undefined = 0\n" +
                          "config.RefMerge {\n" +
                          "  ref1 {\n" +
                          "  }\n" +
                          "}\n",
                          "Trying to assign reference id 'undefined', which is reserved.");

        // --- with unknown / consumed values
        assertConfigFails("\n" +
                          "config.RefMerge {\n" +
                          "  ref1 = first {\n" +
                          "    msg_value = second\n" +
                          "  }\n" +
                          "}\n",
                          "No such reference 'first'");
    }

    @Test
    public void testConfigFails() throws IOException {
        assertConfigFails("config.RefConfig1 {\n" +
                          "  byte_value = true" +
                          "}\n", "Unhandled value \"true\" for type byte");
        assertConfigFails("config.RefConfig1 {\n" +
                          "  i16_value = true" +
                          "}\n", "Unhandled value \"true\" for type i16");
        assertConfigFails("config.RefConfig1 {\n" +
                          "  i32_value = true" +
                          "}\n", "Unhandled value \"true\" for type i32");
        assertConfigFails("config.RefConfig1 {\n" +
                          "  i64_value = true" +
                          "}\n", "Unhandled value \"true\" for type i64");
        assertConfigFails("config.RefConfig1 {\n" +
                          "  double_value = true" +
                          "}\n", "Unhandled value \"true\" for type double");
    }

    private void assertConfigFails(String cfg, String message) throws IOException {
        try {
            File         a      = writeContentTo(cfg, temp.newFile());
            parser.parseConfig(a.toPath(), null);
            fail("No exception on fail: " + message);
        } catch (ConfigException e) {
            try {
                assertThat(e.getMessage(), is(message));
            } catch (AssertionError a) {
                a.initCause(e);
                throw a;
            }
        }
    }

    @Test
    public void testGoodConfig() throws IOException {
        assertGoodConfig("config.RefConfig1 {\n" +
                         "  bool_value = true\n" +
                         "  byte_value = 123\n" +
                         "  i32_value = 12345678\n" +
                         "  i64_value = 12345678901234\n" +
                         "  double_value = 123.123\n" +
                         "  bin_value = hex(01020304)\n" +
                         "  enum_value = 1\n" +
                         "  msg_value = {\n" +
                         "    uri = \"dodo\"" +
                         "  }\n" +
                         "  map_value = {\n" +
                         "    FIRST: { port = 1234 }" +
                         "  }" +
                         "  complex_map = {\n" +
                         "    10: { 12: 15 }" +
                         "  }\n" +
                         "  list_value = [ \"foo\", \"bar\", ]\n" +
                         "  set_value = [ 1, 3, 13 ]" +
                         "}", RefConfig1.builder()
                                        .setBoolValue(true)
                                        .setByteValue((byte) 123)
                                        .setI32Value(12345678)
                                        .setI64Value(12345678901234L)
                                        .setDoubleValue(123.123)
                                        .setBinValue(Binary.fromHexString("01020304"))
                                        .setEnumValue(Value.FIRST)
                                        .setMsgValue(Database.builder()
                                                             .setUri("dodo")
                                                             .build())
                                        .putInMapValue(Value.FIRST, ServicePort.builder()
                                                                               .setPort((short) 1234)
                                                                               .build())
                                        .putInComplexMap(
                                                10, mapOf(12, 15))
                                        .addToListValue("foo", "bar")
                                        .addToSetValue((short) 1, (short) 3, (short) 13)
                                        .build(), null);
        assertGoodConfig("def {\n" +
                         "  ref2 = config.RefConfig1 {\n" +
                         "    enum_value = FIRST\n" +
                         "    map_value = {\n" +
                         "      SECOND: { port = 4040 }\n" +
                         "    }\n" +
                         "  }\n" +
                         "}\n" +
                         "\n" +
                         "config.RefConfig2 {\n" +
                         "  enum_value = ref2.enum_value\n" +
                         "  map_value = ref2.map_value\n" +
                         "" +
                         "}\n",
                         RefConfig2.builder()
                                   .setStrValue("keepsake")
                                   .setEnumValue(Value.FIRST)
                                   .putInMapValue(Value.SECOND, ServicePort.builder().setPort((short) 4040).build())
                                   .build(),
                         RefConfig2.builder()
                                   .setStrValue("keepsake")
                                   .build());
    }

    private <M extends PMessage<M>>
    void assertGoodConfig(@Nonnull String config,
                          @Nonnull M expected,
                          @Nullable M parent)
            throws IOException {
        try {
            File         a      = writeContentTo(config, temp.newFile());
            M            result = parser.parseConfig(a.toPath(), parent).first;
            assertThat(result, is(equalToMessage(expected)));
        } catch (ConfigException e) {
            System.err.println(e.displayString());
            System.err.println();
            throw e;
        }
    }

    @Test
    public void testConfigMapReference() throws IOException {
        File cm1 = temp.newFolder("cm1");
        File cm2 = temp.newFolder("cm2");
        File data1 = temp.newFolder("cm1", "..data");
        File data2 = temp.newFolder("cm2", "..data");
        writeContentTo("config.Database {\n" +
                       "  driver = \"foo\"" +
                       "}",
                       new File(data1, "db.config"));
        writeContentTo("include \"../cm1/db.config\" as parent\n" +
                       "config.Database : parent {}",
                       new File(data2, "db.config"));

        Path p1 = Files.createSymbolicLink(cm1.toPath().resolve("db.config"), Paths.get("..data/db.config"));
        Path p2 = Files.createSymbolicLink(cm2.toPath().resolve("db.config"), Paths.get("..data/db.config"));

        Database db = parser.<Database>parseConfig(p2, null).first;
        assertThat(db, is(Database.builder().setDriver("foo").build()));
    }

    @Test
    public void testConfigMapReferences() throws IOException {
        File cm1 = temp.newFolder("cm1");
        File cm2 = temp.newFolder("cm2");
        File data1 = temp.newFolder("cm1", "..data");
        File data2 = temp.newFolder("cm2", "..data");
        writeContentTo("config.Database {\n" +
                       "  driver = \"foo\"" +
                       "}",
                       new File(data1, "db.config"));
        writeContentTo("include \"../cm1/db.config\" as parent\n" +
                       "config.Database : parent {}",
                       new File(data2, "db.config"));
        writeContentTo("include \"db.config\" as parent\n" +
                       "config.Database : parent {}",
                       new File(data2, "db2.config"));

        Files.createSymbolicLink(cm1.toPath().resolve("db.config"), Paths.get("..data/db.config"));
        Files.createSymbolicLink(cm2.toPath().resolve("db.config"), Paths.get("..data/db.config"));
        Path p3 = Files.createSymbolicLink(cm2.toPath().resolve("db2.config"), Paths.get("..data/db2.config"));

        Database db = parser.<Database>parseConfig(p3, null).first;
        assertThat(db, is(Database.builder().setDriver("foo").build()));
    }

    @Test
    public void testParseFailure() throws IOException {
        writeContentTo("config.Database {}", temp.newFile("a.cfg"));
        writeContentTo("config.Database {}", temp.newFile("b.cfg"));

        assertParseFailure("Error in test.cfg: No message in config: test.cfg",
                           "");
        assertParseFailure("Error in test.cfg on line 1 row 11-12: Invalid termination of number: '1f'\n" +
                           "def { n = 1f }\n" +
                           "----------^^",
                           "def { n = 1f }");
        assertParseFailure("Error in test.cfg on line 3 row 1-3: Unexpected token 'def', expected end of file.\n" +
                           "def { y = \"baa\"}\n" +
                           "^^^",
                           "include \"a.cfg\" as a\n" +
                           "config.Database { driver = \"baa\"}\n" +
                           "def { y = \"baa\"}\n");
        assertParseFailure("Error in test.cfg on line 2 row 1-15: Expected the token 'as', but got 'config.Database'\n" +
                           "config.Database { driver = \"baa\"}\n" +
                           "^^^^^^^^^^^^^^^",
                           "include \"a.cfg\"\n" +
                           "config.Database { driver = \"baa\"}\n");
        assertParseFailure("Error in test.cfg on line 1 row 17-19: Expected token 'as' after included file \"a.cfg\".\n" +
                           "include \"a.cfg\" ass db\n" +
                           "----------------^^^",
                           "include \"a.cfg\" ass db\n" +
                           "config.Database { driver = \"baa\"}\n");
        assertParseFailure("Error in test.cfg on line 2 row 1-15: Expected Include alias, but got 'config.Database'\n" +
                           "config.Database { driver = \"baa\"}\n" +
                           "^^^^^^^^^^^^^^^",
                           "include \"a.cfg\" as\n" +
                           "config.Database { driver = \"baa\"}\n");
        assertParseFailure("Error in test.cfg on line 1 row 20-22: Alias \"def\" is a reserved word.\n" +
                           "include \"a.cfg\" as def\n" +
                           "-------------------^^^",
                           "include \"a.cfg\" as def\n" +
                           "config.Database { driver = \"baa\"}\n");
        assertParseFailure("Error in test.cfg on line 2 row 20: Alias \"a\" is already used.\n" +
                           "include \"a.cfg\" as a\n" +
                           "-------------------^",
                           "include \"a.cfg\" as a\n" +
                           "include \"a.cfg\" as a\n" +
                           "config.Database { driver = \"baa\"}\n");
        assertParseFailure("Error in test.cfg on line 1 row 12: Unexpected newline in string\n" +
                           "def { s = \"\n" +
                           "-----------^",
                           "def { s = \"\n\"}");
        assertParseFailure("Error in test.cfg on line 1 row 12: Unescaped non-printable char in string: '\\t'\n" +
                           "def { s = \"·\"}\n" +
                           "-----------^",
                           "def { s = \"\t\"}");
        assertParseFailure("Error in test.cfg on line 1 row 13: Unexpected end of stream in string\n" +
                           "def { s = \"a\n" +
                           "------------^",
                           "def { s = \"a");
        assertParseFailure("Error in test.cfg on line 1 row 7: Token '1' is not valid reference name.\n" +
                           "def { 1 = \"boo\" }\n" +
                           "------^",
                           "def { 1 = \"boo\" }");
        assertParseFailure("Error in test.cfg on line 1 row 1-2: Unexpected token '44'. Expected include, defines or message type\n" +
                           "44\n" +
                           "^^",
                           "44");
        assertParseFailure("Error in test.cfg on line 1 row 1-3: Unexpected token 'boo'. Expected include, defines or message type\n" +
                           "boo {\n" +
                           "^^^",
                           "boo {\n" +
                           "}\n");

        // Parsing that only fails in strict mode.
        assertParseFailure("Error in test.cfg on line 1 row 11-20: Unknown enum identifier: boo.En\n" +
                           "def { s = boo.En.VAL }\n" +
                           "----------^^^^^^^^^^",
                           "def { s = boo.En.VAL }", true);
    }

    @SuppressWarnings("unchecked")
    private <M extends PMessage<M>> M assertParse(
            String pretty, boolean strict) throws IOException {
        File a = temp.newFile("test.cfg");
        writeContentTo(pretty, a);

        ConfigParser config = new ConfigParser(registry, new FileContentResolver(), this::warning, strict);
        return (M) config.parseConfig(a.toPath(), null).first;
    }


    private void assertParseFailure(String message,
                                    String pretty) throws IOException {
        assertParseFailure(message, pretty, false);
    }

    private void assertParseFailure(String message,
                                    String pretty,
                                    boolean strict) throws IOException {
        File a = temp.newFile("test.cfg");
        writeContentTo(pretty, a);

        ConfigParser config = new ConfigParser(registry, new FileContentResolver(), this::warning, strict);

        try {
            config.parseConfig(a.toPath(), null);
            fail("no exception");
        } catch (ConfigException e) {
            String actual = e.displayString().replaceAll("\\r", "");
            if (!actual.equals(message)) {
                e.printStackTrace();
            }
            assertThat(actual, is(message));
        }
        a.delete();
    }
}
