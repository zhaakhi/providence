package net.morimekta.providence.jax.rs.schema;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.models.media.Schema;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused,rawtypes")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class OneOfSchema extends Schema<OneOfSchema> {

    private List<Schema> oneOf = new ArrayList<>();

    public OneOfSchema() {
        super(null, null);
    }

    public List<Schema> getOneOf() {
        return oneOf;
    }

    public void setOneOf(List<Schema> oneOf) {
        this.oneOf = oneOf;
    }

    public OneOfSchema oneOf(List<Schema> allOf) {
        setOneOf(allOf);
        return this;
    }
}
