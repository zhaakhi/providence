package net.morimekta.providence.jax.rs.schema;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.models.media.Schema;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused,rawtypes")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CompactObjectSchema extends Schema<CompactObjectSchema> {
    private List<Schema> items;
    private Boolean additionalItems;

    public CompactObjectSchema() {
        super("array", null);
        items = new ArrayList<>();
        additionalItems = false;
    }

    public List<Schema> getItems() {
        return items;
    }

    public void setItems(List<Schema> items) {
        this.items = items;
    }

    public CompactObjectSchema items(List<Schema> allOf) {
        setItems(allOf);
        return this;
    }

    public Boolean getAdditionalItems() {
        return additionalItems;
    }

    public void setAdditionalItems(Boolean additionalItems) {
        this.additionalItems = additionalItems;
    }

}
