package net.morimekta.providence.jax.rs.schema;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.models.media.Schema;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused,rawtypes")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AnyOfSchema extends Schema<AnyOfSchema> {

    private List<Schema> anyOf = new ArrayList<>();

    public AnyOfSchema() {
        super(null, null);
    }

    public List<Schema> getAnyOf() {
        return anyOf;
    }

    public void setAnyOf(List<Schema> anyOf) {
        this.anyOf = anyOf;
    }

    public AnyOfSchema anyOf(List<Schema> allOf) {
        setAnyOf(allOf);
        return this;
    }
}
