package net.morimekta.providence.jax.rs.schema;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import io.swagger.v3.oas.models.media.ArraySchema;
import io.swagger.v3.oas.models.media.BooleanSchema;
import io.swagger.v3.oas.models.media.IntegerSchema;
import io.swagger.v3.oas.models.media.NumberSchema;
import io.swagger.v3.oas.models.media.Schema;
import io.swagger.v3.oas.models.media.StringSchema;

import java.util.List;

@SuppressWarnings("rawtypes")
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        defaultImpl = SchemaWrapper.class,
        include = JsonTypeInfo.As.EXISTING_PROPERTY,
        property = "type"
)
@JsonSubTypes({
        @Type(value = ArraySchema.class, name = "array"),
        @Type(value = IntegerSchema.class, name = "integer"),
        @Type(value = StringSchema.class, name = "string"),
        @Type(value = NumberSchema.class, name = "number"),
        @Type(value = BooleanSchema.class, name = "boolean"),

        @Type(value = ObjectSchemaWrapper.class, name = "object"),
})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SchemaWrapper extends Schema {
    @JsonCreator
    @SuppressWarnings("unused")
    public static Schema create(@JsonProperty("type") String type,
                                @JsonProperty("$ref") String ref,
                                @JsonProperty("oneOf") List<Schema> oneOf,
                                @JsonProperty("allOf") List<Schema> allOf,
                                @JsonProperty("anyOf") List<Schema> anyOf) {
        if (type != null) {
            throw new IllegalStateException("Unknown type '" + type + "' for schema instance parsing");
        }
        if (ref != null) {
            return new Schema().$ref(ref);
        }
        if (oneOf != null) {
            return new OneOfSchema().oneOf(oneOf);
        }
        if (allOf != null) {
            return new AllOfSchema().allOf(allOf);
        }
        if (anyOf != null) {
            return new AnyOfSchema().anyOf(anyOf);
        }
        throw new IllegalStateException("No known property to create non-typed schema instance");
    }
}
