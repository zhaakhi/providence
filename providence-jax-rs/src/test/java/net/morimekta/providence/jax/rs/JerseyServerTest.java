package net.morimekta.providence.jax.rs;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import net.morimekta.providence.client.google.ProvidenceHttpBufferedContent;
import net.morimekta.providence.client.google.ProvidenceObjectParser;
import net.morimekta.providence.jax.rs.test_web_app.TestApplication;
import net.morimekta.providence.jax.rs.test_web_app.TestNetUtil;
import net.morimekta.providence.serializer.BinarySerializer;
import net.morimekta.providence.serializer.JsonSerializer;
import net.morimekta.providence.serializer.Serializer;
import net.morimekta.test.providence.jax.rs.calculator.CalculateException;
import net.morimekta.test.providence.jax.rs.calculator.Operand;
import net.morimekta.test.providence.jax.rs.calculator.Operation;
import net.morimekta.test.providence.jax.rs.calculator.Operator;
import net.morimekta.test.providence.jax.rs.number.Imaginary;
import net.morimekta.testing.ResourceUtils;
import net.morimekta.util.FileUtil;
import net.morimekta.util.collect.UnmodifiableList;
import net.morimekta.util.io.IOUtils;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Locale;

import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING;
import static net.morimekta.providence.jax.rs.OpenAPIUtils.normalizeOpenAPI;
import static net.morimekta.providence.jax.rs.OpenAPIUtils.parseOpenAPIJson;
import static net.morimekta.providence.jax.rs.OpenAPIUtils.parseOpenAPIYaml;
import static net.morimekta.providence.jax.rs.OpenAPIUtils.toJson;
import static net.morimekta.providence.jax.rs.OpenAPIUtils.toYaml;
import static net.morimekta.providence.serializer.PrettySerializer.toDebugString;
import static net.morimekta.test.providence.jax.rs.calculator.Operand.withImaginary;
import static net.morimekta.test.providence.jax.rs.calculator.Operand.withNumber;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;

/**
 * Javax.WS.RS integration test_web_app using io.dropwizard.
 */
// @Ignore
@RunWith(DataProviderRunner.class)
public class JerseyServerTest {
    private static Server server;
    private static int port;

    @Before
    public void setUp() throws Exception {
        server = new Server(0);
        ServletContextHandler handler = new ServletContextHandler(ServletContextHandler.NO_SESSIONS);

        ServletHolder holder = handler.addServlet(ServletContainer.class, "/*");
        holder.setInitOrder(0);
        holder.setInitParameter("javax.ws.rs.Application", TestApplication.class.getCanonicalName());

        server.setHandler(handler);
        server.start();
        port = ((ServerConnector) server.getConnectors()[0]).getLocalPort();
    }

    @After
    public void tearDown() throws Exception {
        if (server != null) {
            server.stop();
        }
    }

    @DataProvider
    public static Object[][] testOptions() {
        return new Object[][] {
                { new JsonSerializer(), },
                { new JsonSerializer().pretty(), },
                { new BinarySerializer(), },
                };
    }

    private GenericUrl uri(String service) {
        GenericUrl url = new GenericUrl(String.format(Locale.US, "http://localhost:%d/%s", port, service));;
        return url;
    }

    @Test
    @UseDataProvider("testOptions")
    public void testProvidence(Serializer serializer) throws IOException {
        Operation operation = new Operation(Operator.ADD,
                                            list(withNumber(52d),
                                                 withImaginary(new Imaginary(1d, -1d)),
                                                 withNumber(15d)));

        HttpResponse response = TestNetUtil
                .factory(serializer.mediaType())
                .buildPostRequest(uri("calculator/calculate"),
                                  new ProvidenceHttpBufferedContent(operation, serializer))
                .setParser(new ProvidenceObjectParser(serializer))
                .setThrowExceptionOnExecuteError(false)
                .execute();

        assertThat(response.getStatusCode(), is(equalTo(200)));
        assertThat(response.getHeaders().getContentType(), is(equalTo(serializer.mediaType())));
        Operand op = response.parseAs(Operand.class);

        assertThat(toDebugString(op), is(equalTo(
                "{\n" +
                "  imaginary = {\n" +
                "    v = 68\n" +
                "    i = -1\n" +
                "  }\n" +
                "}")));
    }

    @Test
    @UseDataProvider("testOptions")
    public void testProvidence_Exception(Serializer serializer) throws IOException {
        Operation operation = new Operation(Operator.MULTIPLY,
                                            list(withNumber(52d),
                                                 withImaginary(new Imaginary(1d, -1d)),
                                                 withNumber(15d)));

        HttpResponse response = TestNetUtil
                .factory(serializer.mediaType())
                .buildPostRequest(uri("calculator/calculate"),
                                  new ProvidenceHttpBufferedContent(operation, serializer))
                .setThrowExceptionOnExecuteError(false)
                .setParser(new ProvidenceObjectParser(serializer))
                .execute();

        assertThat(response.getStatusCode(), is(equalTo(400)));
        assertThat(response.getHeaders().getContentType(), is(equalTo(serializer.mediaType())));
        CalculateException ex = response.parseAs(CalculateException.class);

        assertEquals(
                "{\n" +
                "  message = \"Unsupported operation: MULTIPLY\"\n" +
                "  operation = {\n" +
                "    operator = MULTIPLY\n" +
                "    operands = [\n" +
                "      {\n" +
                "        number = 52\n" +
                "      },\n" +
                "      {\n" +
                "        imaginary = {\n" +
                "          v = 1\n" +
                "          i = -1\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        number = 15\n" +
                "      }\n" +
                "    ]\n" +
                "  }\n" +
                "}", toDebugString(ex));
    }


    @Test
    public void testOpenApi_json() throws IOException {
        String json = get(new GenericUrl("http://localhost:" + port + "/openapi.json"));
        // System.err.println(json);
        json = toJson(normalizeOpenAPI(parseOpenAPIJson(json)));
        // System.err.println(json);

        if ("true".equals(System.getenv("UPDATE_OPENAPI"))) {
            writeResource(json, "/openapi.json");
        } else {
            String expected = ResourceUtils.getResourceAsString("/openapi.json");
            assertThat(json, is(expected));
        }
    }

    @Test
    public void testOpenApi_yaml() throws IOException {
        String yaml = get(new GenericUrl("http://localhost:" + port + "/openapi.yaml"));
        // System.err.println(yaml);
        yaml = toYaml(normalizeOpenAPI(parseOpenAPIYaml(yaml)));
        // System.err.println(yaml);

        if ("true".equals(System.getenv("UPDATE_OPENAPI"))) {
            writeResource(yaml, "/openapi.yaml");
        } else {
            String expected = ResourceUtils.getResourceAsString("/openapi.yaml");
            assertThat(yaml, is(expected));
        }
    }

    private void writeResource(String content, String name) throws IOException {
        if (Files.isDirectory(Paths.get("providence-jax-rs"))) {
            Path path = FileUtil.readCanonicalPath(Paths.get(".")
                                                        .toAbsolutePath()
                                                        .resolve("providence-jax-rs/src/test/resources/" + name));
            Files.createDirectories(path.getParent());
            Files.write(path, content.getBytes(StandardCharsets.UTF_8), CREATE, TRUNCATE_EXISTING);
        } else {
            Path path = FileUtil.readCanonicalPath(Paths.get(".")
                                                        .toAbsolutePath()
                                                        .resolve("src/test/resources/" + name));
            Files.createDirectories(path.getParent());
            Files.write(path, content.getBytes(StandardCharsets.UTF_8), CREATE, TRUNCATE_EXISTING);
        }
    }

    public static String get(GenericUrl url) throws IOException {
        return IOUtils.readString(new NetHttpTransport().createRequestFactory().buildGetRequest(url).execute().getContent());
    }

    @SafeVarargs
    private static <T> List<T> list(T... items) {
        return UnmodifiableList.copyOf(items);
    }
}
