package net.morimekta.providence.jax.rs.test_web_app;

import io.swagger.v3.jaxrs2.integration.resources.OpenApiResource;
import io.swagger.v3.oas.integration.SwaggerConfiguration;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import net.morimekta.providence.jax.rs.ProvidenceFeature;
import net.morimekta.providence.jax.rs.ProvidenceModelConverter;
import org.glassfish.jersey.server.ResourceConfig;

import static net.morimekta.util.collect.UnmodifiableSet.setOf;

/**
 * Test application implementation for jersey test app.
 */
public class TestApplication extends ResourceConfig {
    public TestApplication() {
        register(ProvidenceFeature.class);
        register(TestCalculatorResource.class);
        setApplicationName("Calculator");
        addOpenApiSpec();
    }

    private void addOpenApiSpec() {
        OpenAPI oas = new OpenAPI();
        Info info = new Info()
                .title("Test API")
                .description("Test Calculator API")
                .contact(new Contact().email("oss@morimekta.net"))
                .version("1.0.0");

        oas.info(info);

        OpenApiResource openApiResource = new OpenApiResource();
        SwaggerConfiguration oasConfig = new SwaggerConfiguration()
                .openAPI(oas)
                .prettyPrint(true)
                .modelConverterClasses(setOf(ProvidenceModelConverter.class.getName()))
                .resourcePackages(setOf("net.morimekta.providence.jax.rs.test_web_app"));
        openApiResource.setOpenApiConfiguration(oasConfig);
        register(openApiResource);
    }
}
