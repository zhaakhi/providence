package com.google.common.collect;

import java.util.ArrayList;

/** Fake immutable list to wrap for the guava ImmutableList. */
@SuppressWarnings("unused")
public class ImmutableList<T> extends ArrayList<T> {
    public ImmutableList() {}

    public ImmutableList(Builder<T> builder) {
        super(builder);
    }

    public static <T> ImmutableList<T> of(T instance) {
        return new ImmutableList<T>() {{
            add(instance);
        }};
    }

    public static class Builder<T> extends ArrayList<T> {
        public ImmutableList<T> build() {
            return new ImmutableList<>(this);
        }
    }
}
