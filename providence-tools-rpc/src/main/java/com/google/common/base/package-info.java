/**
 * Classes copied verbatim from guava., and trimmed for minimal
 * support. This is to use google-http-client, even though it
 * kinds claims to not require guava, it seems like it does
 * anyway.
 *
 * TODO: Do "repackaging" of the copied guava packages, and make
 * a search of google-http-client to see what is *really* required
 * I want to avoid adding guava as a whole, as it adds ~2MB of
 * classes to the jar file.
 */
package com.google.common.base;