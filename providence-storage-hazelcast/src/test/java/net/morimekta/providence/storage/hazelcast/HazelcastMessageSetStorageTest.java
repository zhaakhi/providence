package net.morimekta.providence.storage.hazelcast;

import com.hazelcast.core.IMap;
import net.morimekta.test.providence.storage.hazelcast.OptionalFields;
import org.junit.Test;

public class HazelcastMessageSetStorageTest extends TestBase {
    @Test
    public void testStorageConformity() {
        IMap<String, OptionalFields> map = instance.getMap(getClass().getName());
        HazelcastMessageSetStorage<String,OptionalFields> storage =
                new HazelcastMessageSetStorage<>(OptionalFields::getStringValue, map);
        assertConformity(OptionalFields::getStringValue, this::modifyNotKey, storage);
    }

    public OptionalFields modifyNotKey(OptionalFields field) {
        return field.mutate()
                    .setIntegerValue(generator.context().getRandom().nextInt())
                    .setBooleanValue(generator.context().getRandom().nextInt(2) == 1)
                    .setLongValue(generator.context().getRandom().nextLong())
                    .build();
    }
}
