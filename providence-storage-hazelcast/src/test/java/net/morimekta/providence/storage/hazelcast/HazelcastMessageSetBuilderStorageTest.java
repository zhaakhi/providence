package net.morimekta.providence.storage.hazelcast;


import com.hazelcast.core.IMap;
import com.hazelcast.query.PredicateBuilder;
import net.morimekta.test.providence.storage.hazelcast.OptionalFields;
import org.junit.Test;

import java.util.List;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;

public class HazelcastMessageSetBuilderStorageTest extends TestBase {
    @Test
    public void testStorageConformity() {
        IMap<String, OptionalFields._Builder> map = instance.getMap(getClass().getName());
        HazelcastMessageSetBuilderStorage<String,OptionalFields,OptionalFields._Builder> storage =
                new HazelcastMessageSetBuilderStorage<>(OptionalFields::getStringValue, map);
        assertConformity(OptionalFields::getStringValue,
                         this::modifyNotKey,
                         storage);
    }

    public OptionalFields modifyNotKey(OptionalFields field) {
        return field.mutate()
                    .setIntegerValue(generator.context().getRandom().nextInt())
                    .setBooleanValue(generator.context().getRandom().nextInt(2) == 1)
                    .setLongValue(generator.context().getRandom().nextLong())
                    .build();
    }

    @Test
    public void testQueries() {
        // To ensure that the messages are stored in an indexable way, we add an index to query it directly.
        IMap<String, OptionalFields._Builder> map = instance.getMap(getClass().getName() + "_queries");
        HazelcastMessageSetBuilderStorage<String,OptionalFields,OptionalFields._Builder> storage =
                new HazelcastMessageSetBuilderStorage<>(OptionalFields::getStringValue, map);

        map.addIndex(OptionalFields._Field.INTEGER_VALUE.getName(), true);

        for (int i = 0; i < 100; ++i) {
            storage.put(generator.generate(OptionalFields.kDescriptor));
        }
        generator.context().withMessageGenerator(OptionalFields.kDescriptor, gen -> {
            gen.setAlwaysPresent(OptionalFields._Field.INTEGER_VALUE);
        });
        OptionalFields toSearchFor = generator.generate(OptionalFields.kDescriptor).mergeWith(
                OptionalFields.builder().setStringValue(UUID.randomUUID().toString()).build());
        storage.put(toSearchFor);

        @SuppressWarnings("unchecked")
        HazelcastMessageSearcher<Integer, String, OptionalFields, OptionalFields._Builder>
                searcher = new HazelcastMessageSearcher<Integer, String, OptionalFields, OptionalFields._Builder>(
                        value -> new PredicateBuilder().getEntryObject()
                                                       .get(OptionalFields._Field.INTEGER_VALUE.getName())
                                                       .equal(value),
                        map);

        List<OptionalFields> res = searcher.search(toSearchFor.getIntegerValue());

        assertThat(res, hasSize(1)); // There is a 100 / 2 p 32 chance of collision.
        assertThat(res, contains(toSearchFor));
    }
}
