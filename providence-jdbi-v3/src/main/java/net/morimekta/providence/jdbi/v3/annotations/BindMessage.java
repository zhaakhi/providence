/*
 * Copyright 2019 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.jdbi.v3.annotations;

import net.morimekta.providence.PMessage;
import net.morimekta.providence.PMessageOrBuilder;
import net.morimekta.providence.descriptor.PField;
import net.morimekta.providence.descriptor.PMessageDescriptor;
import net.morimekta.providence.jdbi.v3.MessageNamedArgumentFinder;
import net.morimekta.util.collect.UnmodifiableMap;
import org.jdbi.v3.sqlobject.customizer.SqlStatementCustomizerFactory;
import org.jdbi.v3.sqlobject.customizer.SqlStatementCustomizingAnnotation;
import org.jdbi.v3.sqlobject.customizer.SqlStatementParameterCustomizer;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.lang.reflect.Type;
import java.util.LinkedHashMap;
import java.util.Map;

@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@SqlStatementCustomizingAnnotation(BindMessage.Factory.class)
public @interface BindMessage {
    String value() default "";
    BindType[] types() default {};

    class Factory implements SqlStatementCustomizerFactory {
        @Override
        @SuppressWarnings("unchecked")
        public SqlStatementParameterCustomizer createForParameter(Annotation annotation,
                                                                  Class<?> sqlObjectType,
                                                                  Method method,
                                                                  Parameter param,
                                                                  int index,
                                                                  Type paramType) {
            if (!(paramType instanceof Class)) {
                throw new IllegalArgumentException(paramType.getTypeName() + " is not a message type");
            }
            Class<?> paramClass = (Class) paramType;
            if (!PMessageOrBuilder.class.isAssignableFrom(paramClass)) {
                throw new IllegalArgumentException(paramClass.getName() + " is not a message class");
            }

            try {
                PMessageDescriptor descriptor = (PMessageDescriptor) paramClass.getDeclaredField("kDescriptor").get(null);
                BindMessage bind = (BindMessage) annotation;

                Map<PField, Integer> typeMapBuilder = new LinkedHashMap<>();
                for (BindType type : bind.types()) {
                    PField field = descriptor.findFieldByName(type.name());
                    if (field == null) {
                        throw new IllegalArgumentException("No field " + type.name() + " in " + descriptor.getQualifiedName());
                    }
                    typeMapBuilder.put(field, type.type());
                }
                final UnmodifiableMap<PField, Integer> map = UnmodifiableMap.copyOf(typeMapBuilder);
                return (stmt, arg) -> stmt.bindNamedArgumentFinder(
                        new MessageNamedArgumentFinder(bind.value(), (PMessageOrBuilder) arg, map));
            } catch (NoSuchFieldException | IllegalAccessException | ClassCastException e) {
                throw new IllegalArgumentException(
                        "Not a valid message class " + paramClass.getName(), e);
            }
        }
    }
}
