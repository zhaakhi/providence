/*
 * Copyright 2019 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.jdbi.v3.annotations;

import net.morimekta.providence.PEnumValue;
import net.morimekta.providence.descriptor.PEnumDescriptor;
import net.morimekta.providence.jdbi.v3.EnumValueArgumentFactory;
import net.morimekta.providence.jdbi.v3.EnumValueMapper;
import org.jdbi.v3.sqlobject.customizer.SqlStatementCustomizer;
import org.jdbi.v3.sqlobject.customizer.SqlStatementCustomizerFactory;
import org.jdbi.v3.sqlobject.customizer.SqlStatementCustomizingAnnotation;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import static net.morimekta.providence.jdbi.v3.annotations.RegisterEnumValueMapper.Factory.getDescriptor;

@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
@SqlStatementCustomizingAnnotation(RegisterEnumValueMappers.Factory.class)
public @interface RegisterEnumValueMappers {
    RegisterEnumValueMapper[] value();

    class Factory implements SqlStatementCustomizerFactory {
        @Override
        public SqlStatementCustomizer createForType(Annotation annotation, Class<?> sqlObjectType) {
            List<EnumValueMapper>               mappers   = new ArrayList<>();
            List<EnumValueArgumentFactory>      factories = new ArrayList<>();
            PEnumDescriptor<?>                  descriptor;
            for (RegisterEnumValueMapper register : ((RegisterEnumValueMappers) annotation).value()) {
                for (Class<? extends PEnumValue<?>> klass : register.value()) {
                    descriptor = getDescriptor(klass);
                    mappers.add(new EnumValueMapper<>(register.acceptUnknown(), descriptor));
                    factories.add(new EnumValueArgumentFactory(klass));
                }
            }
            return stmt -> {
                for (EnumValueMapper mapper : mappers) {
                    stmt.registerColumnMapper(mapper.getType(), mapper);
                }
                for (EnumValueArgumentFactory factory : factories) {
                    stmt.registerArgument(factory);
                }
            };
        }

        @Override
        public SqlStatementCustomizer createForMethod(Annotation annotation, Class<?> sqlObjectType, Method method) {
            return createForType(annotation, sqlObjectType);
        }
    }
}
