package net.morimekta.providence.jdbi.v3;

import net.morimekta.providence.PEnumValue;
import org.jdbi.v3.core.argument.Argument;
import org.jdbi.v3.core.statement.StatementContext;

import javax.annotation.Nonnull;
import java.sql.PreparedStatement;
import java.sql.SQLException;

class EnumValueArgument implements Argument {
    private final PEnumValue value;

    public EnumValueArgument(@Nonnull PEnumValue value) {this.value = value;}

    @Override
    public String toString() {
        return value.asInteger() + " (" + value.asString() + ")";
    }

    @Override
    public void apply(int position, PreparedStatement statement, StatementContext ctx)
            throws SQLException {
        statement.setInt(position, value.asInteger());
    }
}
