/*
 * Copyright 2019 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.jdbi.v3.annotations;

import net.morimekta.providence.PMessage;
import net.morimekta.providence.descriptor.PField;
import net.morimekta.providence.descriptor.PMessageDescriptor;
import net.morimekta.providence.jdbi.v3.MessageRowMapper;
import net.morimekta.util.collect.UnmodifiableMap;
import org.jdbi.v3.sqlobject.customizer.SqlStatementCustomizer;
import org.jdbi.v3.sqlobject.customizer.SqlStatementCustomizerFactory;
import org.jdbi.v3.sqlobject.customizer.SqlStatementCustomizingAnnotation;

import javax.annotation.Nonnull;
import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
@SqlStatementCustomizingAnnotation(RegisterMessageMapper.Factory.class)
@Repeatable(RegisterMessageMappers.class)
public @interface RegisterMessageMapper {
    Class<? extends PMessage<?>> value();
    BindField[] fields() default {};

    class Factory implements SqlStatementCustomizerFactory {
        @Override
        @SuppressWarnings("unchecked")
        public SqlStatementCustomizer createForType(Annotation annotation, Class<?> sqlObjectType) {
            RegisterMessageMapper register = (RegisterMessageMapper) annotation;
            if (register.value().isInterface()) {
                throw new IllegalArgumentException("Interfaces cannot be used as message return types");
            }
            PMessageDescriptor descriptor = getDescriptor(register);
            Map<String, PField> fieldMap = makeFieldMap(descriptor, register);
            MessageRowMapper mapper = new MessageRowMapper(descriptor, fieldMap);
            return stmt -> stmt.registerRowMapper(register.value(), mapper);
        }

        @Override
        public SqlStatementCustomizer createForMethod(Annotation annotation, Class<?> sqlObjectType, Method method) {
            return createForType(annotation, sqlObjectType);
        }

        static Map<String, PField> makeFieldMap(@Nonnull PMessageDescriptor descriptor,
                                                @Nonnull RegisterMessageMapper register) {
            Map<String, PField> map = new HashMap<>();
            for (PField field : descriptor.getFields()) {
                map.put(field.getName(), field);
            }
            for (BindField bind : register.fields()) {
                PField field = descriptor.fieldForName(bind.field());
                map.put(bind.column(), field);
            }
            return UnmodifiableMap.copyOf(map);
        }

        @Nonnull
        static PMessageDescriptor getDescriptor(RegisterMessageMapper register) {
            try {
                return Objects.requireNonNull(
                        (PMessageDescriptor) register.value()
                                                     .getDeclaredField("kDescriptor")
                                                     .get(null));
            } catch (IllegalAccessException | NoSuchFieldException | NullPointerException e) {
                throw new IllegalArgumentException(
                        "Not a valid message class " + register.value().getName());
            }
        }
    }
}
