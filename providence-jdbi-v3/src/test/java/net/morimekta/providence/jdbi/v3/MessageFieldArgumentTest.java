package net.morimekta.providence.jdbi.v3;

import net.morimekta.test.providence.storage.jdbc.CompactFields;
import org.junit.Test;

import java.util.HashSet;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;

public class MessageFieldArgumentTest {
    @Test
    public void testBasic() {
        CompactFields compactFields = CompactFields
                .builder()
                .setId(42)
                .setName("Arthur")
                .build();
        MessageFieldArgument<CompactFields> arg =
                new MessageFieldArgument<>(compactFields, CompactFields._Field.ID);
        MessageFieldArgument<CompactFields> same =
                new MessageFieldArgument<>(compactFields, CompactFields._Field.ID);
        MessageFieldArgument<CompactFields> other =
                new MessageFieldArgument<>(compactFields, CompactFields._Field.NAME);

        assertThat(arg.toString(), is("42"));
        assertThat(arg.hashCode(), is(same.hashCode()));
        assertThat(arg.hashCode(), is(not(other.hashCode())));
        assertThat(arg.equals(arg), is(true));
        assertThat(arg.equals("foo"), is(false));
        assertThat(arg.equals(same), is(true));
        assertThat(arg.equals(other), is(false));

        HashSet<MessageFieldArgument> set = new HashSet<>();
        set.add(arg);
        assertThat(set.contains(same), is(true));
    }
}
