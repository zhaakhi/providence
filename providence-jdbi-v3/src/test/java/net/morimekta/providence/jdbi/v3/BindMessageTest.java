package net.morimekta.providence.jdbi.v3;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import net.morimekta.providence.jdbi.v3.annotations.BindEnumName;
import net.morimekta.providence.jdbi.v3.annotations.BindEnumValue;
import net.morimekta.providence.jdbi.v3.annotations.BindField;
import net.morimekta.providence.jdbi.v3.annotations.BindMessage;
import net.morimekta.providence.jdbi.v3.annotations.BindType;
import net.morimekta.providence.jdbi.v3.annotations.RegisterEnumValueMapper;
import net.morimekta.providence.jdbi.v3.annotations.RegisterMessageMapper;
import net.morimekta.test.providence.storage.jdbc.CompactFields;
import net.morimekta.test.providence.storage.jdbc.Fibonacci;
import net.morimekta.test.providence.storage.jdbc.OptionalFields;
import net.morimekta.test.providence.storage.jdbc.Other;
import net.morimekta.util.collect.UnmodifiableList;
import org.jdbi.v3.core.Handle;
import org.jdbi.v3.sqlobject.SqlObject;
import org.jdbi.v3.sqlobject.config.RegisterArgumentFactory;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindList;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.sql.Types;
import java.util.Collection;
import java.util.List;

import static net.morimekta.util.collect.UnmodifiableList.listOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.fail;

@RunWith(DataProviderRunner.class)
public class BindMessageTest {
    @Rule
    public TestDatabase db = new TestDatabase("/mappings_v3.sql");

    @RegisterMessageMapper(value = OptionalFields.class, fields = {
            @BindField(field = "message", column = "compact")
    })
    @RegisterMessageMapper(CompactFields.class)
    @RegisterArgumentFactory(EnumValueArgumentFactory.class)
    @RegisterEnumValueMapper(Fibonacci.class)
    @RegisterEnumValueMapper(value = Other.class, acceptUnknown = true)
    public interface TestDAO extends SqlObject {
        @SqlUpdate("INSERT INTO mappings_v3.default_mappings\n" +
                   "(id, timestamp_s, timestamp_ms)\n" +
                   "VALUES (:id, :timestamp_s, :timestamp_ms)")
        void insert(
                @BindMessage(types = {
                        @BindType(name = "timestamp_s", type = Types.TIMESTAMP),
                        @BindType(name = "timestamp_ms", type = Types.TIMESTAMP_WITH_TIMEZONE),
                }) OptionalFields fields);

        @SqlQuery("SELECT * FROM mappings_v3.default_mappings\n" +
                  "WHERE id = :id")
        OptionalFields get(@Bind("id") int id);

        MessageUpserter<CompactFields> UPSERTER =
                new MessageUpserter.Builder<>(CompactFields.kDescriptor, "mappings_v3.compact")
                        .setAll()
                        .onDuplicateKeyUpdateAllExcept(CompactFields._Field.ID)
                        .build();
        default void insertAll(Collection<CompactFields> fields) {
            withHandle(handle -> UPSERTER.execute(handle, UnmodifiableList.copyOf(fields)));
        }

        @SqlQuery("SELECT * FROM mappings_v3.compact\n" +
                  "WHERE id IN (<ids>)")
        @RegisterMessageMapper(CompactFields.class)
        List<CompactFields> getAll(@BindList("ids") Collection<Integer> ids);

        @SqlUpdate("INSERT INTO mappings_v3.default_mappings\n" +
                   "(id, fib, compact, name)\n" +
                   "VALUES (:id, :fib, :message, :name)")
        void insert(@Bind("id") Fibonacci id,
                    @Bind("fib") Fibonacci fib,
                    @BindEnumName("message") Fibonacci message,
                    @BindEnumName("name") Fibonacci name);
    }

    public interface AltTestDAO {
        @SqlUpdate("INSERT INTO mappings_v3.default_mappings\n" +
                   "(id, timestamp_s, timestamp_ms, name)\n" +
                   "VALUES (:id, :timestamp_s, :timestamp_ms, :binary_message.name)")
        void insert(
                @BindMessage(types = {
                        @BindType(name = "timestamp_s", type = Types.TIMESTAMP),
                        @BindType(name = "timestamp_ms", type = Types.TIMESTAMP),
                }) OptionalFields fields);

        @SqlUpdate("INSERT INTO mappings_v3.default_mappings\n" +
                   "(id, fib, compact, name)\n" +
                   "VALUES (:id, :fib, :message, :name)")
        void insert(@BindEnumValue("id") Fibonacci id,
                    @BindEnumValue("fib") Fibonacci fib,
                    @BindEnumName("message") Fibonacci message,
                    @BindEnumName("name") Fibonacci name);

        @SqlQuery("SELECT * FROM mappings_v3.default_mappings\n" +
                  "WHERE id = :id")
        @RegisterMessageMapper(OptionalFields.class)
        OptionalFields get(@Bind("id") int id);

        @SqlQuery("SELECT id FROM mappings_v3.default_mappings\n")
        @RegisterEnumValueMapper(value = Fibonacci.class, acceptUnknown = true)
        List<Fibonacci> allFibs();
    }

    @Test
    public void testDAO() {
        try (Handle handle = db.getDBI().open()) {
            TestDAO dao = handle.attach(TestDAO.class);

            OptionalFields source = OptionalFields
                    .builder()
                    .setId(42)
                    .setTimestampS(1234565)
                    .setTimestampMs(123456789012345L)
                    .build();

            dao.insert(source);

            OptionalFields ret = dao.get(42);
            assertThat(ret, is(notNullValue()));
            assertThat(ret, is(source));

            List<CompactFields> compact = listOf(CompactFields.builder().setName("foo").setLabel("bar").setId(42).build());
            dao.insertAll(compact);

            List<CompactFields> all = dao.getAll(listOf(42));
            assertThat(all, is(compact));

            dao.insert(Fibonacci.FIFTEENTH, null, null, Fibonacci.EIGHTEENTH);
        }
    }

    @Test
    public void testAltDAO() {
        try (Handle handle = db.getDBI().open()) {
            AltTestDAO dao = handle.attach(AltTestDAO.class);

            OptionalFields source = OptionalFields
                    .builder()
                    .setId(42)
                    .setTimestampS(1234565)
                    .setTimestampMs(1234)
                    .build();

            dao.insert(source);

            OptionalFields ret = dao.get(42);
            assertThat(ret, is(notNullValue()));
            assertThat(ret, is(source));

            dao.insert(Fibonacci.FIFTEENTH, null, null, Fibonacci.EIGHTEENTH);

            assertThat(dao.allFibs(), is(listOf(null, Fibonacci.FIFTEENTH)));
        }
    }

    public interface BadBindMessage {
        @SuppressWarnings("unused")
        @SqlUpdate("UPDATE default_mappings\n" +
                   "SET present = :present" +
                   "WHERE id = :id")
        void update(@BindMessage int value);
    }
    public interface BadBindMessage2 {
        @SuppressWarnings("unused")
        @SqlUpdate("UPDATE default_mappings\n" +
                   "SET present = :present" +
                   "WHERE id = :id")
        void update(@BindMessage String value);
    }

    @DataProvider
    public static Object[][] badDao() {
        return new Object[][] {
                {BadBindMessage.class, "int is not a message class"},
                {BadBindMessage2.class, "java.lang.String is not a message class"},
        };
    }

    @Test
    @UseDataProvider("badDao")
    public void testBadDao(Class<?> dao, String message) {
        try (Handle handle = db.getDBI().open()) {
            handle.attach(dao);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            try {
                assertThat(e.getMessage(), is(message));
            } catch (AssertionError ae) {
                ae.initCause(e);
                throw ae;
            }
        }
    }
}
