package net.morimekta.providence.model;

/**
 * Struct variant for StructType. The lower-case of the enum value is the
 * thrift keyword.
 * <p>
 * STRUCT:    Normal Structure.
 * UNION:     Only one field set to be valid.
 * EXCEPTION: Handled as exception (Java, Python etc.).
 * <p>
 * Providence Only:
 * <p>
 * INTERFACE: Not a real type. Only interface. Struct can &#39;implement&#39; this
 *            And will then require to have matching fields. Field ids not
 *            allowed.
 */
@javax.annotation.Generated(
        value = "net.morimekta.providence:providence-generator-java",
        comments = "java:serializable")
public enum MessageVariant
        implements net.morimekta.providence.PEnumValue<MessageVariant> {
    STRUCT(1, "STRUCT", "STRUCT"),
    UNION(2, "UNION", "UNION"),
    EXCEPTION(3, "EXCEPTION", "EXCEPTION"),
    INTERFACE(4, "INTERFACE", "INTERFACE"),
    ;

    private final int    mId;
    private final String mName;
    private final String mPojoName;

    MessageVariant(int id, String name, String pojoName) {
        mId = id;
        mName = name;
        mPojoName = pojoName;
    }

    @Override
    public int asInteger() {
        return mId;
    }

    @Override
    @javax.annotation.Nonnull
    public String asString() {
        return mName;
    }

    @Override
    @javax.annotation.Nonnull
    public String getPojoName() {
        return mPojoName;
    }

    /**
     * Find a value based in its ID
     *
     * @param id Id of value
     * @return Value found or null
     */
    @javax.annotation.Nullable
    public static MessageVariant findById(Integer id) {
        if (id == null) {
            return null;
        }
        switch (id) {
            case 1: return MessageVariant.STRUCT;
            case 2: return MessageVariant.UNION;
            case 3: return MessageVariant.EXCEPTION;
            case 4: return MessageVariant.INTERFACE;
            default: return null;
        }
    }

    /**
     * Find a value based in its name
     *
     * @param name Name of value
     * @return Value found or null
     */
    @javax.annotation.Nullable
    public static MessageVariant findByName(String name) {
        if (name == null) {
            return null;
        }
        switch (name) {
            case "STRUCT": return MessageVariant.STRUCT;
            case "UNION": return MessageVariant.UNION;
            case "EXCEPTION": return MessageVariant.EXCEPTION;
            case "INTERFACE": return MessageVariant.INTERFACE;
            default: return null;
        }
    }

    /**
     * Find a value based in its POJO name
     *
     * @param name Name of value
     * @return Value found or null
     */
    @javax.annotation.Nullable
    public static MessageVariant findByPojoName(String name) {
        if (name == null) {
            return null;
        }
        switch (name) {
            case "STRUCT": return MessageVariant.STRUCT;
            case "UNION": return MessageVariant.UNION;
            case "EXCEPTION": return MessageVariant.EXCEPTION;
            case "INTERFACE": return MessageVariant.INTERFACE;
            default: return null;
        }
    }

    /**
     * Get a value based in its ID
     *
     * @param id Id of value
     * @return Value found
     * @throws IllegalArgumentException If no value for id is found
     */
    @javax.annotation.Nonnull
    public static MessageVariant valueForId(int id) {
        MessageVariant value = findById(id);
        if (value == null) {
            throw new IllegalArgumentException("No p_model.MessageVariant for id " + id);
        }
        return value;
    }

    /**
     * Get a value based in its name
     *
     * @param name Name of value
     * @return Value found
     * @throws IllegalArgumentException If no value for name is found, or null name
     */
    @javax.annotation.Nonnull
    public static MessageVariant valueForName(@javax.annotation.Nonnull String name) {
        MessageVariant value = findByName(name);
        if (value == null) {
            throw new IllegalArgumentException("No p_model.MessageVariant for name \"" + name + "\"");
        }
        return value;
    }

    /**
     * Get a value based in its name
     *
     * @param name Name of value
     * @return Value found
     * @throws IllegalArgumentException If no value for name is found, or null name
     */
    @javax.annotation.Nonnull
    public static MessageVariant valueForPojoName(@javax.annotation.Nonnull String name) {
        MessageVariant value = findByPojoName(name);
        if (value == null) {
            throw new IllegalArgumentException("No p_model.MessageVariant for name \"" + name + "\"");
        }
        return value;
    }

    public static final class _Builder extends net.morimekta.providence.PEnumBuilder<MessageVariant> {
        private MessageVariant mValue;

        @Override
        @javax.annotation.Nonnull
        public _Builder setById(int value) {
            mValue = MessageVariant.findById(value);
            return this;
        }

        @Override
        @javax.annotation.Nonnull
        public _Builder setByName(String name) {
            mValue = MessageVariant.findByName(name);
            return this;
        }

        @Override
        public boolean valid() {
            return mValue != null;
        }

        @Override
        public MessageVariant build() {
            return mValue;
        }
    }

    public static final net.morimekta.providence.descriptor.PEnumDescriptor<MessageVariant> kDescriptor;

    @Override
    @javax.annotation.Nonnull
    public net.morimekta.providence.descriptor.PEnumDescriptor<MessageVariant> descriptor() {
        return kDescriptor;
    }

    public static net.morimekta.providence.descriptor.PEnumDescriptorProvider<MessageVariant> provider() {
        return new net.morimekta.providence.descriptor.PEnumDescriptorProvider<MessageVariant>(kDescriptor);
    }

    private static final class _Descriptor
            extends net.morimekta.providence.descriptor.PEnumDescriptor<MessageVariant> {
        public _Descriptor() {
            super("p_model", "MessageVariant", _Builder::new);
        }

        @Override
        @javax.annotation.Nonnull
        public boolean isInnerType() {
            return false;
        }

        @Override
        @javax.annotation.Nonnull
        public boolean isAutoType() {
            return false;
        }

        @Override
        @javax.annotation.Nonnull
        public MessageVariant[] getValues() {
            return MessageVariant.values();
        }

        @Override
        @javax.annotation.Nullable
        public MessageVariant findById(int id) {
            return MessageVariant.findById(id);
        }

        @Override
        @javax.annotation.Nullable
        public MessageVariant findByName(String name) {
            return MessageVariant.findByName(name);
        }

        @Override
        @javax.annotation.Nullable
        public MessageVariant findByPojoName(String name) {
            return MessageVariant.findByPojoName(name);
        }
    }

    static {
        kDescriptor = new _Descriptor();
    }
}
