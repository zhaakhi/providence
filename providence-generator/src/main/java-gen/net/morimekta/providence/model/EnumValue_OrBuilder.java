package net.morimekta.providence.model;

/**
 * &lt;name&gt; (= &lt;value&gt;)
 */
@javax.annotation.Generated(
        value = "net.morimekta.providence:providence-generator-java",
        comments = "java:serializable")
@SuppressWarnings("unused")
public interface EnumValue_OrBuilder extends net.morimekta.providence.model.Decl , net.morimekta.providence.PMessageOrBuilder<EnumValue> {
    /**
     * @return The id value.
     */
    int getId();

    /**
     * @return Optional id value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<Integer> optionalId();

    /**
     * @return If id is present.
     */
    boolean hasId();

    /**
     * @return The annotations value.
     */
    java.util.Map<String,String> getAnnotations();

    /**
     * @return Optional annotations value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Map<String,String>> optionalAnnotations();

    /**
     * @return If annotations is present.
     */
    boolean hasAnnotations();

    /**
     * @return Number of entries in annotations.
     */
    int numAnnotations();

}
