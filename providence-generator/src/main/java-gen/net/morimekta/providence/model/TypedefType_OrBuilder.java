package net.morimekta.providence.model;

/**
 * typedef &lt;type&gt; &lt;name&gt;
 */
@javax.annotation.Generated(
        value = "net.morimekta.providence:providence-generator-java",
        comments = "java:serializable")
@SuppressWarnings("unused")
public interface TypedefType_OrBuilder extends net.morimekta.providence.model.Decl , net.morimekta.providence.PMessageOrBuilder<TypedefType> {
    /**
     * @return The type value.
     */
    @javax.annotation.Nonnull
    String getType();

    /**
     * @return If type is present.
     */
    boolean hasType();

}
