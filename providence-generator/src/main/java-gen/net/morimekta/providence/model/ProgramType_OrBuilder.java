package net.morimekta.providence.model;

/**
 * &lt;namespace&gt;* &lt;include&gt;* &lt;declataion&gt;*
 */
@javax.annotation.Generated(
        value = "net.morimekta.providence:providence-generator-java",
        comments = "java:serializable")
@SuppressWarnings("unused")
public interface ProgramType_OrBuilder extends net.morimekta.providence.PMessageOrBuilder<ProgramType> {
    /**
     * Program documentation must come before the first statement of the header.
     *
     * @return The documentation value.
     */
    String getDocumentation();

    /**
     * Program documentation must come before the first statement of the header.
     *
     * @return Optional documentation value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<String> optionalDocumentation();

    /**
     * @return If documentation is present.
     */
    boolean hasDocumentation();

    /**
     * The program name, deducted from the .thrift IDL file name.
     *
     * @return The program_name value.
     */
    @javax.annotation.Nonnull
    String getProgramName();

    /**
     * @return If program_name is present.
     */
    boolean hasProgramName();

    /**
     * List of included thrift files. Same as from the actual thrift file.
     * <p>
     * include &quot;&lt;program&gt;.thrift&quot;
     *
     * @return The includes value.
     */
    java.util.Map<String,String> getIncludes();

    /**
     * List of included thrift files. Same as from the actual thrift file.
     * <p>
     * include &quot;&lt;program&gt;.thrift&quot;
     *
     * @return Optional includes value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Map<String,String>> optionalIncludes();

    /**
     * @return If includes is present.
     */
    boolean hasIncludes();

    /**
     * @return Number of entries in includes.
     */
    int numIncludes();

    /**
     * Map of language to laguage dependent namespace identifier.
     * <p>
     * namespace &lt;key&gt; &lt;value&gt;
     *
     * @return The namespaces value.
     */
    java.util.Map<String,String> getNamespaces();

    /**
     * Map of language to laguage dependent namespace identifier.
     * <p>
     * namespace &lt;key&gt; &lt;value&gt;
     *
     * @return Optional namespaces value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Map<String,String>> optionalNamespaces();

    /**
     * @return If namespaces is present.
     */
    boolean hasNamespaces();

    /**
     * @return Number of entries in namespaces.
     */
    int numNamespaces();

    /**
     * List of declarations in the program file. Same order as in the thrift file.
     *
     * @return The decl value.
     */
    java.util.List<net.morimekta.providence.model.Declaration> getDecl();

    /**
     * List of declarations in the program file. Same order as in the thrift file.
     *
     * @return Optional decl value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.List<net.morimekta.providence.model.Declaration>> optionalDecl();

    /**
     * @return If decl is present.
     */
    boolean hasDecl();

    /**
     * @return Number of entries in decl.
     */
    int numDecl();

}
