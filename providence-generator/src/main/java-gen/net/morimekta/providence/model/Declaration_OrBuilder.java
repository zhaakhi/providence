package net.morimekta.providence.model;

/**
 * ( &lt;enum&gt; | &lt;typedef&gt; | &lt;struct&gt; | &lt;service&gt; | &lt;const&gt; )
 */
@javax.annotation.Generated(
        value = "net.morimekta.providence:providence-generator-java",
        comments = "java:serializable")
@SuppressWarnings("unused")
public interface Declaration_OrBuilder extends net.morimekta.providence.PMessageOrBuilder<Declaration> {
    /**
     * @return The decl_enum value.
     */
    net.morimekta.providence.model.EnumType getDeclEnum();

    /**
     * @return Optional decl_enum value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<net.morimekta.providence.model.EnumType> optionalDeclEnum();

    /**
     * @return If decl_enum is present.
     */
    boolean hasDeclEnum();

    /**
     * @return The decl_typedef value.
     */
    net.morimekta.providence.model.TypedefType getDeclTypedef();

    /**
     * @return Optional decl_typedef value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<net.morimekta.providence.model.TypedefType> optionalDeclTypedef();

    /**
     * @return If decl_typedef is present.
     */
    boolean hasDeclTypedef();

    /**
     * @return The decl_message value.
     */
    net.morimekta.providence.model.MessageType getDeclMessage();

    /**
     * @return Optional decl_message value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<net.morimekta.providence.model.MessageType> optionalDeclMessage();

    /**
     * @return If decl_message is present.
     */
    boolean hasDeclMessage();

    /**
     * @return The decl_service value.
     */
    net.morimekta.providence.model.ServiceType getDeclService();

    /**
     * @return Optional decl_service value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<net.morimekta.providence.model.ServiceType> optionalDeclService();

    /**
     * @return If decl_service is present.
     */
    boolean hasDeclService();

    /**
     * @return The decl_const value.
     */
    net.morimekta.providence.model.ConstType getDeclConst();

    /**
     * @return Optional decl_const value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<net.morimekta.providence.model.ConstType> optionalDeclConst();

    /**
     * @return If decl_const is present.
     */
    boolean hasDeclConst();

}
