package net.morimekta.providence.model;

/**
 * A meta object describing a parsed program file. This may include the
 * included programs as their own meta file. The lines of the original
 * program .thrift file is also included.
 */
@javax.annotation.Generated(
        value = "net.morimekta.providence:providence-generator-java",
        comments = "java:serializable")
@SuppressWarnings("unused")
public interface ProgramMeta_OrBuilder extends net.morimekta.providence.PMessageOrBuilder<ProgramMeta> {
    /**
     * Full absolute path to the file.
     *
     * @return The file_path value.
     */
    String getFilePath();

    /**
     * Full absolute path to the file.
     *
     * @return Optional file_path value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<String> optionalFilePath();

    /**
     * @return If file_path is present.
     */
    boolean hasFilePath();

    /**
     * The lines of the program file
     *
     * @return The file_lines value.
     */
    java.util.List<String> getFileLines();

    /**
     * The lines of the program file
     *
     * @return Optional file_lines value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.List<String>> optionalFileLines();

    /**
     * @return If file_lines is present.
     */
    boolean hasFileLines();

    /**
     * @return Number of entries in file_lines.
     */
    int numFileLines();

    /**
     * The program type definition
     *
     * @return The program value.
     */
    net.morimekta.providence.model.ProgramType getProgram();

    /**
     * The program type definition
     *
     * @return Optional program value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<net.morimekta.providence.model.ProgramType> optionalProgram();

    /**
     * @return If program is present.
     */
    boolean hasProgram();

    /**
     * Map of program name to meta of included programs
     *
     * @return The includes value.
     */
    java.util.Map<String,net.morimekta.providence.model.ProgramMeta> getIncludes();

    /**
     * Map of program name to meta of included programs
     *
     * @return Optional includes value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Map<String,net.morimekta.providence.model.ProgramMeta>> optionalIncludes();

    /**
     * @return If includes is present.
     */
    boolean hasIncludes();

    /**
     * @return Number of entries in includes.
     */
    int numIncludes();

}
