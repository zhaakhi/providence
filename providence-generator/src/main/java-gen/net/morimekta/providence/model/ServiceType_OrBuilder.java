package net.morimekta.providence.model;

/**
 * service (extends &lt;extend&gt;)? {
 *   (&lt;method&gt; [;,]?)*
 * }
 */
@javax.annotation.Generated(
        value = "net.morimekta.providence:providence-generator-java",
        comments = "java:serializable")
@SuppressWarnings("unused")
public interface ServiceType_OrBuilder extends net.morimekta.providence.model.Decl , net.morimekta.providence.PMessageOrBuilder<ServiceType> {
    /**
     * @return The extend value.
     */
    String getExtend();

    /**
     * @return Optional extend value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<String> optionalExtend();

    /**
     * @return If extend is present.
     */
    boolean hasExtend();

    /**
     * @return The methods value.
     */
    @javax.annotation.Nonnull
    java.util.List<net.morimekta.providence.model.FunctionType> getMethods();

    /**
     * @return If methods is present.
     */
    boolean hasMethods();

    /**
     * @return Number of entries in methods.
     */
    int numMethods();

    /**
     * @return The annotations value.
     */
    java.util.Map<String,String> getAnnotations();

    /**
     * @return Optional annotations value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Map<String,String>> optionalAnnotations();

    /**
     * @return If annotations is present.
     */
    boolean hasAnnotations();

    /**
     * @return Number of entries in annotations.
     */
    int numAnnotations();

}
