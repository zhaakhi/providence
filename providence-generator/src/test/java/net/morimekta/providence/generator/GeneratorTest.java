package net.morimekta.providence.generator;

import net.morimekta.providence.generator.util.FileManager;
import net.morimekta.providence.reflect.ProgramRegistry;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by morimekta on 23.04.17.
 */
public class GeneratorTest {
    @Rule
    public TemporaryFolder tmp = new TemporaryFolder();

    @Test
    public void testGenerator() {
        FileManager fileManager = new FileManager(tmp.getRoot().toPath());
        TestGenerator generator = new TestGenerator(fileManager);

        assertThat(generator.getFileManager(), is(sameInstance(fileManager)));
    }

    @Test
    public void testGeneratorException() {
        GeneratorException e = new GeneratorException("a");
        assertThat(e.getMessage(), is("a"));

        GeneratorException ex = new GeneratorException("b", e);
        assertThat(ex.getMessage(), is("b"));
        assertThat(ex.getCause(), is(sameInstance(e)));
    }

    private static class TestGenerator extends Generator {
        TestGenerator(FileManager manager) {
            super(manager);
        }

        @Override
        public void generate(ProgramRegistry document) throws GeneratorException {
            // ignore
        }
    }
}
