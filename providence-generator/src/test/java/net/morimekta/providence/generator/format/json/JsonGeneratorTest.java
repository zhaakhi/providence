package net.morimekta.providence.generator.format.json;

import net.morimekta.providence.generator.GeneratorException;
import net.morimekta.providence.generator.util.FileManager;
import net.morimekta.providence.reflect.ProgramLoader;
import net.morimekta.testing.ResourceUtils;
import net.morimekta.util.io.IOUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by morimekta on 6/12/16.
 */
public class JsonGeneratorTest {
    @Rule
    public  TemporaryFolder tmp = new TemporaryFolder();

    private File          out;
    private ProgramLoader programLoader;
    private JsonGenerator generator;

    @Before
    public void setUp() throws IOException {
        out = tmp.newFolder("out");

        FileManager     fileManager = new FileManager(out.toPath());

        programLoader = new ProgramLoader();
        generator = new JsonGenerator(fileManager);
    }

    @Test
    public void testGenerate() throws GeneratorException, IOException {
        ResourceUtils.copyResourceTo("/net/morimekta/providence/generator/format/json/test.thrift", tmp.getRoot());
        ResourceUtils.copyResourceTo("/net/morimekta/providence/generator/format/json/included.thrift", tmp.getRoot());
        File file = new File(tmp.getRoot(), "test.thrift");

        generator.generate(programLoader.load(file));

        File test = new File(out, "test.json");
        assertTrue(test.exists());

        assertEquals("{\n" +
                     "    \"program_name\": \"test\",\n" +
                     "    \"includes\": {\n" +
                     "        \"included\": \"included.json\"\n" +
                     "    },\n" +
                     "    \"namespaces\": {\n" +
                     "        \"java\": \"net.morimekta.test.json.test\"\n" +
                     "    },\n" +
                     "    \"decl\": [\n" +
                     "        {\n" +
                     "            \"decl_message\": {\n" +
                     "                \"name\": \"Test\",\n" +
                     "                \"fields\": [\n" +
                     "                    {\n" +
                     "                        \"id\": 1,\n" +
                     "                        \"requirement\": \"REQUIRED\",\n" +
                     "                        \"type\": \"i32\",\n" +
                     "                        \"name\": \"test\"\n" +
                     "                    },\n" +
                     "                    {\n" +
                     "                        \"id\": 15,\n" +
                     "                        \"requirement\": \"OPTIONAL\",\n" +
                     "                        \"type\": \"i32\",\n" +
                     "                        \"name\": \"another\"\n" +
                     "                    },\n" +
                     "                    {\n" +
                     "                        \"id\": 2,\n" +
                     "                        \"type\": \"included.Included\",\n" +
                     "                        \"name\": \"included\"\n" +
                     "                    }\n" +
                     "                ]\n" +
                     "            }\n" +
                     "        }\n" +
                     "    ]\n" +
                     "}\n",
                     IOUtils.readString(new FileInputStream(test)));
    }

}
