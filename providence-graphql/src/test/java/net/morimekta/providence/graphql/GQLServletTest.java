package net.morimekta.providence.graphql;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.client.http.ByteArrayContent;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpResponse;
import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import net.morimekta.providence.descriptor.PField;
import net.morimekta.providence.descriptor.PMessageDescriptor;
import net.morimekta.providence.graphql.gql.GQLField;
import net.morimekta.providence.graphql.internal.TestNetUtil;
import net.morimekta.providence.serializer.JsonSerializer;
import net.morimekta.test.graphql.Character;
import net.morimekta.test.graphql.CharacterUnion;
import net.morimekta.test.graphql.Droid;
import net.morimekta.test.graphql.HeightArguments;
import net.morimekta.test.graphql.Human;
import net.morimekta.test.graphql.Measurement;
import net.morimekta.test.graphql.Movie;
import net.morimekta.test.graphql.MyMutation;
import net.morimekta.test.graphql.MyQuery;
import net.morimekta.test.graphql.NotFoundException;
import net.morimekta.test.graphql.Purpose;
import net.morimekta.testing.RegexDataProviderMethodResolver;
import net.morimekta.util.collect.UnmodifiableList;
import net.morimekta.util.io.IOUtils;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.annotation.Nonnull;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static com.tngtech.java.junit.dataprovider.UseDataProvider.ResolveStrategy.AGGREGATE_ALL_MATCHES;
import static net.morimekta.test.graphql.CharacterUnion.withDroid;
import static net.morimekta.test.graphql.CharacterUnion.withHuman;
import static net.morimekta.testing.ResourceUtils.getResourceAsString;
import static net.morimekta.util.collect.UnmodifiableList.listOf;
import static net.morimekta.util.collect.UnmodifiableMap.mapOf;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(DataProviderRunner.class)
public class GQLServletTest {
    private static final GQLDefinition definition = GQLDefinition
            .builder()
            .query(MyQuery.kDescriptor)
            .mutation(MyMutation.kDescriptor)
            .idField(Character._Field.ID, Human._Field.ID, Droid._Field.ID, Movie._Field.ID)
            .idField(MyQuery.Hero_Request._Field.ID, MyQuery.Heroes_Request._Field.APPEARS_IN)
            .asInterface(CharacterUnion.kDescriptor)
            .build();

    private Map<String, CharacterUnion>     characters;
    private MyQuery.Iface                     queryImpl;
    private MyMutation.Iface                mutationImpl;
    private Server                          server;
    private int                             port;
    private Movie A_NEW_HOPE;
    private Movie THE_EMPIRE_STRIKES_BACK;
    private Movie THE_RETURN_OF_THE_JEDI;

    @Before
    public void setUp() throws Exception {
        AtomicInteger nextId = new AtomicInteger(3);

        A_NEW_HOPE = Movie.builder()
                          .setId("ANH")
                          .setName("A New Hope")
                          .setYear(1977)
                          .build();
        THE_EMPIRE_STRIKES_BACK = Movie.builder()
                                       .setId("TESB")
                                       .setName("The Empire Strikes Back")
                                       .setYear(1979)
                                       .build();
        THE_RETURN_OF_THE_JEDI = Movie.builder()
                                      .setId("TROTJ")
                                      .setName("The Return Of The Jedi")
                                      .setYear(1982)
                                      .build();

        characters = Collections.synchronizedMap(new LinkedHashMap<String, CharacterUnion>() {{
            put("1001", withHuman(
                    Human.builder()
                         .setId("1001")
                         .setName("Luke Skywalker")
                         .addToAppearsIn(A_NEW_HOPE,
                                         THE_EMPIRE_STRIKES_BACK,
                                         THE_RETURN_OF_THE_JEDI)
                         .setHeight(1.72)
                         .build()));
            put("1002", withHuman(
                    Human.builder()
                         .setId("1002")
                         .setName("Han Solo")
                         .addToAppearsIn(A_NEW_HOPE,
                                         THE_EMPIRE_STRIKES_BACK,
                                         THE_RETURN_OF_THE_JEDI)
                         .setHeight(1.82)
                         .build()));
            put("2001", withDroid(
                    Droid.builder()
                         .setId("2001")
                         .setName("C3PO")
                         .addToAppearsIn(A_NEW_HOPE,
                                         THE_EMPIRE_STRIKES_BACK,
                                         THE_RETURN_OF_THE_JEDI)
                         .setPurpose(Purpose.TRANSLATION)
                         .build()));
            put("2002", withDroid(
                    Droid.builder()
                         .setId("2002")
                         .setName("R2D2")
                         .addToAppearsIn(A_NEW_HOPE,
                                         THE_EMPIRE_STRIKES_BACK,
                                         THE_RETURN_OF_THE_JEDI)
                         .setPurpose(Purpose.MECHANIC)
                         .build()));

            put("1010", withHuman(
                    Human.builder()
                         .setId("1010")
                         .setName("Llando")
                         .addToAppearsIn(THE_EMPIRE_STRIKES_BACK)
                         .setHeight(1.78)
                         .build()));
        }});
        queryImpl = new MyQuery.Iface() {
            @Nonnull
            @Override
            public CharacterUnion hero(String id) throws NotFoundException {
                return Optional.ofNullable(characters.get(id))
                               .orElseThrow(
                                       () -> NotFoundException.builder()
                                                              .setMessage("No such hero " + id)
                                                              .setId(id)
                                                              .build());
            }

            @Nonnull
            @Override
            public List<CharacterUnion> heroes(String appearsIn) {
                if (appearsIn != null) {
                    return characters.values()
                                     .stream()
                                     .filter(hero -> hero.getAppearsIn().stream().anyMatch(m -> m.getId().equals(appearsIn)))
                                     .collect(Collectors.toList());
                }
                return UnmodifiableList.copyOf(characters.values());
            }
        };
        mutationImpl = new MyMutation.Iface() {
            @Nonnull
            @Override
            public CharacterUnion store(CharacterUnion pHero) {
                if (!pHero.hasId() && !pHero.getId().isEmpty()) {
                    String id = String.valueOf(nextId.getAndIncrement());
                    switch (pHero.unionField()) {
                        case DROID:
                            pHero = withDroid(pHero.getDroid().mutate().setId(id));
                            break;
                        case HUMAN:
                            pHero = withHuman(pHero.getHuman().mutate().setId(id));
                            break;
                    }
                }
                characters.put(pHero.getId(), pHero);
                return pHero;
            }
        };
        GQLFieldProvider<Human> heightMutator = new GQLFieldProvider<Human>() {
            @Override
            public PMessageDescriptor<Human> getDescriptor() {
                return Human.kDescriptor;
            }

            @Nonnull
            @Override
            public Collection<PField<Human>> getFields() {
                return listOf(Human._Field.HEIGHT);
            }

            @Override
            public Object provide(@Nonnull Human message, @Nonnull PField<Human> field, @Nonnull GQLField selection) {
                if (field == Human._Field.HEIGHT) {
                    HeightArguments args = Optional
                            .<HeightArguments>ofNullable(selection.getArguments())
                            .orElse(HeightArguments.builder().build());
                    if (args.getMeasurement() == Measurement.IMPERIAL) {
                        return message.getHeight() * 3.28084;
                    }
                    return message.getHeight();
                }
                return null;
            }
        };

        // System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "debug");
        // System.setProperty("org.slf4j.simpleLogger.log.org.eclipse.jetty", "error");
        GQLServlet servlet = GQLServlet.builder(definition)
                                       .context(GQLContextFactory.DEFAULT_INSTANCE)
                                       .query((a, b) -> new MyQuery.Processor(queryImpl))
                                       .mutation((a, b) -> new MyMutation.Processor(mutationImpl))
                                       .fieldProvider(heightMutator)
                                       .executor(Executors.newFixedThreadPool(5))
                                       .build();

        server = new Server(0);
        ServletContextHandler handler = new ServletContextHandler();
        handler.addServlet(new ServletHolder(servlet), "/graphql");
        handler.addServlet(new ServletHolder(new GQLSchemaServlet(definition)), "/graphql/schema");
        handler.addServlet(new ServletHolder(new GQLPlaygroundServlet("Foo", "/graphql",
                                                                      mapOf("Key", "FOO", "Authorization", "Bearer BAR"))), "/graphql/playground");
        server.setHandler(handler);
        server.start();

        port = TestNetUtil.getExposedPort(server);
    }

    private GenericUrl url() {
        return new GenericUrl("http://localhost:" + port + "/graphql");
    }

    @After
    public void tearDown() throws Exception {
        System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "error");
        if (server != null) {
            server.stop();
        }
    }

    @Test
    public void testPlayground() throws IOException {
        GenericUrl url = url();
        url.setRawPath("/graphql/playground");
        HttpResponse response = TestNetUtil
                .factory(rq -> rq.setThrowExceptionOnExecuteError(false))
                .buildGetRequest(url)
                .execute();
        String content = IOUtils.readString(response.getContent());
        assertThat(content, containsString("'/graphql'"));
        assertThat(content, containsString("<title>Foo</title>"));
    }

    @Test
    public void testSchema() throws IOException {
        GenericUrl url = url();
        url.setRawPath("/graphql/schema");
        HttpResponse response = TestNetUtil
                .factory(rq -> rq.setThrowExceptionOnExecuteError(false))
                .buildGetRequest(url)
                .execute();
        String content = IOUtils.readString(response.getContent());
        assertThat(content, is(getResourceAsString("/net/morimekta/providence/graphql/schema/schema.graphqls")));
        assertThat(response.getStatusCode(), is(200));
    }

    @DataProvider
    public static Object[][] getQueries() {
        return new Object[][]{
                {"get-queries", "hero-1001"},
                {"get-queries", "hero-2001"},
                {"get-queries", "hero-luke"},
                {"get-queries", "hero-luke-c3po"},
                {"get-queries", "heroes-in-anh"},
                {"get-queries", "introspection-1"},
                {"get-queries", "introspection-2"},
                {"get-queries", "introspection-3"},
        };
    }

    @DataProvider
    public static Object[][] postQueries() {
        return new Object[][] {
                // Introspection also allowed in mutations...
                {"post-queries", "mutation-foo"},
                {"post-queries", "mutation-schema"},
                // Basic mutation
        };
    }

    @Test
    @UseDataProvider("getQueries")
    public void testGet(String group, String testName) throws IOException {
        String query = getResourceAsString(
                "/net/morimekta/providence/graphql/" + group + "/" + testName + ".graphql");
        String responseJson = getResourceAsString(
                "/net/morimekta/providence/graphql/" + group + "/" + testName + ".json");
        HttpResponse response = TestNetUtil
                .factory(rq -> rq.setThrowExceptionOnExecuteError(false))
                .buildGetRequest(url().set("query", query).set("pretty", "true"))
                .execute();
        String content = IOUtils.readString(response.getContent());
        assertThat(content, is(responseJson));
        assertThat(response.getStatusCode(), is(200));
    }

    @Test
    @UseDataProvider(value = "(getQueries|postQueries)",
                     resolveStrategy = AGGREGATE_ALL_MATCHES,
                     resolver = RegexDataProviderMethodResolver.class)
    public void testPostGraphql(String group, String testName) throws IOException {
        String query = getResourceAsString(
                "/net/morimekta/providence/graphql/" + group + "/" + testName + ".graphql");
        String responseJson = getResourceAsString(
                "/net/morimekta/providence/graphql/" + group + "/" + testName + ".json");
        HttpResponse response = TestNetUtil
                .factory(rq -> rq.setThrowExceptionOnExecuteError(false))
                .buildPostRequest(url().set("pretty", "true"),
                                  new ByteArrayContent(GQLServlet.MEDIA_TYPE, query.getBytes(StandardCharsets.UTF_8)))
                .execute();
        String content = IOUtils.readString(response.getContent());
        assertThat(content, is(responseJson));
        assertThat(response.getStatusCode(), is(200));
    }

    private static final ObjectMapper MAPPER = new ObjectMapper();

    @Test
    @UseDataProvider("getQueries")
    public void testPostJson(String group, String testName) throws IOException {
        String query = getResourceAsString(
                "/net/morimekta/providence/graphql/" + group + "/" + testName + ".graphql");
        String responseJson = getResourceAsString(
                "/net/morimekta/providence/graphql/" + group + "/" + testName + ".json");
        ByteArrayOutputStream tmp = new ByteArrayOutputStream();
        MAPPER.writeValue(tmp, mapOf("query", query));
        HttpResponse response = TestNetUtil
                .factory(rq -> rq.setThrowExceptionOnExecuteError(false))
                .buildPostRequest(url().set("pretty", "true"),
                                  new ByteArrayContent(JsonSerializer.JSON_MEDIA_TYPE, tmp.toByteArray()))
                .execute();
        String content = IOUtils.readString(response.getContent());
        assertThat(content, is(responseJson));
        assertThat(response.getStatusCode(), is(200));
    }

    @Test
    @UseDataProvider(value = "(getQueries|postQueries)",
                     resolveStrategy = AGGREGATE_ALL_MATCHES,
                     resolver = RegexDataProviderMethodResolver.class)
    public void testPostJsonWithQuery(String group, String testName) throws IOException {
        String query = getResourceAsString(
                "/net/morimekta/providence/graphql/" + group + "/" + testName + ".graphql");
        String responseJson = getResourceAsString(
                "/net/morimekta/providence/graphql/" + group + "/" + testName + ".json");

        ByteArrayOutputStream tmp = new ByteArrayOutputStream();
        MAPPER.writeValue(tmp, mapOf());
        HttpResponse response = TestNetUtil
                .factory(rq -> rq.setThrowExceptionOnExecuteError(false))
                .buildPostRequest(url().set("query", query).set("pretty", "true"),
                                  new ByteArrayContent(JsonSerializer.JSON_MEDIA_TYPE, tmp.toByteArray()))
                .execute();
        String content = IOUtils.readString(response.getContent());
        assertThat(content, is(responseJson));
        assertThat(response.getStatusCode(), is(200));
    }

    @DataProvider
    public static Object[][] badGetQueries() {
        return new Object[][]{
                {"mutation something {\n" +
                 "  store(hero:{human:{name:\"Chewbacca\" height:2.1}}) {id}\n" +
                 "}\n",
                 "{\n" +
                 "  \"errors\": [\n" +
                 "    {\n" +
                 "      \"message\": \"mutation not allowed in GET request\"\n" +
                 "    }\n" +
                 "  ]\n" +
                 "}"},
                {"{\n" +
                 "  store(hero:{human:{name:\"Chewbacca\" height:2.1}}) {id}\n" +
                 "}\n",
                 "{\n" +
                 "  \"errors\": [\n" +
                 "    {\n" +
                 "      \"message\": \"No method store in graphql.MyQuery\",\n" +
                 "      \"locations\": [\n" +
                 "        {\n" +
                 "          \"line\": 2,\n" +
                 "          \"column\": 3\n" +
                 "        }\n" +
                 "      ]\n" +
                 "    }\n" +
                 "  ]\n" +
                 "}"},
        };
    }

    @Test
    @UseDataProvider("badGetQueries")
    public void testGetBadRequest(String query, String responseJson) throws IOException {
        HttpResponse response = TestNetUtil
                .factory(rq -> rq.setThrowExceptionOnExecuteError(false))
                .buildGetRequest(url().set("query", query).set("pretty", "true"))
                .execute();
        String content = IOUtils.readString(response.getContent());
        assertThat(content, is(responseJson));
        assertThat(response.getStatusCode(), is(400));
    }

    @Test
    public void testGetNoQuery() throws IOException {
        HttpResponse response = TestNetUtil
                .factory(rq -> rq.setThrowExceptionOnExecuteError(false))
                .buildGetRequest(url().set("pretty", "true"))
                .execute();
        String content = IOUtils.readString(response.getContent());
        assertThat(content, is("{\n" +
                               "  \"errors\": [\n" +
                               "    {\n" +
                               "      \"message\": \"No query param in request\"\n" +
                               "    }\n" +
                               "  ]\n" +
                               "}"));
        assertThat(response.getStatusCode(), is(400));
    }

    @Test
    public void testBadPost() throws IOException {
        assertBadPost(url(),
                      JsonSerializer.JSON_MEDIA_TYPE,
                      "{}",
                      "{\n" +
                      "  \"errors\": [\n" +
                      "    {\n" +
                      "      \"message\": \"No query in request\"\n" +
                      "    }\n" +
                      "  ]\n" +
                      "}");
        assertBadPost(url(),
                      JsonSerializer.JSON_MEDIA_TYPE,
                      "{not json}",
                      "{\n" +
                      "  \"errors\": [\n" +
                      "    {\n" +
                      "      \"message\": \"" +
                      "Unexpected character ('n' (code 110)): was expecting double-quote to start field name\\n" +
                      " at [Source: (org.eclipse.jetty.server.Request$1); line: 1, column: 3]\",\n" +
                      "      \"locations\": [\n" +
                      "        {\n" +
                      "          \"line\": 1,\n" +
                      "          \"column\": 3\n" +
                      "        }\n" +
                      "      ]\n" +
                      "    }\n" +
                      "  ]\n" +
                      "}");
        assertBadPost(url(),
                      "text/plain",
                      "does not matter",
                      "{\n" +
                      "  \"errors\": [\n" +
                      "    {\n" +
                      "      \"message\": \"Unknown content-type: text/plain\"\n" +
                      "    }\n" +
                      "  ]\n" +
                      "}");

    }

    private void assertBadPost(GenericUrl url,
                               String contentType,
                               String postContent,
                               String responseJson) throws IOException {
        HttpResponse response = TestNetUtil
                .factory(rq -> rq.setThrowExceptionOnExecuteError(false))
                .buildPostRequest(
                        url,
                        new ByteArrayContent(contentType, postContent.getBytes(StandardCharsets.UTF_8)))
                .execute();
        String content = IOUtils.readString(response.getContent());
        assertThat(content, is(responseJson));
        assertThat(response.getStatusCode(), is(400));

    }
}
