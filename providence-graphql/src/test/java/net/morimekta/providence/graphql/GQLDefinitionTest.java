package net.morimekta.providence.graphql;

import net.morimekta.test.graphql.Character;
import net.morimekta.test.graphql.CharacterUnion;
import net.morimekta.test.graphql.Droid;
import net.morimekta.test.graphql.Human;
import net.morimekta.test.graphql.Movie;
import net.morimekta.test.graphql.MyMutation;
import net.morimekta.test.graphql.MyQuery;
import org.junit.Test;

import static net.morimekta.testing.ResourceUtils.getResourceAsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class GQLDefinitionTest {
    @Test
    public void testSchema() {
        GQLDefinition service = GQLDefinition
                .builder()
                .query(MyQuery.kDescriptor)
                .mutation(MyMutation.kDescriptor)
                .idField(Character._Field.ID,
                         Human._Field.ID,
                         Droid._Field.ID,
                         Movie._Field.ID,
                         MyQuery.Hero_Request._Field.ID,
                         MyQuery.Heroes_Request._Field.APPEARS_IN)
                .asInterface(CharacterUnion.kDescriptor).build();

        assertThat(service.getSchema(),
                   is(getResourceAsString("/net/morimekta/providence/graphql/schema/schema.graphqls")));
    }

    @Test
    public void testSchemaNoMutation() {
        GQLDefinition service = GQLDefinition
                .builder()
                .query(MyQuery.kDescriptor)
                .idField(Character._Field.ID,
                         Human._Field.ID,
                         Droid._Field.ID,
                         Movie._Field.ID,
                         MyQuery.Hero_Request._Field.ID,
                         MyQuery.Heroes_Request._Field.APPEARS_IN)
                .asInterface(CharacterUnion.kDescriptor).build();

        assertThat(service.getSchema(),
                   is(getResourceAsString("/net/morimekta/providence/graphql/schema/schema-no-mutation.graphqls")));
    }
}
