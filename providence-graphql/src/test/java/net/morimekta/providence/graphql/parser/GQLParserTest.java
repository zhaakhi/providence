package net.morimekta.providence.graphql.parser;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import net.morimekta.providence.PMessage;
import net.morimekta.providence.graphql.GQLDefinition;
import net.morimekta.providence.graphql.gql.GQLField;
import net.morimekta.providence.graphql.gql.GQLMethodCall;
import net.morimekta.providence.graphql.gql.GQLOperation;
import net.morimekta.providence.graphql.gql.GQLQuery;
import net.morimekta.providence.serializer.pretty.PrettyException;
import net.morimekta.test.graphql.Character;
import net.morimekta.test.graphql.CharacterUnion;
import net.morimekta.test.graphql.Droid;
import net.morimekta.test.graphql.Human;
import net.morimekta.test.graphql.Movie;
import net.morimekta.test.graphql.MyMutation;
import net.morimekta.test.graphql.MyQuery;
import net.morimekta.util.lexer.LexerException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.fail;

@RunWith(DataProviderRunner.class)
public class GQLParserTest {
    private GQLParser parser;

    @Before
    public void setUp() {
        GQLDefinition definition = GQLDefinition
                .builder()
                .query(MyQuery.kDescriptor)
                .mutation(MyMutation.kDescriptor)
                .idField(Character._Field.ID, Human._Field.ID, Droid._Field.ID)
                .asInterface(CharacterUnion.kDescriptor).build();
        parser = new GQLParser(definition);
        // queryOnlyParser = new GQLQueryParser(new GQLDefinition(Query.kDescriptor, null));
    }

    @Test
    public void testDefault() throws IOException {
        try {
            GQLQuery query = parser.parseQuery("{hero(id:\"1\") {...on Character{name,appears_in{name}}}}", new HashMap<>());
            assertThat(query.toString(), is("{hero(id: \"1\") {... on Character {name, appears_in{name}}}}"));
            assertThat(query.isDefaultOperationAvailable(), is(true));
            GQLOperation operation = query.getOperation(null).orElse(null);
            assertThat(operation, is(notNullValue()));
            assertThat(operation.getService(), is(MyQuery.kDescriptor));
            assertThat(operation.getSelectionSet(), hasSize(1));
            assertThat(operation.hasMethodCall(MyQuery.Method.HERO), is(true));
            assertThat(operation.getSelection(Character._Field.NAME), hasSize(0));
            assertThat(operation.hasSelection(Character._Field.NAME), is(false));
            assertThat(operation.hasSelectionPath(Character._Field.NAME), is(false));

            GQLMethodCall hero = (GQLMethodCall) operation.getSelectionSet().get(0);
            assertThat(hero, is(notNullValue()));
            assertThat(hero.getMethod(), is(MyQuery.Method.HERO));
            assertThat((PMessage) hero.getArguments(), is(notNullValue()));
            assertThat(hero.getSelectionSet(), hasSize(1));
            assertThat(hero.hasSelection(Character._Field.NAME), is(true));
            assertThat(hero.hasSelectionPath(Character._Field.APPEARS_IN, Movie._Field.NAME), is(true));
            assertThat(hero.hasSelectionPath(Character._Field.NAME), is(true));
            assertThat(hero.hasSelectionPath(Movie._Field.NAME), is(false));

            GQLField name = (GQLField) hero.getSelectionSet().get(0).getSelectionSet().get(0);
            assertThat(hero.getSelection(Character._Field.NAME), hasItem(name));
            assertThat(hero.getSelection(Character._Field.NAME), hasSize(1));
            assertThat(hero.getSelection(Character._Field.ID), hasSize(0));
            assertThat(hero.hasSelection(Character._Field.ID), is(false));
            assertThat(hero.getSelection(Human._Field.NAME), hasSize(1));
            assertThat(hero.hasSelection(Human._Field.NAME, Character._Field.NAME), is(true));
            assertThat(hero.hasSelection(Character._Field.APPEARS_IN), is(true));
            assertThat(hero.getSelection(Character._Field.APPEARS_IN), hasSize(1));
            assertThat(name, is(notNullValue()));
            assertThat(name.getField(), is(Character._Field.NAME));
            assertThat((PMessage) name.getArguments(), is(nullValue()));
            assertThat(name.getSelectionSet(), is(nullValue()));
        } catch (PrettyException e) {
            System.err.println(e.displayString());
            throw e;
        }
    }

    @DataProvider
    public static Object[][] queries() {
        return new Object[][] {
                { "{hero(id: \"1\") {name}}",
                  null,
                  "{hero(id: \"1\") {name}}" },
                { "{\n" +
                  "  hero(id: \"1\") {\n" +
                  "    name\n" +
                  "  }\n" +
                  "}\n",
                  null,
                  "{hero(id: \"1\") {name}}" },
                { "query myHero {\n" +
                  "  hero(id: \"1\") {\n" +
                  "    name\n" +
                  "    ... on Human {\n" +
                  "       height(measurement: IMPERIAL)\n" +
                  "    }\n" +
                  "  }\n" +
                  "}\n",
                  null,
                  "query myHero {hero(id: \"1\") {name, ... on Human {height(measurement: IMPERIAL)}}}" },
                { "query myHero {\n" +
                  "  hero(id: \"1\") {\n" +
                  "    name\n" +
                  "    ...humanFields\n" +
                  "  }\n" +
                  "}\n" +
                  "\n" +
                  "fragment humanFields on Human {\n" +
                  "  height(measurement: IMPERIAL)\n" +
                  "}\n",
                  null,
                  "query myHero {hero(id: \"1\") {" +
                  "name, ...humanFields" +
                  "}} " +
                  "fragment humanFields on Human {height(measurement: IMPERIAL)}" },
                { "query myHero($id: String) {\n" +
                  "  hero(id: $id) {\n" +
                  "    name\n" +
                  "    ...humanFields\n" +
                  "  }\n" +
                  "}\n" +
                  "\n" +
                  "fragment humanFields on Human {\n" +
                  "  height(measurement: IMPERIAL)\n" +
                  "}\n",
                  "{\"id\": \"1\"}",
                  "query myHero {hero(id: \"1\") {" +
                  "name, ...humanFields" +
                  "}} " +
                  "fragment humanFields on Human {height(measurement: IMPERIAL)}"},
                { "query hero($withHeight: Boolean) {\n" +
                  "  hero(id: \"1\") {\n" +
                  "    name\n" +
                  "    ... on Human {\n" +
                  "      height @include(if: $withHeight)" +
                  "    }\n" +
                  "  }\n" +
                  "}\n",
                  "{\"withHeight\": true}",
                  "query hero {hero(id: \"1\") {name, ... on Human {height}}}" },
                { "query hero($withHeight: Boolean) {\n" +
                  "  hero(id: \"1\") {\n" +
                  "    name\n" +
                  "    ... on Human {\n" +
                  "      height @include(if: $withHeight)" +
                  "    }\n" +
                  "  }\n" +
                  "}\n",
                  null,
                  "query hero {hero(id: \"1\") {name, ... on Human {}}}" },
                { "query hero($withoutHeight: Boolean) {\n" +
                  "  hero(id: \"1\") {\n" +
                  "    name\n" +
                  "    ... on Human {\n" +
                  "      height @skip(if: $withoutHeight)" +
                  "    }\n" +
                  "  }\n" +
                  "}\n",
                  "{\"withoutHeight\": true}",
                  "query hero {hero(id: \"1\") {name, ... on Human {}}}" },
                { "query hero($withoutHeight: Boolean) {\n" +
                  "  hero(id: \"1\") {\n" +
                  "    name\n" +
                  "    ... on Human {\n" +
                  "      height @skip(if: $withoutHeight)" +
                  "    }\n" +
                  "  }\n" +
                  "}\n",
                  null,
                  "query hero {hero(id: \"1\") {name, ... on Human {height}}}" },
                { "query hero($withHeight: Boolean = true) {\n" +
                  "  hero(id: \"1\") {\n" +
                  "    name\n" +
                  "    ... on Human {\n" +
                  "      height @include(if: $withHeight)" +
                  "    }\n" +
                  "  }\n" +
                  "}\n",
                  "{\"withHeight\": false}",
                  "query hero {hero(id: \"1\") {name, ... on Human {}}}" },
                { "query hero($withHeight: Boolean = true) {\n" +
                  "  hero(id: \"1\") {\n" +
                  "    name\n" +
                  "    ... on Human {\n" +
                  "      height @include(if: $withHeight)" +
                  "    }\n" +
                  "  }\n" +
                  "}\n",
                  null,
                  "query hero {hero(id: \"1\") {name, ... on Human {height}}}" },
        };
    }

    @Test
    @UseDataProvider("queries")
    public void testQueries(String query, String variables, String toString) throws IOException {
        try {
            GQLQuery gql = parser.parseQuery(query, parseVariables(variables));
            assertThat(gql.toString(), is(toString));
        } catch (PrettyException e) {
            System.err.println(e.displayString());
            throw e;
        }
    }

    @DataProvider
    public static Object[][] badQueries() {
        return new Object[][]{
                {"",
                 null,
                 "Empty query"},
                {"{hero(id: \"1\") {nam}}",
                 null,
                 "Error on line 1 row 17-19: Unknown field 'nam' in graphql.Character\n" +
                 "{hero(id: \"1\") {nam}}\n" +
                 "----------------^^^"},
                { "query myHero($id: String) {\n" +
                  "  hero(id: $foo) {\n" +
                  "    name\n" +
                  "    ...humanFields\n" +
                  "  }\n" +
                  "}\n" +
                  "\n" +
                  "fragment humanFields on Human {\n" +
                  "  height(measurement: IMPERIAL)\n" +
                  "}\n",
                  "{\"id\": \"1\", \"foo\": 42}",
                  "Error on line 2 row 12-15: No such variable $foo\n" +
                  "  hero(id: $foo) {\n" +
                  "-----------^^^^"},
                { "query myHero($id: String) {\n" +
                  "  hero(id: $id) {\n" +
                  "    name\n" +
                  "    ...humanFields\n" +
                  "  }\n" +
                  "}\n",
                  "{\"id\": \"1\"}",
                  "Error on line 4 row 8-18: No fragment for reference humanFields\n" +
                  "    ...humanFields\n" +
                  "-------^^^^^^^^^^^"},
                // Multi-Operation Queries -- already defined
                { "query foo {hero(id: \"1\") {name}}\n" +
                  "query foo {hero(id: \"2\") {name}}\n",
                  null,
                  "Error on line 2 row 7-9: Operation with name foo already defined\n" +
                  "query foo {hero(id: \"2\") {name}}\n" +
                  "------^^^"},
                { "{hero(id: \"1\") {name}}\n" +
                  "query foo {hero(id: \"2\") {name}}\n",
                  null,
                  "Error on line 2 row 1-5: Default operation already defined\n" +
                  "query foo {hero(id: \"2\") {name}}\n" +
                  "^^^^^"},
                { "query foo {hero(id: \"1\") {name}}\n" +
                  "{hero(id: \"2\") {name}}\n",
                  null,
                  "Error on line 2 row 1: Operation already defined, default operation not allowed with named operations\n" +
                  "{hero(id: \"2\") {name}}\n" +
                  "^"},
                { "{hero(id: \"2\") {name}}\n" +
                  "mutation foo {store(hero: {human:{name:\"1\"}}) {name}}\n",
                  null,
                  "Error on line 2 row 1-8: Default operation already defined\n" +
                  "mutation foo {store(hero: {human:{name:\"1\"}}) {name}}\n" +
                  "^^^^^^^^"},
                { "mutation foo {store(hero: {human:{name:\"1\"}}) {name}}\n" +
                  "mutation foo {store(hero: {human:{name:\"1\"}}) {name}}\n",
                  null,
                  "Error on line 2 row 10-12: Operation with name foo already defined\n" +
                  "mutation foo {store(hero: {human:{name:\"1\"}}) {name}}\n" +
                  "---------^^^"},
                { "mutation foo {store(hero: {human:{name:\"1\"}}) {name}}\n" +
                  "query foo {hero(id: \"2\") {name}}\n",
                  null,
                  "Error on line 2 row 7-9: Operation with name foo already defined\n" +
                  "query foo {hero(id: \"2\") {name}}\n" +
                  "------^^^"},
                { "query foo {hero(id: \"1\") {name}}\n" +
                  "mutation foo {store(hero: {human:{name:\"1\"}}) {name}}\n",
                  null,
                  "Error on line 2 row 10-12: Operation with name foo already defined\n" +
                  "mutation foo {store(hero: {human:{name:\"1\"}}) {name}}\n" +
                  "---------^^^"},
        };
    }

    @Test
    @UseDataProvider("badQueries")
    public void testBadQueries(String query, String variables, String error) {
        try {
            parser.parseQuery(query, parseVariables(variables));
            fail("no exception");
        } catch (LexerException e) {
            try {
                assertThat(e.displayString(), is(error));
            } catch (AssertionError a) {
                a.initCause(e);
                throw a;
            }
        } catch (IOException e) {
            try {
                assertThat(e.getMessage(), is(error));
            } catch (AssertionError a) {
                a.initCause(e);
                throw a;
            }
        }
    }

    private Map<String, Object> parseVariables(String json) throws IOException {
        if (json == null || json.isEmpty()) return new HashMap<>();
        ObjectMapper mapper = new ObjectMapper();
        JavaType tmp = mapper.getTypeFactory().constructMapType(HashMap.class, String.class, Object.class);
        return mapper.readValue(json, tmp);
    }
}
