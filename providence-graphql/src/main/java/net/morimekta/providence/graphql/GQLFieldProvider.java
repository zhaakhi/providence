package net.morimekta.providence.graphql;

import net.morimekta.providence.PMessage;
import net.morimekta.providence.descriptor.PField;
import net.morimekta.providence.descriptor.PMessageDescriptor;
import net.morimekta.providence.graphql.gql.GQLField;

import javax.annotation.Nonnull;
import java.util.Collection;

/**
 * Interface for functionality that provide field values
 * based on a source message and a GQL field.
 *
 * @param <Message> The message type.
 */
public interface GQLFieldProvider<Message extends PMessage<Message>> {
    /**
     * Get the type descriptor this mutator is for. This may be
     * an interface type. Only used for error messages. This type
     * must be indicative of the fields accepted by the provider.
     *
     * @return The message type being mutated.
     */
    PMessageDescriptor<Message> getDescriptor();

    /**
     * Get the set of fields that can be mutated with this mutator.
     *
     * @return Collection of mutating fields.
     */
    @Nonnull
    Collection<PField<Message>> getFields();

    /**
     * Mutate the message according to the field selection. The field selection
     * will represent a single field in the base message, and will be called for
     * each requested field.
     *
     * @param message The base message builder to be mutated.
     * @param field The source field requested.
     * @param selection The field selection to be filled or changed.
     * @return The mutated value for the field.
     */
    Object provide(@Nonnull Message message, @Nonnull PField<Message> field, @Nonnull GQLField selection);
}
