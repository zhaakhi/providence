package net.morimekta.providence.graphql.parser;

import net.morimekta.util.lexer.TokenType;

public enum GQLTokenType implements TokenType {
    IDENTIFIER,
    STRING,
    INTEGER,
    FLOAT,
    // includes '...'
    SYMBOL,

    // @identifier
    DIRECTIVE,
    // $identifier
    VARIABLE,
    ;
}
