package net.morimekta.providence.graphql.gql;

import net.morimekta.providence.descriptor.PPrimitive;
import net.morimekta.providence.graphql.introspection.Type;
import net.morimekta.providence.graphql.introspection.TypeKind;

/**
 * Specification for a scalar type in GraphQL.
 */
public enum GQLScalar {
    Boolean(PPrimitive.BOOL,
            Type.builder()
                .setKind(TypeKind.SCALAR)
                .setName("Boolean")
                .build()),
    Int(PPrimitive.I64,
        Type.builder()
            .setKind(TypeKind.SCALAR)
            .setName("Int")
            .build()),
    Float(PPrimitive.DOUBLE,
          Type.builder()
              .setKind(TypeKind.SCALAR)
              .setName("Float")
              .build()),
    String(PPrimitive.STRING,
           Type.builder()
               .setKind(TypeKind.SCALAR)
               .setName("String")
               .build()),
    ID(PPrimitive.STRING,
       Type.builder()
           .setKind(TypeKind.SCALAR)
           .setName("ID")
           .build()),
    ;

    public final PPrimitive type;
    public final Type introspection;

    GQLScalar(PPrimitive type, Type introspection) {
        this.type = type;
        this.introspection = introspection;
    }

    public static GQLScalar findByName(String name) {
        switch (name) {
            case "Int": return Int;
            case "Float": return Float;
            case "String": return String;
            case "Boolean": return Boolean;
            case "ID": return ID;
        }
        return null;
    }
}
