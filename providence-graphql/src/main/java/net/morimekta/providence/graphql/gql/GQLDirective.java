package net.morimekta.providence.graphql.gql;

/**
 * A directive represents some modification on a GQL field. It can for
 * the most part be handled in-line, as the query and arguments are
 * parsed, but may be (some time in the future) required to be kept
 * along the field selection.
 */
public enum GQLDirective {
    /**
     * <code>&#64;include(if: $var)</code>
     *
     * Include the field only if 'if' resolves to true.
     */
    include,
    /**
     * <code>&#64;include(if: $var)</code>
     *
     * Include the field only if 'if' resolves to false.
     */
    skip,
    ;

    /**
     * Find directive by the name, not including the '@'.
     *
     * @param name The directive name.
     * @return The directive, or null if not found.
     */
    public static GQLDirective findByName(String name) {
        for (GQLDirective directive : values()) {
            if (directive.name().equals(name)) {
                return directive;
            }
        }
        return null;
    }
}