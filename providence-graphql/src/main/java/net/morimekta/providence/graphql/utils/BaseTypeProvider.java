package net.morimekta.providence.graphql.utils;

import net.morimekta.providence.graphql.GQLDefinition;
import net.morimekta.providence.graphql.introspection.Type;

import javax.annotation.Nonnull;
import java.util.Optional;

abstract class BaseTypeProvider {
    private final GQLDefinition definition;

    BaseTypeProvider(@Nonnull GQLDefinition definition) {
        this.definition = definition;
    }

    Type resolveType(Type type) {
        if (type == null) return null;
        switch (type.getKind()) {
            case NON_NULL:
            case LIST: {
                return type.mutate()
                           .setOfType(resolveType(type.getOfType()))
                           .build();
            }
            case UNION:
            case INTERFACE:
            case OBJECT:
            case INPUT_OBJECT: {
                return Optional
                        .ofNullable(definition.getIntrospectionType(type.getName()))
                        .orElse(type);
            }
        }
        return type;
    }
}
