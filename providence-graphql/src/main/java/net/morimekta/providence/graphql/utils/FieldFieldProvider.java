package net.morimekta.providence.graphql.utils;

import net.morimekta.providence.descriptor.PField;
import net.morimekta.providence.descriptor.PMessageDescriptor;
import net.morimekta.providence.graphql.GQLDefinition;
import net.morimekta.providence.graphql.GQLFieldProvider;
import net.morimekta.providence.graphql.gql.GQLField;
import net.morimekta.providence.graphql.introspection.Field;
import net.morimekta.providence.graphql.introspection.Type;
import net.morimekta.util.collect.UnmodifiableList;

import javax.annotation.Nonnull;
import java.util.Collection;

public class FieldFieldProvider extends BaseTypeProvider implements GQLFieldProvider<Field> {
    public FieldFieldProvider(@Nonnull GQLDefinition definition) {
        super(definition);
    }

    @Override
    public PMessageDescriptor<Field> getDescriptor() {
        return Field.kDescriptor;
    }

    @Nonnull
    @Override
    public Collection<PField<Field>> getFields() {
        return UnmodifiableList.listOf(
                Field._Field.TYPE);
    }

    @Override
    public Type provide(@Nonnull Field message, @Nonnull PField<Field> field, @Nonnull GQLField selection) {
        return resolveType(message.getType());
    }
}
