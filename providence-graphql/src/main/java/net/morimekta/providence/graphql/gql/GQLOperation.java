package net.morimekta.providence.graphql.gql;

import net.morimekta.providence.descriptor.PField;
import net.morimekta.providence.descriptor.PService;
import net.morimekta.providence.descriptor.PServiceMethod;
import net.morimekta.util.collect.UnmodifiableList;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.List;
import java.util.Objects;

/**
 * Representing a single GQL operation, which is either a query
 * or a mutation.
 *
 * {@inheritDoc}
 */
@Immutable
public class GQLOperation implements GQLSelection {
    private final PService           service;
    private final List<GQLSelection> selectionSet;
    private final boolean            mutation;
    private final String             name;

    public GQLOperation(@Nonnull PService service,
                        boolean mutation,
                        @Nullable String name,
                        @Nonnull List<GQLSelection> selectionSet) {
        this.service = service;
        this.mutation = mutation;
        this.name = name;
        this.selectionSet = selectionSet;
    }

    public boolean isMutation() {
        return mutation;
    }

    public boolean hasMethodCall(PServiceMethod method) {
        for (GQLSelection selection : selectionSet) {
            if (selection instanceof GQLMethodCall) {
                GQLMethodCall call = (GQLMethodCall) selection;
                if (call.getMethod().equals(method)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean hasSelection(@Nonnull PField<?>... fields) {
        return false;
    }

    @Override
    public boolean hasSelectionPath(@Nonnull PField<?>... fields) {
        return false;
    }

    @Nonnull
    @Override
    public List<GQLSelection> getSelection(@Nonnull PField<?> field) {
        return UnmodifiableList.listOf();
    }

    @Nullable
    public String getName() {
        return name;
    }

    @Nonnull
    public PService getService() {
        return service;
    }

    @Nonnull
    @Override
    public List<GQLSelection> getSelectionSet() {
        return selectionSet;
    }

    @Override
    public int hashCode() {
        return Objects.hash(GQLOperation.class, name, service, selectionSet);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (name != null)  {
            if (mutation) {
                builder.append("mutation ");
            } else {
                builder.append("query ");
            }
            builder.append(name);
            builder.append(" ");
        }
        builder.append("{");
        boolean first = true;
        for (GQLSelection selection : selectionSet) {
            if (first) {
                first = false;
            } else {
                builder.append(", ");
            }
            builder.append(selection.toString());
        }
        builder.append("}");
        return builder.toString();
    }
}
