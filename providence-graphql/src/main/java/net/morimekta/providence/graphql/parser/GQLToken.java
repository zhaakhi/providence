package net.morimekta.providence.graphql.parser;

import net.morimekta.util.lexer.Token;

import javax.annotation.Nonnull;

public class GQLToken extends Token<GQLTokenType> {
    // Various symbols.
    public static final char kMessageStart  = '{';
    public static final char kMessageEnd    = '}';
    public static final char kKeyValueSep   = ':';
    public static final char kFieldValueSep = '=';
    public static final char kParamsStart   = '(';
    public static final char kParamsEnd     = ')';
    public static final char kListStart     = '[';
    public static final char kListEnd       = ']';
    public static final char kEntrySep      = ',';

    public static final char kDirective     = '@';
    public static final char kVariable      = '$';

    /**
     * Create a slice instance. The slice is only meant to be internal state
     * immutable, and not representing an immutable byte content.
     *
     * @param fb      The buffer to wrap.
     * @param off     The start offset to wrap.
     * @param len     The length to represent.
     * @param type    The token type represented.
     * @param lineNo  The current line number.
     * @param linePos The current line position.
     */
    public GQLToken(char[] fb,
                    int off,
                    int len,
                    @Nonnull GQLTokenType type, int lineNo, int linePos) {
        super(fb, off, len, type, lineNo, linePos);
    }

    public boolean isString() {
        return type == GQLTokenType.STRING;
    }

    public boolean isIdentifier() {
        return type == GQLTokenType.IDENTIFIER;
    }

    public boolean isVariable() {
        return type == GQLTokenType.VARIABLE;
    }

    public boolean isDirectve() {
        return type == GQLTokenType.DIRECTIVE;
    }
}
