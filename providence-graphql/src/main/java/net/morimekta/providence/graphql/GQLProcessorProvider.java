package net.morimekta.providence.graphql;

import net.morimekta.providence.PProcessor;
import net.morimekta.providence.graphql.gql.GQLMethodCall;

/**
 * A class representing the mutation needed for a single message.
 */
@FunctionalInterface
public interface GQLProcessorProvider<Context extends GQLContext> {
    /**
     * Mutate the message according to the field selection. The field selection
     * will represent a single field in the base message, and it should return
     * the mutated message with the new field set.
     *
     * @param context The GQL context for the request. Note that the context is
     *                per call for queries, and shared for mutations.
     * @param methodCall The GQL method to be called.
     * @return The processor
     */
    PProcessor processorFor(Context context, GQLMethodCall methodCall);
}
