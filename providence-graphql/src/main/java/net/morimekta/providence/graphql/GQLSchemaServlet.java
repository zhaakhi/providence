package net.morimekta.providence.graphql;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * A servlet for serving graphql given a GQL service and associated providers.
 *
 * Spec for serving GraphQL over HTTP can be found <a href="https://graphql.org/learn/serving-over-http/">here</a>.
 */
public class GQLSchemaServlet extends HttpServlet {
    private static final String MEDIA_TYPE = "text/plain";

    private final GQLDefinition definition;

    public GQLSchemaServlet(@Nonnull GQLDefinition definition) {
        this.definition = definition;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setStatus(HttpServletResponse.SC_OK);
        resp.setHeader("Content-Type", MEDIA_TYPE);
        resp.getWriter().write(definition.getSchema());
    }
}
