package net.morimekta.providence.graphql.parser;

import net.morimekta.util.lexer.Lexer;

import javax.annotation.Nullable;
import java.io.IOException;
import java.io.Reader;

public class GQLLexer extends Lexer<GQLTokenType, GQLToken> {
    /**
     * Create a lexer instance using a specific tokenizer.
     *
     * @param reader Reader to read graphql from.
     */
    public GQLLexer(Reader reader) {
        super(new GQLTokenizer(reader));
    }

    @Nullable
    @Override
    public GQLToken next() throws IOException {
        GQLToken next = super.next();
        if (next != null) {
            last = next;
        }
        return next;
    }

    public GQLToken getLastToken() {
        return last;
    }

    private GQLToken last;
}
