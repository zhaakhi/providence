package net.morimekta.providence.graphql.parser;

import net.morimekta.util.lexer.LexerException;
import net.morimekta.util.lexer.Token;

import javax.annotation.Nonnull;

public class GQLException extends LexerException {
    public GQLException(@Nonnull String message, @Nonnull Throwable cause) {
        super(cause, message);
    }

    public GQLException(@Nonnull String message, @Nonnull Token token) {
        super(token, message);
    }

    public GQLException(@Nonnull String message) {
        super(message);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "\n" + displayString();
    }
}
