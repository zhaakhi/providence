package net.morimekta.providence.graphql;

import net.morimekta.providence.graphql.gql.GQLOperation;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;

/**
 * Create a context instance to be used in the graphql servlet.
 *
 * @param <Context> The context implementation type.
 */
@FunctionalInterface
public interface GQLContextFactory<Context extends GQLContext> {
    /**
     * Create a context for the given HTTP request and GQL operation.
     *
     * @param request The HTTP request.
     * @param operation The GQL operation.
     * @return The context instance.
     */
    Context createContext(@Nonnull HttpServletRequest request,
                          @Nonnull GQLOperation operation);

    /**
     * Default instance to be used when no special context is needed. The
     * default instance makes the HTTP request and the GQL operation available
     * on processor instantiation.
     */
    GQLContextFactory DEFAULT_INSTANCE = GQLContext.GQLContextImpl::new;
}
