package net.morimekta.providence.storage;

import net.morimekta.test.providence.storage.OptionalFields;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

public class InMemoryMessageSetStoreTest extends TestBase {
    @Rule
    public TemporaryFolder tmp = new TemporaryFolder();

    @Test
    public void testConformity() {
        InMemoryMessageSetStore<String, OptionalFields> store = new InMemoryMessageSetStore<>(
                OptionalFields::getStringValue);
        assertConformity(OptionalFields::getStringValue,
                         this::modifyNotKey,
                         store);
    }

    private OptionalFields modifyNotKey(OptionalFields opt) {
        return opt.mutate()
                  .setIntegerValue(generator.context().getRandom().nextInt())
                  .setLongValue(generator.context().getRandom().nextLong())
                  .setByteValue((byte) generator.context().getRandom().nextInt())
                  .build();
    }
}
