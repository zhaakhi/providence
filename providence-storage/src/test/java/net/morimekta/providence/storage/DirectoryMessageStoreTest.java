package net.morimekta.providence.storage;

import net.morimekta.providence.serializer.PrettySerializer;
import net.morimekta.providence.storage.dir.DefaultFileManager;
import net.morimekta.providence.storage.dir.StringKeyFileManager;
import net.morimekta.test.providence.storage.Containers;
import net.morimekta.test.providence.storage.OptionalFields;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import static net.morimekta.providence.testing.EqualToMessage.equalToMessage;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;

public class DirectoryMessageStoreTest extends TestBase {
    @Rule
    public TemporaryFolder tmp = new TemporaryFolder();

    @Test
    public void testConformity() {
        try (DirectoryMessageStore<String, OptionalFields> store = new DirectoryMessageStore<>(
                new StringKeyFileManager(tmp.getRoot().toPath()),
                OptionalFields.kDescriptor,
                new PrettySerializer().config())) {
            assertConformity(store);
        }

        try (DirectoryMessageStore<String, OptionalFields> store = new DirectoryMessageStore<>(
                new StringKeyFileManager(tmp.getRoot().toPath()),
                OptionalFields.kDescriptor,
                new PrettySerializer().config())) {
            assertThat(store.keys().size(), is(greaterThan(0)));
        }
    }

    public Path pathFromUuid(UUID uuid) {
        return Paths.get(uuid.toString());
    }

    public UUID uuidFromPath(Path path) {
        return UUID.fromString(path.getFileName().toString());
    }

    @Test
    public void testDirectoryStore() throws InterruptedException {
        Map<UUID, Containers> source = new HashMap<>();
        for (int i = 0; i < 10; ++i) {
            source.put(UUID.randomUUID(),
                       generator.generate(Containers.kDescriptor));
        }

        try (DirectoryMessageStore<UUID, Containers> store = new DirectoryMessageStore<>(
                new DefaultFileManager<>(tmp.getRoot().toPath(),
                                         this::pathFromUuid,
                                         this::uuidFromPath),
                Containers.kDescriptor,
                new PrettySerializer().config())) {
            store.putAll(source);
        }

        Thread.sleep(1);

        try (DirectoryMessageStore<UUID, Containers> store = new DirectoryMessageStore<>(
                new DefaultFileManager<>(tmp.getRoot().toPath(),
                                         this::pathFromUuid,
                                         this::uuidFromPath),
                Containers.kDescriptor,
                new PrettySerializer().config())) {
            assertThat(store.keys(), is(source.keySet()));

            Set<UUID> random5 = new HashSet<>();
            source.forEach((k, v) -> {
                if (random5.size() < 5) {
                    random5.add(k);
                }
                assertThat(store.containsKey(k), is(true));
                assertThat(k.toString(), store.get(k), is(equalToMessage(v)));
            });
            for (int i = 0; i < 10; ++i) {
                assertThat(store.containsKey(UUID.randomUUID()), is(false));
            }
            Map<UUID, Containers> stored = store.getAll(source.keySet());
            assertThat(stored.keySet(), is(source.keySet()));
            assertThat(stored.size(), is(source.size()));
            assertThat(stored, is(source));

            store.removeAll(random5);

            for (UUID k : random5) {
                assertThat(store.containsKey(k), is(false));
                assertThat(store.get(k), is(nullValue()));
            }
        }
    }
}
