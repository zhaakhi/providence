package net.morimekta.providence.storage;

import net.morimekta.providence.testing.junit4.GeneratorWatcher;
import net.morimekta.providence.testing.junit4.SimpleGeneratorWatcher;
import net.morimekta.test.providence.storage.OptionalFields;
import net.morimekta.util.collect.UnmodifiableMap;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.UUID;
import java.util.function.Function;

import static net.morimekta.providence.testing.EqualToMessage.equalToMessage;
import static net.morimekta.util.collect.UnmodifiableList.listOf;
import static net.morimekta.util.collect.UnmodifiableSet.setOf;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasSize;

public class TestBase {
    @Rule
    public SimpleGeneratorWatcher generator = GeneratorWatcher.create();

    private OptionalFields          orig1;
    private OptionalFields          orig2;
    private OptionalFields._Builder orig3;
    private OptionalFields._Builder orig4;
    List<OptionalFields> list1;
    private List<OptionalFields>          list2;
    private List<OptionalFields._Builder> list3;
    private List<OptionalFields._Builder> list4;

    @Before
    public void setUp() {
        generator.context().withMessageGenerator(OptionalFields.kDescriptor, gen -> {
            gen.setValueGenerator(OptionalFields._Field.STRING_VALUE, ctx -> UUID.randomUUID().toString());
        });

        orig1 = generator.generate(OptionalFields.kDescriptor);
        orig2 = generator.generate(OptionalFields.kDescriptor);
        orig3 = generator.generate(OptionalFields.kDescriptor).mutate();
        orig4 = generator.generate(OptionalFields.kDescriptor).mutate();

        list1 = listOf(
                generator.generate(OptionalFields.kDescriptor),
                generator.generate(OptionalFields.kDescriptor),
                generator.generate(OptionalFields.kDescriptor),
                generator.generate(OptionalFields.kDescriptor));
        list2 = listOf(
                generator.generate(OptionalFields.kDescriptor),
                generator.generate(OptionalFields.kDescriptor),
                generator.generate(OptionalFields.kDescriptor),
                generator.generate(OptionalFields.kDescriptor));
        list3 = listOf(
                generator.generate(OptionalFields.kDescriptor).mutate(),
                generator.generate(OptionalFields.kDescriptor).mutate(),
                generator.generate(OptionalFields.kDescriptor).mutate(),
                generator.generate(OptionalFields.kDescriptor).mutate());
        list4 = listOf(
                generator.generate(OptionalFields.kDescriptor).mutate(),
                generator.generate(OptionalFields.kDescriptor).mutate(),
                generator.generate(OptionalFields.kDescriptor).mutate(),
                generator.generate(OptionalFields.kDescriptor).mutate());
    }

    void assertConformity(MessageStore<String,OptionalFields> storage) {
        storage.put("1234", orig1);
        storage.putAll(UnmodifiableMap.mapOf("2345", orig2));

        storage.putBuilder("3456", orig3);
        storage.putAllBuilders(UnmodifiableMap.mapOf("4567", orig4));

        assertThat(storage.keys(), hasSize(4));
        assertThat(storage.keys(), containsInAnyOrder("1234", "2345", "3456", "4567"));
        assertThat(storage.size(), is(4));

        OptionalFields get1 = storage.get("1234");
        OptionalFields._Builder get2 = storage.getBuilder("2345");
        OptionalFields get3 = storage.get("3456");
        OptionalFields._Builder get4 = storage.getBuilder("4567");
        OptionalFields get5 = storage.get("5678");
        OptionalFields._Builder get6 = storage.getBuilder("6789");

        // Check that the values are the same.
        assertThat(get1, is(notNullValue()));
        assertThat(get2, is(notNullValue()));
        assertThat(get3, is(notNullValue()));
        assertThat(get4, is(notNullValue()));
        assertThat(get5, is(nullValue()));
        assertThat(get6, is(nullValue()));

        assertThat(get1, is(orig1));
        assertThat(get2, is(orig2.mutate()));
        assertThat(get3, is(orig3.build()));
        assertThat(get4, is(orig4));

        // Check that getting all conforms.
        Map<String, OptionalFields> map1 = storage.getAll(listOf("1234", "3456", "5678"));
        assertThat(map1.keySet(), hasSize(2));
        assertThat(map1, hasEntry("1234", orig1));
        assertThat(map1, Matchers.hasEntry("3456", orig3.build()));

        Map<String, OptionalFields._Builder> map2 = storage.getAllBuilders(listOf("1234", "3456", "5678"));
        assertThat(map2.keySet(), hasSize(2));
        assertThat(map2, Matchers.hasEntry("1234", orig1.mutate()));
        assertThat(map2, hasEntry("3456", orig3));

        // Check remove and removeAll.
        storage.remove("1234");
        storage.removeAll(listOf("1234", "3456"));

        assertThat(storage.get("1234"), is(nullValue()));
        assertThat(storage.get("3456"), is(nullValue()));

        assertThat(storage.containsKey("1234"), is(false));
        assertThat(storage.containsKey("2345"), is(true));

        // Check put overriding.
        storage.put("2345", orig1);
        storage.putBuilder("2345", orig3);

        assertThat(storage.keys(), containsInAnyOrder("2345", "4567"));

        storage.putAll(UnmodifiableMap.mapOf(
                "1234", orig1,
                "2345", orig2));

        assertThat(storage.keys(), containsInAnyOrder("1234", "2345", "4567"));

        storage.putAllBuilders(UnmodifiableMap.mapOf(
                "3456", orig3,
                "4567", orig4));

        assertThat(storage.keys(), containsInAnyOrder("1234", "2345", "3456", "4567"));
    }

    void assertConformity(Function<OptionalFields, String> messageToKey,
                          Function<OptionalFields, OptionalFields> modifyNotKey,
                          MessageSetStore<String, OptionalFields> storage) {
        String key1 = messageToKey.apply(orig1);
        String key2 = messageToKey.apply(orig2);
        String key3 = messageToKey.apply(orig3.build());
        String key4 = messageToKey.apply(orig4.build());
        String randomKey = UUID.randomUUID().toString();

        storage.put(orig1);
        storage.putAll(listOf(orig2));

        storage.putBuilder(orig3);
        storage.putAllBuilders(listOf(orig4));

        assertThat(storage.keys(), hasSize(4));
        assertThat(storage.keys(), containsInAnyOrder(key1, key2, key3, key4));
        assertThat(storage.size(), is(4));


        OptionalFields get1 = storage.get(key1);
        OptionalFields._Builder get2 = storage.getBuilder(key2);
        OptionalFields get3 = storage.get(key3);
        OptionalFields._Builder get4 = storage.getBuilder(key4);
        OptionalFields get5 = storage.get(randomKey);
        OptionalFields._Builder get6 = storage.getBuilder(randomKey);

        // Check that the values are the same.
        assertThat(get1, is(notNullValue()));
        assertThat(get2, is(notNullValue()));
        assertThat(get3, is(notNullValue()));
        assertThat(get4, is(notNullValue()));
        assertThat(get5, is(nullValue()));
        assertThat(get6, is(nullValue()));

        assertThat(get1, is(orig1));
        assertThat(get2, is(orig2.mutate()));
        assertThat(get3, is(orig3.build()));
        assertThat(get4, is(orig4));

        // Check that getting all conforms.
        Map<String, OptionalFields> map1 = storage.getAll(listOf(key1, key3, randomKey));
        assertThat(map1.keySet(), hasSize(2));
        assertThat(map1, hasEntry(key1, orig1));
        assertThat(map1, Matchers.hasEntry(key3, orig3.build()));

        Map<String, OptionalFields._Builder> map2 = storage.getAllBuilders(listOf(key1, key3, randomKey));
        assertThat(map2.keySet(), hasSize(2));
        assertThat(map2, Matchers.hasEntry(key1, orig1.mutate()));
        assertThat(map2, hasEntry(key3, orig3));

        // Check remove and removeAll.
        storage.remove(key1);
        storage.removeAll(listOf(key1, key3));

        assertThat(storage.get(key1), is(nullValue()));
        assertThat(storage.get(key3), is(nullValue()));

        assertThat(storage.containsKey(key1), is(false));
        assertThat(storage.containsKey(key2), is(true));

        // Check add overriding.
        OptionalFields mod1 = modifyNotKey.apply(orig1);
        OptionalFields mod2 = modifyNotKey.apply(orig2);
        OptionalFields new1 = generator.generate(OptionalFields.kDescriptor);
        OptionalFields._Builder mod3 = modifyNotKey.apply(orig3.build()).mutate();
        OptionalFields._Builder mod4 = modifyNotKey.apply(orig4.build()).mutate();
        OptionalFields._Builder new2 = generator.generate(OptionalFields.kDescriptor).mutate();

        storage.put(mod1);
        storage.put(mod2);
        storage.putBuilder(mod3);
        storage.putBuilder(mod4);

        storage.putAll(setOf(new1, orig2));

        storage.putAllBuilders(setOf(new2, orig4));

        assertThat(storage.keys(), containsInAnyOrder(key1, key2, key3, key4,
                                                      new1.getStringValue(),
                                                      new2.getStringValue()));
    }

    void assertConformity(MessageListStore<String,OptionalFields> storage) {
        storage.put("1234", list1);
        storage.putAll(UnmodifiableMap.mapOf("2345", list2));
        storage.putBuilders("3456", list3);
        storage.putAllBuilders(UnmodifiableMap.mapOf("4567", list4));

        assertThat(storage.keys(), hasSize(4));
        assertThat(storage.keys(), hasItems("1234", "2345", "3456", "4567"));
        assertThat(storage.containsKey("1234"), is(true));
        assertThat(storage.containsKey("5678"), is(false));

        List<OptionalFields> opts = storage.get("1234");

        // Check that the values are the same.
        assertThat(opts, is(notNullValue()));
        assertThat(opts, is(list1));

        for (int i = 0; i < 100; ++i) {
            List<OptionalFields> list = new ArrayList<>();
            for (int j = 0; j < 10; ++j) {
                list.add(generator.generate(OptionalFields.kDescriptor));
            }
            storage.put(UUID.randomUUID().toString(), list);
        }
        TreeSet<String> ids = new TreeSet<>(storage.keys());

        assertThat(storage.size(), is(104));
        assertThat(ids, hasSize(104));
        for (String id : ids) {
            assertThat(storage.containsKey(id), Matchers.is(true));
        }
        TreeSet<String> missing = new TreeSet<>();
        for (int i = 0; i < 100; ++i) {
            String uuid = UUID.randomUUID().toString();
            assertThat(storage.containsKey(uuid), Matchers.is(false));;
            missing.add(uuid);
        }

        assertThat(storage.getAll(missing).entrySet(), hasSize(0));
        storage.remove(ids.first());
        storage.removeAll(new ArrayList<>(ids).subList(45, 55));

        assertThat(storage.getAll(ids).entrySet(), hasSize(93));

        Map<String, List<OptionalFields._Builder>> bld = storage.getAllBuilders(new ArrayList<>(ids).subList(30, 45));

        bld.forEach((k, list) -> {
            for (OptionalFields._Builder b : list) {
                b.clearBinaryValue();
                b.clearBooleanValue();
                b.clearByteValue();
            }
        });

        storage.putAllBuilders(bld);

        Map<String, List<OptionalFields>> tmp2 = storage.getAll(bld.keySet());
        tmp2.forEach((k, list) -> {
            for (OptionalFields v : list) {
                assertThat(v.hasBooleanValue(), Matchers.is(false));
                assertThat(v.hasByteValue(), Matchers.is(false));
                assertThat(v.hasBinaryValue(), Matchers.is(false));
            }
        });

        OptionalFields._Builder builder = OptionalFields.builder();
        builder.setIntegerValue(10);
        builder.setBooleanValue(true);
        builder.setDoubleValue(12345.6789);
        String uuid = UUID.randomUUID().toString();
        storage.putBuilders(uuid, listOf(builder));

        List<OptionalFields> list = storage.get(uuid);
        assertThat(list, hasSize(1));
        assertThat(list, Matchers.hasItem(builder.build()));

        List<OptionalFields._Builder> otherBuilder = storage.getBuilders(uuid);

        assertThat(otherBuilder, notNullValue());
        assertThat(otherBuilder, hasSize(1));
        assertThat(otherBuilder.get(0).build(), Matchers.is(equalToMessage(builder.build())));

        String uuid2 = UUID.randomUUID().toString();
        List<OptionalFields> expectedEmpty = new ArrayList<>();
        storage.put(uuid2, expectedEmpty);

        List<OptionalFields> actualEmpty = storage.get(uuid2);

        assertThat(actualEmpty, is(not(sameInstance(expectedEmpty))));
        assertThat(actualEmpty, is(empty()));
    }
}
