package net.morimekta.providence.storage;

import net.morimekta.providence.PMessage;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.nullValue;

@SuppressWarnings("deprecation")
public class MessageStoreUtilsTest {
    @Test
    public void testMutateAll() {
        // Extra cast needed to normalize unknown generic.
        assertThat((List) MessageStoreUtils.toBuilderAll(null), is(nullValue()));
    }

    @Test
    public void testBuildAll() {
        // Extra cast needed to normalize unknown generic.
        assertThat((List) MessageStoreUtils.toMessageAll(null), is(nullValue()));
    }

    @Test
    public void testMutateIfNotNull() {
        // Extra cast needed to normalize unknown generic.
        assertThat((PMessage) MessageStoreUtils.toBuilderIfNonNull(null), is(nullValue()));
    }

    @Test
    public void testBuildIfNotNull() {
        // Extra cast needed to normalize unknown generic.
        assertThat((PMessage) MessageStoreUtils.toMessageIfNotNull(null), is(nullValue()));
    }
}
