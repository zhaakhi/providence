package net.morimekta.providence.storage;

import net.morimekta.providence.PMessage;
import net.morimekta.providence.PMessageBuilder;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Map;

import static net.morimekta.providence.util.MessageUtil.toBuilderIfNonNull;
import static net.morimekta.providence.util.MessageUtil.toBuilderValues;

/**
 * Interface for storing messages of a single type. This is a read-only part
 * of the store.
 *
 * @param <K> The key type.
 * @param <M> The stored message type.
 */
public interface MessageReadOnlyStore<K, M extends PMessage<M>> extends ReadOnlyStore<K, M> {
    /**
     * Get the builder representing the message on the given key. Any modifications
     * to the returned builder will not be reflected onto the store.
     *
     * @param key The key to find builder for.
     * @param <B> The builder type.
     * @return The builder if message was found or null if not.
     */
    @Nullable
    default <B extends PMessageBuilder<M>>
    B getBuilder(@Nonnull K key) {
        return toBuilderIfNonNull(get(key));
    }

    /**
     * Get builders for all keys requested. Any modifications to the returned builders
     * will not be reflected onto the store. The result map fill not contain any
     * (key -&gt; null) entries.
     *
     * @param keys Keys to look up.
     * @param <B> The builder type.
     * @return The map of found entries.
     */
    @Nonnull
    default <B extends PMessageBuilder<M>>
    Map<K,B> getAllBuilders(@Nonnull Collection<K> keys) {
        return toBuilderValues(getAll(keys));
    }
}
