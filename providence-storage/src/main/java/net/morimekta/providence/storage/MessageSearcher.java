package net.morimekta.providence.storage;

import net.morimekta.providence.PMessage;
import net.morimekta.providence.PMessageBuilder;

import javax.annotation.Nonnull;
import java.util.List;

import static net.morimekta.providence.util.MessageUtil.toBuilderAll;

/**
 * Interface for searching a store for a specific search S.
 *
 * @param <Q> Search query param, be it single string or struct or union with search parameters.
 * @param <M> PMessage that we search for.
 */
public interface MessageSearcher<Q, M extends PMessage<M>>
        extends Searcher<Q, M> {
    /**
     * Get a list of builders for the query input. No modifications
     * to the returned builders will be reflected onto the store.
     *
     * @param query The key to look up.
     * @param <B>   The builder type.
     * @return List of builders that matches query Q.
     */
    @Nonnull
    default <B extends PMessageBuilder<M>>
    List<B> searchBuilders(@Nonnull Q query) {
        return toBuilderAll(search(query));
    }
}
