package net.morimekta.providence.storage;

import net.morimekta.providence.PMessage;

import java.util.function.Function;

/**
 * Simple in-memory set storage of providence messages. Uses a local hash map for
 * storing the instances. The store is thread safe through using re-entrant
 * read-write mutex handling, so reading can happen in parallel.
 */
public class InMemoryMessageSetStore<K, M extends PMessage<M>>
        extends InMemorySetStore<K, M>
        implements MessageSetStore<K,M> {
    public InMemoryMessageSetStore(Function<M,K> messageToKey) {
        super(messageToKey);
    }
}
