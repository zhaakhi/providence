package net.morimekta.providence.storage;

import net.morimekta.providence.PMessage;
import net.morimekta.providence.PMessageBuilder;

import javax.annotation.Nonnull;
import java.util.Map;

import static net.morimekta.providence.util.MessageUtil.toMessageValues;

/**
 * Interface for storing messages of a single type.
 */
public interface MessageStore<K, M extends PMessage<M>>
        extends MessageReadOnlyStore<K, M>, ReadWriteStore<K, M> {
    /**
     * Put the message represented by the builder into the store on the given key.
     * Any further modifications to the builder will not be reflected on the store.
     *
     * @param key The key to store the builder on.
     * @param builder The builder to store.
     * @param <B> The builder type.
     */
    default <B extends PMessageBuilder<M>>
    void putBuilder(@Nonnull K key, @Nonnull B builder) {
        put(key, builder.build());
    }

    /**
     * Put a collection of key and builder pairs onto the store. Any further modifications
     * to the builders will not be reflected onto the store.
     *
     * @param builders Map of builders to put into the store.
     * @param <B> The builder type.
     */
    default <B extends PMessageBuilder<M>>
    void putAllBuilders(@Nonnull Map<K,B> builders) {
        putAll(toMessageValues(builders));
    }
}
