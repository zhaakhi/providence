package net.morimekta.providence.storage;

import net.morimekta.util.collect.UnmodifiableMap;
import net.morimekta.util.collect.UnmodifiableSet;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Map;

/**
 * Interface to read and write a value or a range of values for a given key or keys.
 *
 * @param <K> Key to use for gathering information.
 * @param <V> Value to fetch, that is a generic method.
 */
public interface ReadWriteStore<K, V> extends ReadOnlyStore<K, V> {
    /**
     * @param values Put all key value pairs form this map into the storage.
     */
    void putAll(@Nonnull Map<K,V> values);

    /**
     * Remove the values for the given keys.
     * @param keys Map of removed key value pairs.
     */
    void removeAll(Collection<K> keys);

    /**
     * @param key The key to put message at.
     * @param value The value to put.
     */
    default void put(@Nonnull K key, @Nonnull V value) {
        putAll(UnmodifiableMap.mapOf(key, value));
    }

    /**
     * Remove the key value pair from the store.
     *
     * @param key The key to remove.
     */
    default void remove(@Nonnull K key) {
        removeAll(UnmodifiableSet.setOf(key));
    }

}
