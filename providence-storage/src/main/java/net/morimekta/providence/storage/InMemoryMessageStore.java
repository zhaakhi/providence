package net.morimekta.providence.storage;

import net.morimekta.providence.PMessage;

/**
 * Simple in-memory storage of providence messages. Uses a local hash map for
 * storing the instances. The store is thread safe through using re-entrant
 * read-write mutex handling, so reading can happen in parallel.
 */
public class InMemoryMessageStore<K, M extends PMessage<M>>
        extends InMemoryStore<K, M>
        implements MessageStore<K,M> {
    public InMemoryMessageStore() {}
}
