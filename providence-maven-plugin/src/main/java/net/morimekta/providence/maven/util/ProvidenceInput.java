package net.morimekta.providence.maven.util;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;
import org.codehaus.plexus.components.io.fileselectors.IncludeExcludeFileSelector;
import org.codehaus.plexus.util.DirectoryScanner;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static net.morimekta.providence.reflect.util.ReflectionUtils.isThriftBasedFileSyntax;
import static net.morimekta.util.FileUtil.readCanonicalPath;

/**
 * Input utility for providence maven plugins.
 */
public class ProvidenceInput {
    /**
     * Get the set of input files.
     * @param baseDir The maven project base dir.
     * @param inputSelector The input-exclude selector.
     * @param defaultInputInclude The default input include (if not specified).
     * @param print_debug Print debug info to maven log.
     * @param log Maven logger instance.
     * @throws MojoExecutionException If parsing or checking input files failed.
     * @return The set of input files.
     */
    public static Set<Path> getInputFiles(@Nonnull Path baseDir,
                                          IncludeExcludeFileSelector inputSelector,
                                          @Nonnull String defaultInputInclude,
                                          boolean print_debug,
                                          @Nonnull Log log) throws MojoExecutionException {
        try {
            baseDir = readCanonicalPath(baseDir.normalize()).toAbsolutePath();
            TreeSet<Path> inputs = new TreeSet<>();

            DirectoryScanner inputScanner = new DirectoryScanner();
            if (inputSelector != null) {
                if (inputSelector.getIncludes() != null &&
                        inputSelector.getIncludes().length > 0) {
                    if (print_debug) {
                        log.info("Specified includes:");
                        for (String include : inputSelector.getIncludes()) {
                            log.info("    -I " + include);
                        }
                    }
                    inputScanner.setIncludes(inputSelector.getIncludes());
                } else {
                    if (print_debug) {
                        log.info("Default includes: " + defaultInputInclude);
                    }
                    inputScanner.setIncludes(defaultInputInclude.split(","));
                }

                if (inputSelector.getExcludes() != null &&
                        inputSelector.getExcludes().length > 0) {
                    if (print_debug) {
                        log.info("Specified excludes:");
                        for (String exclude : inputSelector.getExcludes()) {
                            log.info("    -E " + exclude);
                        }
                    }
                    inputScanner.setExcludes(inputSelector.getExcludes());
                }
            } else {
                if (print_debug) {
                    log.info("Default input: " + defaultInputInclude);
                }
                inputScanner.setIncludes(defaultInputInclude.split(","));
            }

            inputScanner.setBasedir(baseDir.toFile());
            inputScanner.scan();

            // Include all files included specifically.
            for (String includedFilePath : inputScanner.getIncludedFiles()) {
                Path path = Paths.get(includedFilePath);
                if (isThriftBasedFileSyntax(path)) {
                    inputs.add(baseDir.resolve(path).normalize());
                } else {
                    throw new MojoExecutionException("Not a thrift or providence file: " + includedFilePath);
                }
            }

            // Include all thrift files in included directories.
            for (String name : inputScanner.getIncludedDirectories()) {
                Path dir = baseDir.resolve(name).normalize();
                try (Stream<Path> stream = Files.list(dir)) {
                    List<Path> files = stream.collect(Collectors.toList());
                    for (Path file : files) {
                        file = dir.resolve(file);
                        if (Files.isHidden(file)) {
                            continue;
                        }

                        if (Files.isReadable(file) &&
                            isThriftBasedFileSyntax(file)) {
                            inputs.add(file);
                        }
                    }
                }
            }
            // exclude all files excluded specifically.
            for (String file : inputScanner.getExcludedFiles()) {
                inputs.remove(baseDir.resolve(file).normalize());
            }
            // Exclude all files in excluded directories (and subdirectories).
            for (String dir : inputScanner.getExcludedDirectories()) {
                String path = baseDir.resolve(dir).normalize() + File.separator;
                inputs.removeIf(f -> f.toString().startsWith(path));
            }

            return inputs;
        } catch (IOException e) {
            throw new MojoExecutionException(e.getMessage(), e);
        }
    }

    public static String format(Duration duration) {
        long h = duration.toHours();
        long m = duration.minusHours(h).toMinutes();
        if (h > 0) {
            return String.format(Locale.US, "%d:%02d H", h, m);
        }
        long s = duration.minusHours(h).minusMinutes(m).getSeconds();
        if (m > 0) {
            return String.format(Locale.US, "%d:%02d min", m, s);
        }
        long ms = duration.minusHours(h).minusMinutes(m).minusSeconds(s).toMillis();
        return String.format(Locale.US, "%d.%02d s", s, ms / 10);
    }
}
