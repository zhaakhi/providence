package net.morimekta.providence.example.server.core;

import net.morimekta.providence.server.ProvidenceHttpServlet;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class CoreProcessImpl extends ProvidenceHttpServlet<CoreRequest, CoreResponse> {
    private final CoreService.Iface impl;

    CoreProcessImpl(CoreService.Iface impl) {
        super(CoreRequest.kDescriptor, null, null);
        this.impl = impl;
    }

    @Nonnull
    @Override
    protected CoreResponse handle(@Nonnull HttpServletRequest httpRequest,
                                  @Nonnull CoreRequest request) throws IOException, CoreException {
        return impl.process(request);
    }
}
