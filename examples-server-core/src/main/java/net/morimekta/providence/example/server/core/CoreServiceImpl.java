package net.morimekta.providence.example.server.core;

import javax.annotation.Nonnull;

public class CoreServiceImpl implements CoreService.Iface {
    @Nonnull
    @Override
    public String echo(String pMessage) {
        return pMessage;
    }

    @Nonnull
    @Override
    public CoreResponse process(CoreRequest pRequest) throws CoreException {
        if (pRequest == null) {
            throw CoreException.builder()
                               .setMessage("no request")
                               .build();
        }
        return CoreResponse.builder()
                           .setMessage(pRequest.getMessage())
                           .build();
    }
}
