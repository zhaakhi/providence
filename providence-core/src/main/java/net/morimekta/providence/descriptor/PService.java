/*
 * Copyright 2016 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.descriptor;

import net.morimekta.providence.PType;
import net.morimekta.util.collect.UnmodifiableList;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.Collection;

/**
 * Descriptor for a complete service.
 */
@Immutable
public abstract class PService extends PDeclaredDescriptor<Void> {
    private final PServiceProvider                     extendsService;
    private final Collection<? extends PServiceMethod> methods;

    public PService(@Nonnull String programName,
                    @Nonnull String name,
                    @Nullable PServiceProvider extendsService,
                    @Nonnull Collection<? extends PServiceMethod> methods) {
        super(programName, name);
        this.extendsService = extendsService;
        this.methods = methods;
    }

    public PService(@Nonnull String programName,
                    @Nonnull String name,
                    @Nullable PServiceProvider extendsService,
                    @Nonnull PServiceMethod[] methods) {
        this(programName, name, extendsService, UnmodifiableList.copyOf(methods));
    }

    @Nonnull
    @Override
    public PType getType() {
        return PType.VOID;
    }

    @Nullable
    @Override
    public Void getDefaultValue() {
        return null;
    }

    @Override
    public boolean isInnerType() {
        return false;
    }

    @Override
    public boolean isAutoType() {
        return false;
    }

    /**
     * Get the service that this service extends.
     *
     * @return Extended service or null if none.
     */
    @Nullable
    public PService getExtendsService() {
        if (extendsService != null) {
            return extendsService.getService();
        }
        return null;
    }

    /**
     * Get the collection of methods for the service, including all inherited
     * services.
     *
     * @return The collection of methods.
     */
    @Nonnull
    public Collection<? extends PServiceMethod> getMethods() {
        return methods;
    }

    /**
     * Get the method definition for the given method name.
     *
     * @param name The service method name.
     * @return The service method.
     */
    @Nullable
    public abstract PServiceMethod getMethod(String name);
}
