/*
 * Copyright 2016 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.descriptor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

/**
 * Descriptor for a single service method.
 */
@Immutable
public interface PServiceMethod {
    /**
     * @return Name of the method.
     */
    @Nonnull
    String getName();

    /**
     * @return If the method cal is oneway.
     */
    boolean isOneway();

    /**
     * If the method is a proto stub method. This means that both the
     * request and response is a single message (with struct variant). And
     * the request struct is the method parameter, not it's fields.
     *
     * @return True if the method is a proto stub.
     */
    boolean isProtoStub();

    /**
     * @return The descriptor of the request type.
     */
    @Nonnull
    @SuppressWarnings("rawtypes")
    PStructDescriptor getRequestType();

    /**
     * @return The descriptor of the response type, or null for oneway methods.
     */
    @Nullable
    @SuppressWarnings("rawtypes")
    PUnionDescriptor getResponseType();

    /**
     * Get the service where this method is represented. Note that it
     * may be defined in the extended service and not the service returned.
     *
     * @return The representing service.
     */
    @Nonnull
    PService getService();
}
