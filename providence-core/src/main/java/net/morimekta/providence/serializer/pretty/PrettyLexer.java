package net.morimekta.providence.serializer.pretty;

import net.morimekta.util.lexer.Lexer;
import net.morimekta.util.lexer.Tokenizer;

import java.io.IOException;
import java.io.InputStream;

public class PrettyLexer extends Lexer<PrettyTokenType, PrettyToken> {
    public PrettyLexer(InputStream in) {
        this(new PrettyTokenizer(in));
    }

    public PrettyLexer(Tokenizer<PrettyTokenType, PrettyToken> tokenizer) {
        super(tokenizer);
    }

    public PrettyToken readBinary(char term) throws IOException {
        String end = new String(new char[]{term});
        return readUntil(end, PrettyTokenType.BINARY, false);
    }
}
