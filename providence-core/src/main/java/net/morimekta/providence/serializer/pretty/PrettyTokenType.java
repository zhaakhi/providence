package net.morimekta.providence.serializer.pretty;

import net.morimekta.util.lexer.TokenType;

import java.util.Locale;

public enum PrettyTokenType implements TokenType {
    SYMBOL,
    STRING,
    NUMBER,
    IDENTIFIER,
    // binary content.
    BINARY,
    ;

    @Override
    public String toString() {
        return "<" + name().toLowerCase(Locale.US) + ">";
    }
}
