/*
 * Copyright 2015-2016 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.types;

import net.morimekta.providence.descriptor.PDeclaredDescriptor;
import net.morimekta.providence.descriptor.PService;
import net.morimekta.providence.descriptor.PValueProvider;

import javax.annotation.Nonnull;

/**
 * Registry for declared types referenced in a specific program context.
 * The registry itself does not have a context per se, as these may
 * reference each other recursively.
 */
public abstract class WritableTypeRegistry extends TypeRegistry {
    /**
     * Register a constant value.
     *
     * @param reference The constant reference.
     * @param value     The constant value.
     */
    public abstract void registerConstant(@Nonnull TypeReference reference,
                                          @Nonnull PValueProvider<?> value);

    /**
     * Services are not handled as "declared types", so they need to be registered
     * separately.
     *
     * @param service the service to register.
     */
    public abstract void registerService(@Nonnull PService service);

    /**
     * Register a declared type.
     *
     * @param declaredType The descriptor for the type.
     * @param <T>          The declared java type.
     */
    public abstract <T> void registerType(PDeclaredDescriptor<T> declaredType);

    /**
     * Registers a typedef definition. This is a pair of two type
     * references. One for the reference itself, and one for the target
     * type definition, including the local program context where it
     * was defined.
     *
     * @param reference The typedef reference source..
     * @param target    The qualified type definition that the name represents
     *                  with it's own context.
     */
    public abstract void registerTypedef(@Nonnull TypeReference reference,
                                         @Nonnull TypeReference target);
}
