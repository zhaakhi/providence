package net.morimekta.providence;

/**
 * Base exception thrown on non-declared exceptions on a service call, and
 * other server-side service call issues.
 */
@javax.annotation.Generated(
        value = "net.morimekta.providence:providence-generator-java",
        comments = "java:serializable")
@SuppressWarnings("unused")
public interface PApplicationException_OrBuilder extends net.morimekta.providence.PMessageOrBuilder<PApplicationException> {
    /**
     * Exception message.
     *
     * @return The message value.
     */
    String getMessage();

    /**
     * Exception message.
     *
     * @return Optional message value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<String> optionalMessage();

    /**
     * @return If message is present.
     */
    boolean hasMessage();

    /**
     * The application exception type.
     *
     * @return The type value.
     */
    net.morimekta.providence.PApplicationExceptionType getType();

    /**
     * The application exception type.
     *
     * @return Optional type value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<net.morimekta.providence.PApplicationExceptionType> optionalType();

    /**
     * @return If type is present.
     */
    boolean hasType();

}
