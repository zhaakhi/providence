package net.morimekta.providence;

/**
 * The service call type is a base distinction of what the message means, and
 * lets the server or client select the proper message to be serialized or
 * deserialized from the service method descriptor.
 */
@javax.annotation.Generated(
        value = "net.morimekta.providence:providence-generator-java",
        comments = "java:serializable")
public enum PServiceCallType
        implements net.morimekta.providence.PEnumValue<PServiceCallType> {
    /**
     * Normal service method call request.
     */
    CALL(1, "CALL", "call"),
    /**
     * Normal method call reply. This includes declared exceptions on the
     * service method.
     */
    REPLY(2, "REPLY", "reply"),
    /**
     * An application exception, i.e. either a non-declared exception, or a
     * providence service or serialization exception. This is also happens when
     * such exceptions happen on the server side, it will try to send an
     * application exception back to the client.
     */
    EXCEPTION(3, "EXCEPTION", "exception"),
    /**
     * A one-way call is a request that does not expect a response at all. The
     * client will return as soon as the request is sent.
     */
    ONEWAY(4, "ONEWAY", "oneway"),
    ;

    private final int    mId;
    private final String mName;
    private final String mPojoName;

    PServiceCallType(int id, String name, String pojoName) {
        mId = id;
        mName = name;
        mPojoName = pojoName;
    }

    @Override
    public int asInteger() {
        return mId;
    }

    @Override
    @javax.annotation.Nonnull
    public String asString() {
        return mName;
    }

    @Override
    @javax.annotation.Nonnull
    public String getPojoName() {
        return mPojoName;
    }

    /**
     * Find a value based in its ID
     *
     * @param id Id of value
     * @return Value found or null
     */
    @javax.annotation.Nullable
    public static PServiceCallType findById(Integer id) {
        if (id == null) {
            return null;
        }
        switch (id) {
            case 1: return PServiceCallType.CALL;
            case 2: return PServiceCallType.REPLY;
            case 3: return PServiceCallType.EXCEPTION;
            case 4: return PServiceCallType.ONEWAY;
            default: return null;
        }
    }

    /**
     * Find a value based in its name
     *
     * @param name Name of value
     * @return Value found or null
     */
    @javax.annotation.Nullable
    public static PServiceCallType findByName(String name) {
        if (name == null) {
            return null;
        }
        switch (name) {
            case "CALL": return PServiceCallType.CALL;
            case "REPLY": return PServiceCallType.REPLY;
            case "EXCEPTION": return PServiceCallType.EXCEPTION;
            case "ONEWAY": return PServiceCallType.ONEWAY;
            default: return null;
        }
    }

    /**
     * Find a value based in its POJO name
     *
     * @param name Name of value
     * @return Value found or null
     */
    @javax.annotation.Nullable
    public static PServiceCallType findByPojoName(String name) {
        if (name == null) {
            return null;
        }
        switch (name) {
            case "call": return PServiceCallType.CALL;
            case "CALL": return PServiceCallType.CALL;
            case "reply": return PServiceCallType.REPLY;
            case "REPLY": return PServiceCallType.REPLY;
            case "exception": return PServiceCallType.EXCEPTION;
            case "EXCEPTION": return PServiceCallType.EXCEPTION;
            case "oneway": return PServiceCallType.ONEWAY;
            case "ONEWAY": return PServiceCallType.ONEWAY;
            default: return null;
        }
    }

    /**
     * Get a value based in its ID
     *
     * @param id Id of value
     * @return Value found
     * @throws IllegalArgumentException If no value for id is found
     */
    @javax.annotation.Nonnull
    public static PServiceCallType valueForId(int id) {
        PServiceCallType value = findById(id);
        if (value == null) {
            throw new IllegalArgumentException("No p_service.PServiceCallType for id " + id);
        }
        return value;
    }

    /**
     * Get a value based in its name
     *
     * @param name Name of value
     * @return Value found
     * @throws IllegalArgumentException If no value for name is found, or null name
     */
    @javax.annotation.Nonnull
    public static PServiceCallType valueForName(@javax.annotation.Nonnull String name) {
        PServiceCallType value = findByName(name);
        if (value == null) {
            throw new IllegalArgumentException("No p_service.PServiceCallType for name \"" + name + "\"");
        }
        return value;
    }

    /**
     * Get a value based in its name
     *
     * @param name Name of value
     * @return Value found
     * @throws IllegalArgumentException If no value for name is found, or null name
     */
    @javax.annotation.Nonnull
    public static PServiceCallType valueForPojoName(@javax.annotation.Nonnull String name) {
        PServiceCallType value = findByPojoName(name);
        if (value == null) {
            throw new IllegalArgumentException("No p_service.PServiceCallType for name \"" + name + "\"");
        }
        return value;
    }

    public static final class _Builder extends net.morimekta.providence.PEnumBuilder<PServiceCallType> {
        private PServiceCallType mValue;

        @Override
        @javax.annotation.Nonnull
        public _Builder setById(int value) {
            mValue = PServiceCallType.findById(value);
            return this;
        }

        @Override
        @javax.annotation.Nonnull
        public _Builder setByName(String name) {
            mValue = PServiceCallType.findByName(name);
            return this;
        }

        @Override
        public boolean valid() {
            return mValue != null;
        }

        @Override
        public PServiceCallType build() {
            return mValue;
        }
    }

    public static final net.morimekta.providence.descriptor.PEnumDescriptor<PServiceCallType> kDescriptor;

    @Override
    @javax.annotation.Nonnull
    public net.morimekta.providence.descriptor.PEnumDescriptor<PServiceCallType> descriptor() {
        return kDescriptor;
    }

    public static net.morimekta.providence.descriptor.PEnumDescriptorProvider<PServiceCallType> provider() {
        return new net.morimekta.providence.descriptor.PEnumDescriptorProvider<PServiceCallType>(kDescriptor);
    }

    private static final class _Descriptor
            extends net.morimekta.providence.descriptor.PEnumDescriptor<PServiceCallType> {
        public _Descriptor() {
            super("p_service", "PServiceCallType", _Builder::new);
        }

        @Override
        @javax.annotation.Nonnull
        public boolean isInnerType() {
            return false;
        }

        @Override
        @javax.annotation.Nonnull
        public boolean isAutoType() {
            return false;
        }

        @Override
        @javax.annotation.Nonnull
        public PServiceCallType[] getValues() {
            return PServiceCallType.values();
        }

        @Override
        @javax.annotation.Nullable
        public PServiceCallType findById(int id) {
            return PServiceCallType.findById(id);
        }

        @Override
        @javax.annotation.Nullable
        public PServiceCallType findByName(String name) {
            return PServiceCallType.findByName(name);
        }

        @Override
        @javax.annotation.Nullable
        public PServiceCallType findByPojoName(String name) {
            return PServiceCallType.findByPojoName(name);
        }
    }

    static {
        kDescriptor = new _Descriptor();
    }
}
