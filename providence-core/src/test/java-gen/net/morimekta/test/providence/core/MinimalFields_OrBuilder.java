package net.morimekta.test.providence.core;

@javax.annotation.Generated(
        value = "net.morimekta.providence:providence-generator-java",
        comments = "java:serializable")
@SuppressWarnings("unused")
public interface MinimalFields_OrBuilder extends net.morimekta.test.providence.core.CommonFields , net.morimekta.providence.PMessageOrBuilder<MinimalFields> {
}
