package net.morimekta.test.providence.core;

@javax.annotation.Generated(
        value = "net.morimekta.providence:providence-generator-java",
        comments = "java:serializable")
public enum JsonNamedEnum
        implements net.morimekta.providence.PEnumValue<JsonNamedEnum> {
    FIRST_VALUE(1, "FIRST_VALUE", "first.value"),
    SDECOND_VALUE(2, "SDECOND_VALUE", "second.value"),
    ;

    private final int    mId;
    private final String mName;
    private final String mPojoName;

    JsonNamedEnum(int id, String name, String pojoName) {
        mId = id;
        mName = name;
        mPojoName = pojoName;
    }

    @Override
    public int asInteger() {
        return mId;
    }

    @Override
    @javax.annotation.Nonnull
    public String asString() {
        return mName;
    }

    @Override
    @javax.annotation.Nonnull
    public String getPojoName() {
        return mPojoName;
    }

    /**
     * Find a value based in its ID
     *
     * @param id Id of value
     * @return Value found or null
     */
    @javax.annotation.Nullable
    public static JsonNamedEnum findById(Integer id) {
        if (id == null) {
            return null;
        }
        switch (id) {
            case 1: return JsonNamedEnum.FIRST_VALUE;
            case 2: return JsonNamedEnum.SDECOND_VALUE;
            default: return null;
        }
    }

    /**
     * Find a value based in its name
     *
     * @param name Name of value
     * @return Value found or null
     */
    @javax.annotation.Nullable
    public static JsonNamedEnum findByName(String name) {
        if (name == null) {
            return null;
        }
        switch (name) {
            case "FIRST_VALUE": return JsonNamedEnum.FIRST_VALUE;
            case "SDECOND_VALUE": return JsonNamedEnum.SDECOND_VALUE;
            default: return null;
        }
    }

    /**
     * Find a value based in its POJO name
     *
     * @param name Name of value
     * @return Value found or null
     */
    @javax.annotation.Nullable
    public static JsonNamedEnum findByPojoName(String name) {
        if (name == null) {
            return null;
        }
        switch (name) {
            case "first.value": return JsonNamedEnum.FIRST_VALUE;
            case "FIRST_VALUE": return JsonNamedEnum.FIRST_VALUE;
            case "second.value": return JsonNamedEnum.SDECOND_VALUE;
            case "SDECOND_VALUE": return JsonNamedEnum.SDECOND_VALUE;
            default: return null;
        }
    }

    /**
     * Get a value based in its ID
     *
     * @param id Id of value
     * @return Value found
     * @throws IllegalArgumentException If no value for id is found
     */
    @javax.annotation.Nonnull
    public static JsonNamedEnum valueForId(int id) {
        JsonNamedEnum value = findById(id);
        if (value == null) {
            throw new IllegalArgumentException("No providence.JsonNamedEnum for id " + id);
        }
        return value;
    }

    /**
     * Get a value based in its name
     *
     * @param name Name of value
     * @return Value found
     * @throws IllegalArgumentException If no value for name is found, or null name
     */
    @javax.annotation.Nonnull
    public static JsonNamedEnum valueForName(@javax.annotation.Nonnull String name) {
        JsonNamedEnum value = findByName(name);
        if (value == null) {
            throw new IllegalArgumentException("No providence.JsonNamedEnum for name \"" + name + "\"");
        }
        return value;
    }

    /**
     * Get a value based in its name
     *
     * @param name Name of value
     * @return Value found
     * @throws IllegalArgumentException If no value for name is found, or null name
     */
    @javax.annotation.Nonnull
    public static JsonNamedEnum valueForPojoName(@javax.annotation.Nonnull String name) {
        JsonNamedEnum value = findByPojoName(name);
        if (value == null) {
            throw new IllegalArgumentException("No providence.JsonNamedEnum for name \"" + name + "\"");
        }
        return value;
    }

    public static final class _Builder extends net.morimekta.providence.PEnumBuilder<JsonNamedEnum> {
        private JsonNamedEnum mValue;

        @Override
        @javax.annotation.Nonnull
        public _Builder setById(int value) {
            mValue = JsonNamedEnum.findById(value);
            return this;
        }

        @Override
        @javax.annotation.Nonnull
        public _Builder setByName(String name) {
            mValue = JsonNamedEnum.findByName(name);
            return this;
        }

        @Override
        public boolean valid() {
            return mValue != null;
        }

        @Override
        public JsonNamedEnum build() {
            return mValue;
        }
    }

    public static final net.morimekta.providence.descriptor.PEnumDescriptor<JsonNamedEnum> kDescriptor;

    @Override
    @javax.annotation.Nonnull
    public net.morimekta.providence.descriptor.PEnumDescriptor<JsonNamedEnum> descriptor() {
        return kDescriptor;
    }

    public static net.morimekta.providence.descriptor.PEnumDescriptorProvider<JsonNamedEnum> provider() {
        return new net.morimekta.providence.descriptor.PEnumDescriptorProvider<JsonNamedEnum>(kDescriptor);
    }

    private static final class _Descriptor
            extends net.morimekta.providence.descriptor.PEnumDescriptor<JsonNamedEnum> {
        public _Descriptor() {
            super("providence", "JsonNamedEnum", _Builder::new);
        }

        @Override
        @javax.annotation.Nonnull
        public boolean isInnerType() {
            return false;
        }

        @Override
        @javax.annotation.Nonnull
        public boolean isAutoType() {
            return false;
        }

        @Override
        @javax.annotation.Nonnull
        public JsonNamedEnum[] getValues() {
            return JsonNamedEnum.values();
        }

        @Override
        @javax.annotation.Nullable
        public JsonNamedEnum findById(int id) {
            return JsonNamedEnum.findById(id);
        }

        @Override
        @javax.annotation.Nullable
        public JsonNamedEnum findByName(String name) {
            return JsonNamedEnum.findByName(name);
        }

        @Override
        @javax.annotation.Nullable
        public JsonNamedEnum findByPojoName(String name) {
            return JsonNamedEnum.findByPojoName(name);
        }
    }

    static {
        kDescriptor = new _Descriptor();
    }
}
