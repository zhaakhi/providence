package net.morimekta.test.providence.core;

@SuppressWarnings("unused")
@javax.annotation.Generated(
        value = "net.morimekta.providence:providence-generator-java",
        comments = "java:serializable")
@javax.annotation.concurrent.Immutable
public class UnionOfFields
        implements net.morimekta.test.providence.core.CommonFields,
                   UnionOfFields_OrBuilder,
                   net.morimekta.providence.PUnion<UnionOfFields>,
                   Comparable<UnionOfFields>,
                   java.io.Serializable,
                   net.morimekta.providence.serializer.binary.BinaryWriter {
    private final static long serialVersionUID = -497570480886382202L;

    private final transient net.morimekta.test.providence.core.OptionalFields mOpts;
    private final transient net.morimekta.test.providence.core.MinimalFields mMin;

    private transient final _Field tUnionField;

    private volatile transient int tHashCode;

    // Transient object used during java deserialization.
    private transient UnionOfFields tSerializeInstance;

    /**
     * @param value The union value
     * @return The created union.
     */
    @javax.annotation.Nonnull
    public static UnionOfFields withOpts(@javax.annotation.Nonnull net.morimekta.test.providence.core.OptionalFields_OrBuilder value) {
        return new _Builder().setOpts(value).build();
    }

    /**
     * @param value The union value
     * @return The created union.
     */
    @javax.annotation.Nonnull
    public static UnionOfFields withMin(@javax.annotation.Nonnull net.morimekta.test.providence.core.MinimalFields_OrBuilder value) {
        return new _Builder().setMin(value).build();
    }

    private UnionOfFields(_Builder builder) {
        tUnionField = builder.tUnionField;

        mOpts = tUnionField != _Field.OPTS
                ? null
                : builder.mOpts_builder != null ? builder.mOpts_builder.build() : builder.mOpts;
        mMin = tUnionField != _Field.MIN
                ? null
                : builder.mMin_builder != null ? builder.mMin_builder.build() : builder.mMin;
    }

    public boolean hasOpts() {
        return tUnionField == _Field.OPTS && mOpts != null;
    }

    /**
     * @return The <code>opts</code> value
     */
    public net.morimekta.test.providence.core.OptionalFields getOpts() {
        return mOpts;
    }

    /**
     * @return Optional of the <code>opts</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<net.morimekta.test.providence.core.OptionalFields> optionalOpts() {
        return java.util.Optional.ofNullable(mOpts);
    }

    public boolean hasMin() {
        return tUnionField == _Field.MIN && mMin != null;
    }

    /**
     * @return The <code>min</code> value
     */
    public net.morimekta.test.providence.core.MinimalFields getMin() {
        return mMin;
    }

    /**
     * @return Optional of the <code>min</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<net.morimekta.test.providence.core.MinimalFields> optionalMin() {
        return java.util.Optional.ofNullable(mMin);
    }

    /**
     * @return The union field as implemented type.
     */
    public net.morimekta.test.providence.core.CommonFields asCommonFields() {
        switch (unionField()) {
            case OPTS: return getOpts();
            case MIN: return getMin();
            default: throw new IllegalStateException("Impossible");
        }
    }

    /**
     * @return The string_value value.
     */
    public String getStringValue() {
        return asCommonFields().getStringValue();
    }

    /**
     * @return Optional string_value value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<String> optionalStringValue() {
        return asCommonFields().optionalStringValue();
    }

    /**
     * @return If string_value is present.
     */
    public boolean hasStringValue() {
        return asCommonFields().hasStringValue();
    }

    @Override
    public boolean has(int key) {
        switch(key) {
            case 1: return tUnionField == _Field.OPTS;
            case 2: return tUnionField == _Field.MIN;
            default: return false;
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T get(int key) {
        switch(key) {
            case 1: return (T) mOpts;
            case 2: return (T) mMin;
            default: return null;
        }
    }

    @Override
    public boolean unionFieldIsSet() {
        return tUnionField != null;
    }

    @Override
    @javax.annotation.Nonnull
    public _Field unionField() {
        if (tUnionField == null) throw new IllegalStateException("No union field set in providence.UnionOfFields");
        return tUnionField;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) return true;
        if (o == null || !o.getClass().equals(getClass())) return false;
        UnionOfFields other = (UnionOfFields) o;
        return java.util.Objects.equals(tUnionField, other.tUnionField) &&
               java.util.Objects.equals(mOpts, other.mOpts) &&
               java.util.Objects.equals(mMin, other.mMin);
    }

    @Override
    public int hashCode() {
        if (tHashCode == 0) {
            tHashCode = java.util.Objects.hash(
                    UnionOfFields.class,
                    _Field.OPTS, mOpts,
                    _Field.MIN, mMin);
        }
        return tHashCode;
    }

    @Override
    public String toString() {
        return "providence.UnionOfFields" + asString();
    }

    @Override
    @javax.annotation.Nonnull
    public String asString() {
        StringBuilder out = new StringBuilder();
        out.append("{");

        switch (tUnionField) {
            case OPTS: {
                out.append("opts:")
                   .append(mOpts.asString());
                break;
            }
            case MIN: {
                out.append("min:")
                   .append(mMin.asString());
                break;
            }
        }
        out.append('}');
        return out.toString();
    }

    @Override
    public int compareTo(UnionOfFields other) {
        if (tUnionField == null || other.tUnionField == null) return Boolean.compare(tUnionField != null, other.tUnionField != null);
        int c = tUnionField.compareTo(other.tUnionField);
        if (c != 0) return c;

        switch (tUnionField) {
            case OPTS:
                return mOpts.compareTo(other.mOpts);
            case MIN:
                return mMin.compareTo(other.mMin);
            default: return 0;
        }
    }

    private void writeObject(java.io.ObjectOutputStream oos) throws java.io.IOException {
        oos.defaultWriteObject();
        net.morimekta.providence.serializer.BinarySerializer.INSTANCE.serialize(oos, this);
    }

    private void readObject(java.io.ObjectInputStream ois)
            throws java.io.IOException, ClassNotFoundException {
        ois.defaultReadObject();
        tSerializeInstance = net.morimekta.providence.serializer.BinarySerializer.INSTANCE.deserialize(ois, kDescriptor);
    }

    private Object readResolve() throws java.io.ObjectStreamException {
        return tSerializeInstance;
    }

    @Override
    public int writeBinary(net.morimekta.util.io.BigEndianBinaryWriter writer) throws java.io.IOException {
        int length = 0;

        if (tUnionField != null) {
            switch (tUnionField) {
                case OPTS: {
                    length += writer.writeByte((byte) 12);
                    length += writer.writeShort((short) 1);
                    length += net.morimekta.providence.serializer.binary.BinaryFormatUtils.writeMessage(writer, mOpts);
                    break;
                }
                case MIN: {
                    length += writer.writeByte((byte) 12);
                    length += writer.writeShort((short) 2);
                    length += net.morimekta.providence.serializer.binary.BinaryFormatUtils.writeMessage(writer, mMin);
                    break;
                }
                default: break;
            }
        }
        length += writer.writeByte((byte) 0);
        return length;
    }

    @javax.annotation.Nonnull
    @Override
    public _Builder mutate() {
        return new _Builder(this);
    }

    public enum _Field implements net.morimekta.providence.descriptor.PField<UnionOfFields> {
        OPTS(1, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "opts", "opts", net.morimekta.test.providence.core.OptionalFields.provider(), null, null),
        MIN(2, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "min", "min", net.morimekta.test.providence.core.MinimalFields.provider(), null, null),
        ;

        private final int mId;
        private final net.morimekta.providence.descriptor.PRequirement mRequired;
        private final String mName;
        private final String mPojoName;
        private final net.morimekta.providence.descriptor.PDescriptorProvider mTypeProvider;
        private final net.morimekta.providence.descriptor.PStructDescriptorProvider mArgumentsProvider;
        private final net.morimekta.providence.descriptor.PValueProvider<?> mDefaultValue;

        _Field(int id, net.morimekta.providence.descriptor.PRequirement required, String name, String pojoName, net.morimekta.providence.descriptor.PDescriptorProvider typeProvider, net.morimekta.providence.descriptor.PStructDescriptorProvider argumentsProvider, net.morimekta.providence.descriptor.PValueProvider<?> defaultValue) {
            mId = id;
            mRequired = required;
            mName = name;
            mPojoName = pojoName;
            mTypeProvider = typeProvider;
            mArgumentsProvider = argumentsProvider;
            mDefaultValue = defaultValue;
        }

        @Override
        public int getId() { return mId; }

        @javax.annotation.Nonnull
        @Override
        public net.morimekta.providence.descriptor.PRequirement getRequirement() { return mRequired; }

        @javax.annotation.Nonnull
        @Override
        public net.morimekta.providence.descriptor.PDescriptor getDescriptor() { return mTypeProvider.descriptor(); }

        @Override
        @javax.annotation.Nullable
        public net.morimekta.providence.descriptor.PStructDescriptor getArgumentsType() { return mArgumentsProvider == null ? null : mArgumentsProvider.descriptor(); }

        @javax.annotation.Nonnull
        @Override
        public String getName() { return mName; }

        @javax.annotation.Nonnull
        @Override
        public String getPojoName() { return mPojoName; }

        @Override
        public boolean hasDefaultValue() { return mDefaultValue != null; }

        @Override
        @javax.annotation.Nullable
        public Object getDefaultValue() {
            return hasDefaultValue() ? mDefaultValue.get() : null;
        }

        @Override
        @javax.annotation.Nonnull
        public net.morimekta.providence.descriptor.PMessageDescriptor<UnionOfFields> onMessageType() {
            return kDescriptor;
        }

        @Override
        public String toString() {
            return net.morimekta.providence.descriptor.PField.asString(this);
        }

        /**
         * @param id Field ID
         * @return The identified field or null
         */
        public static _Field findById(int id) {
            switch (id) {
                case 1: return _Field.OPTS;
                case 2: return _Field.MIN;
            }
            return null;
        }

        /**
         * @param name Field name
         * @return The named field or null
         */
        public static _Field findByName(String name) {
            if (name == null) return null;
            switch (name) {
                case "opts": return _Field.OPTS;
                case "min": return _Field.MIN;
            }
            return null;
        }

        /**
         * @param name Field POJO name
         * @return The named field or null
         */
        public static _Field findByPojoName(String name) {
            if (name == null) return null;
            switch (name) {
                case "opts": return _Field.OPTS;
                case "min": return _Field.MIN;
            }
            return null;
        }

        /**
         * @param id Field ID
         * @return The identified field
         * @throws IllegalArgumentException If no such field
         */
        public static _Field fieldForId(int id) {
            _Field field = findById(id);
            if (field == null) {
                throw new IllegalArgumentException("No such field id " + id + " in providence.UnionOfFields");
            }
            return field;
        }

        /**
         * @param name Field name
         * @return The named field
         * @throws IllegalArgumentException If no such field
         */
        public static _Field fieldForName(String name) {
            if (name == null) {
                throw new IllegalArgumentException("Null name argument");
            }
            _Field field = findByName(name);
            if (field == null) {
                throw new IllegalArgumentException("No such field \"" + name + "\" in providence.UnionOfFields");
            }
            return field;
        }

        /**
         * @param name Field POJO name
         * @return The named field
         * @throws IllegalArgumentException If no such field
         */
        public static _Field fieldForPojoName(String name) {
            if (name == null) {
                throw new IllegalArgumentException("Null name argument");
            }
            _Field field = findByPojoName(name);
            if (field == null) {
                throw new IllegalArgumentException("No such field \"" + name + "\" in providence.UnionOfFields");
            }
            return field;
        }
    }

    @javax.annotation.Nonnull
    public static net.morimekta.providence.descriptor.PUnionDescriptorProvider<UnionOfFields> provider() {
        return new _Provider();
    }

    @Override
    @javax.annotation.Nonnull
    public net.morimekta.providence.descriptor.PUnionDescriptor<UnionOfFields> descriptor() {
        return kDescriptor;
    }

    public static final net.morimekta.providence.descriptor.PUnionDescriptor<UnionOfFields> kDescriptor;

    private static final class _Descriptor
            extends net.morimekta.providence.descriptor.PUnionDescriptor<UnionOfFields> {
        public _Descriptor() {
            super("providence", "UnionOfFields", _Builder::new, false);
        }

        @Override
        @javax.annotation.Nonnull
        public boolean isInnerType() {
            return false;
        }

        @Override
        @javax.annotation.Nonnull
        public boolean isAutoType() {
            return false;
        }

        @Override
        @javax.annotation.Nonnull
        public _Field[] getFields() {
            return _Field.values();
        }

        @Override
        @javax.annotation.Nullable
        public _Field findFieldByName(String name) {
            return _Field.findByName(name);
        }

        @Override
        @javax.annotation.Nullable
        public _Field findFieldByPojoName(String name) {
            return _Field.findByPojoName(name);
        }

        @Override
        @javax.annotation.Nullable
        public _Field findFieldById(int id) {
            return _Field.findById(id);
        }

        @Override
        public net.morimekta.providence.descriptor.PInterfaceDescriptor getImplementing() {
            return net.morimekta.test.providence.core.CommonFields.kDescriptor;
        }
    }

    static {
        kDescriptor = new _Descriptor();
    }

    private static final class _Provider extends net.morimekta.providence.descriptor.PUnionDescriptorProvider<UnionOfFields> {
        @Override
        @javax.annotation.Nonnull
        public net.morimekta.providence.descriptor.PUnionDescriptor<UnionOfFields> descriptor() {
            return kDescriptor;
        }
    }

    /**
     * Make a <code>providence.UnionOfFields</code> builder.
     * @return The builder instance.
     */
    public static _Builder builder() {
        return new _Builder();
    }

    public static class _Builder
            extends net.morimekta.providence.PMessageBuilder<UnionOfFields>
            implements UnionOfFields_OrBuilder,
                       net.morimekta.providence.serializer.binary.BinaryReader {
        private UnionOfFields._Field tUnionField;

        private boolean modified;

        private net.morimekta.test.providence.core.OptionalFields mOpts;
        private net.morimekta.test.providence.core.OptionalFields._Builder mOpts_builder;
        private net.morimekta.test.providence.core.MinimalFields mMin;
        private net.morimekta.test.providence.core.MinimalFields._Builder mMin_builder;

        /**
         * Make a providence.UnionOfFields builder instance.
         */
        public _Builder() {
            modified = false;
        }

        /**
         * Make a mutating builder off a base providence.UnionOfFields.
         *
         * @param base The base UnionOfFields
         */
        public _Builder(UnionOfFields base) {
            this();

            tUnionField = base.tUnionField;

            mOpts = base.mOpts;
            mMin = base.mMin;
        }

        @javax.annotation.Nonnull
        @Override
        public UnionOfFields._Builder merge(UnionOfFields from) {
            if (!from.unionFieldIsSet()) {
                return this;
            }

            switch (from.unionField()) {
                case OPTS: {
                    if (tUnionField == UnionOfFields._Field.OPTS && mOpts != null) {
                        mOpts = mOpts.mutate().merge(from.getOpts()).build();
                    } else {
                        setOpts(from.getOpts());
                    }
                    break;
                }
                case MIN: {
                    if (tUnionField == UnionOfFields._Field.MIN && mMin != null) {
                        mMin = mMin.mutate().merge(from.getMin()).build();
                    } else {
                        setMin(from.getMin());
                    }
                    break;
                }
            }
            return this;
        }

        /**
         * Set the <code>opts</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public UnionOfFields._Builder setOpts(net.morimekta.test.providence.core.OptionalFields_OrBuilder value) {
            if (value == null) {
                return clearOpts();
            }

            tUnionField = UnionOfFields._Field.OPTS;
            modified = true;
            if (value instanceof net.morimekta.test.providence.core.OptionalFields._Builder) {
                value = ((net.morimekta.test.providence.core.OptionalFields._Builder) value).build();
            } else if (!(value instanceof net.morimekta.test.providence.core.OptionalFields)) {
                throw new java.lang.IllegalArgumentException("Invalid type for providence.OptionalFields: " + value.getClass().getName());
            }
            mOpts = (net.morimekta.test.providence.core.OptionalFields) value;
            mOpts_builder = null;
            return this;
        }

        /**
         * Checks for explicit presence of the <code>opts</code> field.
         *
         * @return True if opts has been set.
         */
        public boolean isSetOpts() {
            return tUnionField == UnionOfFields._Field.OPTS;
        }

        /**
         * Checks for presence of the <code>opts</code> field.
         *
         * @return True if opts is present.
         */
        public boolean hasOpts() {
            return tUnionField == UnionOfFields._Field.OPTS;
        }

        /**
         * Clear the <code>opts</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public UnionOfFields._Builder clearOpts() {
            if (tUnionField == UnionOfFields._Field.OPTS) tUnionField = null;
            modified = true;
            mOpts = null;
            mOpts_builder = null;
            return this;
        }

        /**
         * Get the builder for the contained <code>opts</code> message field.
         *
         * @return The field message builder
         */
        @javax.annotation.Nonnull
        public net.morimekta.test.providence.core.OptionalFields._Builder mutableOpts() {
            if (tUnionField != UnionOfFields._Field.OPTS) {
                clearOpts();
            }
            tUnionField = UnionOfFields._Field.OPTS;
            modified = true;

            if (mOpts != null) {
                mOpts_builder = mOpts.mutate();
                mOpts = null;
            } else if (mOpts_builder == null) {
                mOpts_builder = net.morimekta.test.providence.core.OptionalFields.builder();
            }
            return mOpts_builder;
        }

        /**
         * @return The <code>opts</code> field value
         */
        public net.morimekta.test.providence.core.OptionalFields getOpts() {
            return mOpts_builder != null ? mOpts_builder.build() : mOpts;
        }

        /**
         * @return Optional <code>opts</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<net.morimekta.test.providence.core.OptionalFields> optionalOpts() {
            return java.util.Optional.ofNullable(mOpts_builder != null ? mOpts_builder.build() : mOpts);
        }

        /**
         * Set the <code>min</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public UnionOfFields._Builder setMin(net.morimekta.test.providence.core.MinimalFields_OrBuilder value) {
            if (value == null) {
                return clearMin();
            }

            tUnionField = UnionOfFields._Field.MIN;
            modified = true;
            if (value instanceof net.morimekta.test.providence.core.MinimalFields._Builder) {
                value = ((net.morimekta.test.providence.core.MinimalFields._Builder) value).build();
            } else if (!(value instanceof net.morimekta.test.providence.core.MinimalFields)) {
                throw new java.lang.IllegalArgumentException("Invalid type for providence.MinimalFields: " + value.getClass().getName());
            }
            mMin = (net.morimekta.test.providence.core.MinimalFields) value;
            mMin_builder = null;
            return this;
        }

        /**
         * Checks for explicit presence of the <code>min</code> field.
         *
         * @return True if min has been set.
         */
        public boolean isSetMin() {
            return tUnionField == UnionOfFields._Field.MIN;
        }

        /**
         * Checks for presence of the <code>min</code> field.
         *
         * @return True if min is present.
         */
        public boolean hasMin() {
            return tUnionField == UnionOfFields._Field.MIN;
        }

        /**
         * Clear the <code>min</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public UnionOfFields._Builder clearMin() {
            if (tUnionField == UnionOfFields._Field.MIN) tUnionField = null;
            modified = true;
            mMin = null;
            mMin_builder = null;
            return this;
        }

        /**
         * Get the builder for the contained <code>min</code> message field.
         *
         * @return The field message builder
         */
        @javax.annotation.Nonnull
        public net.morimekta.test.providence.core.MinimalFields._Builder mutableMin() {
            if (tUnionField != UnionOfFields._Field.MIN) {
                clearMin();
            }
            tUnionField = UnionOfFields._Field.MIN;
            modified = true;

            if (mMin != null) {
                mMin_builder = mMin.mutate();
                mMin = null;
            } else if (mMin_builder == null) {
                mMin_builder = net.morimekta.test.providence.core.MinimalFields.builder();
            }
            return mMin_builder;
        }

        /**
         * @return The <code>min</code> field value
         */
        public net.morimekta.test.providence.core.MinimalFields getMin() {
            return mMin_builder != null ? mMin_builder.build() : mMin;
        }

        /**
         * @return Optional <code>min</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<net.morimekta.test.providence.core.MinimalFields> optionalMin() {
            return java.util.Optional.ofNullable(mMin_builder != null ? mMin_builder.build() : mMin);
        }

        /**
         * Checks if the <code>UnionOfFields</code> union has been modified since the
         * builder was created.
         *
         * @return True if UnionOfFields has been modified.
         */
        public boolean isUnionModified() {
            return modified;
        }

        @Override
        public boolean equals(Object o) {
            if (o == this) return true;
            if (o == null || !o.getClass().equals(getClass())) return false;
            UnionOfFields._Builder other = (UnionOfFields._Builder) o;
            return java.util.Objects.equals(tUnionField, other.tUnionField) &&
                   java.util.Objects.equals(getOpts(), other.getOpts()) &&
                   java.util.Objects.equals(getMin(), other.getMin());
        }

        @Override
        public int hashCode() {
            return java.util.Objects.hash(
                    UnionOfFields.class,
                    UnionOfFields._Field.OPTS, getOpts(),
                    UnionOfFields._Field.MIN, getMin());
        }

        @Override
        @SuppressWarnings("unchecked")
        public net.morimekta.providence.PMessageBuilder mutator(int key) {
            switch (key) {
                case 1: return mutableOpts();
                case 2: return mutableMin();
                default: throw new IllegalArgumentException("Not a message field ID: " + key);
            }
        }

        @javax.annotation.Nonnull
        @Override
        @SuppressWarnings("unchecked")
        public UnionOfFields._Builder set(int key, Object value) {
            if (value == null) return clear(key);
            switch (key) {
                case 1: setOpts((net.morimekta.test.providence.core.OptionalFields) value); break;
                case 2: setMin((net.morimekta.test.providence.core.MinimalFields) value); break;
                default: break;
            }
            return this;
        }

        @Override
        public boolean isSet(int key) {
            switch (key) {
                case 1: return tUnionField == UnionOfFields._Field.OPTS;
                case 2: return tUnionField == UnionOfFields._Field.MIN;
                default: break;
            }
            return false;
        }

        @Override
        public boolean isModified(int key) {
            return modified;
        }

        @Override
        @SuppressWarnings("unchecked")
        public <T> T get(int key) {
            switch(key) {
                case 1: return (T) getOpts();
                case 2: return (T) getMin();
                default: return null;
            }
        }

        @Override
        public boolean has(int key) {
            switch(key) {
                case 1: return tUnionField == _Field.OPTS;
                case 2: return tUnionField == _Field.MIN;
                default: return false;
            }
        }

        @javax.annotation.Nonnull
        @Override
        @SuppressWarnings("unchecked")
        public UnionOfFields._Builder addTo(int key, Object value) {
            switch (key) {
                default: break;
            }
            return this;
        }

        @javax.annotation.Nonnull
        @Override
        public UnionOfFields._Builder clear(int key) {
            switch (key) {
                case 1: clearOpts(); break;
                case 2: clearMin(); break;
                default: break;
            }
            return this;
        }

        @Override
        public boolean valid() {
            if (tUnionField == null) {
                return false;
            }

            switch (tUnionField) {
                case OPTS: return mOpts != null || mOpts_builder != null;
                case MIN: return mMin != null || mMin_builder != null;
                default: return true;
            }
        }

        @Override
        public UnionOfFields._Builder validate() {
            if (!valid()) {
                throw new java.lang.IllegalStateException("No union field set in providence.UnionOfFields");
            }
            return this;
        }

        @javax.annotation.Nonnull
        @Override
        public net.morimekta.providence.descriptor.PUnionDescriptor<UnionOfFields> descriptor() {
            return UnionOfFields.kDescriptor;
        }

        @Override
        public void readBinary(net.morimekta.util.io.BigEndianBinaryReader reader, boolean strict) throws java.io.IOException {
            byte type = reader.expectByte();
            while (type != 0) {
                int field = reader.expectShort();
                switch (field) {
                    case 1: {
                        if (type == 12) {
                            mOpts = net.morimekta.providence.serializer.binary.BinaryFormatUtils.readMessage(reader, net.morimekta.test.providence.core.OptionalFields.kDescriptor, strict);
                            tUnionField = UnionOfFields._Field.OPTS;
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.UnionOfFields.opts, should be struct(12)");
                        }
                        break;
                    }
                    case 2: {
                        if (type == 12) {
                            mMin = net.morimekta.providence.serializer.binary.BinaryFormatUtils.readMessage(reader, net.morimekta.test.providence.core.MinimalFields.kDescriptor, strict);
                            tUnionField = UnionOfFields._Field.MIN;
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for providence.UnionOfFields.min, should be struct(12)");
                        }
                        break;
                    }
                    default: {
                        net.morimekta.providence.serializer.binary.BinaryFormatUtils.readFieldValue(reader, new net.morimekta.providence.serializer.binary.BinaryFormatUtils.FieldInfo(field, type), null, false);
                        break;
                    }
                }
                type = reader.expectByte();
            }
        }

        @Override
        @javax.annotation.Nonnull
        public UnionOfFields build() {
            return new UnionOfFields(this);
        }
    }
}
