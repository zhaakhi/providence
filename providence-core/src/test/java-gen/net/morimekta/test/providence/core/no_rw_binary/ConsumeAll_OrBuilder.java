package net.morimekta.test.providence.core.no_rw_binary;

@javax.annotation.Generated(
        value = "net.morimekta.providence:providence-generator-java",
        comments = "java:no_rw_binary")
@SuppressWarnings("unused")
public interface ConsumeAll_OrBuilder extends net.morimekta.providence.PMessageOrBuilder<ConsumeAll> {
}
