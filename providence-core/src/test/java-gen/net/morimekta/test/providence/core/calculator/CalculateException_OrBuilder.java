package net.morimekta.test.providence.core.calculator;

@javax.annotation.Generated(
        value = "net.morimekta.providence:providence-generator-java",
        comments = "java:serializable")
@SuppressWarnings("unused")
public interface CalculateException_OrBuilder extends net.morimekta.providence.PMessageOrBuilder<CalculateException> {
    /**
     * @return The message value.
     */
    @javax.annotation.Nonnull
    String getMessage();

    /**
     * @return If message is present.
     */
    boolean hasMessage();

    /**
     * @return The operation value.
     */
    net.morimekta.test.providence.core.calculator.Operation getOperation();

    /**
     * @return Optional operation value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<net.morimekta.test.providence.core.calculator.Operation> optionalOperation();

    /**
     * @return If operation is present.
     */
    boolean hasOperation();

}
