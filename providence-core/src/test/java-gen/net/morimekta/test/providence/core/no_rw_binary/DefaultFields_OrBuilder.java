package net.morimekta.test.providence.core.no_rw_binary;

@javax.annotation.Generated(
        value = "net.morimekta.providence:providence-generator-java",
        comments = "java:no_rw_binary")
@SuppressWarnings("unused")
public interface DefaultFields_OrBuilder extends net.morimekta.providence.PMessageOrBuilder<DefaultFields> {
    /**
     * @return The booleanValue value.
     */
    boolean isBooleanValue();

    /**
     * @return If booleanValue is present.
     */
    boolean hasBooleanValue();

    /**
     * @return The byteValue value.
     */
    byte getByteValue();

    /**
     * @return If byteValue is present.
     */
    boolean hasByteValue();

    /**
     * @return The shortValue value.
     */
    short getShortValue();

    /**
     * @return If shortValue is present.
     */
    boolean hasShortValue();

    /**
     * @return The integerValue value.
     */
    int getIntegerValue();

    /**
     * @return If integerValue is present.
     */
    boolean hasIntegerValue();

    /**
     * @return The longValue value.
     */
    long getLongValue();

    /**
     * @return If longValue is present.
     */
    boolean hasLongValue();

    /**
     * @return The doubleValue value.
     */
    double getDoubleValue();

    /**
     * @return If doubleValue is present.
     */
    boolean hasDoubleValue();

    /**
     * @return The stringValue value.
     */
    @javax.annotation.Nonnull
    String getStringValue();

    /**
     * @return If stringValue is present.
     */
    boolean hasStringValue();

    /**
     * @return The binaryValue value.
     */
    @javax.annotation.Nonnull
    net.morimekta.util.Binary getBinaryValue();

    /**
     * @return If binaryValue is present.
     */
    boolean hasBinaryValue();

    /**
     * @return The enumValue value.
     */
    net.morimekta.test.providence.core.no_rw_binary.Value getEnumValue();

    /**
     * @return Optional enumValue value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<net.morimekta.test.providence.core.no_rw_binary.Value> optionalEnumValue();

    /**
     * @return If enumValue is present.
     */
    boolean hasEnumValue();

    /**
     * @return The compactValue value.
     */
    net.morimekta.test.providence.core.no_rw_binary.CompactFields getCompactValue();

    /**
     * @return Optional compactValue value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<net.morimekta.test.providence.core.no_rw_binary.CompactFields> optionalCompactValue();

    /**
     * @return If compactValue is present.
     */
    boolean hasCompactValue();

}
