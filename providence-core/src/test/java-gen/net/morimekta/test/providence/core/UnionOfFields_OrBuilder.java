package net.morimekta.test.providence.core;

@javax.annotation.Generated(
        value = "net.morimekta.providence:providence-generator-java",
        comments = "java:serializable")
@SuppressWarnings("unused")
public interface UnionOfFields_OrBuilder extends net.morimekta.providence.PMessageOrBuilder<UnionOfFields> {
    /**
     * @return The opts value.
     */
    net.morimekta.test.providence.core.OptionalFields getOpts();

    /**
     * @return Optional opts value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<net.morimekta.test.providence.core.OptionalFields> optionalOpts();

    /**
     * @return If opts is present.
     */
    boolean hasOpts();

    /**
     * @return The min value.
     */
    net.morimekta.test.providence.core.MinimalFields getMin();

    /**
     * @return Optional min value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<net.morimekta.test.providence.core.MinimalFields> optionalMin();

    /**
     * @return If min is present.
     */
    boolean hasMin();

}
