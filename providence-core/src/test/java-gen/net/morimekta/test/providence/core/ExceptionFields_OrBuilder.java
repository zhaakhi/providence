package net.morimekta.test.providence.core;

@javax.annotation.Generated(
        value = "net.morimekta.providence:providence-generator-java",
        comments = "java:serializable")
@SuppressWarnings("unused")
public interface ExceptionFields_OrBuilder extends net.morimekta.providence.PMessageOrBuilder<ExceptionFields> {
    /**
     * @return The boolean_value value.
     */
    boolean isBooleanValue();

    /**
     * @return Optional boolean_value value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<Boolean> optionalBooleanValue();

    /**
     * @return If boolean_value is present.
     */
    boolean hasBooleanValue();

    /**
     * @return The byte_value value.
     */
    byte getByteValue();

    /**
     * @return Optional byte_value value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<Byte> optionalByteValue();

    /**
     * @return If byte_value is present.
     */
    boolean hasByteValue();

    /**
     * @return The short_value value.
     */
    short getShortValue();

    /**
     * @return Optional short_value value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<Short> optionalShortValue();

    /**
     * @return If short_value is present.
     */
    boolean hasShortValue();

    /**
     * @return The integer_value value.
     */
    int getIntegerValue();

    /**
     * @return Optional integer_value value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<Integer> optionalIntegerValue();

    /**
     * @return If integer_value is present.
     */
    boolean hasIntegerValue();

    /**
     * @return The long_value value.
     */
    long getLongValue();

    /**
     * @return Optional long_value value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<Long> optionalLongValue();

    /**
     * @return If long_value is present.
     */
    boolean hasLongValue();

    /**
     * @return The double_value value.
     */
    double getDoubleValue();

    /**
     * @return Optional double_value value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<Double> optionalDoubleValue();

    /**
     * @return If double_value is present.
     */
    boolean hasDoubleValue();

    /**
     * @return The string_value value.
     */
    String getStringValue();

    /**
     * @return Optional string_value value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<String> optionalStringValue();

    /**
     * @return If string_value is present.
     */
    boolean hasStringValue();

    /**
     * @return The binary_value value.
     */
    net.morimekta.util.Binary getBinaryValue();

    /**
     * @return Optional binary_value value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<net.morimekta.util.Binary> optionalBinaryValue();

    /**
     * @return If binary_value is present.
     */
    boolean hasBinaryValue();

    /**
     * @return The enum_value value.
     */
    net.morimekta.test.providence.core.Value getEnumValue();

    /**
     * @return Optional enum_value value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<net.morimekta.test.providence.core.Value> optionalEnumValue();

    /**
     * @return If enum_value is present.
     */
    boolean hasEnumValue();

    /**
     * @return The compact_value value.
     */
    net.morimekta.test.providence.core.CompactFields getCompactValue();

    /**
     * @return Optional compact_value value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<net.morimekta.test.providence.core.CompactFields> optionalCompactValue();

    /**
     * @return If compact_value is present.
     */
    boolean hasCompactValue();

}
