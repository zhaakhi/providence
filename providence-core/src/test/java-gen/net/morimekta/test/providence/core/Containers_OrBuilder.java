package net.morimekta.test.providence.core;

@javax.annotation.Generated(
        value = "net.morimekta.providence:providence-generator-java",
        comments = "java:serializable")
@SuppressWarnings("unused")
public interface Containers_OrBuilder extends net.morimekta.providence.PMessageOrBuilder<Containers> {
    /**
     * @return The boolean_list value.
     */
    java.util.List<Boolean> getBooleanList();

    /**
     * @return Optional boolean_list value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.List<Boolean>> optionalBooleanList();

    /**
     * @return If boolean_list is present.
     */
    boolean hasBooleanList();

    /**
     * @return Number of entries in boolean_list.
     */
    int numBooleanList();

    /**
     * @return The byte_list value.
     */
    java.util.List<Byte> getByteList();

    /**
     * @return Optional byte_list value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.List<Byte>> optionalByteList();

    /**
     * @return If byte_list is present.
     */
    boolean hasByteList();

    /**
     * @return Number of entries in byte_list.
     */
    int numByteList();

    /**
     * @return The short_list value.
     */
    java.util.List<Short> getShortList();

    /**
     * @return Optional short_list value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.List<Short>> optionalShortList();

    /**
     * @return If short_list is present.
     */
    boolean hasShortList();

    /**
     * @return Number of entries in short_list.
     */
    int numShortList();

    /**
     * @return The integer_list value.
     */
    java.util.List<Integer> getIntegerList();

    /**
     * @return Optional integer_list value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.List<Integer>> optionalIntegerList();

    /**
     * @return If integer_list is present.
     */
    boolean hasIntegerList();

    /**
     * @return Number of entries in integer_list.
     */
    int numIntegerList();

    /**
     * @return The long_list value.
     */
    java.util.List<Long> getLongList();

    /**
     * @return Optional long_list value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.List<Long>> optionalLongList();

    /**
     * @return If long_list is present.
     */
    boolean hasLongList();

    /**
     * @return Number of entries in long_list.
     */
    int numLongList();

    /**
     * @return The double_list value.
     */
    java.util.List<Double> getDoubleList();

    /**
     * @return Optional double_list value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.List<Double>> optionalDoubleList();

    /**
     * @return If double_list is present.
     */
    boolean hasDoubleList();

    /**
     * @return Number of entries in double_list.
     */
    int numDoubleList();

    /**
     * @return The string_list value.
     */
    java.util.List<String> getStringList();

    /**
     * @return Optional string_list value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.List<String>> optionalStringList();

    /**
     * @return If string_list is present.
     */
    boolean hasStringList();

    /**
     * @return Number of entries in string_list.
     */
    int numStringList();

    /**
     * @return The binary_list value.
     */
    java.util.List<net.morimekta.util.Binary> getBinaryList();

    /**
     * @return Optional binary_list value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.List<net.morimekta.util.Binary>> optionalBinaryList();

    /**
     * @return If binary_list is present.
     */
    boolean hasBinaryList();

    /**
     * @return Number of entries in binary_list.
     */
    int numBinaryList();

    /**
     * @return The boolean_set value.
     */
    java.util.Set<Boolean> getBooleanSet();

    /**
     * @return Optional boolean_set value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Set<Boolean>> optionalBooleanSet();

    /**
     * @return If boolean_set is present.
     */
    boolean hasBooleanSet();

    /**
     * @return Number of entries in boolean_set.
     */
    int numBooleanSet();

    /**
     * @return The byte_set value.
     */
    java.util.Set<Byte> getByteSet();

    /**
     * @return Optional byte_set value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Set<Byte>> optionalByteSet();

    /**
     * @return If byte_set is present.
     */
    boolean hasByteSet();

    /**
     * @return Number of entries in byte_set.
     */
    int numByteSet();

    /**
     * @return The short_set value.
     */
    java.util.Set<Short> getShortSet();

    /**
     * @return Optional short_set value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Set<Short>> optionalShortSet();

    /**
     * @return If short_set is present.
     */
    boolean hasShortSet();

    /**
     * @return Number of entries in short_set.
     */
    int numShortSet();

    /**
     * @return The integer_set value.
     */
    java.util.Set<Integer> getIntegerSet();

    /**
     * @return Optional integer_set value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Set<Integer>> optionalIntegerSet();

    /**
     * @return If integer_set is present.
     */
    boolean hasIntegerSet();

    /**
     * @return Number of entries in integer_set.
     */
    int numIntegerSet();

    /**
     * @return The long_set value.
     */
    java.util.Set<Long> getLongSet();

    /**
     * @return Optional long_set value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Set<Long>> optionalLongSet();

    /**
     * @return If long_set is present.
     */
    boolean hasLongSet();

    /**
     * @return Number of entries in long_set.
     */
    int numLongSet();

    /**
     * @return The double_set value.
     */
    java.util.Set<Double> getDoubleSet();

    /**
     * @return Optional double_set value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Set<Double>> optionalDoubleSet();

    /**
     * @return If double_set is present.
     */
    boolean hasDoubleSet();

    /**
     * @return Number of entries in double_set.
     */
    int numDoubleSet();

    /**
     * @return The string_set value.
     */
    java.util.Set<String> getStringSet();

    /**
     * @return Optional string_set value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Set<String>> optionalStringSet();

    /**
     * @return If string_set is present.
     */
    boolean hasStringSet();

    /**
     * @return Number of entries in string_set.
     */
    int numStringSet();

    /**
     * @return The binary_set value.
     */
    java.util.Set<net.morimekta.util.Binary> getBinarySet();

    /**
     * @return Optional binary_set value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Set<net.morimekta.util.Binary>> optionalBinarySet();

    /**
     * @return If binary_set is present.
     */
    boolean hasBinarySet();

    /**
     * @return Number of entries in binary_set.
     */
    int numBinarySet();

    /**
     * @return The boolean_map value.
     */
    java.util.Map<Boolean,Boolean> getBooleanMap();

    /**
     * @return Optional boolean_map value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Map<Boolean,Boolean>> optionalBooleanMap();

    /**
     * @return If boolean_map is present.
     */
    boolean hasBooleanMap();

    /**
     * @return Number of entries in boolean_map.
     */
    int numBooleanMap();

    /**
     * @return The byte_map value.
     */
    java.util.Map<Byte,Byte> getByteMap();

    /**
     * @return Optional byte_map value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Map<Byte,Byte>> optionalByteMap();

    /**
     * @return If byte_map is present.
     */
    boolean hasByteMap();

    /**
     * @return Number of entries in byte_map.
     */
    int numByteMap();

    /**
     * @return The short_map value.
     */
    java.util.Map<Short,Short> getShortMap();

    /**
     * @return Optional short_map value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Map<Short,Short>> optionalShortMap();

    /**
     * @return If short_map is present.
     */
    boolean hasShortMap();

    /**
     * @return Number of entries in short_map.
     */
    int numShortMap();

    /**
     * @return The integer_map value.
     */
    java.util.Map<Integer,Integer> getIntegerMap();

    /**
     * @return Optional integer_map value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Map<Integer,Integer>> optionalIntegerMap();

    /**
     * @return If integer_map is present.
     */
    boolean hasIntegerMap();

    /**
     * @return Number of entries in integer_map.
     */
    int numIntegerMap();

    /**
     * @return The long_map value.
     */
    java.util.Map<Long,Long> getLongMap();

    /**
     * @return Optional long_map value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Map<Long,Long>> optionalLongMap();

    /**
     * @return If long_map is present.
     */
    boolean hasLongMap();

    /**
     * @return Number of entries in long_map.
     */
    int numLongMap();

    /**
     * @return The double_map value.
     */
    java.util.Map<Double,Double> getDoubleMap();

    /**
     * @return Optional double_map value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Map<Double,Double>> optionalDoubleMap();

    /**
     * @return If double_map is present.
     */
    boolean hasDoubleMap();

    /**
     * @return Number of entries in double_map.
     */
    int numDoubleMap();

    /**
     * @return The string_map value.
     */
    java.util.Map<String,String> getStringMap();

    /**
     * @return Optional string_map value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Map<String,String>> optionalStringMap();

    /**
     * @return If string_map is present.
     */
    boolean hasStringMap();

    /**
     * @return Number of entries in string_map.
     */
    int numStringMap();

    /**
     * @return The binary_map value.
     */
    java.util.Map<net.morimekta.util.Binary,net.morimekta.util.Binary> getBinaryMap();

    /**
     * @return Optional binary_map value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Map<net.morimekta.util.Binary,net.morimekta.util.Binary>> optionalBinaryMap();

    /**
     * @return If binary_map is present.
     */
    boolean hasBinaryMap();

    /**
     * @return Number of entries in binary_map.
     */
    int numBinaryMap();

    /**
     * @return The enum_list value.
     */
    java.util.List<net.morimekta.test.providence.core.Value> getEnumList();

    /**
     * @return Optional enum_list value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.List<net.morimekta.test.providence.core.Value>> optionalEnumList();

    /**
     * @return If enum_list is present.
     */
    boolean hasEnumList();

    /**
     * @return Number of entries in enum_list.
     */
    int numEnumList();

    /**
     * @return The enum_set value.
     */
    java.util.Set<net.morimekta.test.providence.core.Value> getEnumSet();

    /**
     * @return Optional enum_set value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Set<net.morimekta.test.providence.core.Value>> optionalEnumSet();

    /**
     * @return If enum_set is present.
     */
    boolean hasEnumSet();

    /**
     * @return Number of entries in enum_set.
     */
    int numEnumSet();

    /**
     * @return The enum_map value.
     */
    java.util.Map<net.morimekta.test.providence.core.Value,net.morimekta.test.providence.core.Value> getEnumMap();

    /**
     * @return Optional enum_map value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Map<net.morimekta.test.providence.core.Value,net.morimekta.test.providence.core.Value>> optionalEnumMap();

    /**
     * @return If enum_map is present.
     */
    boolean hasEnumMap();

    /**
     * @return Number of entries in enum_map.
     */
    int numEnumMap();

    /**
     * @return The message_list value.
     */
    java.util.List<net.morimekta.test.providence.core.OptionalFields> getMessageList();

    /**
     * @return Optional message_list value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.List<net.morimekta.test.providence.core.OptionalFields>> optionalMessageList();

    /**
     * @return If message_list is present.
     */
    boolean hasMessageList();

    /**
     * @return Number of entries in message_list.
     */
    int numMessageList();

    /**
     * @return The message_set value.
     */
    java.util.Set<net.morimekta.test.providence.core.OptionalFields> getMessageSet();

    /**
     * @return Optional message_set value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Set<net.morimekta.test.providence.core.OptionalFields>> optionalMessageSet();

    /**
     * @return If message_set is present.
     */
    boolean hasMessageSet();

    /**
     * @return Number of entries in message_set.
     */
    int numMessageSet();

    /**
     * @return The message_map value.
     */
    java.util.Map<String,net.morimekta.test.providence.core.OptionalFields> getMessageMap();

    /**
     * @return Optional message_map value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Map<String,net.morimekta.test.providence.core.OptionalFields>> optionalMessageMap();

    /**
     * @return If message_map is present.
     */
    boolean hasMessageMap();

    /**
     * @return Number of entries in message_map.
     */
    int numMessageMap();

    /**
     * @return The message_key_map value.
     */
    java.util.Map<net.morimekta.test.providence.core.CompactFields,String> getMessageKeyMap();

    /**
     * @return Optional message_key_map value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Map<net.morimekta.test.providence.core.CompactFields,String>> optionalMessageKeyMap();

    /**
     * @return If message_key_map is present.
     */
    boolean hasMessageKeyMap();

    /**
     * @return Number of entries in message_key_map.
     */
    int numMessageKeyMap();

    /**
     * @return The required_fields value.
     */
    net.morimekta.test.providence.core.RequiredFields getRequiredFields();

    /**
     * @return Optional required_fields value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<net.morimekta.test.providence.core.RequiredFields> optionalRequiredFields();

    /**
     * @return If required_fields is present.
     */
    boolean hasRequiredFields();

    /**
     * @return The default_fields value.
     */
    net.morimekta.test.providence.core.DefaultFields getDefaultFields();

    /**
     * @return Optional default_fields value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<net.morimekta.test.providence.core.DefaultFields> optionalDefaultFields();

    /**
     * @return If default_fields is present.
     */
    boolean hasDefaultFields();

    /**
     * @return The optional_fields value.
     */
    net.morimekta.test.providence.core.OptionalFields getOptionalFields();

    /**
     * @return Optional optional_fields value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<net.morimekta.test.providence.core.OptionalFields> optionalOptionalFields();

    /**
     * @return If optional_fields is present.
     */
    boolean hasOptionalFields();

    /**
     * @return The union_fields value.
     */
    net.morimekta.test.providence.core.UnionFields getUnionFields();

    /**
     * @return Optional union_fields value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<net.morimekta.test.providence.core.UnionFields> optionalUnionFields();

    /**
     * @return If union_fields is present.
     */
    boolean hasUnionFields();

    /**
     * @return The exception_fields value.
     */
    net.morimekta.test.providence.core.ExceptionFields getExceptionFields();

    /**
     * @return Optional exception_fields value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<net.morimekta.test.providence.core.ExceptionFields> optionalExceptionFields();

    /**
     * @return If exception_fields is present.
     */
    boolean hasExceptionFields();

    /**
     * @return The default_values value.
     */
    net.morimekta.test.providence.core.DefaultValues getDefaultValues();

    /**
     * @return Optional default_values value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<net.morimekta.test.providence.core.DefaultValues> optionalDefaultValues();

    /**
     * @return If default_values is present.
     */
    boolean hasDefaultValues();

    /**
     * @return The map_list_compact value.
     */
    java.util.Map<Integer,java.util.List<net.morimekta.test.providence.core.CompactFields>> getMapListCompact();

    /**
     * @return Optional map_list_compact value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Map<Integer,java.util.List<net.morimekta.test.providence.core.CompactFields>>> optionalMapListCompact();

    /**
     * @return If map_list_compact is present.
     */
    boolean hasMapListCompact();

    /**
     * @return Number of entries in map_list_compact.
     */
    int numMapListCompact();

    /**
     * @return The list_map_numbers value.
     */
    java.util.List<java.util.Map<Integer,Integer>> getListMapNumbers();

    /**
     * @return Optional list_map_numbers value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.List<java.util.Map<Integer,Integer>>> optionalListMapNumbers();

    /**
     * @return If list_map_numbers is present.
     */
    boolean hasListMapNumbers();

    /**
     * @return Number of entries in list_map_numbers.
     */
    int numListMapNumbers();

    /**
     * @return The set_list_numbers value.
     */
    java.util.Set<java.util.List<Integer>> getSetListNumbers();

    /**
     * @return Optional set_list_numbers value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.Set<java.util.List<Integer>>> optionalSetListNumbers();

    /**
     * @return If set_list_numbers is present.
     */
    boolean hasSetListNumbers();

    /**
     * @return Number of entries in set_list_numbers.
     */
    int numSetListNumbers();

    /**
     * @return The list_list_numbers value.
     */
    java.util.List<java.util.List<Integer>> getListListNumbers();

    /**
     * @return Optional list_list_numbers value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<java.util.List<java.util.List<Integer>>> optionalListListNumbers();

    /**
     * @return If list_list_numbers is present.
     */
    boolean hasListListNumbers();

    /**
     * @return Number of entries in list_list_numbers.
     */
    int numListListNumbers();

}
