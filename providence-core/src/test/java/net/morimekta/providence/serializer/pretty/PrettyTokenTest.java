package net.morimekta.providence.serializer.pretty;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.fail;

public class PrettyTokenTest {
    @Test
    public void testIdentifiers() throws IOException {
        assertThat(token("foo").toString(), is("foo"));
        assertThat(token("foo").isSymbol('f'), is(false));
        assertThat(token("foo").isStringLiteral(), is(false));
        assertThat(token("foo").isInteger(), is(false));
        assertThat(token("foo").isReal(), is(false));
        assertThat(token("foo").isIdentifier(), is(true));
        assertThat(token("foo").isQualifiedIdentifier(), is(false));
        assertThat(token("foo").isReferenceIdentifier(), is(true));

        assertThat(token("foo.bar").toString(), is("foo.bar"));
        assertThat(token("foo.bar").isSymbol('f'), is(false));
        assertThat(token("foo.bar").isStringLiteral(), is(false));
        assertThat(token("foo.bar").isInteger(), is(false));
        assertThat(token("foo.bar").isReal(), is(false));
        assertThat(token("foo.bar").isIdentifier(), is(false));
        assertThat(token("foo.bar").isQualifiedIdentifier(), is(true));
        assertThat(token("foo.bar").isReferenceIdentifier(), is(true));

        assertThat(token("foo.bar.more").toString(), is("foo.bar.more"));
        assertThat(token("foo.bar.more").isSymbol('f'), is(false));
        assertThat(token("foo.bar.more").isStringLiteral(), is(false));
        assertThat(token("foo.bar.more").isInteger(), is(false));
        assertThat(token("foo.bar.more").isReal(), is(false));
        assertThat(token("foo.bar.more").isIdentifier(), is(false));
        assertThat(token("foo.bar.more").isQualifiedIdentifier(), is(false));
        assertThat(token("foo.bar.more").isReferenceIdentifier(), is(true));

        assertThat(token("foo").hashCode(), is(token("foo").hashCode()));
        assertThat(token("foo").hashCode(), is(not(token("bar").hashCode())));

        assertThat(token("foo"), is(token("foo")));
        assertThat(token("foo"), is(not(token("bar"))));
        assertThat(token("foo").toString(), is("foo"));
    }

    @Test
    public void testLiteral() throws IOException {
        assertThat(token("\"foo\"").isStringLiteral(), is(true));
        assertThat(token("\"foo\"").decodeString(true), is("foo"));
        assertThat(token("\"foo\"").toString(), is("\"foo\""));
        assertThat(token("\"\\b\\f\\n\\r\\0\\t\\\"\\\'\\\\\\u2021\\\"\\177\\033\\0\"").decodeString(true),
                   is("\b\f\n\r\0\t\"\'\\\u2021\"\177\033\0"));
    }

    @Test
    public void testBadLiteral() throws IOException {
        assertBadLiteral("\"\\02\"", "Invalid escaped char: '\\02'");
        assertBadLiteral("\"\\03b\"", "Invalid escaped char: '\\03b'");
        assertBadLiteral("\"\\g\"", "Invalid escaped char: '\\g'");
        assertBadLiteral("\"\u0003\"", "Unescaped non-printable char in string: '\\003'");
        assertBadLiteral("\"\\u00gl\"", "Invalid escaped unicode char: '\\u00gl'");
        assertBadLiteral("\"\\u32\"", "Invalid escaped unicode char: '\\u32'");

        assertThat(token("\"\\02\"").decodeString(false), is("?"));
        assertThat(token("\"\\03b\"").decodeString(false), is("?"));
        assertThat(token("\"\\g\"").decodeString(false), is("?"));
        assertThat(token("\"\\u00gl\"").decodeString(false), is("?"));
        assertThat(token("\"\\u32\"").decodeString(false), is("?"));
    }

    private void assertBadLiteral(String str, String message) {
        try {
            token(str).decodeString(true);
            fail();
        } catch (IllegalArgumentException | IOException e) {
            assertThat(e.getMessage(), is(message));
        }
    }

    private PrettyToken token(String str) throws IOException {
        PrettyTokenizer tokenizer = new PrettyTokenizer(new ByteArrayInputStream(str.getBytes(StandardCharsets.UTF_8)));
        return tokenizer.parseNextToken();
    }
}
