/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.serializer;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import net.morimekta.providence.serializer.pretty.PrettyException;
import net.morimekta.providence.util_internal.MessageGenerator;
import net.morimekta.test.providence.core.Containers;
import net.morimekta.test.providence.core.calculator.Calculator;
import net.morimekta.test.providence.core.calculator.Operation;
import net.morimekta.test.providence.core.calculator.Operator;
import net.morimekta.test.providence.core.number.Imaginary;
import net.morimekta.testing.ResourceUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static java.nio.charset.StandardCharsets.UTF_8;
import static net.morimekta.providence.serializer.PrettySerializer.parseDebugString;
import static net.morimekta.providence.serializer.PrettySerializer.toDebugString;
import static net.morimekta.test.providence.core.calculator.Operand.withImaginary;
import static net.morimekta.test.providence.core.calculator.Operand.withNumber;
import static net.morimekta.test.providence.core.calculator.Operand.withOperation;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@RunWith(DataProviderRunner.class)
public class PrettySerializerTest {
    private Operation mOperation;
    private String    mFormatted;

    @Rule
    public MessageGenerator generator = new MessageGenerator();

    @Before
    public void setUp() {
        mOperation = Operation.builder()
                              .setOperator(Operator.MULTIPLY)
                              .addToOperands(withOperation(Operation.builder()
                                                                    .setOperator(Operator.ADD)
                                                                    .addToOperands(withNumber(1234))
                                                                    .addToOperands(withNumber(4.321))
                                                                    .build()))
                              .addToOperands(withImaginary(new Imaginary(1.7, -2.0)))
                              .build();

        mFormatted = "{\n" +
                     "  operator = MULTIPLY\n" +
                     "  operands = [\n" +
                     "    {\n" +
                     "      operation = {\n" +
                     "        operator = ADD\n" +
                     "        operands = [\n" +
                     "          {\n" +
                     "            number = 1234\n" +
                     "          },\n" +
                     "          {\n" +
                     "            number = 4.321\n" +
                     "          }\n" +
                     "        ]\n" +
                     "      }\n" +
                     "    },\n" +
                     "    {\n" +
                     "      imaginary = {\n" +
                     "        v = 1.7\n" +
                     "        i = -2\n" +
                     "      }\n" +
                     "    }\n" +
                     "  ]\n" +
                     "}";
    }


    @Test
    public void testDebugString() {
        String debug = toDebugString(mOperation);
        Operation parsed = parseDebugString(debug, Operation.kDescriptor);
        assertThat(parsed, is(mOperation));
    }

    @Test
    public void testFormat() {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrettySerializer serializer = new PrettySerializer();
        serializer.serialize(baos, mOperation);
        assertEquals(mFormatted, new String(baos.toByteArray(), UTF_8));
    }

    @Test
    public void testParse() throws IOException {
        try {
            PrettySerializer serializer = new PrettySerializer();

            Operation actual = serializer.deserialize(getClass().getResourceAsStream("/json/calculator/pretty.cfg"),
                                                      Operation.kDescriptor);

            assertEquals(mOperation, actual);
        } catch (PrettyException e) {
            System.err.println(e.displayString());
            throw e;
        }
    }

    @Test
    public void testConfig() throws IOException {
        PrettySerializer serializer = new PrettySerializer().config();
        Containers containers = generator.generate(Containers.kDescriptor);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        serializer.serialize(out, containers);

        ByteArrayInputStream in = new ByteArrayInputStream(out.toByteArray());
        Containers res = serializer.deserialize(in, Containers.kDescriptor);

        assertThat(res, is(containers));
    }

    @Test
    public void testConfig_2() throws IOException {
        ByteArrayInputStream in = new ByteArrayInputStream(ResourceUtils.getResourceAsBytes("/compat/config.cfg"));
        PrettySerializer serializer = new PrettySerializer().config();

        Containers c = serializer.deserialize(in, Containers.kDescriptor);
        assertThat(c, is(notNullValue()));
    }

    @DataProvider
    public static Object[][] dataConfigFailure() {
        return new Object[][]{
                {"foo",
                 "Error on line 1, row 1-3: Expected message start or qualifier, but got 'foo'\n" +
                 "foo\n" +
                 "^^^"},
                {"calculator.Operation foo",
                 "Error on line 1, row 22-24: Expected message start after qualifier, but got 'foo'\n" +
                 "calculator.Operation foo\n" +
                 "---------------------^^^"},
                {"calculator.Operand {",
                 "Error on line 1, row 1-18: Expected qualifier calculator.Operation or message start, but got 'calculator.Operand'\n" +
                 "calculator.Operand {\n" +
                 "^^^^^^^^^^^^^^^^^^"},
                {"{\n" +
                 "  1 = 123\n" +
                 "}\n",
                 "Error on line 2, row 3: Expected field name, but got '1'\n" +
                 "  1 = 123\n" +
                 "--^"},
        };
    }

    @Test
    @UseDataProvider
    public void testConfigFailure(String content,
                                  String output) throws IOException {
        ByteArrayInputStream in = new ByteArrayInputStream(
                content.getBytes(StandardCharsets.UTF_8));
        PrettySerializer serializer = new PrettySerializer().config();

        try {
            serializer.deserialize(in, Operation.kDescriptor);
            fail("no exception");
        } catch (SerializerException e) {
            try {
                assertThat(e.displayString(), is(output));
            } catch (AssertionError a) {
                e.printStackTrace();
                throw a;
            }
        }
    }

    @DataProvider
    public static Object[][] dataServiceFailure() {
        return new Object[][]{
                {"foo",
                 "Error on line 1, row 1-3: No such call type foo\n" +
                 "foo\n" +
                 "^^^"},
                {"1: call bar",
                 "Error on line 1, row 9-11: no such method bar on service calculator.Calculator\n" +
                 "1: call bar\n" +
                 "--------^^^"},
                {"1: call calculate",
                 "Error on line 1, row 18: Expected call params start, but got end of file\n" +
                 "1: call calculate\n" +
                 "-----------------^"},
        };
    }

    @Test
    @UseDataProvider
    public void testServiceFailure(String content,
                                   String output) throws IOException {
        ByteArrayInputStream in = new ByteArrayInputStream(
                content.getBytes(StandardCharsets.UTF_8));
        PrettySerializer serializer = new PrettySerializer().config();

        try {
            serializer.deserialize(in, Calculator.kDescriptor);
            fail("no exception");
        } catch (SerializerException e) {
            try {
                assertThat(e.displayString(), is(output));
            } catch (AssertionError a) {
                a.initCause(e);
                e.printStackTrace();
                throw a;
            }
        }
    }
}
