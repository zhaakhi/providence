package net.morimekta.providence.serializer.pretty;

import net.morimekta.providence.PApplicationExceptionType;
import net.morimekta.providence.PServiceCallType;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;

public class PrettyExceptionTest {
    @Test
    public void testException() {
        PrettyException e = new PrettyException("message");
        assertThat(e.getMessage(), is("message"));
        assertThat(e.toString(), is("PrettyException\n" +
                                    "Error: message"));
        e.setFile("file");
        e.setLineNo(1);
        e.setLinePos(4);
        e.setLength(5);

        assertThat(e.toString(), is("PrettyException\n" +
                                    "Error in file on line 1, row 4: message"));
        assertThat(e.displayString(), is(
                "Error in file on line 1, row 4: message"));

        e.setLine("   balls");

        assertThat(e.toString(), is("PrettyException\n" +
                                    "Error in file on line 1, row 4-8: message\n" +
                                    "   balls\n" +
                                    "---^^^^^"));
        assertThat(e.displayString(), is(
                "Error in file on line 1, row 4-8: message\n" +
                "   balls\n" +
                "---^^^^^"));

        e.setMethodName("balle");
        e.setSequenceNo(44);
        e.setExceptionType(PApplicationExceptionType.INVALID_MESSAGE_TYPE);
        e.setCallType(PServiceCallType.CALL);

        assertThat(e.toString(),
                   is("PrettyException\n" +
                      "Error in file on line 1, row 4-8: message\n" +
                      "   balls\n" +
                      "---^^^^^"));
        assertThat(e.displayString(),
                   is("Error in file on line 1, row 4-8: message\n" +
                      "   balls\n" +
                      "---^^^^^"));
    }

    @Test
    public void testConstructor() {
        NumberFormatException nfe = new NumberFormatException("nfe");
        PrettyException       a   = new PrettyException(nfe, "message %s", "is").setFile("file");

        assertThat(a.getCause(), is(sameInstance(nfe)));
        assertThat(a.getMessage(), is("message is"));
        assertThat(a.getFile(), is("file"));
        assertThat(a.getCause(), is(nfe));
    }
}
