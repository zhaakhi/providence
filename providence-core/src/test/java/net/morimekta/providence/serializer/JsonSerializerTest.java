/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.serializer;

import net.morimekta.providence.PMessage;
import net.morimekta.providence.PServiceCall;
import net.morimekta.providence.descriptor.PMessageDescriptor;
import net.morimekta.providence.descriptor.PService;
import net.morimekta.test.providence.core.CompactFields;
import net.morimekta.test.providence.core.Containers;
import net.morimekta.test.providence.core.JsonNamedEnum;
import net.morimekta.test.providence.core.JsonNamedValues;
import net.morimekta.test.providence.core.MinimalFields;
import net.morimekta.test.providence.core.OptionalFields;
import net.morimekta.test.providence.core.RequiredFields;
import net.morimekta.test.providence.core.UnionOfFields;
import net.morimekta.test.providence.core.Value;
import net.morimekta.test.providence.core.calculator.Calculator;
import net.morimekta.util.Binary;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.fail;

/**
 * @author Stein Eldar Johnsen
 * @since 18.10.15
 */
public class JsonSerializerTest {
    private JsonSerializer compact = new JsonSerializer(true);
    private JsonSerializer named   = new JsonSerializer(true).named();
    private JsonSerializer pretty    = new JsonSerializer(true).pretty();
    private JsonSerializer jsonNamed = new JsonSerializer(true)
            .pretty()
            .withPojoNamedEnums()
            .withPojoNamedFields();
    private JsonSerializer lenient   = new JsonSerializer(false);

    @Test
    public void testJsonString() {
        RequiredFields struct =
                RequiredFields.builder()
                              .setBooleanValue(true)
                              .setByteValue((byte) 123)
                              .setShortValue((short) 12345)
                              .setIntegerValue(1234567890)
                              .setLongValue(1234567890123456789L)
                              .setBinaryValue(Binary.fromHexString("AABBCCDD"))
                              .setStringValue("\"nee'ds\033\u2021esc")
                              .setDoubleValue(12345.12345)
                              .setEnumValue(Value.EIGHTEENTH)
                              .setCompactValue(CompactFields.builder()
                                                            .setName("my_category")
                                                            .setId(44)
                                                            .build())
                              .build();

        String json = JsonSerializer.toJsonString(struct);
        RequiredFields parsed = JsonSerializer.parseJsonString(json, RequiredFields.kDescriptor);
        assertThat(parsed, is(struct));

        String pretty = JsonSerializer.toPrettyJsonString(struct);
        parsed = JsonSerializer.parseJsonString(pretty, RequiredFields.kDescriptor);
        assertThat(parsed, is(struct));
    }

    @Test
    public void testProperties() {
        assertThat(compact.binaryProtocol(), is(false));
        assertThat(compact.mediaType(), is(JsonSerializer.MEDIA_TYPE));
        assertThat(named.mediaType(), is(JsonSerializer.JSON_MEDIA_TYPE));
        assertThat(pretty.mediaType(), is(JsonSerializer.JSON_MEDIA_TYPE));
        assertThat(lenient.mediaType(), is(JsonSerializer.MEDIA_TYPE));
    }

    @Test
    public void testSerializer_CompactFields() throws IOException {
        assertSerializer(named,
                         CompactFields.builder()
                                      .setName("my_category")
                                      .setId(44)
                                      .build(),
                         "[\"my_category\",44]");
        assertSerializer(compact,
                         CompactFields.builder()
                                      .setName("my_category")
                                      .setId(44)
                                      .setLabel("My Category")
                                      .build(),
                         "[\"my_category\",44,\"My Category\"]");
    }

    @Test
    public void testSerializer_JsonNames() throws IOException {
        assertSerializer(jsonNamed,
                         JsonNamedValues.builder()
                                        .setStringValue("my_category")
                                        .setEnumValue(JsonNamedEnum.SDECOND_VALUE)
                                        .build(),
                         "{\n" +
                         "    \"string-value\": \"my_category\",\n" +
                         "    \"enumValue\": \"second.value\"\n" +
                         "}");
    }

    @Test
    public void testSerializer_FlattenUnionOf() throws IOException {
        assertSerializer(named,
                         UnionOfFields.withMin(MinimalFields.builder().setStringValue("foo").build()),
                         "{\"min\":{\"string_value\":\"foo\"}}");
        assertSerializer(pretty.withFlattenUnionOf(),
                         UnionOfFields.withMin(MinimalFields.builder().setStringValue("foo").build()),
                         "{\n" +
                         "    \"__typename\": \"MinimalFields\",\n" +
                         "    \"string_value\": \"foo\"\n" +
                         "}");
        assertSerializer(named.withFlattenUnionOf(),
                         UnionOfFields.withOpts(OptionalFields.builder()
                                                              .setIntegerValue(123)
                                                              .setStringValue("foo").build()),
                         "{\"__typename\":\"OptionalFields\",\"integer_value\":123,\"string_value\":\"foo\"}");
    }

    @Test
    public void testSerializer_RequiredFields() throws IOException {
        RequiredFields struct =
                RequiredFields.builder()
                              .setBooleanValue(true)
                              .setByteValue((byte) 123)
                              .setShortValue((short) 12345)
                              .setIntegerValue(1234567890)
                              .setLongValue(1234567890123456789L)
                              .setBinaryValue(Binary.fromHexString("AABBCCDD"))
                              .setStringValue("\"nee'ds\033\u2021esc")
                              .setDoubleValue(12345.12345)
                              .setEnumValue(Value.EIGHTEENTH)
                              .setCompactValue(CompactFields.builder()
                                                            .setName("my_category")
                                                            .setId(44)
                                                            .build())
                              .build();

        assertSerializer(named,
                         struct,
                         "{\"boolean_value\":true," +
                         "\"byte_value\":123," +
                         "\"short_value\":12345," +
                         "\"integer_value\":1234567890," +
                         "\"long_value\":1234567890123456789," +
                         "\"double_value\":12345.12345," +
                         "\"string_value\":\"\\\"nee'ds\\u001b\u2021esc\"," +
                         "\"binary_value\":\"qrvM3Q\"," +
                         "\"enum_value\":\"EIGHTEENTH\"," +
                         "\"compact_value\":[\"my_category\",44]}");
        assertSerializer(compact,
                         struct,
                         "{\"1\":true," +
                         "\"2\":123," +
                         "\"3\":12345," +
                         "\"4\":1234567890," +
                         "\"5\":1234567890123456789," +
                         "\"6\":12345.12345," +
                         "\"7\":\"\\\"nee'ds\\u001b\u2021esc\"," +
                         "\"8\":\"qrvM3Q\"," +
                         "\"9\":4181," +
                         "\"10\":[\"my_category\",44]}");
        assertSerializer(pretty,
                         struct,
                         "{\n" +
                         "    \"boolean_value\": true,\n" +
                         "    \"byte_value\": 123,\n" +
                         "    \"short_value\": 12345,\n" +
                         "    \"integer_value\": 1234567890,\n" +
                         "    \"long_value\": 1234567890123456789,\n" +
                         "    \"double_value\": 12345.12345,\n" +
                         "    \"string_value\": \"\\\"nee'ds\\u001b\u2021esc\",\n" +
                         "    \"binary_value\": \"qrvM3Q\",\n" +
                         "    \"enum_value\": \"EIGHTEENTH\",\n" +
                         "    \"compact_value\": [\n" +
                         "        \"my_category\",\n" +
                         "        44\n" +
                         "    ]\n" +
                         "}");
        assertSerializer(lenient,
                         RequiredFields.builder().build(),
                         "{\"1\":false,\"2\":0,\"3\":0,\"4\":0," +
                         "\"5\":0,\"6\":0,\"7\":\"\",\"8\":\"\"}");
    }

    private <M extends PMessage<M>>
    void assertSerializer(JsonSerializer serializer,
                          M obj,
                          String json) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        int len = serializer.serialize(out, obj);

        assertThat(new String(out.toByteArray(), UTF_8), is(json));
        assertThat(len, is(out.toByteArray().length));

        ByteArrayInputStream in = new ByteArrayInputStream(out.toByteArray());
        M res = serializer.deserialize(in, obj.descriptor());

        assertThat(res, is(equalTo(obj)));
        assertThat(in.read(), is(-1));
    }

    @Test
    public void testSerializer_services() throws IOException {
        assertSerializer(compact,
                         Calculator.kDescriptor,
                         "[\"iamalive\",4,44,{}]");
        assertSerializer(compact,
                         Calculator.kDescriptor,
                         "[\"ping\",2,44,{\"0\":true}]");
    }

    private <M extends PMessage<M>>
    void assertSerializer(JsonSerializer serializer,
                          PService service,
                          String json) throws IOException {
        ByteArrayInputStream in = new ByteArrayInputStream(json.getBytes(UTF_8));
        PServiceCall<M> call = serializer.deserialize(in, service);

        assertThat(in.read(), is(-1));

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        int len = serializer.serialize(out, call);

        assertThat(new String(out.toByteArray(), UTF_8), is(json));
        assertThat(len, is(out.toByteArray().length));

        in = new ByteArrayInputStream(out.toByteArray());
        PServiceCall<M> res = serializer.deserialize(in, service);

        assertThat(res, is(equalTo(call)));
        assertThat(in.read(), is(-1));
    }

    @Test
    public void testDeserialize_simpleFails() {
        PMessageDescriptor<?> opt = OptionalFields.kDescriptor;
        PMessageDescriptor<?> cnt = Containers.kDescriptor;
        PService calc = Calculator.kDescriptor;

        assertFail(opt, compact, "",
                   "Empty json body");
        assertFail(opt, compact, "[false, 123, 54321]",
                   "OptionalFields is not compatible for compact struct notation.");
        assertFail(cnt, compact, "{\"optional_fields\":[false,123,54321]}",
                   "OptionalFields is not compatible for compact struct notation.");
        assertFail(opt, compact, "null",
                   "Null value as body.");
        assertFail(opt, compact, "\"not a message\"",
                   "expected message start, found: '\"not a message\"'");

        assertFail(opt, compact, "{\"boolean_value\":\"str\"}",
                   "No boolean value for token: '\"str\"'");
        assertFail(opt, compact, "{\"boolean_value\":{}}",
                   "No boolean value for token: '{'");
        assertFail(opt, compact, "{\"byte_value\":\"str\"}",
                   "Not a valid byte value: '\"str\"'");
        assertFail(opt, compact, "{\"short_value\":\"str\"}",
                   "Not a valid short value: '\"str\"'");
        assertFail(opt, compact, "{\"integer_value\":\"str\"}",
                   "Not a valid int value: '\"str\"'");
        assertFail(opt, compact, "{\"long_value\":\"str\"}",
                   "Not a valid long value: '\"str\"'");
        assertFail(opt, compact, "{\"double_value\":\"str\"}",
                   "Not a valid double value: '\"str\"'");
        assertFail(opt, compact, "{\"string_value\":55}",
                   "Not a valid string value: '55'");
        assertFail(opt, compact, "{\"binary_value\":55}",
                   "Not a valid binary value: 55");
        assertFail(opt, compact, "{\"binary_value\":\"g./ar,bl'e\"}",
                   "Unable to parse Base64 data: \"g./ar,bl'e\"");

        assertFail(calc, lenient, "{}",
                   "Expected service call start ('['): but found '{'");
        assertFail(calc, lenient, "[123]",
                   "Expected method name (string literal): but found '123'");
        assertFail(calc, lenient, "[\"iamalive\", 77]",
                   "Service call type 77 is not valid");
        assertFail(calc, lenient, "[\"iamalive\", 1, -55]",
                   "Expected entry sep (','): but found ']'");
        assertFail(calc, lenient, "[\"iamalive\", 2, -55, {\"0\": 6}]",
                   "No response type for calculator.Calculator.iamalive()");
        assertFail(calc, lenient, "[\"iamalive\", \"boo\", -55, {\"0\": 6}]",
                   "Service call type \"boo\" is not valid");
        assertFail(calc, lenient, "[\"iamalive\", false, -55, {\"0\": 6}]",
                   "Invalid service call type token false");
        assertFail(calc, compact, "[\"calculate\", \"reply\", 55, {}]",
                   "No union field set in calculator.Calculator.calculate.response");
        assertFail(calc, compact, "[\"ping\", \"reply\", 55, {\"0\": 3}]",
                   "Not a void token value: '3'");

        assertFail(opt, compact, "{\"binary_value\",\"AAss\"}",
                   "Expected field KV sep (':'): but found ','");
        assertFail(opt, compact, "{\"enum_value\":false}",
                   "false is not a enum value type");
        assertFail(Containers.kDescriptor, compact, "{\"enum_map\":[]}",
                   "Invalid start of map '['");
        assertFail(Containers.kDescriptor, compact, "{\"enum_set\":{}}",
                   "Invalid start of set '{'");
        assertFail(Containers.kDescriptor, compact, "{\"enum_list\":{}}",
                   "Invalid start of list '{'");

        assertFail(Containers.kDescriptor, compact, "{\"boolean_map\":{\"fleece\":false}}",
                   "Invalid boolean value: \"fleece\"");
        assertFail(Containers.kDescriptor, compact, "{\"byte_map\":{\"5boo\":55}}",
                   "Unable to parse numeric value 5boo");
        assertFail(Containers.kDescriptor, compact, "{\"short_map\":{\"5boo\":55}}",
                   "Unable to parse numeric value 5boo");
        assertFail(Containers.kDescriptor, compact, "{\"integer_map\":{\"5boo\":55}}",
                   "Unable to parse numeric value 5boo");
        assertFail(Containers.kDescriptor, compact, "{\"long_map\":{\"5boo\":4}}",
                   "Unable to parse numeric value 5boo");
        assertFail(Containers.kDescriptor, compact, "{\"double_map\":{\"5.5boo\":4.4}}",
                   "Unable to parse double from key \"5.5boo\"");
        assertFail(Containers.kDescriptor, compact, "{\"double_map\":{\"5.5 boo\":4.4}}",
                   "Garbage after double: \"5.5 boo\"");
        assertFail(Containers.kDescriptor, compact, "{\"double_map\":{\"boo 2\":4.4}}",
                   "Unable to parse double from key \"boo 2\"");

        assertFail(Containers.kDescriptor, compact, "{\"binary_map\":{\"\\_(^.^)_/\":\"\"}}",
                   "Unable to parse Base64 data");
        assertFail(Containers.kDescriptor, compact, "{\"enum_map\":{\"1\":\"BOO\"}}",
                   "\"BOO\" is not a known enum value for providence.Value");
        assertFail(Containers.kDescriptor, compact, "{\"enum_map\":{\"BOO\":\"1\"}}",
                   "\"BOO\" is not a known enum value for providence.Value");
        assertFail(Containers.kDescriptor, compact, "{\"message_key_map\":{\"{\\\"1\\\":55}\":\"str\"}}",
                   "Error parsing message key: Not a valid string value: '55'");

    }

    private <M extends PMessage<M>>
    void assertFail(PMessageDescriptor<M> descriptor,
                    JsonSerializer serializer,
                    String json,
                    String exception) {
        ByteArrayInputStream bais = new ByteArrayInputStream(json.getBytes(UTF_8));
        try {
            serializer.deserialize(bais, descriptor);
            fail("no exception");
        } catch (IOException e) {
            if (!e.getMessage().equals(exception)) {
                e.printStackTrace();
            }
            assertThat(e.getMessage(), is(exception));
        }
    }

    private void assertFail(PService service,
                            JsonSerializer serializer,
                            String json,
                            String exception) {
        ByteArrayInputStream bais = new ByteArrayInputStream(json.getBytes(UTF_8));
        try {
            serializer.deserialize(bais, service);
            fail("no exception");
        } catch (IOException e) {
            if (!e.getMessage().equals(exception)) {
                e.printStackTrace();
            }
            assertThat(e.getMessage(), is(exception));
        }
    }
}
