package net.morimekta.providence.descriptor;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class PAnnotationTest {
    @Test
    public void testForTag() {
        assertThat(PAnnotation.forTag("container"), is(PAnnotation.CONTAINER));
        assertThat(PAnnotation.forTag("gurba"), is(PAnnotation.NONE));
    }
}
