package net.morimekta.providence.util;

import net.morimekta.test.providence.core.CompactFields;
import net.morimekta.test.providence.core.ExceptionFields;
import net.morimekta.test.providence.core.MessageFieldException;
import net.morimekta.test.providence.core.OptionalFields;
import net.morimekta.util.Binary;
import org.junit.Test;

import java.util.UUID;

import static net.morimekta.test.providence.core.OptionalFields._Field.BINARY_VALUE;
import static net.morimekta.test.providence.core.OptionalFields._Field.COMPACT_VALUE;
import static net.morimekta.test.providence.core.OptionalFields._Field.INTEGER_VALUE;
import static net.morimekta.test.providence.core.OptionalFields._Field.SHORT_VALUE;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.fail;

@SuppressWarnings("deprecation")
public class MessageValidatorTest {
    @Test
    public void testValidate() throws MessageFieldException {
        MessageValidator<OptionalFields, MessageFieldException> validator =
                MessageValidator.builder(OptionalFields.kDescriptor,
                                 text -> MessageFieldException.builder()
                                                        .setStringValue(text)
                                                        .build())
                                .expectNotNull()
                                .expectMissing(SHORT_VALUE)
                                .expectPresent(BINARY_VALUE, INTEGER_VALUE)
                                .expect("Whoha!", msg -> msg.getIntegerValue() < 1_000_000)
                                .expect(COMPACT_VALUE,
                                        CompactFields.kDescriptor,
                                        builder -> builder.expectNotNull()
                                                          .expect("too low ID value", c -> c.getId() > 10))
                                .build();

        validator.validate(OptionalFields.builder()
                                         .setBinaryValue(Binary.fromUUID(UUID.randomUUID()))
                                         .setCompactValue(CompactFields.builder()
                                                                       .setId(42)
                                                                       .build())
                                         .setIntegerValue(1234));
        try {
            validator.validate(null);
            fail("no exception");
        } catch (MessageFieldException e) {
            assertThat(e.getStringValue(), is("Null providence.OptionalFields value."));
        }

        try {
            validator.validate(OptionalFields.builder()
                                             .setShortValue((short) 123)
                                             .setIntegerValue(1234)
                                             .setBinaryValue(Binary.fromUUID(UUID.randomUUID()))
                                             .setCompactValue(CompactFields.builder()
                                                                           .setId(42)
                                                                           .build())
                                             .build());
            fail("no exception");
        } catch (MessageFieldException e) {
            assertThat(e.getStringValue(), is("short_value present on providence.OptionalFields"));
        }

        try {
            validator.validate(OptionalFields.builder()
                                             .setIntegerValue(1234)
                                             .setCompactValue(CompactFields.builder()
                                                                           .setId(42)
                                                                           .build())
                                             .build());
            fail("no exception");
        } catch (MessageFieldException e) {
            assertThat(e.getStringValue(), is("binary_value not present on providence.OptionalFields"));
        }

        try {
            validator.validate(OptionalFields.builder()
                                             .setIntegerValue(12345678)
                                             .setBinaryValue(Binary.fromUUID(UUID.randomUUID()))
                                             .setCompactValue(CompactFields.builder()
                                                                           .setId(42)
                                                                           .build())
                                             .build());
            fail("no exception");
        } catch (MessageFieldException e) {
            assertThat(e.getStringValue(), is("Whoha!"));
        }

        try {
            validator.validate(OptionalFields.builder()
                                             .setBinaryValue(Binary.fromUUID(UUID.randomUUID()))
                                             .setIntegerValue(1234));
            fail("no exception");
        } catch (MessageFieldException e) {
            assertThat(e.getStringValue(), is("compact_value not present on providence.OptionalFields"));
        }


        try {
            validator.validate(OptionalFields.builder()
                                             .setBinaryValue(Binary.fromUUID(UUID.randomUUID()))
                                             .setCompactValue(CompactFields.builder()
                                                                           .setId(5)
                                                                           .build())
                                             .setIntegerValue(1234));
            fail("no exception");
        } catch (MessageFieldException e) {
            assertThat(e.getStringValue(), is("too low ID value"));
        }
    }

    @Test
    public void testValidate_AllowNull() throws ExceptionFields {
        MessageValidator<OptionalFields, ExceptionFields> validator =
                MessageValidator.builder(OptionalFields.kDescriptor,
                                         text -> ExceptionFields.builder()
                                                                .setStringValue(text)
                                                                .build())
                                .expectMissing(SHORT_VALUE)
                                .expectPresent(BINARY_VALUE, INTEGER_VALUE)
                                .expect("Whoha!", msg -> msg.getIntegerValue() < 1_000_000)
                                .expect(COMPACT_VALUE,
                                        CompactFields.kDescriptor,
                                        builder -> builder.expectNotNull()
                                                          .expect("too low ID value", c -> c.getId() > 10))
                                .build();
        validator.validate(null);
    }
}
