package net.morimekta.providence.util;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import net.morimekta.providence.PMessage;
import net.morimekta.providence.PMessageBuilder;
import net.morimekta.providence.PMessageOrBuilder;
import net.morimekta.providence.descriptor.PDescriptor;
import net.morimekta.providence.descriptor.PField;
import net.morimekta.providence.descriptor.PList;
import net.morimekta.providence.descriptor.PMap;
import net.morimekta.providence.descriptor.PMessageDescriptor;
import net.morimekta.providence.descriptor.PPrimitive;
import net.morimekta.providence.descriptor.PSet;
import net.morimekta.test.providence.core.CompactFields;
import net.morimekta.test.providence.core.Containers;
import net.morimekta.test.providence.core.DefaultValues;
import net.morimekta.test.providence.core.OptionalFields;
import net.morimekta.test.providence.core.Value;
import net.morimekta.test.providence.core.calculator.Operation;
import net.morimekta.test.providence.core.calculator.Operator;
import net.morimekta.test.providence.core.number.Imaginary;
import net.morimekta.util.Binary;
import net.morimekta.util.collect.UnmodifiableList;
import org.hamcrest.Matcher;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static java.nio.charset.StandardCharsets.UTF_8;
import static net.morimekta.providence.util.MessageUtil.coerce;
import static net.morimekta.providence.util.MessageUtil.coerceStrict;
import static net.morimekta.providence.util.MessageUtil.getInMessage;
import static net.morimekta.providence.util.MessageUtil.getTargetModifications;
import static net.morimekta.providence.util.MessageUtil.keyPathToFields;
import static net.morimekta.providence.util.MessageUtil.messageToMap;
import static net.morimekta.providence.util.MessageUtil.optionalInMessage;
import static net.morimekta.providence.util.MessageUtil.toBuilderAll;
import static net.morimekta.providence.util.MessageUtil.toBuilderIfNonNull;
import static net.morimekta.providence.util.MessageUtil.toBuilderValues;
import static net.morimekta.providence.util.MessageUtil.toMapInternal;
import static net.morimekta.providence.util.MessageUtil.toMessageAll;
import static net.morimekta.providence.util.MessageUtil.toMessageIfNotNull;
import static net.morimekta.providence.util.MessageUtil.toMessageOrBuilderValues;
import static net.morimekta.providence.util.MessageUtil.toMessageOrBuilders;
import static net.morimekta.providence.util.MessageUtil.toMessageValues;
import static net.morimekta.test.providence.core.OptionalFields._Field.COMPACT_VALUE;
import static net.morimekta.test.providence.core.OptionalFields._Field.INTEGER_VALUE;
import static net.morimekta.test.providence.core.OptionalFields._Field.SHORT_VALUE;
import static net.morimekta.util.collect.UnmodifiableList.listOf;
import static net.morimekta.util.collect.UnmodifiableMap.mapOf;
import static net.morimekta.util.collect.UnmodifiableSet.setOf;
import static net.morimekta.util.collect.UnmodifiableSortedMap.sortedMapOf;
import static net.morimekta.util.collect.UnmodifiableSortedSet.sortedSetOf;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.fail;

@RunWith(DataProviderRunner.class)
public class MessageUtilTest {
    @Test
    public void testMutateAll() {
        // Extra cast needed to normalize unknown generic.
        assertThat((List) toBuilderAll(null), is(nullValue()));
    }

    @Test
    public void testBuildAll() {
        // Extra cast needed to normalize unknown generic.
        assertThat((List) toMessageAll(null), is(nullValue()));
    }

    @Test
    public void testMutateValues() {
        // Extra cast needed to normalize unknown generic.
        assertThat((Map) toBuilderValues(null), is(nullValue()));
    }

    @Test
    public void testBuildValues() {
        // Extra cast needed to normalize unknown generic.
        assertThat((Map) toMessageValues(null), is(nullValue()));
    }

    @Test
    public void testMutateIfNotNull() {
        // Extra cast needed to normalize unknown generic.
        assertThat((PMessage) toBuilderIfNonNull(null), is(nullValue()));
    }

    @Test
    public void testBuildIfNotNull() {
        // Extra cast needed to normalize unknown generic.
        assertThat((PMessage) toMessageIfNotNull(null), is(nullValue()));
    }

    @Test
    public void testToMessageOrBuilder() {
        List<OptionalFields>                          optionals  = new ArrayList<>();
        Collection<PMessageOrBuilder<OptionalFields>> builders = toMessageOrBuilders(optionals);
        assertThat(builders, is(sameInstance(optionals)));

        Map<String, OptionalFields> map = new HashMap<>();
        Map<String, PMessageOrBuilder<OptionalFields>> orBuilder = toMessageOrBuilderValues(map);
        assertThat(orBuilder, is(sameInstance(map)));
    }

    @DataProvider
    public static Object[][] GetTargetModifications() {
        return new Object[][] {
                {OptionalFields
                         .builder()
                         .build(),
                 OptionalFields
                         .builder()
                         .build(),
                 setOf()},
                {OptionalFields
                         .builder()
                         .setBinaryValue(Binary.fromBase64("abcd"))
                         .setStringValue("old")
                         .setIntegerValue(321)
                         .build(),
                 OptionalFields.builder()
                               .setStringValue("new")
                               .setLongValue(1234L)
                               .setIntegerValue(321)
                         .build(),
                 setOf(OptionalFields._Field.BINARY_VALUE,
                       OptionalFields._Field.STRING_VALUE,
                       OptionalFields._Field.LONG_VALUE)},
        };
    }

    @Test
    @UseDataProvider("GetTargetModifications")
    @SuppressWarnings("unchecked")
    public void testGetTargetModifications(PMessage a, PMessage b, Set<PField> fields) {
        PMessageBuilder builder = getTargetModifications(a, b);
        assertThat(builder.modifiedFields(), is(fields));
        assertThat(builder.build(), is(b));
    }

    @DataProvider
    public static Object[][] KeyPathToFields() {
        return new Object[][] {
                {OptionalFields.kDescriptor, "short_value", listOf(SHORT_VALUE)},
                {OptionalFields.kDescriptor, "compact_value", listOf(COMPACT_VALUE)},
                {OptionalFields.kDescriptor, "compact_value.id", listOf(COMPACT_VALUE, CompactFields._Field.ID)}
        };
    }

    @Test
    @UseDataProvider("KeyPathToFields")
    public void testKeyPathToFields(PMessageDescriptor descriptor, String key, List<PField> expected) {
        List<PField> fields = UnmodifiableList.copyOf(MessageUtil.keyPathToFields(descriptor, key));
        assertThat(fields, is(expected));
    }

    @DataProvider
    public static Object[][] KeyPathToFields_Failures() {
        return new Object[][] {
                {OptionalFields.kDescriptor, "", "Empty field name in ''"},
                {OptionalFields.kDescriptor, "foo", "Message providence.OptionalFields has no field named foo"},
                {OptionalFields.kDescriptor, "foo.bar", "Message providence.OptionalFields has no field named foo"},
                {OptionalFields.kDescriptor, "short_value.foo", "Field 'short_value' is not of message type in providence.OptionalFields"},
                {OptionalFields.kDescriptor, "compact_value.", "Empty field name in 'compact_value.'"},
                {OptionalFields.kDescriptor, ".compact_value", "Empty field name in '.compact_value'"},
                {OptionalFields.kDescriptor, "compact_value..foo", "Empty field name in 'compact_value..foo'"},
                {OptionalFields.kDescriptor, "compact_value.foo", "Message providence.CompactFields has no field named foo"},
        };
    }

    @Test
    @UseDataProvider("KeyPathToFields_Failures")
    public void testKeyPathToFields(PMessageDescriptor descriptor, String key, String message) {
        try {
            keyPathToFields(descriptor, key);
            fail("no exception: " + message);
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is(message));
        }
    }

    @DataProvider
    public static Object[][] GetInMessage() {
        return new Object[][] {
                {OptionalFields.builder().build(),
                 listOf(COMPACT_VALUE),
                 is(nullValue())},
                {OptionalFields.builder().build(),
                 listOf(COMPACT_VALUE, CompactFields._Field.NAME),
                 is(nullValue())},
                {null,
                 listOf(COMPACT_VALUE, CompactFields._Field.NAME),
                 is(nullValue())},

                {OptionalFields
                         .builder()
                         .setCompactValue(CompactFields.builder().setName("foo").build())
                         .build(),
                 listOf(COMPACT_VALUE),
                 is(CompactFields.builder().setName("foo").build())},
                {OptionalFields
                         .builder()
                         .setCompactValue(CompactFields.builder().setName("foo").build())
                         .build(),
                 listOf(COMPACT_VALUE, CompactFields._Field.NAME),
                 is("foo")},

                {DefaultValues.builder().build(),
                 listOf(DefaultValues._Field.COMPACT_VALUE, CompactFields._Field.NAME),
                 is("foo")},
                {DefaultValues.builder().build(),
                 listOf(DefaultValues._Field.INTEGER_VALUE),
                 is(1234567890)},
        };
    }

    @Test
    @UseDataProvider("GetInMessage")
    public void testGetInMessage(PMessage message, List<PField> fields, Matcher<Object> expected) {
        assertThat(getInMessage(message, fields.toArray(new PField[0])).orElse(null), expected);
    }

    @DataProvider
    public static Object[][] GetInMessage_Failure() {
        return new Object[][] {
                {OptionalFields.builder().build(),
                 listOf(),
                 "No fields arguments"},
                {OptionalFields.builder().build(),
                 listOf(INTEGER_VALUE, CompactFields._Field.NAME),
                 "Intermediate field integer_value is not a message"},
        };
    }

    @Test
    @UseDataProvider("GetInMessage_Failure")
    public void testGetInMessage_Failure(PMessage message, List<PField> fields, String expected) {
        try {
            getInMessage(message, fields.toArray(new PField[0]));
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is(expected));
        }
    }

    @DataProvider
    public static Object[][] OptionalInMessage() {
        return new Object[][] {
                {OptionalFields.builder().build(),
                 listOf(COMPACT_VALUE),
                 is(nullValue())},
                {OptionalFields.builder().build(),
                 listOf(COMPACT_VALUE, CompactFields._Field.NAME),
                 is(nullValue())},
                {null,
                 listOf(COMPACT_VALUE, CompactFields._Field.NAME),
                 is(nullValue())},

                {OptionalFields
                         .builder()
                         .setCompactValue(CompactFields.builder().setName("foo").build())
                         .build(),
                 listOf(COMPACT_VALUE),
                 is(CompactFields.builder().setName("foo").build())},
                {OptionalFields
                         .builder()
                         .setCompactValue(CompactFields.builder().setName("foo").build())
                         .build(),
                 listOf(COMPACT_VALUE, CompactFields._Field.NAME),
                 is("foo")},

                {DefaultValues.builder().build(),
                 listOf(DefaultValues._Field.COMPACT_VALUE, CompactFields._Field.NAME),
                 is(nullValue())},
                {DefaultValues.builder().build(),
                 listOf(DefaultValues._Field.INTEGER_VALUE),
                 is(nullValue())},
        };
    }

    @Test
    @UseDataProvider("OptionalInMessage")
    public void testOptionalInMessage(PMessage message, List<PField> fields, Matcher<Object> expected) {
        assertThat(optionalInMessage(message, fields.toArray(new PField[0])).orElse(null), expected);
    }

    @DataProvider
    public static Object[][] OptionalInMessage_Failure() {
        return new Object[][] {
                {OptionalFields.builder().build(),
                 listOf(),
                 "No fields arguments"},
                {OptionalFields.builder().setIntegerValue(123).build(),
                 listOf(INTEGER_VALUE, CompactFields._Field.NAME),
                 "Intermediate field integer_value is not a message"},
                };
    }

    @Test
    @UseDataProvider("OptionalInMessage_Failure")
    public void testOptionalInMessage_Failure(PMessage message, List<PField> fields, String expected) {
        try {
            optionalInMessage(message, fields.toArray(new PField[0]));
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is(expected));
        }
    }

    @DataProvider
    public static Object[][] dataToMap() {
        return new Object[][]{
                {OptionalFields.builder().build(), mapOf()},
                {OptionalFields
                         .builder()
                         .setIntegerValue(1234)
                         .setCompactValue(new CompactFields("foo", 2, "bar"))
                         .build(),
                 mapOf("integer_value", 1234,
                       "compact_value", mapOf("name", "foo", "id", 2, "label", "bar"))},
                {Containers
                         .builder()
                         .setLongList(listOf(2L, 3L))
                         .setIntegerMap(mapOf(1, 2, 3, 4))
                         .setDoubleSet(setOf(3.3, 4.4))
                         .build(),
                 mapOf("long_list", listOf(2L, 3L),
                       "integer_map", mapOf(1, 2, 3, 4),
                       "double_set", setOf(3.3, 4.4))},
                {Containers
                         .builder()
                         .setMessageSet(setOf(OptionalFields.builder().build()))
                         .setMessageMap(mapOf("foo", OptionalFields.builder().build()))
                         .build(),
                 sortedMapOf("message_set", setOf(sortedMapOf()),
                             "message_map", mapOf("foo", sortedMapOf()))},
                };
    }

    @Test
    @UseDataProvider
    @SuppressWarnings("unchecked")
    public void testToMap(PMessage message, Map expected) {
        Map<String, Object> out = messageToMap(message);
        assertThat(out, equalTo(toMapInternal(expected)));
        assertThat(coerce(message.descriptor(), out).orElse(null), is(message));
    }

    @DataProvider
    public static Object[][] coerceStrictData() {
        return new Object[][]{
                // Void
                {PPrimitive.VOID, null, null},
                {PPrimitive.VOID, true, true},
                // Boolean
                {PPrimitive.BOOL, null, null},
                {PPrimitive.BOOL, true, true},
                {PPrimitive.BOOL, false, false},
                {PPrimitive.BOOL, 1, true},
                {PPrimitive.BOOL, 0, false},
                {PPrimitive.BOOL, Value.FIRST, true},
                {PPrimitive.BOOL, Value.SECOND, true},
                // Byte
                {PPrimitive.BYTE, null, null},
                {PPrimitive.BYTE, 42, (byte) 42},
                {PPrimitive.BYTE, 42.0, (byte) 42},
                {PPrimitive.BYTE, true, (byte) 1},
                {PPrimitive.BYTE, false, (byte) 0},
                {PPrimitive.BYTE, Value.FOURTH, (byte) 5},
                // Short
                {PPrimitive.I16, null, null},
                {PPrimitive.I16, 42, (short) 42},
                {PPrimitive.I16, 42.0, (short) 42},
                {PPrimitive.I16, true, (short) 1},
                {PPrimitive.I16, false, (short) 0},
                {PPrimitive.I16, Value.FOURTEENTH, (short) 610},
                // Integer
                {PPrimitive.I32, null, null},
                {PPrimitive.I32, 42L, 42},
                {PPrimitive.I32, 42.0, 42},
                {PPrimitive.I32, true, 1},
                {PPrimitive.I32, false, 0},
                {PPrimitive.I32, Value.TWENTIETH, 10946},
                // Long
                {PPrimitive.I64, null, null},
                {PPrimitive.I64, 42, 42L},
                {PPrimitive.I64, 42.0, 42L},
                {PPrimitive.I64, true, 1L},
                {PPrimitive.I64, false, 0L},
                {PPrimitive.I64, Value.SECOND, 2L},
                // Double
                {PPrimitive.DOUBLE, null, null},
                {PPrimitive.DOUBLE, 42, 42.0},
                {PPrimitive.DOUBLE, 42.0, 42.0},
                {PPrimitive.DOUBLE, 42.0f, 42.0},
                // String
                {PPrimitive.STRING, null, null},
                {PPrimitive.STRING, "foo", "foo"},
                // Binary
                {PPrimitive.BINARY, null, null},
                {PPrimitive.BINARY, Binary.wrap("foo".getBytes(UTF_8)), Binary.wrap("foo".getBytes(UTF_8))},
                // Enum
                {Value.kDescriptor, Value.SECOND, Value.SECOND},
                {Value.kDescriptor, 5, Value.FOURTH},
                {Value.kDescriptor, "FOURTH", Value.FOURTH},
                // Message
                {Imaginary.kDescriptor,
                 Imaginary.builder().setV(7).build(),
                 Imaginary.builder().setV(7).build()},
                // List
                {PList.provider(PPrimitive.I32.provider()).descriptor(),
                 listOf(true, 2L, 3),
                 listOf(1, 2, 3)},
                {PList.provider(PPrimitive.I32.provider()).descriptor(),
                 sortedSetOf(3, 2, 1),
                 listOf(1, 2, 3)},
                // Map
                {PMap.provider(PPrimitive.STRING.provider(), PPrimitive.STRING.provider()).descriptor(),
                 sortedMapOf("foo", "bar"),
                 mapOf("foo", "bar")}
        };
    }

    @Test
    @UseDataProvider("coerceStrictData")
    public void testCoerceStrict(PDescriptor descriptor, Object in, Object out) {
        assertThat(coerceStrict(descriptor, in).orElse(null), is(out));
    }

    @DataProvider
    public static Object[][] coerceNotStrictData() {
        return new Object[][]{
                // Boolean
                {PPrimitive.BOOL, "1", Boolean.TRUE,
                 "Invalid value type class java.lang.String for type bool"},
                {PPrimitive.BOOL, "0", Boolean.FALSE,
                 "Invalid value type class java.lang.String for type bool"},
                // Byte
                {PPrimitive.BYTE, "42", (byte) 42,
                 "Invalid value type class java.lang.String for type byte"},
                {PPrimitive.BYTE, "0x42", (byte) 66,
                 "Invalid value type class java.lang.String for type byte"},
                // Short
                {PPrimitive.I16, "42", (short) 42,
                 "Invalid value type class java.lang.String for type i16"},
                {PPrimitive.I16, "0x42", (short) 66,
                 "Invalid value type class java.lang.String for type i16"},
                // Integer
                {PPrimitive.I32, "42", 42,
                 "Invalid value type class java.lang.String for type i32"},
                {PPrimitive.I32, "0x42", 66,
                 "Invalid value type class java.lang.String for type i32"},
                // Long
                {PPrimitive.I64, "42", 42L,
                 "Invalid value type class java.lang.String for type i64"},
                {PPrimitive.I64, "0x42", 66L,
                 "Invalid value type class java.lang.String for type i64"},
                // Double
                {PPrimitive.DOUBLE, Value.FOURTEENTH, 610.0,
                 "Invalid value type class net.morimekta.test.providence.core.Value for type double"},
                // String
                {PPrimitive.STRING, Value.FOURTEENTH, "FOURTEENTH",
                 "Invalid value type class net.morimekta.test.providence.core.Value for type string"},
                {PPrimitive.STRING, 123, "123",
                 "Invalid value type class java.lang.Integer for type string"},
                // Binary
                {PPrimitive.BINARY, "AAb=", Binary.fromBase64("AAb="),
                 "Invalid value type class java.lang.String for type binary"},
                // Enum
                {Operator.kDescriptor, "4", Operator.MULTIPLY,
                 "Unknown calculator.Operator value for string '4'"},
                {Operator.kDescriptor, "0x4", Operator.MULTIPLY,
                 "Unknown calculator.Operator value for string '0x4'"},
                // Message
                {Imaginary.kDescriptor,
                 mapOf("i", 23, "v", 5.6),
                 Imaginary.builder().setI(23).setV(5.6).build(),
                 "Invalid value type class net.morimekta.util.collect.UnmodifiableMap for message number.Imaginary"},
                // List
                // List
                {PList.provider(PPrimitive.I32.provider()).descriptor(),
                 new ArrayList<Integer>(){{ add(2); add(null); add(5);}},
                 listOf(2, 5),
                 "Null value in list"},
                // Set
                {PSet.sortedProvider(PPrimitive.I32.provider()).descriptor(),
                 new ArrayList<Integer>(){{ add(2); add(null); add(5);}},
                 sortedSetOf(2, 5),
                 "Null value in set"},
                // Map
                {PMap.provider(PPrimitive.STRING.provider(), PPrimitive.I32.provider()).descriptor(),
                 new HashMap<String, Integer>() {{ put("foo", null); put("bar", 42);}},
                 mapOf("bar", 42),
                 "Null key or value in map"}
        };
    }

    @Test
    @UseDataProvider("coerceNotStrictData")
    public void testCoerceNotStrict(PDescriptor descriptor,
                                    Object in,
                                    Object out,
                                    String messageOnStrict) {
        assertThat(coerce(descriptor, in).orElse(null), is(out));
        try {
            coerceStrict(descriptor, in);
            fail("no exception: " + messageOnStrict);
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is(messageOnStrict));
        }
    }

    @DataProvider
    public static Object[][] coerceFailData() {
        return new Object[][]{
                // Void
                {PPrimitive.VOID, false, "Invalid void value false"},
                {PPrimitive.VOID, "", "Invalid value type class java.lang.String for type void"},
                // Boolean
                {PPrimitive.BOOL, "3",
                 "Unknown boolean value for string '3'",
                 "Invalid value type class java.lang.String for type bool"},
                {PPrimitive.BOOL, 7.4,
                 "Invalid value type class java.lang.Double for type bool"},
                // Byte
                {PPrimitive.BYTE, 123.45,
                 "Truncating byte decimals from 123.45"},
                {PPrimitive.BYTE, 1234,
                 "byte value outside of bounds: 1234 > 127"},
                {PPrimitive.BYTE, -1234,
                 "byte value outside of bounds: -1234 < -128"},
                {PPrimitive.BYTE, Binary.empty(),
                 "Invalid value type class net.morimekta.util.Binary for type byte"},
                {PPrimitive.BYTE, "0xfoo",
                 "Invalid string value '0xfoo' for type byte",
                 "Invalid value type class java.lang.String for type byte"},
                // Short
                {PPrimitive.I16, 123.45,
                 "Truncating i16 decimals from 123.45"},
                {PPrimitive.I16, 123456,
                 "i16 value outside of bounds: 123456 > 32767"},
                {PPrimitive.I16, -123456,
                 "i16 value outside of bounds: -123456 < -32768"},
                {PPrimitive.I16, Binary.empty(),
                 "Invalid value type class net.morimekta.util.Binary for type i16"},
                {PPrimitive.I16, "0xfoo",
                 "Invalid string value '0xfoo' for type i16",
                 "Invalid value type class java.lang.String for type i16"},
                // Short
                {PPrimitive.I32, 123.45,
                 "Truncating i32 decimals from 123.45"},
                {PPrimitive.I32, 12345678901234L,
                 "i32 value outside of bounds: 12345678901234 > 2147483647"},
                {PPrimitive.I32, -12345678901234L,
                 "i32 value outside of bounds: -12345678901234 < -2147483648"},
                {PPrimitive.I32, Binary.empty(),
                 "Invalid value type class net.morimekta.util.Binary for type i32"},
                {PPrimitive.I32, "0xfoo",
                 "Invalid string value '0xfoo' for type i32",
                 "Invalid value type class java.lang.String for type i32"},
                // Short
                {PPrimitive.I64, 123.45,
                 "Truncating long decimals from 123.45"},
                {PPrimitive.I64, Binary.empty(),
                 "Invalid value type class net.morimekta.util.Binary for type i64"},
                {PPrimitive.I64, "0xfoo",
                 "Invalid string value '0xfoo' for type i64",
                 "Invalid value type class java.lang.String for type i64"},
                // Double
                {PPrimitive.DOUBLE, "0xfoo",
                 "Invalid value type class java.lang.String for type double"},
                // Binary
                {PPrimitive.BINARY, 1234,
                 "Invalid value type class java.lang.Integer for type binary"},
                // Enum
                {Value.kDescriptor, 123.45,
                 "Invalid value type class java.lang.Double for enum providence.Value"},
                {Value.kDescriptor, 4,
                 "Unknown providence.Value value for id 4"},
                {Value.kDescriptor, Operator.ADD,
                 "Invalid value type class net.morimekta.test.providence.core.calculator.Operator for enum providence.Value"},
                {Operator.kDescriptor, "7",
                 "Unknown calculator.Operator value for string '7'"},
                {Operator.kDescriptor, "0x7",
                 "Unknown calculator.Operator value for string '0x7'"},
                // Message
                {Operation.kDescriptor, Imaginary.builder().build(),
                 "Unable to cast message type number.Imaginary to calculator.Operation"},
                {Imaginary.kDescriptor,
                 mapOf(2, 23),
                 "Invalid message map key: 2",
                 "Invalid value type class net.morimekta.util.collect.UnmodifiableMap for message number.Imaginary"},
                {Imaginary.kDescriptor,
                 mapOf("boo", 23),
                 "No such field boo in number.Imaginary",
                 "Invalid value type class net.morimekta.util.collect.UnmodifiableMap for message number.Imaginary"},
                // List
                {PList.provider(PPrimitive.I32.provider()).descriptor(),
                 123,
                 "Invalid value type class java.lang.Integer for list<i32>"},
                // Set
                {PSet.provider(PPrimitive.BYTE.provider()).descriptor(),
                 123,
                 "Invalid value type class java.lang.Integer for set<byte>"},
                // Map
                {PMap.provider(PPrimitive.STRING.provider(), PPrimitive.STRING.provider()).descriptor(),
                 123,
                 "Invalid value type class java.lang.Integer for map<string,string>"}
        };
    }

    @Test
    @UseDataProvider("coerceFailData")
    public void testCoerceFail(PDescriptor descriptor,
                               Object in,
                               String message,
                               String messageOnStrict) {
        try {
            coerce(descriptor, in);
            fail("no exception: " + message);
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is(message));
        }
        try {
            coerceStrict(descriptor, in);
            fail("no exception: " + message);
        } catch (IllegalArgumentException e) {
            if (messageOnStrict != null) {
                assertThat(e.getMessage(), is(messageOnStrict));
            } else {
                assertThat(e.getMessage(), is(message));
            }
        }
    }


    @Test
    public void testConstructor()
            throws NoSuchMethodException, InvocationTargetException, InstantiationException {
        Constructor<MessageUtil> constructor = MessageUtil.class.getDeclaredConstructor();
        try {
            constructor.newInstance();
            fail("access granted");
        } catch (IllegalAccessException e) {
            assertThat(e.getMessage(), allOf(
                    containsString(" net.morimekta.providence.util.MessageUtilTest "),
                    containsString(" net.morimekta.providence.util.MessageUtil ")));
        }
    }
}
