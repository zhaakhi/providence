package net.morimekta.providence.util;

import net.morimekta.test.providence.core.CompactFields;
import net.morimekta.test.providence.core.MessageFieldException;
import net.morimekta.test.providence.core.OptionalFields;
import net.morimekta.test.providence.core.OptionalFields_OrBuilder;
import net.morimekta.util.Binary;
import org.junit.Test;

import java.util.List;
import java.util.UUID;

import static junit.framework.TestCase.fail;
import static net.morimekta.providence.util.MessageValidation.pathPrefix;
import static net.morimekta.test.providence.core.OptionalFields._Field.BINARY_VALUE;
import static net.morimekta.test.providence.core.OptionalFields._Field.COMPACT_VALUE;
import static net.morimekta.test.providence.core.OptionalFields._Field.INTEGER_VALUE;
import static net.morimekta.test.providence.core.OptionalFields._Field.SHORT_VALUE;
import static net.morimekta.test.providence.core.OptionalFields._Field.STRING_VALUE;
import static net.morimekta.util.collect.UnmodifiableList.listOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class MessageValidationTest {
    public void assertValidation(MessageValidation<OptionalFields, MessageFieldException> validator,
                                 OptionalFields_OrBuilder message,
                                 List<MessageFieldException> expectedErrors) throws MessageFieldException {
        if (expectedErrors.isEmpty()) {
            assertThat(validator.validate(message), is(message));
            assertThat(validator.isValid(message), is(true));
            return;
        }

        try {
            validator.validate(message);
            fail("No exception, expected: " + expectedErrors.get(0));
        } catch (MessageFieldException e) {
            assertThat(e, is(expectedErrors.get(0)));
        }

        assertThat(validator.isValid(message), is(false));
        assertThat(validator.validationErrors(message), is(expectedErrors));
    }

    private MessageFieldException mfe(String message) {
        return MessageFieldException.builder()
                                    .setStringValue(message)
                                    .build();
    }

    @Test
    public void testValidate() throws MessageFieldException {
        MessageValidation<OptionalFields, MessageFieldException> validator =
                MessageValidation.builder(OptionalFields.kDescriptor,
                                          (ex) -> {
                                              if (ex instanceof MessageFieldException) {
                                                  return (MessageFieldException) ex;
                                              }
                                              return MessageFieldException.builder()
                                                                    .setStringValue(ex.getMessage())
                                                                    .build()
                                                                    .initCause(ex);
                                          })
                                 .expectNotNull()
                                 .expectMissing(SHORT_VALUE)
                                 .expectPresent(BINARY_VALUE, INTEGER_VALUE)
                                 .build()
                                 .toBuilder()
                                 .expect((path, msg) -> {
                                     if (msg.getIntegerValue() >= 1_000_000) throw new IllegalStateException("Whoha! @" + path);
                                 })
                                 .expectIfPresent(STRING_VALUE, (String value) -> {
                                     if (value.length() == 0) throw new IllegalArgumentException("String at (unknown) is empty");
                                 })
                                 .expectIfPresent(BINARY_VALUE, (String path, Binary b) -> {
                                     if (b.length() == 0) throw new IllegalArgumentException("Binary at " + path + " is empty");
                                 })
                                 .expectIfPresent(COMPACT_VALUE,
                                                  CompactFields.kDescriptor,
                                                  builder -> builder.expectNotNull()
                                                                    .expect((path, c) -> {
                                                                        if (c.getId() <= 10) {
                                                                            throw new IllegalStateException(pathPrefix(path) + "too low ID value");
                                                                        }
                                                                    }))
                                 .build();
        assertValidation(validator,
                         OptionalFields.builder()
                                       .setBinaryValue(Binary.fromUUID(UUID.randomUUID()))
                                       .setCompactValue(CompactFields.builder().setId(42).build())
                                       .setIntegerValue(1234),
                         listOf());
        assertValidation(validator, null, listOf(mfe("Null providence.OptionalFields value.")));
        assertValidation(validator,
                         OptionalFields.builder()
                                             .setShortValue((short) 123)
                                             .setIntegerValue(1234)
                                             .setBinaryValue(Binary.fromUUID(UUID.randomUUID()))
                                             .setCompactValue(CompactFields.builder()
                                                                           .setId(42)
                                                                           .build()),
                         listOf(mfe("short_value present on providence.OptionalFields")));
        assertValidation(validator,
                         OptionalFields.builder()
                                             .setIntegerValue(1234)
                                             .setCompactValue(CompactFields.builder()
                                                                           .setId(42)
                                                                           .build())
                                             .build(),
                         listOf(mfe("binary_value not present on providence.OptionalFields")));
        assertValidation(validator,
                         OptionalFields.builder()
                                             .setIntegerValue(12345678)
                                             .setBinaryValue(Binary.fromUUID(UUID.randomUUID()))
                                             .setCompactValue(CompactFields.builder()
                                                                           .setId(42)
                                                                           .build())
                                             .build(),
                         listOf(mfe("Whoha! @")));
        assertValidation(validator,
                         OptionalFields.builder()
                                             .setBinaryValue(Binary.fromUUID(UUID.randomUUID()))
                                             .setIntegerValue(1234).build(),
                         listOf(mfe("compact_value not present on providence.OptionalFields")));
        assertValidation(validator,
                         OptionalFields.builder()
                                             .setBinaryValue(Binary.fromUUID(UUID.randomUUID()))
                                             .setCompactValue(CompactFields.builder()
                                                                           .setId(5)
                                                                           .build())
                                             .setIntegerValue(1234)
                                       .build(),
                         listOf(mfe("compact_value: too low ID value")));
    }

    @Test
    public void testValidate_AllowNull() throws MessageFieldException {
        MessageValidation<OptionalFields, MessageFieldException> validator =
                MessageValidation.builder(OptionalFields.kDescriptor,
                                          (ex) -> {
                                              if (ex instanceof MessageFieldException) {
                                                  return (MessageFieldException) ex;
                                              }
                                              return MessageFieldException.builder()
                                                                    .setStringValue(ex.getMessage())
                                                                    .build();
                                          })
                                 .expectMissing(SHORT_VALUE)
                                 .expectPresent(BINARY_VALUE, INTEGER_VALUE)
                                 .expect(msg -> {
                                     if (msg.getIntegerValue() >= 1_000_000) throw new IllegalStateException("Whoha!");
                                 })
                                 .expectIfPresent(COMPACT_VALUE,
                                                  CompactFields.kDescriptor,
                                                  builder -> builder.expectNotNull()
                                                                    .expect(c -> {
                                                                        if (c.getId() <= 10) throw new IllegalStateException("too low ID value");
                                                                    }))
                                 .build();
        assertValidation(validator,
                         null,
                         listOf());
    }
}
