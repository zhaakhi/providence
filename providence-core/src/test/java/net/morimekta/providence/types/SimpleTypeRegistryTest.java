package net.morimekta.providence.types;

import net.morimekta.providence.PType;
import net.morimekta.providence.descriptor.PDescriptorProvider;
import net.morimekta.providence.descriptor.PList;
import net.morimekta.providence.descriptor.PMap;
import net.morimekta.providence.descriptor.PPrimitive;
import net.morimekta.providence.descriptor.PServiceProvider;
import net.morimekta.providence.descriptor.PSet;
import net.morimekta.test.providence.core.calculator.Calculator;
import net.morimekta.test.providence.core.calculator.Calculator2;
import net.morimekta.test.providence.core.calculator.Operation;
import net.morimekta.test.providence.core.calculator.Operator;
import net.morimekta.test.providence.core.number.Imaginary;
import org.junit.Before;
import org.junit.Test;

import static net.morimekta.providence.types.TypeReference.parseType;
import static net.morimekta.util.collect.UnmodifiableMap.mapOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.fail;

/**
 * Tests for the TypeRegistry, WritableTypeRegistry and SimpleTypeRegistry class.
 */
public class SimpleTypeRegistryTest {
    private SimpleTypeRegistry registry;

    @Before
    public void setUp() {
        registry = new SimpleTypeRegistry();
        registry.registerType(Operation.kDescriptor);
        registry.registerService(Calculator.kDescriptor);
        registry.registerService(Calculator2.kDescriptor);
        registry.registerConstant(parseType("number.kSqrtMinus1"),
                                  () -> Imaginary.builder()
                                           .setV(0)
                                           .setI(1)
                                           .build());
        registry.registerTypedef(parseType("number.I"), parseType("number.Imaginary"));
        registry.registerTypedef(parseType("number.real"), parseType("double"));
    }

    @Test
    public void testService() {
        assertThat(registry.requireService(parseType("calculator.Calculator")),
                   is(sameInstance(Calculator.kDescriptor)));

        try {
            registry.requireService(parseType("gurba.Calculator"));
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("No program 'gurba' for service 'gurba.Calculator'"));
        }
        try {
            registry.requireService(parseType("calculator.Gurba"));
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("No service 'Gurba' in program 'calculator'"));
        }
        try {
            registry.requireService(parseType("number.Imaginary"));
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Declared name 'Imaginary' is not a service in program 'number'"));
        }
        try {
            registry.requireService(parseType("i32"));
            fail();
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Not a valid service name 'i32'"));
        }
    }

    @Test
    public void testDeclaredType() {
        assertThat(registry.requireMessageType(parseType("number.Imaginary")),
                   is(sameInstance(Imaginary.kDescriptor)));
        assertThat(registry.requireEnumType(parseType("calculator.Operator")),
                   is(sameInstance(Operator.kDescriptor)));

        try {
            registry.requireDeclaredType(parseType("fake.News"));
            fail();
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("No program 'fake' for type 'fake.News'"));
        }
        try {
            registry.requireDeclaredType(parseType("number.Fake"));
            fail();
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("No type 'Fake' in program 'number'"));
        }
        try {
            registry.requireDeclaredType(parseType("calculator.Calculator"));
            fail();
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Declared name 'Calculator' is not a type in program 'calculator'"));
        }
        try {
            registry.requireDeclaredType(parseType("i32"));
            fail();
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Not a valid type name 'i32'"));
        }
    }

    @Test
    public void testFinalTypename() {
        assertThat(registry.finalTypeReference(parseType("number.real")).toString(),
                   is("double"));

        assertThat(registry.finalTypeReference(parseType("set<number.real>")).toString(),
                   is("set<double>"));

        assertThat(registry.finalTypeReference(parseType("list<number.I>")).toString(),
                   is("list<number.Imaginary>"));
        assertThat(registry.finalTypeReference(parseType("set<number.I>")).toString(),
                   is("set<number.Imaginary>"));

        assertThat(registry.requireMessageType(parseType("number.I")),
                   is(Imaginary.kDescriptor));
        assertThat(registry.requireEnumType(parseType("calculator.Operator")),
                   is(Operator.kDescriptor));
    }

    @Test
    public void testGetProvider_map() {
        PDescriptorProvider p1 = registry.getTypeProvider(parseType("number", "map<real,I>"),
                                                          mapOf("container", "sorted"));
        assertThat(p1.descriptor().getType(), is(PType.MAP));
        PMap map = (PMap) p1.descriptor();
        assertThat(map.keyDescriptor(), is(PPrimitive.DOUBLE));
        assertThat(map.itemDescriptor(), is(Imaginary.kDescriptor));

        p1 = registry.getTypeProvider(parseType("number", "map<real,map<i32,I>>"),
                                      mapOf("container", "ordered"));
        assertThat(p1.descriptor().getType(), is(PType.MAP));
        map = (PMap) p1.descriptor();
        assertThat(map.keyDescriptor(), is(PPrimitive.DOUBLE));
        assertThat(map.itemDescriptor().getType(), is(PType.MAP));
        map = (PMap) map.itemDescriptor();
        assertThat(map.keyDescriptor(), is(PPrimitive.I32));
        assertThat(map.itemDescriptor(), is(Imaginary.kDescriptor));
    }

    @Test
    public void testGetProvider_set() {
        PDescriptorProvider p1 = registry.getTypeProvider(parseType("number", "set<I>"),
                                                          mapOf("container", "sorted"));
        assertThat(p1.descriptor().getType(), is(PType.SET));
        PSet set = (PSet) p1.descriptor();
        assertThat(set.itemDescriptor(), is(Imaginary.kDescriptor));

        p1 = registry.getTypeProvider(parseType("number", "set<set<i32>>"),
                                      mapOf("container", "ordered"));
        assertThat(p1.descriptor().getType(), is(PType.SET));
        set = (PSet) p1.descriptor();
        assertThat(set.itemDescriptor().getType(), is(PType.SET));
        PSet list = (PSet) set.itemDescriptor();
        assertThat(list.itemDescriptor(), is(PPrimitive.I32));
    }


    @Test
    public void testGetProvider_list() {
        PDescriptorProvider p1 = registry.getTypeProvider(parseType("list<number.I>"));
        assertThat(p1.descriptor().getType(), is(PType.LIST));
        PList list = (PList) p1.descriptor();
        assertThat(list.itemDescriptor(), is(Imaginary.kDescriptor));
    }

    @Test
    public void testGetServiceProvider() {
        PServiceProvider srv = registry.getServiceProvider(parseType("number", "calculator.Calculator2"));

        assertThat(srv.getService(), is(Calculator2.kDescriptor));
        assertThat(srv.getService().getExtendsService(), is(Calculator.kDescriptor));
    }
}
