package net.morimekta.providence.types;

import net.morimekta.providence.descriptor.PContainer;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class ThriftContainerTest {
    @Test
    public void testForTag() {
        assertThat(PContainer.typeForName("sorted"), is(PContainer.Type.SORTED));
        assertThat(PContainer.typeForName("SORTED"), is(PContainer.Type.SORTED));
        assertThat(PContainer.typeForName("ordered"), is(PContainer.Type.ORDERED));
        assertThat(PContainer.typeForName("OrderED"), is(PContainer.Type.ORDERED));
        assertThat(PContainer.typeForName("boo"), is(PContainer.Type.DEFAULT));
        assertThat(PContainer.typeForName(""), is(PContainer.Type.DEFAULT));
        assertThat(PContainer.typeForName(null), is(PContainer.Type.DEFAULT));
    }
}
