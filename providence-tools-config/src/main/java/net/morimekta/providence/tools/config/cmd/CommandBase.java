package net.morimekta.providence.tools.config.cmd;

import net.morimekta.providence.config.ConfigLoader;
import net.morimekta.providence.reflect.ProgramLoader;
import net.morimekta.providence.tools.common.formats.FormatUtils;
import net.morimekta.providence.tools.config.ConfigOptions;
import net.morimekta.util.FileWatcher;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Map;

/**
 * Base command of the type that needs a real providence config.
 */
public abstract class CommandBase implements Command {
    @Override
    public void execute(ConfigOptions options) throws IOException {
        Map<String, Path> includeMap = FormatUtils.getIncludeMap(options.getRc(), options.getIncludes(), options.verbose());

        ProgramLoader loader = new ProgramLoader();
        for (Path file : includeMap.values()) {
            loader.load(file);
        }

        execute(new ConfigLoader(loader.getGlobalRegistry(), new FileWatcher(), warning -> {
            if (!options.silent()) {
                System.err.println(warning.displayString());
            }
        }, options.strict()));
    }

    public abstract void execute(ConfigLoader configLoader) throws IOException;
}
