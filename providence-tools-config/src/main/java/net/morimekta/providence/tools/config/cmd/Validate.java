package net.morimekta.providence.tools.config.cmd;

import net.morimekta.console.args.Argument;
import net.morimekta.console.args.ArgumentParser;
import net.morimekta.console.util.Parser;
import net.morimekta.providence.config.ConfigLoader;
import net.morimekta.providence.config.parser.ConfigException;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

/**
 * Resolve an overview over the available params for the config.
 */
public class Validate extends CommandBase {
    private List<Path> files = new ArrayList<>();

    @Override
    public void execute(ConfigLoader configLoader) {
        ConfigException tmp = null;
        for (Path file : files) {
            try {
                configLoader.getConfig(file);
            } catch (ConfigException e) {
                if (tmp == null) {
                    tmp = e;
                }
                System.err.println(e.displayString());
                System.err.println();
            }
        }
        if (tmp != null) {
            throw new IllegalStateException("There were files with errors...");
        }
    }

    private void addFile(Path file) {
        this.files.add(file);
    }

    @Override
    public ArgumentParser parser(ArgumentParser parent) {
        ArgumentParser parser = new ArgumentParser(parent,
                                                   "validate",
                                                   "Verify content of config files, also checking includes");
        parser.add(new Argument("file", "Config files to validate", Parser.filePath(this::addFile), null, null, true, true, false));
        return parser;
    }
}
