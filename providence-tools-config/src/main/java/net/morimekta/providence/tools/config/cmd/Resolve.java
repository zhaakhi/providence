package net.morimekta.providence.tools.config.cmd;

import net.morimekta.console.args.Argument;
import net.morimekta.console.args.ArgumentParser;
import net.morimekta.console.args.Option;
import net.morimekta.console.util.Parser;
import net.morimekta.providence.PMessage;
import net.morimekta.providence.config.ConfigLoader;
import net.morimekta.providence.serializer.PrettySerializer;
import net.morimekta.providence.serializer.Serializer;
import net.morimekta.providence.tools.common.formats.Format;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

/**
 * Resolve the resulting config.
 */
public class Resolve extends CommandBase {
    private Serializer serializer = new PrettySerializer().config();
    private List<Path> files      = new ArrayList<>();

    @Override
    @SuppressWarnings("unchecked")
    public void execute(ConfigLoader configLoader) throws IOException {
        if (files.isEmpty()) {
            throw new IllegalArgumentException("No config files to resolve");
        }

        PMessage message = null;
        for (Path configFile : files) {
            if (message == null) {
                message = configLoader.getConfig(configFile);
            } else {
                message = configLoader.getConfig(configFile, message);
            }
        }

        serializer.serialize(System.out, message);
        System.out.println();
    }

    @Override
    public ArgumentParser parser(ArgumentParser parent) {
        ArgumentParser parser = new ArgumentParser(parent,
                                                   "resolve",
                                                   "Resolve config files to final config message.");
        parser.add(new Option("--format", "f", "fmt", "the output format", this::setSerializer, "pretty"));
        parser.add(new Argument("file", "Config files to resolve", Parser.filePath(this::addFile), null, null, true, true, false));
        return parser;
    }

    private void addFile(Path file) {
        this.files.add(file);
    }

    private void setSerializer(String name) {
        Format format = Format.forName(name);
        serializer = format.createSerializer(false);
    }
}
