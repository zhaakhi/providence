package net.morimekta.providence.server;

import com.google.api.client.http.ByteArrayContent;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpResponse;
import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import net.morimekta.providence.serializer.BinarySerializer;
import net.morimekta.providence.serializer.DefaultSerializerProvider;
import net.morimekta.providence.serializer.JsonSerializer;
import net.morimekta.providence.serializer.Serializer;
import net.morimekta.providence.server.internal.NoLogging;
import net.morimekta.test.providence.service.Failure;
import net.morimekta.test.providence.service.OtherFailure;
import net.morimekta.test.providence.service.Request;
import net.morimekta.test.providence.service.Response;
import net.morimekta.testing.DataProviderUtil;
import net.morimekta.util.io.IOUtils;
import org.awaitility.Awaitility;
import org.eclipse.jetty.http.HttpStatus;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.log.Log;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import static java.util.Arrays.asList;
import static net.morimekta.providence.server.internal.TestNetUtil.factory;
import static net.morimekta.providence.server.internal.TestNetUtil.getExposedPort;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(DataProviderRunner.class)
public class ProvidenceHttpServletTest {
    private static class TestServlet extends ProvidenceHttpServlet<Request, Response> {
        private TestServlet() {
            super(Request.kDescriptor, new TestExceptionHandler(), new DefaultSerializerProvider());
        }

        @Nonnull
        @Override
        protected Response handle(@Nonnull HttpServletRequest httpRequest, @Nonnull Request request)
                throws Failure, ExecutionException {
            if (request.getText().startsWith("fail ")) {
                throw Failure.builder()
                             .setText(request.getText().substring(5))
                             .build();
            }
            if (request.getText().startsWith("other ")) {
                throw new ExecutionException(request.getText(),
                                             OtherFailure.builder()
                                                         .setOther(request.getText().substring(6))
                                                         .build());
            }

            return Response.builder()
                           .setText(request.getText())
                           .build();
        }
    }

    private static class TestExceptionHandler extends ExceptionHandler {
        @Nonnull
        @Override
        protected Throwable getResponseException(Throwable e) {
            if (e instanceof ExecutionException && e.getCause() != null) {
                return getResponseException(e.getCause());
            }
            return super.getResponseException(e);
        }

        @Override
        protected int statusCodeForException(@Nonnull Throwable exception) {
            if (exception instanceof Failure) {
                return HttpStatus.EXPECTATION_FAILED_417;
            }
            if (exception instanceof OtherFailure) {
                return HttpStatus.FAILED_DEPENDENCY_424;
            }
            return super.statusCodeForException(exception);
        }
    }

    private int    port;
    private Server server;

    private static final String ENDPOINT = "foo";

    private GenericUrl endpoint() {
        return new GenericUrl("http://localhost:" + port + "/" + ENDPOINT);
    }

    @Before
    public void setUpServer() throws Exception {
        Awaitility.setDefaultPollDelay(2, TimeUnit.MILLISECONDS);
        Log.setLog(new NoLogging());

        server = new Server(0);
        ServletContextHandler handler = new ServletContextHandler();
        handler.addServlet(new ServletHolder(new TestServlet()),
                           "/" + ENDPOINT);

        server.setHandler(handler);
        server.start();
        port = getExposedPort(server);
    }

    @After
    public void tearDownServer() {
        try {
            server.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testSimpleRequest() throws IOException {
        String num = "" + new Random().nextInt(100);
        String text = "yes " + num;

        HttpResponse response = post(JsonSerializer.JSON_MEDIA_TYPE, "{\"text\": \"" + text + "\"}");
        assertThat(response.getStatusCode(), is(HttpStatus.OK_200));

        String content = IOUtils.readString(response.getContent());
        assertThat(content, is("{\"text\":\"" + text + "\"}"));
    }

    @DataProvider
    public static Object[][] overrideAccept() {
        return DataProviderUtil.buildDataDimensions(
                asList(new BinarySerializer(), new JsonSerializer(), new JsonSerializer().named() ),
                asList(new BinarySerializer(), new JsonSerializer(), new JsonSerializer().named() ));
    }

    @Test
    @UseDataProvider("overrideAccept")
    public void testSimpleRequest_OverrideAccept(Serializer toSend, Serializer toReceive) throws IOException {
        String num = "" + new Random().nextInt(100);
        String text = "yes " + num;

        Request request = Request.builder()
                                 .setText(text)
                                 .build();
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        toSend.serialize(out, request);

        HttpResponse httpResponse = factory(rq -> rq.getHeaders().setAccept(toReceive.mediaType()))
                .buildPostRequest(endpoint(), new ByteArrayContent(toSend.mediaType(), out.toByteArray()))
                .execute();

        assertThat(httpResponse.getStatusCode(), is(HttpStatus.OK_200));
        assertThat(httpResponse.getContentType(), is(toReceive.mediaType()));

        Response response = toReceive.deserialize(httpResponse.getContent(), Response.kDescriptor);
        assertThat(response, is(Response.builder()
                                        .setText(text)
                                        .build()));
    }

    @Test
    public void testSimpleRequest_badAccept() throws IOException {
        Serializer toSend = new JsonSerializer();

        String num = "" + new Random().nextInt(100);
        String text = "yes " + num;

        Request request = Request.builder()
                                 .setText(text)
                                 .build();
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        toSend.serialize(out, request);

        HttpResponse httpResponse = factory(rq -> rq.getHeaders().setAccept("not-a media type, ,"))
                .buildPostRequest(endpoint(), new ByteArrayContent(toSend.mediaType(), out.toByteArray()))
                .setThrowExceptionOnExecuteError(false)
                .execute();

        assertThat(httpResponse.getStatusCode(), is(HttpStatus.OK_200));
        Response response = toSend.deserialize(httpResponse.getContent(), Response.kDescriptor);
        assertThat(response, is(Response.builder()
                                        .setText(text)
                                        .build()));
    }

    @Test
    public void testFailedRequest() throws IOException {
        String num = "" + new Random().nextInt(100);
        String text = "fail " + num;

        HttpResponse response = post(JsonSerializer.JSON_MEDIA_TYPE, "{\"text\": \"" + text + "\"}");
        assertThat(response.getStatusCode(), is(HttpStatus.EXPECTATION_FAILED_417));

        String content = IOUtils.readString(response.getContent());
        assertThat(content, is("{\"text\":\"" + num + "\"}"));
    }

    @Test
    public void testOtherFailedRequest() throws IOException {
        String num = "" + new Random().nextInt(100);
        String text = "other " + num;

        HttpResponse response = post(JsonSerializer.JSON_MEDIA_TYPE, "{\"text\": \"" + text + "\"}");
        assertThat(response.getStatusCode(), is(HttpStatus.FAILED_DEPENDENCY_424));

        String content = IOUtils.readString(response.getContent());
        assertThat(content, is("{\"other\":\"" + num + "\"}"));
    }

    @Test
    public void testBadRequest() throws IOException {
        HttpResponse response;

        response = post("text/url-encoded", "method=test");
        assertThat(response.getStatusCode(), is(HttpStatus.BAD_REQUEST_400));
        assertThat(IOUtils.readString(response.getContent()), containsString("Unknown content-type: text/url-encoded"));

        response = post("application/json", "[\"foo\", 1, 1, {}]");
        assertThat(response.getStatusCode(), is(HttpStatus.BAD_REQUEST_400));
        assertThat(response.getStatusMessage(), is("Bad Request"));
        assertThat(IOUtils.readString(response.getContent()), containsString("Request is not compatible for compact struct notation."));
    }

    private HttpResponse post(String contentType, String content) throws IOException {
        HttpRequest request = factory()
                .buildPostRequest(
                        endpoint(), new ByteArrayContent(contentType, content.getBytes(StandardCharsets.UTF_8)))
                .setThrowExceptionOnExecuteError(false);
        request.getHeaders().setAccept("*/*");
        return request.execute();
    }

}
