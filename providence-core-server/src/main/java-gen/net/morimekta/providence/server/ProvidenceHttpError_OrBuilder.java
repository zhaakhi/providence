package net.morimekta.providence.server;

@javax.annotation.Generated(
        value = "net.morimekta.providence:providence-generator-java",
        comments = "java:serializable")
@SuppressWarnings("unused")
public interface ProvidenceHttpError_OrBuilder extends net.morimekta.providence.PMessageOrBuilder<ProvidenceHttpError> {
    /**
     * @return The message value.
     */
    String getMessage();

    /**
     * @return Optional message value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<String> optionalMessage();

    /**
     * @return If message is present.
     */
    boolean hasMessage();

    /**
     * @return The status_code value.
     */
    int getStatusCode();

    /**
     * @return Optional status_code value.
     */
    @javax.annotation.Nonnull
    java.util.Optional<Integer> optionalStatusCode();

    /**
     * @return If status_code is present.
     */
    boolean hasStatusCode();

}
