package net.morimekta.providence.server;

@SuppressWarnings("unused")
@javax.annotation.Generated(
        value = "net.morimekta.providence:providence-generator-java",
        comments = "java:serializable")
@javax.annotation.concurrent.Immutable
public class ProvidenceHttpError
        extends Exception
        implements ProvidenceHttpError_OrBuilder,
                   net.morimekta.providence.PMessage<ProvidenceHttpError>,
                   net.morimekta.providence.PException,
                   Comparable<ProvidenceHttpError>,
                   net.morimekta.providence.serializer.binary.BinaryWriter {
    private final static int kDefaultStatusCode = 0;

    private final static long serialVersionUID = -8543316838990827604L;

    private final transient String mMessage;
    private final transient Integer mStatusCode;

    private volatile transient int tHashCode;

    // Transient object used during java deserialization.
    private transient ProvidenceHttpError tSerializeInstance;

    private ProvidenceHttpError(_Builder builder) {
        super(builder.mMessage);

        mMessage = builder.mMessage;
        mStatusCode = builder.mStatusCode;
    }

    public boolean hasMessage() {
        return mMessage != null;
    }

    /**
     * @return The <code>message</code> value
     */
    public String getMessage() {
        return mMessage;
    }

    /**
     * @return Optional of the <code>message</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<String> optionalMessage() {
        return java.util.Optional.ofNullable(mMessage);
    }

    public boolean hasStatusCode() {
        return mStatusCode != null;
    }

    /**
     * @return The <code>status_code</code> value
     */
    public int getStatusCode() {
        return hasStatusCode() ? mStatusCode : kDefaultStatusCode;
    }

    /**
     * @return Optional of the <code>status_code</code> field value.
     */
    @javax.annotation.Nonnull
    public java.util.Optional<Integer> optionalStatusCode() {
        return java.util.Optional.ofNullable(mStatusCode);
    }

    @Override
    public boolean has(int key) {
        switch(key) {
            case 1: return mMessage != null;
            case 2: return mStatusCode != null;
            default: return false;
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T get(int key) {
        switch(key) {
            case 1: return (T) mMessage;
            case 2: return (T) mStatusCode;
            default: return null;
        }
    }

    @Override
    public String origGetMessage() {
        return super.getMessage();
    }

    @Override
    public String origGetLocalizedMessage() {
        return super.getLocalizedMessage();
    }

    @Override
    public ProvidenceHttpError initCause(Throwable cause) {
        return (ProvidenceHttpError) super.initCause(cause);
    }

    @Override
    public ProvidenceHttpError fillInStackTrace() {
        return (ProvidenceHttpError) super.fillInStackTrace();
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) return true;
        if (o == null || !o.getClass().equals(getClass())) return false;
        ProvidenceHttpError other = (ProvidenceHttpError) o;
        return java.util.Objects.equals(mMessage, other.mMessage) &&
               java.util.Objects.equals(mStatusCode, other.mStatusCode);
    }

    @Override
    public int hashCode() {
        if (tHashCode == 0) {
            tHashCode = java.util.Objects.hash(
                    ProvidenceHttpError.class,
                    _Field.MESSAGE, mMessage,
                    _Field.STATUS_CODE, mStatusCode);
        }
        return tHashCode;
    }

    @Override
    public String toString() {
        return "p_http.ProvidenceHttpError" + asString();
    }

    @Override
    @javax.annotation.Nonnull
    public String asString() {
        StringBuilder out = new StringBuilder();
        out.append("{");

        boolean first = true;
        if (hasMessage()) {
            first = false;
            out.append("message:")
               .append('\"')
               .append(net.morimekta.util.Strings.escape(mMessage))
               .append('\"');
        }
        if (hasStatusCode()) {
            if (!first) out.append(',');
            out.append("status_code:")
               .append(mStatusCode);
        }
        out.append('}');
        return out.toString();
    }

    @Override
    public int compareTo(ProvidenceHttpError other) {
        int c;

        c = Boolean.compare(mMessage != null, other.mMessage != null);
        if (c != 0) return c;
        if (mMessage != null) {
            c = mMessage.compareTo(other.mMessage);
            if (c != 0) return c;
        }

        c = Boolean.compare(mStatusCode != null, other.mStatusCode != null);
        if (c != 0) return c;
        if (mStatusCode != null) {
            c = Integer.compare(mStatusCode, other.mStatusCode);
            if (c != 0) return c;
        }

        return 0;
    }

    private void writeObject(java.io.ObjectOutputStream oos) throws java.io.IOException {
        oos.defaultWriteObject();
        net.morimekta.providence.serializer.BinarySerializer.INSTANCE.serialize(oos, this);
    }

    private void readObject(java.io.ObjectInputStream ois)
            throws java.io.IOException, ClassNotFoundException {
        ois.defaultReadObject();
        tSerializeInstance = net.morimekta.providence.serializer.BinarySerializer.INSTANCE.deserialize(ois, kDescriptor);
        if (getCause() != null) {
            tSerializeInstance.initCause(getCause());
        }
        tSerializeInstance.setStackTrace(getStackTrace());
    }

    private Object readResolve() throws java.io.ObjectStreamException {
        return tSerializeInstance;
    }

    @Override
    public int writeBinary(net.morimekta.util.io.BigEndianBinaryWriter writer) throws java.io.IOException {
        int length = 0;

        if (hasMessage()) {
            length += writer.writeByte((byte) 11);
            length += writer.writeShort((short) 1);
            net.morimekta.util.Binary tmp_1 = net.morimekta.util.Binary.wrap(mMessage.getBytes(java.nio.charset.StandardCharsets.UTF_8));
            length += writer.writeUInt32(tmp_1.length());
            length += writer.writeBinary(tmp_1);
        }

        if (hasStatusCode()) {
            length += writer.writeByte((byte) 8);
            length += writer.writeShort((short) 2);
            length += writer.writeInt(mStatusCode);
        }

        length += writer.writeByte((byte) 0);
        return length;
    }

    @javax.annotation.Nonnull
    @Override
    public _Builder mutate() {
        return new _Builder(this);
    }

    public enum _Field implements net.morimekta.providence.descriptor.PField<ProvidenceHttpError> {
        MESSAGE(1, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "message", "message", net.morimekta.providence.descriptor.PPrimitive.STRING.provider(), null, null),
        STATUS_CODE(2, net.morimekta.providence.descriptor.PRequirement.OPTIONAL, "status_code", "statusCode", net.morimekta.providence.descriptor.PPrimitive.I32.provider(), null, null),
        ;

        private final int mId;
        private final net.morimekta.providence.descriptor.PRequirement mRequired;
        private final String mName;
        private final String mPojoName;
        private final net.morimekta.providence.descriptor.PDescriptorProvider mTypeProvider;
        private final net.morimekta.providence.descriptor.PStructDescriptorProvider mArgumentsProvider;
        private final net.morimekta.providence.descriptor.PValueProvider<?> mDefaultValue;

        _Field(int id, net.morimekta.providence.descriptor.PRequirement required, String name, String pojoName, net.morimekta.providence.descriptor.PDescriptorProvider typeProvider, net.morimekta.providence.descriptor.PStructDescriptorProvider argumentsProvider, net.morimekta.providence.descriptor.PValueProvider<?> defaultValue) {
            mId = id;
            mRequired = required;
            mName = name;
            mPojoName = pojoName;
            mTypeProvider = typeProvider;
            mArgumentsProvider = argumentsProvider;
            mDefaultValue = defaultValue;
        }

        @Override
        public int getId() { return mId; }

        @javax.annotation.Nonnull
        @Override
        public net.morimekta.providence.descriptor.PRequirement getRequirement() { return mRequired; }

        @javax.annotation.Nonnull
        @Override
        public net.morimekta.providence.descriptor.PDescriptor getDescriptor() { return mTypeProvider.descriptor(); }

        @Override
        @javax.annotation.Nullable
        public net.morimekta.providence.descriptor.PStructDescriptor getArgumentsType() { return mArgumentsProvider == null ? null : mArgumentsProvider.descriptor(); }

        @javax.annotation.Nonnull
        @Override
        public String getName() { return mName; }

        @javax.annotation.Nonnull
        @Override
        public String getPojoName() { return mPojoName; }

        @Override
        public boolean hasDefaultValue() { return mDefaultValue != null; }

        @Override
        @javax.annotation.Nullable
        public Object getDefaultValue() {
            return hasDefaultValue() ? mDefaultValue.get() : null;
        }

        @Override
        @javax.annotation.Nonnull
        public net.morimekta.providence.descriptor.PMessageDescriptor<ProvidenceHttpError> onMessageType() {
            return kDescriptor;
        }

        @Override
        public String toString() {
            return net.morimekta.providence.descriptor.PField.asString(this);
        }

        /**
         * @param id Field ID
         * @return The identified field or null
         */
        public static _Field findById(int id) {
            switch (id) {
                case 1: return _Field.MESSAGE;
                case 2: return _Field.STATUS_CODE;
            }
            return null;
        }

        /**
         * @param name Field name
         * @return The named field or null
         */
        public static _Field findByName(String name) {
            if (name == null) return null;
            switch (name) {
                case "message": return _Field.MESSAGE;
                case "status_code": return _Field.STATUS_CODE;
            }
            return null;
        }

        /**
         * @param name Field POJO name
         * @return The named field or null
         */
        public static _Field findByPojoName(String name) {
            if (name == null) return null;
            switch (name) {
                case "message": return _Field.MESSAGE;
                case "statusCode": return _Field.STATUS_CODE;
                case "status_code": return _Field.STATUS_CODE;
            }
            return null;
        }

        /**
         * @param id Field ID
         * @return The identified field
         * @throws IllegalArgumentException If no such field
         */
        public static _Field fieldForId(int id) {
            _Field field = findById(id);
            if (field == null) {
                throw new IllegalArgumentException("No such field id " + id + " in p_http.ProvidenceHttpError");
            }
            return field;
        }

        /**
         * @param name Field name
         * @return The named field
         * @throws IllegalArgumentException If no such field
         */
        public static _Field fieldForName(String name) {
            if (name == null) {
                throw new IllegalArgumentException("Null name argument");
            }
            _Field field = findByName(name);
            if (field == null) {
                throw new IllegalArgumentException("No such field \"" + name + "\" in p_http.ProvidenceHttpError");
            }
            return field;
        }

        /**
         * @param name Field POJO name
         * @return The named field
         * @throws IllegalArgumentException If no such field
         */
        public static _Field fieldForPojoName(String name) {
            if (name == null) {
                throw new IllegalArgumentException("Null name argument");
            }
            _Field field = findByPojoName(name);
            if (field == null) {
                throw new IllegalArgumentException("No such field \"" + name + "\" in p_http.ProvidenceHttpError");
            }
            return field;
        }
    }

    @javax.annotation.Nonnull
    public static net.morimekta.providence.descriptor.PExceptionDescriptorProvider<ProvidenceHttpError> provider() {
        return new _Provider();
    }

    @Override
    @javax.annotation.Nonnull
    public net.morimekta.providence.descriptor.PExceptionDescriptor<ProvidenceHttpError> descriptor() {
        return kDescriptor;
    }

    public static final net.morimekta.providence.descriptor.PExceptionDescriptor<ProvidenceHttpError> kDescriptor;

    private static final class _Descriptor
            extends net.morimekta.providence.descriptor.PExceptionDescriptor<ProvidenceHttpError> {
        public _Descriptor() {
            super("p_http", "ProvidenceHttpError", _Builder::new, true);
        }

        @Override
        @javax.annotation.Nonnull
        public boolean isInnerType() {
            return false;
        }

        @Override
        @javax.annotation.Nonnull
        public boolean isAutoType() {
            return false;
        }

        @Override
        @javax.annotation.Nonnull
        public _Field[] getFields() {
            return _Field.values();
        }

        @Override
        @javax.annotation.Nullable
        public _Field findFieldByName(String name) {
            return _Field.findByName(name);
        }

        @Override
        @javax.annotation.Nullable
        public _Field findFieldByPojoName(String name) {
            return _Field.findByPojoName(name);
        }

        @Override
        @javax.annotation.Nullable
        public _Field findFieldById(int id) {
            return _Field.findById(id);
        }
    }

    static {
        kDescriptor = new _Descriptor();
    }

    private static final class _Provider extends net.morimekta.providence.descriptor.PExceptionDescriptorProvider<ProvidenceHttpError> {
        @Override
        @javax.annotation.Nonnull
        public net.morimekta.providence.descriptor.PExceptionDescriptor<ProvidenceHttpError> descriptor() {
            return kDescriptor;
        }
    }

    /**
     * Make a <code>p_http.ProvidenceHttpError</code> builder.
     * @return The builder instance.
     */
    public static _Builder builder() {
        return new _Builder();
    }

    public static class _Builder
            extends net.morimekta.providence.PMessageBuilder<ProvidenceHttpError>
            implements ProvidenceHttpError_OrBuilder,
                       net.morimekta.providence.serializer.binary.BinaryReader {
        private Throwable cause;
        private java.util.BitSet optionals;
        private java.util.BitSet modified;

        private String mMessage;
        private Integer mStatusCode;

        /**
         * Make a p_http.ProvidenceHttpError builder instance.
         */
        public _Builder() {
            optionals = new java.util.BitSet(2);
            modified = new java.util.BitSet(2);
        }

        /**
         * Make a mutating builder off a base p_http.ProvidenceHttpError.
         *
         * @param base The base ProvidenceHttpError
         */
        public _Builder(ProvidenceHttpError base) {
            this();

            if (base.hasMessage()) {
                optionals.set(0);
                mMessage = base.mMessage;
            }
            if (base.hasStatusCode()) {
                optionals.set(1);
                mStatusCode = base.mStatusCode;
            }
        }

        @javax.annotation.Nonnull
        @Override
        public ProvidenceHttpError._Builder merge(ProvidenceHttpError from) {
            if (from.hasMessage()) {
                optionals.set(0);
                modified.set(0);
                mMessage = from.getMessage();
            }

            if (from.hasStatusCode()) {
                optionals.set(1);
                modified.set(1);
                mStatusCode = from.getStatusCode();
            }
            return this;
        }

        /**
         * Set the <code>message</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public ProvidenceHttpError._Builder setMessage(String value) {
            if (value == null) {
                return clearMessage();
            }

            optionals.set(0);
            modified.set(0);
            mMessage = value;
            return this;
        }

        /**
         * Checks for explicit presence of the <code>message</code> field.
         *
         * @return True if message has been set.
         */
        public boolean isSetMessage() {
            return optionals.get(0);
        }

        /**
         * Checks for presence of the <code>message</code> field.
         *
         * @return True if message is present.
         */
        public boolean hasMessage() {
            return optionals.get(0);
        }

        /**
         * Checks if the <code>message</code> field has been modified since the
         * builder was created.
         *
         * @return True if message has been modified.
         */
        public boolean isModifiedMessage() {
            return modified.get(0);
        }

        /**
         * Clear the <code>message</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public ProvidenceHttpError._Builder clearMessage() {
            optionals.clear(0);
            modified.set(0);
            mMessage = null;
            return this;
        }

        /**
         * @return The <code>message</code> field value
         */
        public String getMessage() {
            return mMessage;
        }

        /**
         * @return Optional <code>message</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<String> optionalMessage() {
            return java.util.Optional.ofNullable(mMessage);
        }

        /**
         * Set the <code>status_code</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public ProvidenceHttpError._Builder setStatusCode(Integer value) {
            if (value == null) {
                return clearStatusCode();
            }

            optionals.set(1);
            modified.set(1);
            mStatusCode = value;
            return this;
        }

        /**
         * Set the <code>status_code</code> field value.
         *
         * @param value The new value
         * @return The builder
         */
        @javax.annotation.Nonnull
        public ProvidenceHttpError._Builder setStatusCode(int value) {
            optionals.set(1);
            modified.set(1);
            mStatusCode = value;
            return this;
        }

        /**
         * Checks for explicit presence of the <code>status_code</code> field.
         *
         * @return True if status_code has been set.
         */
        public boolean isSetStatusCode() {
            return optionals.get(1);
        }

        /**
         * Checks for presence of the <code>status_code</code> field.
         *
         * @return True if status_code is present.
         */
        public boolean hasStatusCode() {
            return optionals.get(1);
        }

        /**
         * Checks if the <code>status_code</code> field has been modified since the
         * builder was created.
         *
         * @return True if status_code has been modified.
         */
        public boolean isModifiedStatusCode() {
            return modified.get(1);
        }

        /**
         * Clear the <code>status_code</code> field.
         *
         * @return The builder
         */
        @javax.annotation.Nonnull
        public ProvidenceHttpError._Builder clearStatusCode() {
            optionals.clear(1);
            modified.set(1);
            mStatusCode = null;
            return this;
        }

        /**
         * @return The <code>status_code</code> field value
         */
        public int getStatusCode() {
            return isSetStatusCode() ? mStatusCode : kDefaultStatusCode;
        }

        /**
         * @return Optional <code>status_code</code> field value
         */
        @javax.annotation.Nonnull
        public java.util.Optional<Integer> optionalStatusCode() {
            return java.util.Optional.ofNullable(mStatusCode);
        }

        /**
         * Initializes the cause of the p_http.ProvidenceHttpError
         *
         * @param cause The cause
         * @return Builder instance
         */
        @javax.annotation.Nonnull
        public ProvidenceHttpError._Builder initCause(Throwable cause) {
            this.cause = cause;
            return this;
        }

        @Override
        public boolean equals(Object o) {
            if (o == this) return true;
            if (o == null || !o.getClass().equals(getClass())) return false;
            ProvidenceHttpError._Builder other = (ProvidenceHttpError._Builder) o;
            return java.util.Objects.equals(optionals, other.optionals) &&
                   java.util.Objects.equals(mMessage, other.mMessage) &&
                   java.util.Objects.equals(mStatusCode, other.mStatusCode);
        }

        @Override
        public int hashCode() {
            return java.util.Objects.hash(
                    ProvidenceHttpError.class, optionals,
                    ProvidenceHttpError._Field.MESSAGE, mMessage,
                    ProvidenceHttpError._Field.STATUS_CODE, mStatusCode);
        }

        @Override
        @SuppressWarnings("unchecked")
        public net.morimekta.providence.PMessageBuilder mutator(int key) {
            switch (key) {
                default: throw new IllegalArgumentException("Not a message field ID: " + key);
            }
        }

        @javax.annotation.Nonnull
        @Override
        @SuppressWarnings("unchecked")
        public ProvidenceHttpError._Builder set(int key, Object value) {
            if (value == null) return clear(key);
            switch (key) {
                case 1: setMessage((String) value); break;
                case 2: setStatusCode((int) value); break;
                default: break;
            }
            return this;
        }

        @Override
        public boolean isSet(int key) {
            switch (key) {
                case 1: return optionals.get(0);
                case 2: return optionals.get(1);
                default: break;
            }
            return false;
        }

        @Override
        public boolean isModified(int key) {
            switch (key) {
                case 1: return modified.get(0);
                case 2: return modified.get(1);
                default: break;
            }
            return false;
        }

        @Override
        @SuppressWarnings("unchecked")
        public <T> T get(int key) {
            switch(key) {
                case 1: return (T) getMessage();
                case 2: return (T) (Object) getStatusCode();
                default: return null;
            }
        }

        @Override
        public boolean has(int key) {
            switch(key) {
                case 1: return mMessage != null;
                case 2: return mStatusCode != null;
                default: return false;
            }
        }

        @javax.annotation.Nonnull
        @Override
        @SuppressWarnings("unchecked")
        public ProvidenceHttpError._Builder addTo(int key, Object value) {
            switch (key) {
                default: break;
            }
            return this;
        }

        @javax.annotation.Nonnull
        @Override
        public ProvidenceHttpError._Builder clear(int key) {
            switch (key) {
                case 1: clearMessage(); break;
                case 2: clearStatusCode(); break;
                default: break;
            }
            return this;
        }

        @Override
        public boolean valid() {
            return true;
        }

        @Override
        public ProvidenceHttpError._Builder validate() {
            return this;
        }

        @javax.annotation.Nonnull
        @Override
        public net.morimekta.providence.descriptor.PExceptionDescriptor<ProvidenceHttpError> descriptor() {
            return ProvidenceHttpError.kDescriptor;
        }

        @Override
        public void readBinary(net.morimekta.util.io.BigEndianBinaryReader reader, boolean strict) throws java.io.IOException {
            byte type = reader.expectByte();
            while (type != 0) {
                int field = reader.expectShort();
                switch (field) {
                    case 1: {
                        if (type == 11) {
                            int len_1 = reader.expectUInt32();
                            mMessage = new String(reader.expectBytes(len_1), java.nio.charset.StandardCharsets.UTF_8);
                            optionals.set(0);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for p_http.ProvidenceHttpError.message, should be struct(12)");
                        }
                        break;
                    }
                    case 2: {
                        if (type == 8) {
                            mStatusCode = reader.expectInt();
                            optionals.set(1);
                        } else {
                            throw new net.morimekta.providence.serializer.SerializerException("Wrong type " + net.morimekta.providence.serializer.binary.BinaryType.asString(type) + " for p_http.ProvidenceHttpError.status_code, should be struct(12)");
                        }
                        break;
                    }
                    default: {
                        net.morimekta.providence.serializer.binary.BinaryFormatUtils.readFieldValue(reader, new net.morimekta.providence.serializer.binary.BinaryFormatUtils.FieldInfo(field, type), null, false);
                        break;
                    }
                }
                type = reader.expectByte();
            }
        }

        @Override
        @javax.annotation.Nonnull
        public ProvidenceHttpError build() {
            ProvidenceHttpError e = new ProvidenceHttpError(this);

            try {
                StackTraceElement[] stackTrace = e.getStackTrace();
                StackTraceElement[] subTrace = new StackTraceElement[stackTrace.length - 1];
                System.arraycopy(stackTrace, 1, subTrace, 0, subTrace.length);
                e.setStackTrace(subTrace);
            } catch (Throwable ignored) {
            }

            if (cause != null) {
                e.initCause(cause);
            }

            return e;
        }
    }
}
