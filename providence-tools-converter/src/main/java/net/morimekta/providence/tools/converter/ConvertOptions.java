/*
 * Copyright (c) 2016, Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.morimekta.providence.tools.converter;

import net.morimekta.console.args.Argument;
import net.morimekta.console.args.ArgumentException;
import net.morimekta.console.args.ArgumentParser;
import net.morimekta.console.args.Flag;
import net.morimekta.console.args.Option;
import net.morimekta.console.util.STTY;
import net.morimekta.providence.PMessage;
import net.morimekta.providence.descriptor.PMessageDescriptor;
import net.morimekta.providence.descriptor.PService;
import net.morimekta.providence.logging.MessageReader;
import net.morimekta.providence.logging.MessageWriter;
import net.morimekta.providence.reflect.GlobalRegistry;
import net.morimekta.providence.reflect.ProgramLoader;
import net.morimekta.providence.reflect.contained.CProgram;
import net.morimekta.providence.reflect.parser.ThriftException;
import net.morimekta.providence.serializer.Serializer;
import net.morimekta.providence.tools.common.CommonOptions;
import net.morimekta.providence.tools.common.formats.ConvertStream;
import net.morimekta.providence.tools.common.formats.ConvertStreamParser;
import net.morimekta.providence.tools.common.formats.Format;
import net.morimekta.providence.tools.common.formats.FormatUtils;
import net.morimekta.providence.types.TypeReference;
import net.morimekta.util.Strings;
import net.morimekta.util.collect.Unmodifiables;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collector;
import java.util.stream.Stream;

import static net.morimekta.console.util.Parser.path;
import static net.morimekta.providence.types.TypeReference.parseType;

/**
 * Options used by the providence converter.
 */
@SuppressWarnings("all")
public class ConvertOptions extends CommonOptions {
    @Nonnull
    protected List<Path>    includes  = new ArrayList<>();
    @Nonnull
    protected ConvertStream in        = new ConvertStream(Format.json);
    @Nonnull
    protected ConvertStream out       = new ConvertStream(Format.pretty_json);
    protected boolean       strict    = false;
    protected boolean       listTypes = false;
    protected String        type      = null;

    public ConvertOptions(STTY tty) {
        super(tty);
    }

    @Override
    public ArgumentParser getArgumentParser(String prog, String description) throws IOException {
        ArgumentParser parser = super.getArgumentParser(prog, description);

        parser.add(new Option("--include", "I", "dir", "Include from directories.",
                              path(this::addInclude), "${PWD}", true, false, false));
        parser.add(new Option("--in", "i", "spec", "Input specification",
                              new ConvertStreamParser(in).andApply(this::setIn), in.toString()));
        parser.add(new Option("--out", "o", "spec", "Output specification",
                              new ConvertStreamParser(out).andApply(this::setOut), out.toString()));
        parser.add(new Flag("--strict", "S", "Read incoming messages strictly.",
                            this::setStrict));
        parser.add(new Flag("--list-types", "L", "List the parsed types based on the input files",
                            this::setListTypes));
        parser.add(new Argument("type", "Qualified identifier name from definitions to use for parsing source file.",
                                this::setType));

        return parser;
    }

    private void addInclude(Path include) {
        this.includes.add(include);
    }

    private void setIn(ConvertStream in) {
        this.in = in;
    }

    private void setOut(ConvertStream out) {
        this.out = out;
    }

    private void setListTypes(Boolean b) {
        this.listTypes = b;
    }

    private void setStrict(boolean strict) {
        this.strict = strict;
    }

    private void setType(String type) {
        this.type = type;
    }

    private Serializer getSerializer(Format format) {
        return format.createSerializer(strict);
    }

    public GlobalRegistry getProgramRegistry() throws IOException {
        Map<String, Path> includeMap = FormatUtils.getIncludeMap(getRc(), includes, verbose());
        if (type.isEmpty()) {
            throw new ArgumentException("Missing input type name");
        }

        String programName = type.substring(0, type.lastIndexOf("."));
        programName = programName.replaceAll("[-.]", "_");

        ProgramLoader loader = new ProgramLoader();

        if (!includeMap.containsKey(programName)) {
            throw new ArgumentException("No program " + programName + " found in include path.\n" +
                                        "Found: " + Strings.join(", ", new TreeSet<Object>(includeMap.keySet())));
        }

        loader.load(includeMap.get(programName));
        return loader.getGlobalRegistry();
    }

    @SuppressWarnings("unchecked")
    public <Message extends PMessage<Message>>
    PMessageDescriptor<Message> getDefinition() throws ThriftException, IOException {
        GlobalRegistry registry = getProgramRegistry();
        try {
            return registry.requireMessageType(parseType(type));
        } catch (IllegalArgumentException e) {
            try {
                registry.requireService(parseType(type));
                return null;
            } catch (IllegalArgumentException e2) {
                e.addSuppressed(e2);
                throw e;
            }
        }
    }

    public PService getServiceDefinition() throws ThriftException, IOException {
        GlobalRegistry registry = getProgramRegistry();
        TypeReference typeReference;
        try {
            typeReference = parseType(type);
        } catch (Exception e) {
            throw new ArgumentException(
                    "Bad type reference %s: %s",
                    type, e.getMessage());
        }

        try {
            return registry.requireService(typeReference);
        } catch (Exception e) {
            String programName = type.substring(0, type.lastIndexOf("."));
            programName = programName.replaceAll("[-.]", "_");
            Map<String, Path> includeMap = FormatUtils.getIncludeMap(getRc(), includes, verbose());
            String            filePath   = includeMap.get(programName).toString();

            CProgram document;
            try {
                document = registry.registryForPath(filePath).getProgram();
            } catch (UncheckedIOException e2) {
                throw new ArgumentException(e2.getCause().getMessage());
            }

            Set<String> services =
                    document.getServices()
                            .stream().map(s -> s.getQualifiedName())
                            .collect(Unmodifiables.toSortedSet());

            throw new ArgumentException(
                    "%s\n" +
                    "Found %s",
                    e.getMessage(),
                    services.size() == 0 ? "none" : Strings.join(", ", services));
        }
    }

    public <Message extends PMessage<Message>>
    Collector<Message, ?, Integer> getOutput()
            throws IOException {
        return FormatUtils.getOutput(out, strict);
    }

    public <Message extends PMessage<Message>>
    Stream<Message> getInput() throws ThriftException, IOException {
        PMessageDescriptor<Message> descriptor = getDefinition();
        return FormatUtils.getInput(descriptor, in, strict);
    }

    public MessageReader getServiceInput() throws IOException {
        return FormatUtils.getServiceInput(in, strict);
    }

    public MessageWriter getServiceOutput()
            throws IOException {
        return FormatUtils.getServiceOutput(out, strict);
    }

}
