package net.morimekta.providence.client;

import net.morimekta.providence.PMessageOrBuilder;
import net.morimekta.providence.PServiceCall;
import net.morimekta.providence.serializer.Serializer;

import javax.annotation.Nonnull;

/**
 * HTTP content wrapper for providence messages.
 *
 * @since 2.0.0
 * @deprecated Use {@link net.morimekta.providence.client.google.ProvidenceHttpContent}.
 */
@Deprecated
@SuppressWarnings("unused")
public class ProvidenceHttpContent extends net.morimekta.providence.client.google.ProvidenceHttpContent {
    @SuppressWarnings("unchecked")
    public ProvidenceHttpContent(@Nonnull PMessageOrBuilder message,
                                 @Nonnull Serializer serializer) {
        super(message, serializer);
    }

    @SuppressWarnings("unchecked")
    public ProvidenceHttpContent(@Nonnull PServiceCall serviceCall,
                                 @Nonnull Serializer serializer) {
        super(serviceCall, serializer);
    }
}
