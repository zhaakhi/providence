package net.morimekta.providence.client;

import net.morimekta.providence.PMessageOrBuilder;
import net.morimekta.providence.PServiceCall;
import net.morimekta.providence.serializer.Serializer;

import javax.annotation.Nonnull;
import java.io.IOException;

/**
 * HTTP content wrapper for providence messages. Use this is the
 * receiving end requires the content to have a known size before
 * it being sent. Otherwise use {@link ProvidenceHttpContent}.
 *
 * @since 1.8.0
 * @deprecated Use {@link net.morimekta.providence.client.google.ProvidenceHttpBufferedContent}.
 */
@Deprecated
@SuppressWarnings("unused")
public class ProvidenceHttpBufferedContent extends net.morimekta.providence.client.google.ProvidenceHttpBufferedContent {
    @SuppressWarnings("unchecked")
    public ProvidenceHttpBufferedContent(@Nonnull PMessageOrBuilder message,
                                         @Nonnull Serializer serializer) throws IOException {
        super(message, serializer);
    }

    @SuppressWarnings("unchecked")
    public ProvidenceHttpBufferedContent(@Nonnull PServiceCall serviceCall,
                                         @Nonnull Serializer serializer) throws IOException {
        super(serviceCall, serializer);
    }
}
