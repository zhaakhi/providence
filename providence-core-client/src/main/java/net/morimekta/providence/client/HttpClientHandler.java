/*
 * Copyright 2016-2017 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.providence.client;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequestFactory;
import net.morimekta.providence.PServiceCallInstrumentation;
import net.morimekta.providence.client.google.GoogleHttpClientHandler;
import net.morimekta.providence.serializer.SerializerProvider;

import javax.annotation.Nonnull;
import java.util.function.Supplier;

/**
 * HTTP client handler using the google HTTP client interface.
 *
 * @deprecated Use {@link GoogleHttpClientHandler}.
 */
@Deprecated
@SuppressWarnings("unused")
public class HttpClientHandler extends GoogleHttpClientHandler {

    /**
     * Create a HTTP client with default transport, serialization and no instrumentation.
     *
     * @param urlSupplier The HTTP url supplier.
     */
    public HttpClientHandler(@Nonnull Supplier<GenericUrl> urlSupplier) {
        super(urlSupplier);
    }

    /**
     * Create a HTTP client with default serialization and no instrumentation.
     *
     * @param urlSupplier The HTTP url supplier.
     * @param factory The HTTP request factory.
     */
    public HttpClientHandler(@Nonnull Supplier<GenericUrl> urlSupplier,
                             @Nonnull HttpRequestFactory factory) {
        super(urlSupplier, factory);
    }

    /**
     * Create a HTTP client with no instrumentation.
     *
     * @param urlSupplier The HTTP url supplier.
     * @param factory The HTTP request factory.
     * @param serializerProvider The serializer provider.
     */
    public HttpClientHandler(@Nonnull Supplier<GenericUrl> urlSupplier,
                             @Nonnull HttpRequestFactory factory,
                             @Nonnull SerializerProvider serializerProvider) {
        super(urlSupplier, factory, serializerProvider);
    }

    /**
     * Create a HTTP client.
     *
     * @param urlSupplier The HTTP url supplier.
     * @param factory The HTTP request factory.
     * @param serializerProvider The serializer provider.
     * @param instrumentation The service call instrumentation.
     */
    public HttpClientHandler(@Nonnull Supplier<GenericUrl> urlSupplier,
                             @Nonnull HttpRequestFactory factory,
                             @Nonnull SerializerProvider serializerProvider,
                             @Nonnull PServiceCallInstrumentation instrumentation) {
        super(urlSupplier, factory, serializerProvider, instrumentation);
    }
}
