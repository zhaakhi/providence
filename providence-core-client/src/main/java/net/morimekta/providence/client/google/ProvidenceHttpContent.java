package net.morimekta.providence.client.google;

import com.google.api.client.http.HttpContent;
import net.morimekta.providence.PMessage;
import net.morimekta.providence.PMessageOrBuilder;
import net.morimekta.providence.PServiceCall;
import net.morimekta.providence.serializer.Serializer;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.OutputStream;

/**
 * HTTP content wrapper for providence messages.
 *
 * @since 2.0.0
 */
public class ProvidenceHttpContent implements HttpContent {
    private final Serializer serializer;
    private final PMessageOrBuilder message;
    private final PServiceCall call;
    private int length;

    @SuppressWarnings("unchecked")
    public ProvidenceHttpContent(@Nonnull PMessageOrBuilder message,
                                 @Nonnull Serializer serializer) {
        this.serializer = serializer;
        this.message = message;
        this.call = null;
        this.length = -1;
    }

    @SuppressWarnings("unchecked")
    public ProvidenceHttpContent(@Nonnull PServiceCall serviceCall,
                                 @Nonnull Serializer serializer) {
        this.serializer = serializer;
        this.message = null;
        this.call = serviceCall;
        this.length = -1;
    }

    @Override
    public long getLength() {
        return length;
    }

    @Override
    public String getType() {
        return serializer.mediaType();
    }

    @Override
    public boolean retrySupported() {
        return true;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void writeTo(OutputStream out) throws IOException {
        if (message != null) {
            length = serializer.serialize(out, message);
        } else if (call != null) {
            length = serializer.serialize(out, call);
        }
    }
}
