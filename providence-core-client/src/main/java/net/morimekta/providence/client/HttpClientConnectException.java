/*
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */
package net.morimekta.providence.client;

import javax.annotation.Nonnull;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.URL;
import java.util.Arrays;

import static net.morimekta.util.collect.Unmodifiables.toList;

/**
 * A {@link ConnectException} that specifies the host that was
 * being connected to.
 *
 * @since 2.0
 */
public class HttpClientConnectException extends ConnectException {
    private static final long serialVersionUID = 0xa013ef97976d90b7L;

    /**
     * Creates a HttpClientConnectException based on original {@link ConnectException}.
     *
     * @param cause The initial connect exception cause.
     * @param url The cost tried to connect ro.
     * @param remoteAddresses Remote addresses
     */
    public HttpClientConnectException(
            @Nonnull final ConnectException cause,
            @Nonnull final URL url,
            @Nonnull final InetAddress... remoteAddresses) {
        super(makeMessage(cause, url, remoteAddresses));
        initCause(cause);
    }

    private static String makeMessage(
            @Nonnull Throwable cause,
            @Nonnull final URL url,
            @Nonnull final InetAddress... remoteAddresses) {
        while (cause.getCause() != null) {
            cause = cause.getCause();
        }
        return "Connect to " + url.getAuthority() +
                (remoteAddresses.length > 0
                 ? " " + Arrays.stream(remoteAddresses)
                               .map(a -> a.toString()
                                          .replaceFirst(":(0:)+", "::")
                                          .replaceFirst("^0::", "::"))
                               .collect(toList())
                 : "") +
                (" failed: " + cause.getMessage());
    }
}
