package net.morimekta.providence.client;

import net.morimekta.providence.PApplicationException;
import net.morimekta.providence.PApplicationExceptionType;
import net.morimekta.providence.PServiceCall;
import net.morimekta.providence.PServiceCallHandler;
import net.morimekta.providence.PServiceCallInstrumentation;
import net.morimekta.providence.PServiceCallType;
import net.morimekta.providence.client.internal.NoLogging;
import net.morimekta.providence.serializer.DefaultSerializerProvider;
import net.morimekta.providence.serializer.Serializer;
import net.morimekta.providence.serializer.SerializerException;
import net.morimekta.providence.serializer.SerializerProvider;
import net.morimekta.test.providence.client.Failure;
import net.morimekta.test.providence.client.Request;
import net.morimekta.test.providence.client.Response;
import net.morimekta.test.providence.client.TestService;
import org.apache.thrift.TException;
import org.apache.thrift.TProcessor;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.server.TServlet;
import org.awaitility.Awaitility;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.log.Log;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static net.morimekta.providence.client.internal.TestNetUtil.getExposedPort;
import static net.morimekta.providence.testing.EqualToMessage.equalToMessage;
import static net.morimekta.util.collect.UnmodifiableList.listOf;
import static org.awaitility.Awaitility.waitAtMost;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * Test that we can connect to a thrift servlet and get reasonable input and output.
 */
public abstract class HttpClientHandlerTestBase {
    protected static final String ENDPOINT  = "test";
    private static final String NOT_FOUND = "not_found";
    private static final String HTML      = "html";
    private static final String RESPONSE  = "response";

    protected int                                                port;
    private   net.morimekta.test.thrift.client.TestService.Iface impl;
    private   Server             server;
    protected SerializerProvider provider;
    private   ArrayList<String>  contentTypes;
    private   PServiceCallInstrumentation                        instrumentation;
    private   AtomicReference<PServiceCall<?>>                   reply;

    protected abstract PServiceCallHandler handler(String url, SerializerProvider provider, PServiceCallInstrumentation instrumentation);

    private TestService.Client endpoint(
            SerializerProvider provider,
            PServiceCallInstrumentation instrumentation) {
        return new TestService.Client(handler("http://localhost:" + port + "/" + ENDPOINT, provider, instrumentation));
    }

    private TestService.Client notfound(
            SerializerProvider provider,
            PServiceCallInstrumentation instrumentation) {
        return new TestService.Client(handler("http://localhost:" + port + "/" + NOT_FOUND, provider, instrumentation));
    }

    private TestService.Client html() {
        return new TestService.Client(handler("http://localhost:" + port + "/" + HTML, null, null));
    }

    private TestService.Client response() {
        return new TestService.Client(handler("http://localhost:" + port + "/" + RESPONSE, null, null));
    }

    @Before
    public void setUp() throws Exception {
        Awaitility.setDefaultPollDelay(50, TimeUnit.MILLISECONDS);
        Log.setLog(new NoLogging());

        impl = mock(net.morimekta.test.thrift.client.TestService.Iface.class);
        TProcessor processor = new net.morimekta.test.thrift.client.TestService.Processor<>(impl);
        instrumentation = mock(PServiceCallInstrumentation.class);

        provider = new DefaultSerializerProvider();
        server = new Server(0);
        ServletContextHandler handler = new ServletContextHandler();
        handler.addServlet(new ServletHolder(new TServlet(processor, new TBinaryProtocol.Factory())),
                           "/" + ENDPOINT);
        handler.addServlet(new ServletHolder(new HttpServlet() {
            @Override
            protected void doPost(HttpServletRequest req, HttpServletResponse resp)
                    throws IOException {
                resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            }
        }), "/" + NOT_FOUND);
        handler.addServlet(new ServletHolder(new HttpServlet() {
            @Override
            protected void doPost(HttpServletRequest req, HttpServletResponse resp)
                    throws IOException {
                resp.setStatus(HttpServletResponse.SC_OK);
                resp.setContentType("text/html");
                resp.getWriter().print("<html></html>");
            }
        }), "/" + HTML);
        handler.addServlet(new ServletHolder(new HttpServlet() {
            @Override
            protected void doPost(HttpServletRequest req, HttpServletResponse resp)
                    throws IOException {
                resp.setStatus(HttpServletResponse.SC_OK);
                Serializer serializer = provider.getDefault();
                resp.setContentType(serializer.mediaType());
                serializer.serialize(resp.getOutputStream(), reply.get());
            }
        }), "/" + RESPONSE);

        contentTypes = new ArrayList<>();
        reply = new AtomicReference<>();

        server.setHandler(handler);
        server.setRequestLog((request, response) -> contentTypes.addAll(
                Collections.list(request.getHeaders("Content-Type"))
                           .stream()
                           .map(Object::toString)
                           .collect(Collectors.toList())));
        server.start();
        port = getExposedPort(server);
    }

    @After
    public void tearDown() {
        try {
            server.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testSimple() throws IOException, TException, Failure {
        TestService.Iface client = endpoint(null, null);

        when(impl.test(any(net.morimekta.test.thrift.client.Request.class)))
                .thenReturn(new net.morimekta.test.thrift.client.Response("response"));

        Response response = client.test(new Request("request"));

        waitAtMost(Duration.ofMillis(100)).until(() -> contentTypes.size() > 0);

        verify(impl).test(any(net.morimekta.test.thrift.client.Request.class));
        verifyNoMoreInteractions(impl);

        assertThat(response, is(equalToMessage(new Response("response"))));
        assertThat(contentTypes, is(equalTo(listOf("application/vnd.apache.thrift.binary"))));
    }

    @Test
    public void testBadRequest() throws IOException {
        PServiceCallHandler handler = handler("http://localhost:" + port + "/" + ENDPOINT, null, null);

        try {
            handler.handleCall(new PServiceCall<>("foo",
                                                  PServiceCallType.EXCEPTION,
                                                  1,
                                                  new PApplicationException("", PApplicationExceptionType.INTERNAL_ERROR)),
                               TestService.kDescriptor);
            fail("no exception");
        } catch (PApplicationException e) {
            assertThat(e.getMessage(), is("Request with invalid call type: EXCEPTION"));
            assertThat(e.getType(), is(PApplicationExceptionType.INVALID_MESSAGE_TYPE));
        }
    }

    private static class TestServiceBypass extends TestService {
        Test_Response testResponse(String text) {
            return Test_Response.withSuccess(Response.builder().setText(text));
        }
    }

    @Test
    public void testBadResponse() throws IOException, Failure {
        TestService.Iface client = html();
        try {
            client.test(new Request("request"));
            fail("no exception");
        } catch (PApplicationException e) {
            assertThat(e.getMessage(), is("Unknown content-type in response: text/html;charset=utf-8"));
            assertThat(e.getType(), is(PApplicationExceptionType.INVALID_PROTOCOL));
        }

        client = response();

        reply.set(new PServiceCall<>("foo", PServiceCallType.REPLY, 1, new TestServiceBypass().testResponse("foo")));
        try {
            client.test(new Request("request"));
            fail("no exception");
        } catch (SerializerException e) {
            assertThat(e.getMessage(), is("No such method foo on client.TestService"));
            assertThat(e.getExceptionType(), is(PApplicationExceptionType.UNKNOWN_METHOD));
            assertThat(e.getMethodName(), is("foo"));
            assertThat(e.getSequenceNo(), is(1));
        }

        reply.set(new PServiceCall<>("test", PServiceCallType.CALL, 2, new TestServiceBypass().testResponse("bar")));
        try {
            client.test(new Request("request"));
            fail("no exception");
        } catch (PApplicationException e) {
            assertThat(e.getMessage(), is("Reply with invalid call type: CALL"));
            assertThat(e.getType(), is(PApplicationExceptionType.INVALID_MESSAGE_TYPE));
        }

        reply.set(new PServiceCall<>("test", PServiceCallType.REPLY, 100, new TestServiceBypass().testResponse("baz")));
        try {
            client.test(new Request("request"));
            fail("no exception");
        } catch (PApplicationException e) {
            assertThat(e.getMessage(), is("Reply sequence out of order: call = 2, reply = 100"));
            assertThat(e.getType(), is(PApplicationExceptionType.BAD_SEQUENCE_ID));
        }

        reply.set(new PServiceCall<>("test", PServiceCallType.REPLY, 4, new PApplicationException("foo", PApplicationExceptionType.INTERNAL_ERROR)));
        try {
            client.test(new Request("request"));
            fail("no exception");
        } catch (SerializerException e) {
            assertThat(e.getMessage(), is("Wrong type string(11) for client.TestService.test.response.fail, should be struct(12)"));
            assertThat(e.getExceptionType(), is(PApplicationExceptionType.PROTOCOL_ERROR));
            assertThat(e.getMethodName(), is("test"));
            assertThat(e.getSequenceNo(), is(4));
        }
    }

    @Test
    public void testInstrumentationFail() throws IOException, Failure, TException {
        TestService.Iface client = endpoint(provider, instrumentation);
        when(impl.test(any(net.morimekta.test.thrift.client.Request.class)))
                .thenReturn(new net.morimekta.test.thrift.client.Response("response"));
        doThrow(new NullPointerException()).when(instrumentation).onComplete(anyDouble(), any(PServiceCall.class), any(PServiceCall.class));

        client.test(Request.builder().build());
        waitAtMost(Duration.ofMillis(100)).until(() -> contentTypes.size() > 0);

        verify(instrumentation).onComplete(anyDouble(), any(PServiceCall.class), any(PServiceCall.class));

        client = notfound(provider, instrumentation);
        doThrow(new NullPointerException()).when(instrumentation).onTransportException(any(IOException.class),
                                                                                       anyDouble(),
                                                                                       any(PServiceCall.class),
                                                                                       isNull());

        try {
            client.test(Request.builder().build());
            waitAtMost(Duration.ofMillis(100)).until(() -> contentTypes.size() > 0);
            fail("no exception");
        } catch (Exception ignore) {
        }

        verify(instrumentation).onTransportException(any(IOException.class), anyDouble(), any(PServiceCall.class), isNull());
    }

    @Test
    public void testSimpleRequest() throws IOException, TException, Failure {
        TestService.Iface client = endpoint(provider, instrumentation);

        when(impl.test(any(net.morimekta.test.thrift.client.Request.class)))
                .thenReturn(new net.morimekta.test.thrift.client.Response("response"));

        Response response = client.test(new Request("request"));
        waitAtMost(Duration.ofMillis(100)).until(() -> contentTypes.size() > 0);

        verify(impl).test(any(net.morimekta.test.thrift.client.Request.class));
        verify(instrumentation).onComplete(anyDouble(), any(PServiceCall.class), any(PServiceCall.class));
        verifyNoMoreInteractions(impl, instrumentation);

        assertThat(response, is(equalToMessage(new Response("response"))));
        assertThat(contentTypes, is(equalTo(listOf("application/vnd.apache.thrift.binary"))));
    }

    @Test
    public void testSimpleRequest_oneway() throws IOException, TException {
        TestService.Iface client = endpoint(provider, instrumentation);

        AtomicBoolean called = new AtomicBoolean();
        doAnswer(i -> {
            called.set(true);
            return null;
        }).when(impl).onewayMethod();

        client.onewayMethod();

        waitAtMost(Duration.ofMillis(200)).untilTrue(called);
        waitAtMost(Duration.ofMillis(100)).until(() -> contentTypes.size() > 0);

        verify(impl).onewayMethod();
        verify(instrumentation).onComplete(anyDouble(), any(PServiceCall.class), isNull());
        verifyNoMoreInteractions(impl, instrumentation);

        assertThat(contentTypes, is(equalTo(listOf("application/vnd.apache.thrift.binary"))));
    }

    @Test
    @Ignore("Apache thrift server does not fill in the '0: void success;' field.")
    public void testSimpleRequest_void() throws IOException, TException, Failure {
        TestService.Iface client = endpoint(provider, instrumentation);

        AtomicBoolean called = new AtomicBoolean();
        doAnswer(i -> {
            called.set(true);
            return null;
        }).when(impl).voidMethod(anyInt());

        client.voidMethod(5);

        waitAtMost(Duration.ofMillis(200)).untilTrue(called);
        waitAtMost(Duration.ofMillis(100)).until(() -> contentTypes.size() > 0);

        verify(impl).voidMethod(5);
        verify(instrumentation).onComplete(anyDouble(), any(PServiceCall.class), isNull());
        verifyNoMoreInteractions(impl, instrumentation);

        assertThat(contentTypes, is(equalTo(listOf("application/vnd.apache.thrift.binary"))));
    }

    @Test
    public void testSimpleRequest_exception() throws IOException, TException {
        TestService.Iface client = endpoint(provider, instrumentation);

        when(impl.test(any(net.morimekta.test.thrift.client.Request.class)))
                .thenThrow(new net.morimekta.test.thrift.client.Failure("failure"));

        try {
            client.test(new Request("request"));
            fail("No exception");
        } catch (Failure ex) {
            assertThat(ex.getText(), is("failure"));
        }

        verify(impl).test(any(net.morimekta.test.thrift.client.Request.class));
        verify(instrumentation).onComplete(anyDouble(), any(PServiceCall.class), any(PServiceCall.class));
        verifyNoMoreInteractions(impl, instrumentation);
    }

    @Test
    public void testSimpleRequest_404_notFound() throws IOException, Failure {
        TestService.Iface client = notfound(provider, instrumentation);

        try {
            client.test(new Request("request"));
            fail("No exception");
        } catch (HttpResponseException ex) {
            assertThat(ex.getStatusCode(), is(404));
            assertThat(ex.getStatusMessage(), is("Not Found"));
        }

        verify(instrumentation).onTransportException(any(HttpResponseException.class), anyDouble(), any(PServiceCall.class), isNull());
        verifyNoMoreInteractions(impl, instrumentation);
    }

    @Test
    public void testSimpleRequest_405_notSupported() throws IOException, Failure {
        TestService.Iface client = new TestService.Client(handler("http://localhost:" + port + "/does_not_exists", provider, instrumentation));

        try {
            client.test(new Request("request"));
            fail("No exception");
        } catch (HttpResponseException ex) {
            assertThat(ex.getStatusCode(), is(405));
            // assertThat(ex.getStatusMessage(), is("HTTP method POST is not supported by this URL"));
        }

        verify(instrumentation).onTransportException(any(HttpResponseException.class), anyDouble(), any(PServiceCall.class), isNull());
        verifyNoMoreInteractions(impl, instrumentation);
    }
}
