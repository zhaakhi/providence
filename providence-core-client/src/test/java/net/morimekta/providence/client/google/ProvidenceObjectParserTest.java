package net.morimekta.providence.client.google;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import net.morimekta.providence.PMessage;
import net.morimekta.providence.serializer.BinarySerializer;
import net.morimekta.providence.serializer.JsonSerializer;
import net.morimekta.providence.serializer.Serializer;
import net.morimekta.providence.serializer.SerializerException;
import net.morimekta.test.providence.client.Request;
import net.morimekta.test.providence.client.Response;
import net.morimekta.testing.DataProviderUtil;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.lang.reflect.Type;

import static java.nio.charset.StandardCharsets.UTF_8;
import static net.morimekta.util.collect.UnmodifiableList.listOf;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.fail;

@RunWith(DataProviderRunner.class)
public class ProvidenceObjectParserTest {
    @DataProvider
    public static Object[][] ParseAndClose() {
        return DataProviderUtil.buildDataDimensions(
                listOf(new JsonSerializer().named(), new BinarySerializer()),
                listOf(new Response("foo"), new Request("bar")));
    }

    @Test
    @UseDataProvider("ParseAndClose")
    public void testParseAndClose(Serializer serializer, PMessage<?> expected) throws IOException {
        InputStreamReader reader;

        ProvidenceObjectParser parser = new ProvidenceObjectParser(serializer);
        ByteArrayOutputStream  out    = new ByteArrayOutputStream();
        serializer.serialize(out, expected);

        ByteArrayInputStream in = new ByteArrayInputStream(out.toByteArray());
        in.reset();

        assertThat(parser.parseAndClose(in, UTF_8, expected.getClass()), is(expected));

        in.reset();
        assertThat(parser.parseAndClose(in, UTF_8, (Type) expected.getClass()), is(expected));

        try {
            in.reset();
            reader = new InputStreamReader(in);
            assertThat(parser.parseAndClose(reader, expected.getClass()), is(expected));
        } catch (IllegalStateException e) {
            assertThat(serializer, not(instanceOf(JsonSerializer.class)));
            assertThat(e.getMessage(), containsString(" does not support Reader deserialization"));
        }

        try {
            in.reset();
            reader = new InputStreamReader(in);
            assertThat(parser.parseAndClose(reader, (Type) expected.getClass()), is(expected));
        } catch (IllegalStateException e) {
            assertThat(serializer, not(instanceOf(JsonSerializer.class)));
            assertThat(e.getMessage(), containsString(" does not support Reader deserialization"));
        }
    }

    @Test
    public void testParseAndCloseBadInput() throws IOException {
        ProvidenceObjectParser parser = new ProvidenceObjectParser(new JsonSerializer());
        ByteArrayInputStream in = new ByteArrayInputStream(new byte[]{0});
        Reader reader = new StringReader(" ");

        try {
            parser.parseAndClose(in, UTF_8, (Type) Integer.TYPE);
            fail("No exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Class int is not a providence message"));
        }
        try {
            parser.parseAndClose(in, UTF_8, String.class);
            fail("No exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Class java.lang.String is not a providence message"));
        }
        try {
            parser.parseAndClose(reader, (Type) Integer.TYPE);
            fail("No exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Class int is not a providence message"));
        }
        try {
            parser.parseAndClose(reader, String.class);
            fail("No exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Class java.lang.String is not a providence message"));
        }
        try {
            parser.parseAndClose(reader, Request.class);
            fail("No exception");
        } catch (SerializerException e) {
            assertThat(e.getMessage(), is("Empty json body"));
        }
    }
}
