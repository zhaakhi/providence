package net.morimekta.providence.client.okhttp;

import net.morimekta.providence.PServiceCallHandler;
import net.morimekta.providence.PServiceCallInstrumentation;
import net.morimekta.providence.client.HttpClientHandlerTestBase;
import net.morimekta.providence.serializer.SerializerProvider;
import okhttp3.OkHttpClient;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Test that we can connect to a thrift servlet and get reasonable input and output.
 */
public class OkHttpClientHandlerTest extends HttpClientHandlerTestBase {
    @Override
    protected PServiceCallHandler handler(String url,
                                          SerializerProvider provider,
                                          PServiceCallInstrumentation instrumentation) {
        URL urlInstance;
        try {
            urlInstance = new URL(url);
        } catch (MalformedURLException e) {
            throw new AssertionError(e.getMessage(), e);
        }
        if (provider != null && instrumentation != null) {
            return new OkHttpClientHandler(() -> urlInstance, new OkHttpClient.Builder().build(), provider, instrumentation);
        } else if (provider != null) {
            return new OkHttpClientHandler(() -> urlInstance, new OkHttpClient.Builder().build(), provider);
        }
        return new OkHttpClientHandler(() -> urlInstance);
    }
}
