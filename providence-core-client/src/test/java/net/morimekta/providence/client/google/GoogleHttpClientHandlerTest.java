package net.morimekta.providence.client.google;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpResponse;
import net.morimekta.providence.PServiceCall;
import net.morimekta.providence.PServiceCallHandler;
import net.morimekta.providence.PServiceCallInstrumentation;
import net.morimekta.providence.PServiceCallType;
import net.morimekta.providence.client.HttpClientHandlerTestBase;
import net.morimekta.providence.serializer.SerializerProvider;
import net.morimekta.test.providence.client.Request;
import org.junit.Test;

import java.io.IOException;

import static net.morimekta.providence.client.internal.TestNetUtil.factory;
import static org.apache.http.HttpStatus.SC_OK;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Test that we can connect to a thrift servlet and get reasonable input and output.
 */
public class GoogleHttpClientHandlerTest extends HttpClientHandlerTestBase {
    @Override
    protected PServiceCallHandler handler(String url,
                                          SerializerProvider provider,
                                          PServiceCallInstrumentation instrumentation) {
        GenericUrl genericUrl = new GenericUrl(url);
        if (provider != null && instrumentation != null) {
            return new GoogleHttpClientHandler(() -> genericUrl, factory(), provider, instrumentation);
        } else if (provider != null) {
            return new GoogleHttpClientHandler(() -> genericUrl, factory(), provider);
        }
        return new GoogleHttpClientHandler(() -> genericUrl, factory());
    }

    @Test
    public void testServiceCall_buffered() throws IOException {
        PServiceCall<Request> call = new PServiceCall<>(
                "foo", PServiceCallType.CALL, 1, new Request("foo"));

        // NOTE: This throws exception reading response while creating the HTTP exception.
        // And it just calls e.printStackTrace() instead of adding suppressed.
        HttpResponse response = factory()
                .buildPostRequest(new GenericUrl("http://localhost:" + port + "/" + ENDPOINT),
                                  new ProvidenceHttpBufferedContent(call, provider.getDefault()))
                .execute();
        assertThat(response.getStatusCode(), is(SC_OK));
    }

    @Test
    public void testServiceCall() throws IOException {
        PServiceCall<Request> call = new PServiceCall<>(
                "foo", PServiceCallType.CALL, 1, new Request("foo"));

        // NOTE: This throws exception reading response while creating the HTTP exception.
        // And it just calls e.printStackTrace() instead of adding suppressed.
        HttpResponse response = factory()
                .buildPostRequest(new GenericUrl("http://localhost:" + port + "/" + ENDPOINT),
                                  new ProvidenceHttpContent(call, provider.getDefault()))
                .execute();
        assertThat(response.getStatusCode(), is(SC_OK));
    }
}
