package net.morimekta.providence.testing.util;

import net.morimekta.providence.testing.generator.GeneratorContext;
import net.morimekta.test.providence.testing.OptionalFields;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;

public class MessageDiffTest {
    @Test
    public void testMessageDiff() {
        GeneratorContext.Simple context = new GeneratorContext.Simple();
        OptionalFields a = context.generatorFor(OptionalFields.kDescriptor).generate(context);
        OptionalFields b = context.generatorFor(OptionalFields.kDescriptor).generate(context);
        assertThat(a, is(not(b)));
    }
}
