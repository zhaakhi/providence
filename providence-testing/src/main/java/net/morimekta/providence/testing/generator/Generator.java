package net.morimekta.providence.testing.generator;

/**
 * Basic generator interface. Each instance of this interface
 * will always generate the same class of value, e.g. one instance
 * for enum value, one for int etc.
 */
@FunctionalInterface
public interface Generator<Context extends GeneratorContext<Context>, T> {
    /**
     * @param ctx The generator to use for internal values.
     * @return The generated value.
     */
    T generate(Context ctx);
}
