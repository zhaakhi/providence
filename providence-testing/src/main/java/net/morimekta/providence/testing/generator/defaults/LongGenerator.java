package net.morimekta.providence.testing.generator.defaults;

import net.morimekta.providence.testing.generator.GeneratorContext;
import net.morimekta.providence.testing.generator.Generator;

/**
 * Default generator for a long field.
 */
public class LongGenerator<Context extends GeneratorContext<Context>>
        implements Generator<Context, Long> {
    @Override
    public Long generate(Context ctx) {
        return ctx.getRandom().nextLong();
    }
}
